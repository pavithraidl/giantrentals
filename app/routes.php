<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array(
    'as' => '/',
    'uses' => 'RoutesController@getHome'
));

Route::get('/book/vehicle', array(
    'as' => 'book/vehicle',
    'uses' => 'RoutesController@getVehicle'
));

Route::get('/book/scaffolding', array(
    'as' => 'book/scaffolding',
    'uses' => 'RoutesController@getScaffold'
));

Route::get('/scaffold/get-session-scaffold-list', array(
    'as' => '/scaffold/get-session-scaffold-list',
    'uses' => 'ScaffoldController@getSessionList'
));

Route::post('/scaffold/remove-scaffold', array(
    'as' => '/scaffold/remove-scaffold',
    'uses' => 'ScaffoldController@removeScaffold'
));

Route::get('/book/event-gear', array(
    'as' => 'book/event-gear',
    'uses' => 'RoutesController@getEvent'
));

Route::get('/my-account', array(
    'as' => 'my-account',
    'uses' => 'RoutesController@getMyAccount'
));

Route::get('/contact-us', array(
    'as' => 'contact-us',
    'uses' => 'RoutesController@getContactUs'
));

Route::get('/vehicle/find-my-vehicle', array(
    'as' => 'vehicle-find-my-vehicle',
    'uses' => 'VehicleBookingController@getFindMyVehicle'
));

Route::get('/scaffold/add-scaffold', array(
    'as' => 'scaffold-add-scaffold',
    'uses' => 'ScaffoldController@getAddScaffold'
));

Route::get('/event/add-event', array(
    'as' => 'event-add-event',
    'uses' => 'EventsController@getAddEvent'
));

Route::post('/event/remove-gear', array(
    'as' => 'event-remove-gear',
    'uses' => 'EventsController@removeGear'
));

Route::get('/events/get-session-gear-list', array(
    'as' => 'events-get-session-gear-list',
    'uses' => 'EventsController@getSessionList'
));

Route::get('/book/confirm', array(
    'as' => 'book/confirm',
    'uses' => 'RoutesController@getBookConfirm'
));

Route::post('/book/confirm-booking', array(
    'as' => 'book/confirm-booking',
    'uses' => 'UserController@postBooking'
));

Route::get('/booking/recorded', array(
    'as' => 'booking/recorded',
    'uses' => 'RoutesController@getBookRecorded'
));

Route::get('/vehicle/get-session-vehicle-list', array(
    'as' => 'get-session-list',
    'uses' => 'VehicleBookingController@getSessionList'
));

Route::post('/post-booking-info', array(
    'as' => 'post-booking-info',
    'uses' => 'UserController@postBookingInfo'
));

Route::get('/vehicle/previous-vehicle-booking-list', array(
    'as' => 'previous-vehicle-booking-list',
    'uses' => 'VehicleBookingController@getPreviousBookingList'
));

Route::get('/rent-vehicle', array(
    'as' => 'vehicle-rent-vehicle',
    'uses' => 'RoutesController@getRentVehicle'
));

Route::post('/vehicle/remove-booking-item', array(
    'as' => 'vehicle-remove-booking-item',
    'uses' => 'VehicleBookingController@removeBooking'
));

Route::post('/scaffold/invoice/add-discount', array(
    'as' => 'scaffold-invoice-add-discount',
    'uses' => 'ScaffoldController@postDiscount'
));

Route::get('/test', function () {

});

Route::get('/test-function', function () {
    $bookingList = ScaffoldingBooking::where('status', 1)->orderBy('created_at', 'asc')->lists('id');
    print_r($bookingList);
});

Route::get('/test-email', function () {
    return View::make('emails.customer-booking-details');
});

Route::get('/design', function () {
    return View::make('frontend/design');
});

//region User Account URL List
Route::get('/sign-in', array(
    'as' => 'get-sing-in',
    'uses' => 'RoutesController@getSignIn'
));

Route::post('/post/sign-in', array(
    'as' => 'post-sign-in',
    'uses' => 'UserController@postFrontendSingIn'
));

Route::post('/post-sign-up', array(
    'as' => 'post-sign-up',
    'uses' => 'UserController@postSignUp'
));

Route::get('/active/{code}', array(
    'as' => 'active',
    'uses' => 'UserController@getActive'
));

Route::post('/post-active', array(
    'as' => 'post-active',
    'uese' => 'UserController@postActive'
));

Route::get('/login', array(
    'as' => 'login',
    'uses' => 'RoutesController@getLogin'
));

Route::post('/post-login', array(
    'as' => 'post-login',
    'uses' => 'UserController@postLogin'
));

Route::get('/log-out', array(
    'as' => 'log-out',
    'uses' => 'UserController@getLogOut'
));
//endregion

Route::group(array('before' => 'auth'), function () {
    $auth = new UserController();
    $auth->urlAccessAthenticator();

    Route::get('/internal/home', array(
        'as' => 'internal-home',
        'uses' => 'RoutesController@getInternal'
    ));

    Route::get('/get-internal-logout', array(
        'as' => 'get-internal-logout',
        'uses' => 'UserController@getInternalLogOut'
    ));

    //region System Exceptions
    Route::get('/internal/system/exception', array(
        'as' => 'internal-system-exception',
        'uses' => 'RoutesController@getInternalSystemException'
    ));

    Route::post('/internal-get-system-exception-list', array(
        'as' => 'internal-get-system-exception-list',
        'uses' => 'ErrorController@getInternalSystemExceptionList'
    ));

    Route::post('/internal-update-system-exception-status', array(
        'as' => 'internal-update-system-exception-status',
        'uses' => 'ErrorController@updateInternalSystemExceptionStatus'
    ));
    //endregion

    //region System Intrusions
    Route::get('/internal/system/intrusion', array(
        'as' => 'internal-system-intrusion',
        'uses' => 'RoutesController@getInternalSystemIntrusion'
    ));

    Route::post('/internal-get-system-intrusion-list', array(
        'as' => 'internal-get-system-intrusion-list',
        'uses' => 'ErrorController@getInternalSystemIntrusionList'
    ));

    Route::post('/internal-update-system-intrusion-status', array(
        'as' => 'internal-update-system-intrusion-status',
        'uses' => 'ErrorController@updateInternalSystemIntrusionStatus'
    ));
    //endregion

    //region User Manage URL Roots
    Route::get('/get-internal-user-manage', array(
        'as' => 'get-internal-user-manage',
        'uses' => 'RoutesController@getUserManage'
    ));

    Route::get('/internal-user-manage-get-user-list', array(
        'as' => 'internal-user-manage-get-user-list',
        'uses' => 'UserManageController@getUserList'
    ));

    Route::get('/user-manage-display-info', array(
        'as' => 'user-manage-display-info',
        'uses' => 'UserManageController@getUserDisplayInfo'
    ));

    Route::get('/user-manage-get-users-access', array(
        'as' => 'user-manage-get-users-access',
        'uses' => 'UserManageController@getUsersAccess'
    ));

    Route::post('/user-manage-allow-deny-system-pages', array(
        'as' => 'user-manage-allow-deny-system-pages',
        'uses' => 'UserManageController@allowDenySystemPages'
    ));

    Route::post('/user-manage-active-deactive-user', array(
        'as' => 'user-manage-active-deactive-user',
        'uses' => 'UserManageController@activeDeactiveUser'
    ));

    Route::post('/user-manage-delete-user', array(
        'as' => 'user-manage-delete-user',
        'uses' => 'UserManageController@deleteUser'
    ));

    Route::post('/user-manage-add-new-user', array(
        'as' => 'user-manage-add-new-user',
        'uses' => 'UserManageController@addNewUser'
    ));

    Route::post('/user-manage-resend-activation-email', array(
        'as' => 'user-manage-resend-activation-email',
        'uses' => 'UserManageController@resendActivationEmail'
    ));

    Route::get('/internal-user-get-activity-list', array(
        'as' => 'internal-user-get-activity-list',
        'uses' => 'UserManageController@getActivityList'
    ));

    Route::post('/user/manage/operation-access', array(
        'as' => 'user-manage-operation-access',
        'uses' => 'UserManageController@allowDenyOperationAccess'
    ));
    //endregion

    //region Vehicle Booking
    Route::get('/internal/vehicle-manage-booking', array(
        'as' => 'internal-vehicle-manage-booking',
        'uses' => 'RoutesController@getInternalVehicleManageBooking'
    ));

    Route::get('/internal/vehicle/get-vehicle-booking-list', array(
        'as' => 'internal-vehicle-get-vehicle-booking-list',
        'uses' => 'VehicleBookingController@getInternalBookingList'
    ));

    Route::get('/internal/vehicle/add-new-vehicle', array(
        'as' => 'internal-vehicle-add-new-vehicle',
        'uses' => 'VehicleBookingController@addNewVehicle'
    ));

    Route::get('/internal/vehicle/edit-vehicle-info', array(
        'as' => 'internal-vehicle-edit-vehicle-info',
        'uses' => 'VehicleBookingController@updateVehicleInfo'
    ));

    Route::get('/internal/vehicle/enable-disable-vehicle', array(
        'as' => 'internal-vehicle-enable-disable-vehicle',
        'uses' => 'VehicleBookingController@enableDisableVehicle'
    ));

    Route::get('/internal/vehicle-manage-vehicle', array(
        'as' => 'internal-vehicle-manage-vehicle',
        'uses' => 'RoutesController@getManageVehicles'
    ));

    Route::get('/internal/vehicle/single-vehicle-booking-list', array(
        'as' => '/internal-vehicle-single-vehicle-booking-list',
        'uses' => 'VehicleBookingController@getSingleVehicleBookingList'
    ));

    Route::get('/internal/vehicle/single-vehicle-details', array(
        'as' => 'internal-vehicle-single-vehicle-details',
        'uses' => 'VehicleBookingController@getSingleVehicleDetails'
    ));

    Route::get('/internal/vehicle/all-booking-list', array(
        'as' => 'internal-vehicle-all-booking-list',
        'uses' => 'VehicleBookingController@getAllBookingList'
    ));

    Route::get('/internal/vehicle/get-vehicle-plate-list', array(
        'as' => 'internal-vehicle-get-vehicle-plate-list',
        'uses' => 'VehicleBookingController@getVehiclePlateList'
    ));

    Route::get('/internal/vehicle/get-available-vehicles', array(
        'as' => 'internal-vehicle-get-available-vehicles',
        'uses' => 'VehicleBookingController@getAvailableVehicles'
    ));

    Route::get('/internal/vehicle/get-booking-price', array(
        'as' => 'internal-vehicle-get-booking-price',
        'uses' => 'VehicleBookingController@getBookingPrice'
    ));

    Route::get('/internal/vehicle/save-vehicle-booking', array(
        'as' => 'internal-vehicle-save-vehicle-booking',
        'uses' => 'VehicleBookingController@saveVehicleBooking'
    ));

    Route::get('/internal/vehicle/change-booking-status', array(
        'as' => 'internal-vehicle-change-booking-status',
        'uses' => 'VehicleBookingController@changeVehicleStatus'
    ));

    Route::get('/internal/vehicle/get-booking-slider-initiating-data', array(
        'as' => 'internal-vehicle-get-booking-slider-initiating-data',
        'uses' => 'VehicleBookingController@getBookingSliderInitiatingData'
    ));
    //endregion

    Route::get('/internal-scaffolding', array (
        'as' => 'internal-scaffolding',
        'uses' => 'RoutesController@getScaffoldingBookings'
    ));

    Route::get('/scaffolding/manage-booking/get-booking-list', array(
        'as' => 'internal-scaffolding-manage-booking-get-booking-list',
        'uses' => 'ScaffoldController@getBookingList'
    ));

    Route::get('/internal/scaffolding/get-single-booking-details', array(
        'as' => 'internal-scaffolding-get-single-booking-details',
        'uses' => 'ScaffoldController@getSingleBookingDetails'
    ));

    Route::get('/scaffolding/get-cart-item-list', array(
        'as' => 'scaffolding-get-cart-item-list',
        'uses' => 'ScaffoldController@getCartItemList'
    ));

    Route::post('/scaffolding/update-cart-status', array(
        'as' => 'scaffolding-update-cart-status',
        'uses' => 'ScaffoldController@updateCartStatus'
    ));

    Route::get('/internal/scaffolding-manage', array(
        'as' => 'internal-scaffolding-manage',
        'uses' => 'RoutesController@getScaffoldingMange'
    ));

    Route::get('/internal/scaffolding/get-scaffolding-part-list', array(
        'as' => 'get-scaffolding-part-list',
        'uses' => 'ScaffoldController@getScaffoldingPartList'
    ));


});

Route::group(array('before' => 'guest'), function () {
    Route::get('/internal-login', array(
        'as' => 'get-internal-login',
        'uses' => 'RoutesController@getInternalLogin'
    ));

    Route::post('/internal-login', array(
        'as' => 'post-internal-login',
        'uses' => 'UserController@postInternalLogin'
    ));

    //Activate account (GET)
    Route::get('/account/active/{code}', array(
        'as' => 'account-activate',
        'uses' => 'AccountController@getActivate',
    ));

    //Activate account (POST)
    Route::post('/account/active/{code}', array(
        'as' => 'account-active-post',
        'uses' => 'UserController@postActivate',
    ));

    //Error 404 page
    Route::get('/404', array(
        'as' => '404',
        'uses' => 'HomeController@get404'
    ));

    App::missing(function($exception)
    {
        try{
            $t = Auth::user()->id;
            return Response::view('error.404', array(), 404);
        }
        catch(Exception $ex){
            return Response::view('error._404', array(), 404);
        }
    });

});

Route::get("/vehicles/book", "FrontEndController@vehicleBook");