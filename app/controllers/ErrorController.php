<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/08/2016 04:33.
 */
 
class ErrorController extends BaseController {
	
    public function saveExceptionDetails($controller, $func, $ex) {
        //this function will send the exception details to the database
        //create a new row in the exception model
        try {
            $exception = new Exceptions();
            $exception -> controller = $controller;
            $exception -> func = $func;
            $exception -> exception = $ex;
            $exception -> status = 2;
            $exception -> created_at = (new DateTime("now", new DateTimeZone('Pacific/Auckland')))->format('Y-m-d H:i:s');
            $exception -> save();

            return 1;
        }catch (Exception $ex) {
            Log::error($ex);

            return 0;
        }
    }


    public function getInternalSystemExceptionList() {
        $paginate = Input::get('paginate');
        try {
            $ret = null;
            $exceptionList = Exceptions::where('status', '!=', 0)->orderBy('status', 'desc')->orderBy('created_at', 'desc')->forPage($paginate, 10)->lists('id');
            $timeSince = new CalculationController();

            foreach ($exceptionList as $exceptionId) {
                $exception = Exceptions::where('id', $exceptionId);
                $controller = $exception->pluck('controller');
                $func = Exceptions::where('id', $exceptionId)->pluck('func');
                $exeMsg = Exceptions::where('id', $exceptionId)->pluck('exception');
                $status = Exceptions::where('id', $exceptionId)->pluck('status');
                $createdAt = Exceptions::where('id', $exceptionId)->pluck('created_at');

                //calculation
                $createdAt = ($timeSince->timeAgo((new DateTime($createdAt))->format('Y-m-d H:i:s')));

                $ret[] = array(
                    'id' => $exceptionId,
                    'controller' => $controller,
                    'func' => $func,
                    'exe_msg' => $exeMsg,
                    'status' => $status,
                    'created_at' => $createdAt
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $this->saveExceptionDetails('ErrorController', 'getInternalExceptionList', $ex);
            return 0;
        }
    }

    public function updateInternalSystemExceptionStatus() {
        $exceptionId = Input::get('exception_id');
        try {
            Exceptions::where('id', $exceptionId)->update(array(
                'status' => 1
            ));

            //create activity for the activity log
            $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');

            $activity = sprintf("%s Solved Exception %s.", $userName, $exceptionId);
            $addActivity = new UserManageController();
            $addActivity -> addActivity($activity);

            return 1;
        } catch (Exception $ex) {
            $this->saveExceptionDetails('ErrorController', 'updateInternalSystemExceptionStatus', $ex);
            return 0;
        }
    }

    public function getInternalSystemIntrusionList() {
        $paginate = Input::get('paginate');
        try {
            $ret = null;
            $intrusionList = SystemIntrusions::where('status', '!=', 0)->orderBy('status', 'desc')->orderBy('created_at', 'desc')->forPage($paginate, 10)->lists('id');
            $timeSince = new CalculationController();

            foreach ($intrusionList as $intrusionId) {
                $controller = SystemIntrusions::where('id', $intrusionId)->pluck('controller');
                $func = SystemIntrusions::where('id', $intrusionId)->pluck('func');
                $msg = SystemIntrusions::where('id', $intrusionId)->pluck('msg');
                $userName = sprintf('%s %s', UserInfo::where('id', User::where('id', SystemIntrusions::where('id', $intrusionId)->pluck('user_id'))->pluck('info_id'))->pluck('fname'), UserInfo::where('id', User::where('id', SystemIntrusions::where('id', $intrusionId)->pluck('user_id'))->pluck('info_id'))->pluck('lname'));
                $status = SystemIntrusions::where('id', $intrusionId)->pluck('status');
                $createdAt = Exceptions::where('id', $intrusionId)->pluck('created_at');

                //calculation
                $createdAt = ($timeSince->timeAgo((new DateTime($createdAt))->format('Y-m-d H:i:s')));

                $ret[] = array(
                    'id' => $intrusionId,
                    'controller' => $controller,
                    'func' => $func,
                    'exe_msg' => $msg,
                    'status' => $status,
                    'user' => $userName,
                    'created_at' => $createdAt
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $this->saveExceptionDetails('ErrorController', 'getInternalSystemIntrusionList', $ex);
            return 0;
        }
    }

    public function updateInternalSystemIntrusionStatus() {
        $intrusionId = Input::get('intrusion_id');
        try {
            SystemIntrusions::where('id', $intrusionId)->update(array(
                'status' => 1
            ));

            //create activity for the activity log
            $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');

            $activity = sprintf("%s Ignored or Solved Intrusion %s.", $userName, $intrusionId);
            $addActivity = new UserManageController();
            $addActivity -> addActivity($activity);

            return 1;
        } catch (Exception $ex) {
            $this->saveExceptionDetails('ErrorController', 'updateInternalSystemIntrusionStatus', $ex);
            return 0;
        }
    }

}
