<?php

/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/08/2016 06:58.
 */
class ScaffoldController extends BaseController
{

    //region Frontend
    public function getAddScaffold() {
        $pickupd = Input::get('pickupd');
        $pickupt = Input::get('pickupt');
        $returnd = Input::get('returnd');
        $returnt = Input::get('returnt');
        $typeId = Input::get('type');
        $qty = Input::get('qty');

        try {
            $from = sprintf('%s %s:00:00', $pickupd, str_pad($pickupt, 2, '0', STR_PAD_LEFT));
            $to = sprintf('%s %s:00:00', $returnd, str_pad($returnt, 2, '0', STR_PAD_LEFT));
            $bookedItemEachList[0] = null;$bookedItemEachList[1] = null;
            $bookedAllItemListArray = [];

            $partList = ScaffoldingTypeParts::where('scaffolding_type_id', $typeId)->lists('scaffolding_part_id');
            foreach ($partList as $partId) {
                $partQty = ScaffoldingTypeParts::where('scaffolding_type_id', $typeId)->where('scaffolding_part_id', $partId)->pluck('quantity');
                $requiredQty = $partQty * $qty;

                $scaffoldListFrom = ScaffoldingCart::whereBetween('booking_from', array($from, $to))->where('scaffolding_type_id', $typeId)->where('status', '!=', 0)->lists('id');
                $scaffoldListTo = ScaffoldingCart::whereBetween('booking_to', array($from, $to))->where('scaffolding_type_id', $typeId)->where('status', '!=', 0)->lists('id');
                $scaffoldListDifference1 = ScaffoldingCart::where('booking_from', '<', $from)->where('booking_to', '>', $to)->where('scaffolding_type_id', $typeId)->lists('id');
                $scaffoldListDifference2 = ScaffoldingCart::where('booking_from', '>', $from)->where('booking_to', '<', $to)->where('scaffolding_type_id', $typeId)->lists('id');

                $allBookedCartList = array_unique(array_merge($scaffoldListFrom, $scaffoldListTo, $scaffoldListDifference1, $scaffoldListDifference2));
//                $allBookedItemList = null;
//                foreach ($allBookedCartList as $cartId) {
//                    $cartItemList = ScaffoldingCartItems::where('cart_id', $cartId)->lists('id');
//                    foreach ($cartItemList as $itemId) {
//                        $allBookedItemList[] = $itemId;
//                    }
//                }
                $allBookedItemList = ScaffoldingCartItems::whereIn('cart_id', $allBookedCartList)->lists('scaffolding_part_each_id');

                $allItemEachList = ScaffoldingPartEach::where('scaffolding_part_id', $partId)->where('status', '!=', 0)->lists('id');

                $availableItemEachList = array_diff($allItemEachList, $allBookedItemList);
                $availableCount = count($availableItemEachList);

                if($availableCount > $requiredQty) {
                    $requiredItemList = array_slice($availableItemEachList, 0, $requiredQty);
                    foreach ($requiredItemList as $item) {
                        array_push($bookedAllItemListArray, $item);
                    }


                    //calculation
                    $diff = strtotime($to) - strtotime($from);
                    $hours = $diff / 3600;


                    $days = $hours / 24;
                    $restHours = $hours % 24;

                    if ($restHours > 0) {
                        $days++;
                    }

                    $userId = null;
                    if(Auth::check()) {
                        $userId = Auth::user()->id;
                    }

                    $dayRate = ScaffoldingType::where('id', $typeId)->pluck('day_rate');
                    $typeName = ScaffoldingType::where('id', $typeId)->pluck('name');

                    session_start();
                    if(isset($_SESSION["session_id"])) {
                        $sessionId = $_SESSION["session_id"];
                        $jobId = BookingJob::where('session_id', $sessionId)->pluck('id');
                    }
                    else {
                        $sessionId = str_random(120);
                        $_SESSION["session_id"] = $sessionId;

                        $job = new BookingJob();
                        $job -> session_id = $sessionId;
                        $job -> user_id = $userId;
                        $job -> info_id = null;
                        $job -> licence = null;
                        $job -> status = 2;
                        $job -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                        $job -> save();
                        $jobId = $job -> id;
                    }

                    if(!$jobId) {
                        $job = new BookingJob();
                        $job -> session_id = $sessionId;
                        $job -> user_id = $userId;
                        $job -> info_id = null;
                        $job -> licence = null;
                        $job -> status = 2;
                        $job -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                        $job -> save();
                        $jobId = $job -> id;
                    }

                    //processing data
                    $shortDate = new CalculationController();

                    $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                    $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';


                    $days = (int)$days;
                    $total = $days * $dayRate*$qty;

                    $cart = new ScaffoldingCart();
                    $cart -> job_id = $jobId;
                    $cart -> qty = $qty;
                    $cart -> scaffolding_type_id = $typeId;
                    $cart -> booking_from = $from;
                    $cart -> booking_to = $to;
                    $cart -> status = 4;
                    $cart -> price = $total;
                    $cart -> day_rate = $dayRate;
                    $cart -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $cart -> save();
                    $cartId = $cart -> id;

                    foreach ($bookedAllItemListArray as $itemId) {
                        $cartItem = new ScaffoldingCartItems();
                        $cartItem -> scaffolding_part_each_id = $itemId;
                        $cartItem -> cart_id = $cartId;
                        $cartItem -> status = 1;
                        $cartItem -> save();

                    }


                    $ret = array(
                        'rate' => number_format($dayRate, 2, '.', ''),
                        'days' => $days,
                        'name' => $typeName,
                        'total' => number_format($total, 2, '.', '' ),
                        'total_int' => $total,
                        'cart_id' => $cartId,
                        'from' => $fromShort,
                        'to' => $toShort
                    );

                    return json_encode($ret);
                }
                else {
                    return -1;
                }
            }

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('getAddScaffold', 'ScaffoldController', $ex);

            return 0;
        }
    }

    public function getSessionList() {
        try {
            session_start();
            if(isset($_SESSION["session_id"])) {
                $sessionId = $_SESSION['session_id'];
                $jobId = BookingJob::where('session_id', $sessionId)->pluck('id');
                $scaffoldCartList = ScaffoldingCart::where('job_id', $jobId)->lists('id');
                $ret = null;

                foreach ($scaffoldCartList as $cartId) {
                    $qty = ScaffoldingCart::where('id', $cartId)->pluck('qty');

                    $typeId = ScaffoldingCart::where('id', $cartId)->pluck('scaffolding_type_id');
                    $typeName = ScaffoldingType::where('id', $typeId)->pluck('name');
                    $dayRate = ScaffoldingCart::where('id', $cartId)->pluck('day_rate');

                    $from = ScaffoldingCart::where('id', $cartId)->pluck('booking_from');
                    $to = ScaffoldingCart::where('id', $cartId)->pluck('booking_to');

                    //processing data
                    $shortDate = new CalculationController();

                    $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                    $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';

                    //calculation
                    $diff = strtotime($to) - strtotime($from);
                    $hours = $diff / 3600;


                    $days = $hours / 24;
                    $restHours = $hours % 24;

                    if ($restHours > 0) {
                        $days++;
                    }

                    $days = (int)$days;
                    $total = $days * $dayRate * $qty;

                    $ret[] = array(
                        'rate' => number_format($dayRate, 2, '.', ''),
                        'days' => $days,
                        'name' => $typeName,
                        'qty' => $qty,
                        'total' => number_format($total, 2, '.', '' ),
                        'total_int' => $total,
                        'cart_id' => $cartId,
                        'from' => $fromShort,
                        'to' => $toShort
                    );
                }

                return json_encode($ret);
            }


        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'getSessionList', $ex);

            return 0;
        }
    }

    public function removeScaffold() {
        $cartId = Input::get('cart_id');

        try {
            ScaffoldingCartItems::where('cart_id', $cartId)->delete();
            ScaffoldingCart::where('id', $cartId)->delete();

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'getRemoveScaffold', $ex);

            return 0;
        }
    }

    public function calculatePrice()
    { //tested and ok
        $typeId = Input::get('type_id');
        $quantity = Input::get('quantity');
        $from = Input::get('from');
        $to = Input::get('to');

        try {
            $availability = $this->checkAvailability($typeId, $from, $to, $quantity);
            if ($availability == true) {
                $dayRate = ScaffoldingType::where('id', $typeId)->pluck('day_rate');

                $diff = strtotime($to) - strtotime($from);
                $hours = $diff / 3600;


                $days = $hours / 24;
                $restHours = $hours % 24;

                if ($restHours > 0) {
                    $days++;
                }

                $days = (int)$days;

                $ret = $days * $dayRate * $quantity;

                return json_encode($ret);
            } else {
                return json_encode('Quantity is not available!');
            }


        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'calculatePrice', $ex);

            return 0;
        }
    }

    public function checkAvailability($typeId, $from, $to, $quantity) //tested and ok
    {
        try {
            $partList = ScaffoldingTypeParts::where('scaffolding_type_id', $typeId)->lists('scaffolding_part_id');
            $availability = false;

            foreach ($partList as $partId) {
                $bookedQuantity = ScaffoldingCart::where('scaffolding_part_id', $partId)->where('status', '!=', 0)
                    ->whereBetween('booking_from', array($from, $to))
                    ->whereBetween('booking_to', array($from, $to))->count();

                $availableQuantity = (ScaffoldingPartEach::where('scaffodling_part_id', $partId)->where('status', '!=', 0)->count()) - $bookedQuantity;
                $requiredQuantity = (ScaffoldingTypeParts::where('scaffolding_type_id', $typeId)
                        ->where('scaffolding_part_id', $partId)->pluck('quantity')) * $quantity;


                if ($availableQuantity >= $requiredQuantity) {
                    $availability = true;
                } else {
                    $availability = false;
                    break;
                }
            }

            return $availability;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'checkAvailability', $ex);

            return 0;
        }
    }

    public function addScaffoldingCartItems() //tested and ok
    {
        $typeId = Input::get('type_id');
        $quantity = Input::get('quantity');
        $from = Input::get('from');
        $to = Input::get('to');

        try {
            $availability = $this->checkAvailability($typeId, $from, $to, $quantity);

            if ($availability == true) {
                $sessionId = Session::get('session_id');
                $bookingId = null;

                if ($sessionId == '') {
                    $sessionId = str_random(60);
                    Session::put('session_id', $sessionId);
                    $booking = new ScaffoldingBooking();
                    $booking->session_id = $sessionId;
                    $booking->status = 4;
                    $booking->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $booking->save();
                    $bookingId = $booking->id;
                } else if ($bookingId == '' || $bookingId == null) {
                    $sessionId = str_random(60);
                    Session::put('session_id', $sessionId);
                    $booking = new ScaffoldingBooking();
                    $booking->session_id = $sessionId;
                    $booking->status = 4;
                    $booking->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $booking->save();
                    $bookingId = $booking->id;
                } else {
                    $bookingId = ScaffoldingBooking::where('session_id', $sessionId)->pluck('id');
                }

                $partList = ScaffoldingTypeParts::where('scaffolding_type_id', $typeId)->lists('scaffolding_part_id');

                foreach ($partList as $partId) {
                    $typeQuantity = ScaffoldingTypeParts::where('scaffolding_type_id', $typeId)->where('scaffolding_part_id', $partId)->pluck('quantity'); //required part quantity to build one type
                    $typeTotalQuantity = $quantity * $typeQuantity; //total number of required types
                    $partEachIdList = ScaffoldingPartEach::where('scaffodling_part_id', $partId)->where('status', '!=', 0)->orderBy('status', 'desc')->lists('id');

                    for ($counter = 0; $typeTotalQuantity > $counter; $counter++) {
                        $cart = new ScaffoldingCart();
                        $cart->booking_id = $bookingId;
                        $cart->scaffolding_type_id = $typeId;
                        $cart->scaffolding_part_id = $partEachIdList[$counter];
                        $cart->booking_from = $from;
                        $cart->booking_to = $to;
                        $cart->status = 3;
                        $cart->release_at = null;
                        $cart->return_at = null;
                        $cart->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                        $cart->save();

                        ScaffoldingPartEach::where('id', $partEachIdList[$counter])->update(array(
                            'status' => 2
                        ));
                    }
                }

                return 1;

            } else {
                return json_encode('No Enough Stock Available!');
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'addScaffoldingCartItems', $ex);

            return 0;
        }

    }

    public function bookScaffolding()
    { //tested and ok
        $fName = Input::get('first_name');
        $lName = Input::get('last_name');
        $email = Input::get('email');
        $telephone = Input::get('telephone');


        if (Auth::check()) {
            $customerInfoId = UserInfo::where('user_id', Auth::user()->id)->where('status', 1)->pluck('id');
        } else {
            $customer = new UserInfo();
            $customer->fname = $fName;
            $customer->lname = $lName;
            $customer->email = $email;
            $customer->telephone = $telephone;
            $customer->status = 1;
            $customer->save();
            $customerInfoId = $customer->id;
        }

        $sessionId = Session::get('session_id');

        if ($sessionId == '' || $sessionId == null) {
            return json_encode('Session is expired!');
        } else {
            $bookingId = ScaffoldingBooking::where('session_id', $sessionId)->pluck('id');

            $booking = ScaffoldingBooking::where('id', $bookingId)->update(array(
                'customer_info_id' => $customerInfoId,
                'status' => 3,
                'created_at' => \Carbon\Carbon::now('Pacific/Auckland')
            ));
        }


        if ($booking) {
            Mail::send('emails.scaf-booking-customer', array(
                'fName' => $fName),
                function ($message) use ($fName, $lName, $email) {
                    $message->to($email, sprintf("%s %s", $fName, $lName))->subject('Giant Rentals vehicle booking confirmation!');
                });

            Mail::send('emails.scaf-booking-admin', array(
                'fName' => $fName,
                'telephone' => $telephone,
                'email' => $email),
                function ($message) use ($fName, $lName, $email) {
                    $message->to('pavithraisuru@gmail.com', sprintf("%s %s", $fName, $lName))->subject('Giant Rentals new vehicle booking');
                });
        }

        return 1;
    }
    //endregion

    //region Backend
    public function addScaffoldingPart()
    { //tested and ok
        $partCode = Input::get('part_code');
        $name = Input::get('name');
        $quantity = Input::get('quantity');

        try {
            $addPart = new ScaffoldingPart();
            $addPart->part_code = $partCode;
            $addPart->name = $name;
            $addPart->created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $addPart->save();
            $partId = $addPart->id;

            for ($i = 0; $i < $quantity; $i++) {
                $addPartEach = new ScaffoldingPartEach();
                $addPartEach->scaffodling_part_id = $partId;
                $addPartEach->status = 1;
                $addPartEach->save();
            }

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'addScaffoldingPartType', $ex);

            return 0;
        }
    }

    public function updateScaffoldingPartStock()
    { //tested and ok
        $partCode = Input::get('part_code');
        $quantity = Input::get('quantity');

        try {
            $partId = ScaffoldingPart::where('part_code', $partCode)->pluck('id');

            for ($i = 0; $i < $quantity; $i++) {
                $addPartEach = new ScaffoldingPartEach();
                $addPartEach->scaffodling_part_id = $partId;
                $addPartEach->status = 1;
                $addPartEach->save();
            }

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'addScaffoldingType', $ex);

            return 0;
        }
    }

    public function addScaffoldingTypes()
    { //tested and ok
        $name = Input::get('name');
        $dayRate = Input::get('day_rate');
        $typeParts = Input::get('type_parts'); //2d array is required Ex: typeParts = {7:4, 8:4, 9:2, 10:1} ... {part_id:quantity}

        try {
            $newType = new ScaffoldingType();
            $newType->name = $name;
            $newType->day_rate = $dayRate;
            $newType->created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $newType->save();
            $typeId = $newType->id;

            foreach ($typeParts as $partId => $quantity) {
                $newTypeParts = new ScaffoldingTypeParts();
                $newTypeParts->scaffolding_type_id = $typeId;
                $newTypeParts->scaffolding_part_id = $partId;
                $newTypeParts->quantity = $quantity;
                $newTypeParts->save();
            }

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'addScaffoldingTypesPartTypes', $ex);

            return 0;
        }
    }

    public function getBookingList() {
        $status = Input::get('status');
        $paginate = Input::get('paginate');

        try {
            $deleteAccess = 0;
            if(UserOperationAccess::where('user_id', Auth::user()->id)->where('operation_id', 4)->pluck('id') || Auth::user()->roll == 1)
                $deleteAccess = 1;

            $bookingList = ScaffoldingCart::where('status', $status)->orderBy('created_at', 'asc')->forPage($paginate, 50)->lists('job_id');
            $bookingList = array_unique($bookingList);
            $ret = null;
            $shortDate = new CalculationController();

            foreach ($bookingList as $jobId) {
                $bookingDate = BookingJob::where('id', $jobId)->pluck('created_at');
                $infoId = BookingJob::where('id', $jobId)->pluck('info_id');
                $name = sprintf("%s %s", UserInfo::where('id', $infoId)->pluck('fname'), UserInfo::where('id', $infoId)->pluck('lname'));
                $collect = ScaffoldingCart::where('id', $jobId)->orderBy('booking_from', 'asc')->first();
                $collect = $collect['booking_from'];
                $from = sprintf("<strong>%s</strong> %s", $shortDate->shortDate($collect), substr($collect, -9));
                $from = substr($from, 0, -3);

                $amount = ScaffoldingCart::where('job_id', $jobId)->where('status', '!=', 0)->sum('price');


                $ret[] = array(
                    'id' => $jobId,
                    'inv_id' => str_pad($jobId, 6, '0', STR_PAD_LEFT),
                    'bookingdate' => $bookingDate,
                    'deleteaccess' => $deleteAccess,
                    'name' => $name,
                    'tag' => substr($name, 0, 1),
                    'price' => $amount,
                    'from' => $from
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'getBookingList', $ex);

            return 0;
        }
    }

    public function getSingleBookingDetails() {
        $jobId = Input::get('booking_id');

        try {
            $cartList = ScaffoldingCart::where('job_id', $jobId)->where('status', '!=', 0)->lists('id');
            $bookingStatus = BookingJob::where('id', $jobId)->pluck('status');
            $infoId = BookingJob::where('id', $jobId)->pluck('info_id');
            $name = sprintf('%s %s', UserInfo::where('id', $infoId)->pluck('fname'), UserInfo::where('id', $infoId)->pluck('lname'));
            $telephone = UserInfo::where('id', $infoId)->pluck('telephone');
            $email = UserInfo::where('id', $infoId)->pluck('email');
            $createdAt = ScaffoldingCart::where('id', $jobId)->pluck('created_at');
            $payment = 0; $discount = 0;

            $cart = [];
            $subTotal = 0;

            $shortDate = new CalculationController();

            //Calculations
            $createdAt = ($shortDate->shortFullDate((new DateTime($createdAt))->format('Y-m-d H:i:s')));

            foreach ($cartList as $cartId) {
                $typeId = ScaffoldingCart::where('id', $cartId)->pluck('scaffolding_type_id');
                $typeName = ScaffoldingType::where('id', $typeId)->pluck('name');
                $dayRate = ScaffoldingCart::where('id', $cartId)->pluck('day_rate');
                $qty = ScaffoldingCart::where('id', $cartId)->pluck('qty');
                $totalPrice = ScaffoldingCart::where('id', $cartId)->pluck('price');
                $from = ScaffoldingCart::where('id', $cartId)->pluck('booking_from');
                $to = ScaffoldingCart::where('id', $cartId)->pluck('booking_to');
                $status = ScaffoldingCart::where('id', $cartId)->pluck('status');
                $discount = ScaffoldingCart::where('id', $cartId)->pluck('discount');
                $payment = ScaffoldingCart::where('id', $cartId)->pluck('payment');
                $subTotal += $totalPrice;

                $cart[] = array(
                    'id' => $cartId,
                    'type_id' => $typeId,
                    'type_name' => $typeName,
                    'qty' => $qty,
                    'day_rate' => $dayRate,
                    'price' => $totalPrice,
                    'from' => $shortDate->shortFullDate($from),
                    'to' => $shortDate->shortFullDate($to),
                    'status' => $status
                );
            }

            $discountValue = ($discount/100) * $subTotal;
            $fullTotal = $subTotal - $discountValue;

            $ret = array(
                'booking_id' => str_pad($jobId, 6, '0', STR_PAD_LEFT),
                'name' => $name,
                'telephone' => $telephone,
                'status' => $bookingStatus,
                'created_at' => $createdAt,
                'email' => $email,
                'sub_total' => $subTotal,
                'discount' => $discount,
                'full_total' => $fullTotal,
                'payment' => $payment
            );

            array_push($ret, $cart);

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'getSingleBookingDetails', $ex);

            return 0;
        }
    }

    public function getCartItemList() {
        $cartId = Input::get('cart_id');
        $arr = null;

        try {
            $cartItemList = ScaffoldingCartItems::where('cart_id', $cartId)->lists('id');

            foreach ($cartItemList as $itemId) {
                $scaffoldingPartId = ScaffoldingPartEach::where('id', ScaffoldingCartItems::where('id', $itemId)->pluck('scaffolding_part_each_id'))->pluck('scaffolding_part_id');
                $partName = ScaffoldingPart::where('id', $scaffoldingPartId)->pluck('name');
                $status = ScaffoldingCartItems::where('id', $itemId)->pluck('status');

                $arr[] = array(
                    'id' => $itemId,
                    'item_id' => str_pad($itemId, 5, '0', STR_PAD_LEFT),
                    'name' => $partName,
                    'status' => $status
                );
            }

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'getCartItemList', $ex);

            return 0;
        }
    }

    public function updateCartStatus() {
        $cartId = Input::get('cart_id');

        try {
            ScaffoldingCart::where('id', $cartId)->decrement('status', 1);

//            $status = ScaffoldingCart::where('id', $cartId)->pluck('status');
//            if($status == 2) {
//                $itemEachList = ScaffoldingCartItems::where('cart_id', $cartId)->status('1')->lists('scaffolding_part_each_id');
//                foreach ($itemEachList as $itemId) {
//                    ScaffoldingPartEach::where('id', $itemId)->update('status', 2);
//                }
//            }
//            else if($status == 1) {
//                $itemEachList = ScaffoldingCartItems::where('cart_id', $cartId)->status('1')->lists('scaffolding_part_each_id');
//                foreach ($itemEachList as $itemId) {
//                    ScaffoldingPartEach::where('id', $itemId)->update('status', 1);
//                }
//            }


            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'updateCartStatus', $ex);

            return 0;
        }
    }

    public function getScaffoldingPartList() {
        $partId = Input::get('type_id');

        try {
            $typeItemList = ScaffoldingPartEach::where('scaffolding_part_id', $partId)->lists('id');
            $timeAgo = new CalculationController();

            foreach ($typeItemList as $itemId) {
                $status = ScaffoldingPartEach::where('id', $itemId)->pluck('status');
                $createdAt = ScaffoldingPartEach::where('id', $itemId)->pluck('created_at');
                $createdAt = $timeAgo->shortFullDate((new DateTime($createdAt))->format('Y-m-d H:i:s'));

                $ret[] = array(
                    'id' => $itemId,
                    'part_id' => str_pad($itemId, 8, '0', STR_PAD_LEFT),
                    'status' => $status,
                    'created_at' => $createdAt
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'updateCartStatus', $ex);

            return 0;
        }
    }

    public function getSingleBookingElement()
    { //tested and ok

        $bookingListType = Input::get('booking_list_type'); //require to send the status of the booking list

        try {
            $bookingList = ScaffoldingBooking::where('status', $bookingListType)->lists('id');
            $cart[] = null;
            $ret[] = null;

            foreach ($bookingList as $bookingId) {
                $customerInfoId = ScaffoldingBooking::where('id', $bookingId)->pluck('customer_info_id');
                $userInfo = UserInfo::where('id', $customerInfoId);
                $fName = $userInfo->pluck('fname');
                $lName = $userInfo->pluck('lname');
                $email = $userInfo->pluck('email');
                $telephone = $userInfo->pluck('telephone');
                $created_at = ScaffoldingBooking::where('id', $bookingId)->pluck('created_at');

                $cartList = ScaffoldingCart::where('booking_id', $bookingId)->where('status', $bookingListType)->lists('id');


                foreach ($cartList as $cartId) {
                    $scaffoldingCart = ScaffoldingCart::where('id', $cartId);
                    $typeId = $scaffoldingCart->pluck('scaffolding_type_id');
                    $partId = $scaffoldingCart->pluck('scaffolding_part_id');
                    $from = $scaffoldingCart->pluck('booking_from');
                    $to = $scaffoldingCart->pluck('booking_to');


                    $cart[] = array(
                        'cart_id' => $cartId,
                        'type_id' => $typeId,
                        'part_id' => $partId,
                        'from' => $from,
                        'to' => $to
                    );
                }

                $ret[] = array(
                    'booking_id' => $bookingId,
                    'f_name' => $fName,
                    'l_name' => $lName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'created_at' => $created_at,
                    'cart' => $cart
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'getSingleBookingElement', $ex);

            return 0;
        }
    }

    public function getBookingTitles()
    {
        $bookingListType = Input::get('booking_list_type'); //require to send the status of the booking list

        try {
            $bookingList = ScaffoldingCart::where('status', $bookingListType)->lists('booking_id');
            $bookingList = array_unique($bookingList);
            $ret[] = null;

            foreach ($bookingList as $bookingId) {
                $customerInfoId = ScaffoldingBooking::where('id', $bookingId)->pluck('customer_info_id');
                $userInfo = UserInfo::where('id', $customerInfoId);
                $fName = $userInfo->pluck('fname');
                $lName = $userInfo->pluck('lname');
                $email = $userInfo->pluck('email');
                $telephone = $userInfo->pluck('telephone');
                $created_at = ScaffoldingBooking::where('id', $bookingId)->pluck('created_at');

                $cartListCount = ScaffoldingCart::where('booking_id', $bookingId)->where('status', $bookingListType)->count('id');


                $ret[] = array(
                    'booking_id' => $bookingId,
                    'f_name' => $fName,
                    'l_name' => $lName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'created_at' => $created_at,
                    'cart_item_count' => $cartListCount
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'getBookingTitles', $ex);

            return 0;
        }
    }

    public function getSingleBookingCartList()
    {
        $bookingListType = Input::get('booking_list_type'); //require to send the status of the booking list
        $bookingId = Input::get('booking_id');

        try {
            $cartList = ScaffoldingCart::where('booking_id', $bookingId)->where('status', $bookingListType)->lists('id');
            $ret[] = null;

            foreach ($cartList as $cartId) {
                $scaffoldingCart = ScaffoldingCart::where('id', $cartId);
                $typeId = $scaffoldingCart->pluck('scaffolding_type_id');
                $partId = $scaffoldingCart->pluck('scaffolding_part_id');
                $from = $scaffoldingCart->pluck('booking_from');
                $to = $scaffoldingCart->pluck('booking_to');


                $ret[] = array(
                    'cart_id' => $cartId,
                    'type_id' => $typeId,
                    'part_id' => $partId,
                    'from' => $from,
                    'to' => $to
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'getSingleBookingCartList', $ex);

            return 0;
        }
    }

    public function postDiscount() {
        $jobId = Input::get('bookingid');
        $value = Input::get('value');

        try {
            ScaffoldingCart::where('job_id', $jobId)->update(array(
                'discount' => $value
            ));

            return 1;
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('ScaffoldController', 'postDiscount', $ex);

            return 0;
        }
    }
    //endregion
}
