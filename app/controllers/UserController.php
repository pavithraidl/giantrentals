<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/08/2016 06:34.
 */
class UserController extends BaseController {
    //Frontend Controller
    public function postFrontendSingIn() { //tested and ok
        //get relevant details from the front end
        $firstName = Input::get( 'fname' );
        $lastName  = Input::get( 'lname' );
        $telephone = Input::get( 'telephone' );
        $email     = Input::get( 'email' );
        $password  = Input::get( 'password' );
        $rollId    = Input::get('roll_id');
        try {
            if ( User::where( 'email', $email )->pluck( 'id' ) ) {
                return - 1;
            }
            else if($rollId < 3 || $rollId == 1045) {
                $code = str_random( 60 );
                $userInfo   = new UserInfo();
                $userInfo->fname = $firstName;
                $userInfo->lname  = $lastName;
                $userInfo->email       = $email;
                $userInfo->telephone    = $telephone;
                $userInfo->status    = 1;
                $userInfo->save();
                $infoId = $userInfo->id;
                $user             = new User();
                $user->email      = $email;
                $user->password   = Hash::make( $password );
                $user->active     = 0;
                $user->code       = $code;
                $user->roll       = $rollId;
                $user->info_id    = $infoId;
                $user->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $user->updated_at = \Carbon\Carbon::now('Pacific/Auckland');
                $user->save();
                if ( $user ) {
                    Mail::send('emails.active-account', array(
                        'link' => URL::route( 'account-activate', $code ),
                        'name' => sprintf( "%s %s", $firstName, $lastName )),
                        function($message) use($firstName, $lastName, $email) {
                            $message->to($email, sprintf("%s %s", $firstName, $lastName ))->subject('Giant Rentals Account Email Confirmation!');
                        });
                }
                return 1;
            }
        } catch ( Exception $ex ) {
            $errorController = new ErrorController();
            $errorController->saveExceptionDetails( 'UserController', 'postFrontendSingIn', $ex );
            return 0;
        }
    }
    //External User Controller
    //-----------------------------------------------------------------------------------------------------------------
    public function postSignUp() { //tested and ok
        //get relevant details from the front end
        $firstName = Input::get( 'fname' );
        $lastName  = Input::get( 'lname' );
        $telephone = Input::get( 'telephone' );
        $email     = Input::get( 'email' );
        $password  = Input::get( 'password' );
        $rollId    = Input::get('roll_id');
        try {
            if ( User::where( 'email', $email )->pluck( 'id' ) ) {
                return - 1;
            } else {
                $code = str_random( 60 );
                $userInfo   = new UserInfo();
                $userInfo->fname = $firstName;
                $userInfo->lname  = $lastName;
                $userInfo->email       = $email;
                $userInfo->telephone    = $telephone;
                $userInfo->status    = 1;
                $userInfo->save();
                $infoId = $userInfo->id;
                $user             = new User();
                $user->email      = $email;
                $user->password   = Hash::make( $password );
                $user->active     = 0;
                $user->code       = $code;
                $user->roll       = $rollId;
                $user->info_id    = $infoId;
                $user->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $user->updated_at = \Carbon\Carbon::now('Pacific/Auckland');
                $user->save();
                if ( $user ) {
                    Mail::send('emails.active-account', array(
                        'link' => URL::route( 'account-activate', $code ),
                        'name' => sprintf( "%s %s", $firstName, $lastName )),
                        function($message) use($firstName, $lastName, $email) {
                            $message->to($email, sprintf("%s %s", $firstName, $lastName ))->subject('Giant Rentals Account Email Confirmation!');
                        });
                }
                return 1;
            }
        } catch ( Exception $ex ) {
            $errorController = new ErrorController();
            $errorController->saveExceptionDetails( 'UserController', 'postSignIn', $ex );
            return 0;
        }
    }
    public function getActivate($code) {
        if(User::where('code', $code)->where('active', 1)->pluck('id')) {
            return View::make('active', array(
                'pageName' => 'already-active'
            ));
        }
        else {
            return View::make('active', array(
                'pageName' => 'active',
                'code' => $code
            ));
        }
    }
    public function postActive() { //tested and ok
        $code = Input::get('code');
        try {
            $user = User::where('code', $code)->where('active', '!=', 1);
            if($user->count()) {
                $user = $user->first();
                $user->active = 1;
                $user->code = '';
                $user->save();
                if($user->save()) {
                    $ret = URL::Route('login');
                    return $ret;
                }
                else {
                    return 0;
                }
            }
            else {
                return -2;
            }
        } catch ( Exception $ex ) {
            $errorController = new ErrorController();
            $errorController->saveExceptionDetails( 'UserController', 'postActivate', $ex );
            return 0;
        }
    }
    public function postLogin() { //tested and ok
        try{
            //validate the login details
            $validator = Validator::make(Input::all(), array(
                'email' => 'required|email',
                'password' => 'required'
            ));
            if ($validator->fails()) {
                // Redirect to the sign in page with errors
                return Redirect::route('login')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                //remember user
                $remember = (Input::has('chk-remember')) ? true : false;
                // Sign in user to the dash board
                $email = Input::get('email');
                $password = Input::get('password');
                $previous = Input::get('previous');
                $auth = Auth::attempt(array(
                    'email' => $email,
                    'password' => $password,
                    'active' => 1
                ), $remember);
                if ($auth) {
                    if(DB::table('users')->where('email', $email)->pluck('active') == 0){
                        return Redirect::route('login')
                            ->with('global', 'This account is not Activated yet. Check your emails to active!');
                    }
                    else{
                        return Redirect::To($previous);
                    }
                }
                else {
                    return Redirect::route('login')
                        ->with('global', 'Email/password wrong, or account not activated!');
                }
            }
        }catch ( Exception $ex ) {
            $errorController = new ErrorController();
            $errorController->saveExceptionDetails( 'UserController', 'postLogin', $ex );
            return 0;
        }
    }
    public function getLogOut() { //tested and ok
        Auth::logout();
        return Redirect::route('login');
    }
// External User Controller
//----------------------------------------------------------------------------------------------------------------------
//Activate the account (GET)
    public function getInternalActivate($code)
    {
        $blockIp = new UserVisitController();
        if($blockIp->getUserBlockIp()){
            try{
                if (User::where('code', '=', $code)->where('active', '=', 0)->pluck('id')) {
                    $user = DB::table('users')->where('code', $code)->first();
                    return View::make('account.activate', array(
                        'code' => $code,
                        'email' => $user->email
                    ));
                } else {
                    return View::make('error._404');
                }
            }catch (Exception $ex) {
                $errorController = new ErrorController();
                $errorController->CatchError(4, $ex);
                return 0;
            }
        }
        else{
            return View::make('error._404');
        }
    }
//Active account (POST)
    public function postInternalActivate($code)
    {
        try{
            // validate the inputs
            $validator = Validator::make(Input::all(), array(
                'password' => 'min:6|required',
                'confirmPassword' => 'required|same:password'
            ));
            //validation fails, then pass the validation error to main page
            if ($validator->fails()) {
                return Redirect::route('account-activate', array($code))
                    ->withErrors($validator)//validation error message
                    ->withInput();//input fields values
            } else {
                if (User::where('code', '=', $code)
                    ->where('active', '=', 0)
                ) {
                    //save the user account data to the database
                    $password = Input::get('password');
                    $email = Input::get('email');
                    $user = User::where('code', '=', $code)
                        ->where('active', '=', 0);
                    //add record to the activity log
                    DB::table('activity_log')->insert(
                        array(
                            'userid' => DB::table('users')->where('code', $code)->pluck('id'),
                            'activity' => "Account Activated successfully.",
                            'created_at' => \Carbon\Carbon::now('Pacific/Auckland')
                        )
                    );
                    if ($user->count()) {
                        $user = $user->first();
                        // Update user to active state
                        $user->active = 1;
                        $user->code = '';
                        $user->password = Hash::make($password);
                        $user->save();
                        if ($user->save()) {
                            return Redirect::route('login')
                                ->with('global', 'Activated! you can now sign in');
                        }
                    } else {
                        return Redirect::route('account-activate')
                            ->with('global', 'We could not active your account. Try again later.');
                    }
                }
            }
        }catch (Exception $ex) {
            $errorController = new ErrorController();
            $errorController->CatchError(5, $ex);
            return 0;
        }
    }
//Login (POST)
    public function postInternalLogIn()
    {
        try{
            //validate the login details
            $validator = Validator::make(Input::all(), array(
                'email' => 'required|email',
                'password' => 'required'
            ));
            if ($validator->fails()) {
                // Redirect to the sign in page with errors
                return Redirect::route('login')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                //remember user
                $remember = (Input::has('remember')) ? true : false;
                // Sign in user to the dash board
                $email = Input::get('email');
                $password = Input::get('password');
                $auth = Auth::attempt(array(
                    'email' => $email,
                    'password' => $password,
                    'active' => 1
                ), $remember);
                if ($auth) {
                    if(User::where('email', $email)->pluck('active') == 0){
                        return Redirect::route('login')
                            ->with('global', 'Your account has been deactivated by the organization administration!');
                    }
                    else{
                        return Redirect::route('internal-home');
                    }
                } else {
                    return Redirect::route('get-internal-login')
                        ->with('global', 'Email/password wrong, or account not activated!');
                }
            }
        }catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserController', 'postLogIn', $ex);
            return 0;
        }
    }
    public function postBooking() {
        $fName = Input::get('fname');
        $lName = Input::get('lname');
        $licence = Input::get('licence');
        $email = Input::get('email');
        $phone = Input::get('phone');
        $jobId = Input::get('jobid');
        try {

            $user = new UserInfo();
            $user -> fname = $fName;
            $user -> lname = $lName;
            $user -> email = $email;
            $user -> telephone = $phone;
            $user -> photoid = $licence;
            $user -> save();
            $infoId = $user -> id;

            $bookingJob = BookingJob::where('id', $jobId)->update(array(
                'info_id' => $infoId,
                'licence' => $licence,
                'status' => 2
            ));

            VehicleBooking::where('job_id', $jobId)->where('status', 1)->update(array('status' => 2));

            $invoiceEmail = SystemConfig::where('id', 1)->pluck('value');

            VehicleBooking::where('job_id', $jobId)->where('status', 4)->update(array('status' => 3));
            ScaffoldingCart::where('job_id', $jobId)->where('status', 4)->update(array('status' => 3));
            EventCart::where('job_id', $jobId)->where('status', 4)->update(array('status' => 3));
            $fullJobId = str_pad($jobId, 5, '0', STR_PAD_LEFT);

            BookingJob::where('id', $jobId)->update(array('session_id' => null));

            if ( $bookingJob ) {
                Mail::send('emails.customer-booking-details', array(
                    'page' => 'Booking Job Sheet',
                    'jobId' => $jobId,
                    'infoId' => $infoId),
                    function($message) use($fName, $lName, $email) {
                        $message->to($email, sprintf("%s %s", $fName, $lName ))->subject('Giant Rentals Booking Job Sheet!');
                    });

                Mail::send('emails.customer-booking-details', array(
                    'page' => 'Booking Job Sheet',
                    'jobId' => $jobId,
                    'infoId' => $infoId),
                    function($message) use($fName, $lName, $invoiceEmail, $fullJobId) {
                        $message->to($invoiceEmail, sprintf("%s %s", $fName, $lName ))->subject(sprintf("%s %s %s - Giant Rentals Booking Job Sheet!", $fullJobId, $fName, $lName ));
                    });
            }
            return 1;
        }  catch(Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserController', 'postBooking', $ex);
            return 0;
        }
    }
// get logout (GET)
    public function getInternalLogOut()
    {
        Auth::logout();
        return Redirect::route('get-internal-login');
    }
    public function urlAccessAthenticator() {
        try{
            if(User::where('id', Auth::user()->id)->pluck('active') != 1) {
                Auth::logout();
            }
        } catch(Exception $ex) {
            Auth::logout();
        }
    }
}