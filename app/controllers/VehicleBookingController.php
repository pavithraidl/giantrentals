<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 29/08/2016 11:35.
 */

use Carbon\Carbon;
class VehicleBookingController extends BaseController
{

    //region Frontend
    public function getFindMyVehicle() {
        $pickupd = Input::get('pickupd');
        $pickupt = Input::get('pickupt');
        $returnd = Input::get('returnd');
        $returnt = Input::get('returnt');

        try {
            $from = sprintf('%s %s:00:00', $pickupd, str_pad($pickupt, 2, '0', STR_PAD_LEFT));
            $to = sprintf('%s %s:00:00', $returnd, str_pad($returnt, 2, '0', STR_PAD_LEFT));

            $dayRate = VehicleConfig::where('id', '1')->pluck('value');

            $vehicleListFrom = VehicleBooking::whereBetween('booking_from', array($from, $to))->where('status', '!=',0)->lists('vehicle_id');
            $vehicleListTo = VehicleBooking::whereBetween('booking_to', array($from, $to))->where('status', '!=',0)->lists('vehicle_id');
            $vehicleListDifference1 = VehicleBooking::where('booking_from', '<', $from)->where('booking_to', '>', $to)->where('status', '!=',0)->lists('vehicle_id');
            $vehicleListDifference2 = VehicleBooking::where('booking_from', '>', $from)->where('booking_to', '<', $to)->where('status', '!=',0)->lists('vehicle_id');

            $bookedVehicleList = array_merge($vehicleListFrom, $vehicleListTo, $vehicleListDifference1, $vehicleListDifference2);
            $allVehicleList = Vehicles::where('status', 1)->lists('id');

            $availableList = array_diff($allVehicleList, $bookedVehicleList);

            if($availableList != null) {
                $selectedVehicle = null;
                $currentPriority = 1000;

                foreach ($availableList as $vehicleId) {
                    $priority = Vehicles::where('id', $vehicleId)->pluck('priority');
                    if($priority < $currentPriority) {
                        $selectedVehicle = $vehicleId;
                        $currentPriority = $priority;
                    }
                }


                $vehiclePlate = Vehicles::where('id', $selectedVehicle)->pluck('plate');

                //calculation
                $diff = strtotime($to) - strtotime($from);
                $hours = $diff / 3600;


                $days = $hours / 24;
                $restHours = $hours % 24;

                if ($restHours > 0) {
                    $days++;
                }

                $userId = null;
                if(Auth::check()) {
                    $userId = Auth::user()->id;
                }

                session_start();
                if(isset($_SESSION["session_id"])) {
                    $sessionId = $_SESSION["session_id"];
                    $jobId = BookingJob::where('session_id', $sessionId)->pluck('id');
                }
                else {
                    $sessionId = str_random(120);
                    $_SESSION["session_id"] = $sessionId;

                    $job = new BookingJob();
                    $job -> session_id = $sessionId;
                    $job -> user_id = $userId;
                    $job -> info_id = null;
                    $job -> licence = null;
                    $job -> status = 1;
                    $job -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $job -> save();
                    $jobId = $job -> id;
                }

                if(!$jobId) {
                    $job = new BookingJob();
                    $job -> session_id = $sessionId;
                    $job -> user_id = $userId;
                    $job -> info_id = null;
                    $job -> licence = null;
                    $job -> status = 1;
                    $job -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $job -> save();
                    $jobId = $job -> id;
                }

                $booking = new VehicleBooking();
                $booking -> job_id = $jobId;
                $booking -> vehicle_id = $selectedVehicle;
                $booking -> booking_from = $from;
                $booking -> booking_to = $to;
                $booking -> rate = $dayRate;
                $booking -> status = 1;
                $booking -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $booking -> save();
                $bookingId = $booking -> id;


                $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');

                //processing data
                $shortDate = new CalculationController();

                $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';


                $days = (int)$days;
                $total = $days * $dayRate;

                $ret = array(
                    'rate' => number_format($dayRate, 2, '.', ''),
                    'days' => $days,
                    'plate' => $vehiclePlate,
                    'vehilce_id' => $selectedVehicle,
                    'total' => number_format($total, 2, '.', '' ),
                    'total_int' => $total,
                    'booking_id' => $bookingId,
                    'from' => $fromShort,
                    'to' => $toShort
                );
            }
            else {
                $ret = -1;
            }


            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getFindMyVehicle', $ex);

            return 0;
        }
    }

    public function getSessionList() {
        try {
            session_start();
            if(isset($_SESSION["session_id"])) {
                $sessionId = $_SESSION['session_id'];
                $jobId = BookingJob::where('session_id', $sessionId)->pluck('id');
                $vehicleBookingList = VehicleBooking::where('job_id', $jobId)->where('status', 1)->lists('id');
                $ret = null;

                foreach ($vehicleBookingList as $bookingId) {
                    $vehicleId = VehicleBooking::where('id', $bookingId)->pluck('vehicle_id');
                    $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                    $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');

                    $vehiclePlate = Vehicles::where('id', $vehicleId)->pluck('plate');
                    $dayRate = VehicleConfig::where('id', '1')->pluck('value');

                    $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                    $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');

                    //processing data
                    $shortDate = new CalculationController();

                    $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                    $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';

                    //calculation
                    $diff = strtotime($to) - strtotime($from);
                    $hours = $diff / 3600;


                    $days = $hours / 24;
                    $restHours = $hours % 24;

                    if ($restHours > 0) {
                        $days++;
                    }

                    $days = (int)$days;
                    $total = $days * $dayRate;

                    $ret[] = array(
                        'rate' => number_format($dayRate, 2, '.', ''),
                        'days' => $days,
                        'plate' => $vehiclePlate,
                        'vehilce_id' => $vehicleId,
                        'total' => number_format($total, 2, '.', '' ),
                        'booking_id' => $bookingId,
                        'total_int' => $total,
                        'from' => $fromShort,
                        'to' => $toShort
                    );
                }

                return json_encode($ret);
            }


        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getSessionList', $ex);

            return 0;
        }
    }

    public function removeBooking() {
        $bookingId = Input::get('bookingid');

        try {
            VehicleBooking::where('id', $bookingId)->delete();

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'removeBooking', $ex);

            return 0;
        }
    }

    public function vehicleBooking()
    { //tested and ok
        if (!Auth::check()) {
            $fName = Input::get('first_name');
            $lName = Input::get('last_name');
            $email = Input::get('email');
            $telephone = Input::get('telephone');
        }
        $plate = Input::get('plate');
        $driversAge = Input::get('drivers_age');
        $from = Input::get('from');
        $to = Input::get('to');

        try {
            //confirm availability before make the booking
            $availability = $this->checkAvailability($plate, $from, $to);
            if ($availability == 'ok') {
                //setup the user details
                if (Auth::check()) {
                    $customerInfoId = UserInfo::where('user_id', Auth::user()->id)->where('status', 1)->pluck('id');
                } else {
                    $customer = new UserInfo();
                    $customer->fname = $fName;
                    $customer->lname = $lName;
                    $customer->email = $email;
                    $customer->telephone = $telephone;
                    $customer->status = 1;
                    $customer->save();
                    $customerInfoId = $customer->id;
                }

                //get vehicle id from the plate number
                $vehicleId = Vehicles::where('plate', $plate)->pluck('id');

                //crete the new row in the booking model
                $booking = new VehicleBooking();
                $booking->vehicle_id = $vehicleId;
                $booking->customer_info_id = $customerInfoId;
                $booking->drivers_age = $driversAge;
                $booking->booking_from = $from;
                $booking->booking_to = $to;
                $booking->status = 3;
                $booking->created_at = (new DateTime("now", new DateTimeZone('Pacific/Auckland')))->format('Y-m-d H:i:s');
                $booking->save();

                if ($booking) {
                    Mail::send('emails.vehicle-booking-customer', array(
                        'fName' => $fName,
                        'plate' => $plate,
                        'drivers_age' => $driversAge,
                        'from' => $from,
                        'to' => $to),
                        function ($message) use ($fName, $lName, $email) {
                            $message->to($email, sprintf("%s %s", $fName, $lName))->subject('Giant Rentals vehicle booking confirmation!');
                        });

                    Mail::send('emails.vehicle-booking-admin', array(
                        'fName' => $fName,
                        'plate' => $plate,
                        'drivers_age' => $driversAge,
                        'from' => $from,
                        'to' => $to,
                        'telephone' => $telephone,
                        'email' => $email),
                        function ($message) use ($fName, $lName, $email) {
                            $message->to('pavithraisuru@gmail.com', sprintf("%s %s", $fName, $lName))->subject('Giant Rentals new vehicle booking');
                        });
                }

                return 1;
            } else {
                return json_encode('not available');
            }

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'vehicleBooking', $ex);

            return 0;
        }
    }
    //endregion

    //region Backend
    public function addNewVehicle()
    {
        //function tested and ok
        $plate = Input::get('plate');
        $modal = Input::get('modal');
        $year = Input::get('year');
        $dayRate = Input::get('rate');
        $color = Input::get('color');
        $description = Input::get('description');

        try {
            if(UserOperationAccess::where('user_id', Auth::user()->id)->where('operation_id', 1)->where('status', 1)->pluck('id') || User::where('id', Auth::user()->id)->pluck('roll') < 2) {
                $vehicle = new Vehicles();
                $vehicle->plate = $plate;
                $vehicle->modal = $modal;
                $vehicle->year = $year;
                $vehicle->rate = $dayRate;
                $vehicle->display_class = $color;
                $vehicle->description = $description;
                $vehicle->status = 1;
                $vehicle->created_at = (new DateTime("now", new DateTimeZone('Pacific/Auckland')))->format('Y-m-d H:i:s');
                $vehicle->save();

                //create activity for the activity log
                $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname') . ' ' . UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');
                $activity = sprintf("%s Added new vehicle %s.", $userName, $plate);
                $addActivity = new UserManageController();
                $addActivity->addActivity($activity);

                return 1;
            }
            else {
                $intrusion = new UserManageController();
                $intrusion->systemIntrusions('VehicleBookingController', 'addVehicle', 'Try to add a vehicle when access is not granted. Operation code = 1.');
                return -1;
            }


        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'addVehicle', $ex);

            return 0;
        }
    }

    function updateVehicleInfo() {
        //function tested and ok
        $plate = Input::get('plate');
        $modal = Input::get('modal');
        $year = Input::get('year');
        $dayRate = Input::get('rate');
        $color = Input::get('color');
        $description = Input::get('description');

        try {
            if(UserOperationAccess::where('user_id', Auth::user()->id)->where('operation_id', 2)->where('status', 1)->pluck('id') || User::where('id', Auth::user()->id)->pluck('roll') < 2) {
                $vehicleId = Vehicles::where('plate', $plate)->pluck('id');
                Vehicles::where('id', $vehicleId)->update(array(
                    'modal' => $modal,
                    'year' => $year,
                    'rate' => $dayRate,
                    'display_class' => $color,
                    'description' => $description,
                ));

                //create activity for the activity log
                $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname') . ' ' . UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');
                $activity = sprintf("%s Update vehicle Info %s.", $userName, $plate);
                $addActivity = new UserManageController();
                $addActivity->addActivity($activity);

                return 1;
            }
            else {
                $intrusion = new UserManageController();
                $intrusion->systemIntrusions('VehicleBookingController', 'updateVehicleInfo', 'Try to update a vehicle when access is not granted. Operation code = 2.');
                return -1;
            }


        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'updateVehicleInfo', $ex);

            return 0;
        }
    }

    public function getInternalBookingList()
    {
        //send the booking vehicle list
        $type = Input::get('type');

        try {
            if($type == 3) {
                $bookingList = VehicleBooking::where('status', $type)->orderBy('booking_from', 'asc')->lists('id');
            }
            else if($type == 2) {
                $bookingList = VehicleBooking::where('status', $type)->orderBy('booking_to', 'asc')->lists('id');
            }
            else {
                $bookingList = VehicleBooking::where('status', $type)->orderBy('created_at', 'desc')->lists('id');
            }

            $ret = null;

            foreach ($bookingList as $bookingId) {
                //get required details from the model to send front end
                $vehicleId = VehicleBooking::where('id', $bookingId)->pluck('vehicle_id');
                $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');
                $jobId = VehicleBooking::where('id', $bookingId)->pluck('job_id');
                $infoId = BookingJob::where('id', $jobId)->pluck('info_id');
                $fName = UserInfo::where('id', $infoId)->pluck('fname');
                $lName = UserInfo::where('id', $infoId)->pluck('lname');
                $email = UserInfo::where('id', $infoId)->pluck('email');
                $telephone = UserInfo::where('id', $infoId)->pluck('telephone');
                $plate = Vehicles::where('id', $vehicleId)->pluck('plate');
                $createdAt = VehicleBooking::where('id', $bookingId)->pluck('created_at');
                $releaseAt = VehicleBooking::where('id', $bookingId)->pluck('release_at');
                $returnAt = VehicleBooking::where('id', $bookingId)->pluck('return_at');

                //processing data
                $shortDate = new CalculationController();

                $fromShort = substr($from, -8).' <strong>'.$shortDate->shortDate($from).'</strong>';
                $toShort = substr($to, -8).' <strong>'.$shortDate->shortDate($to).'</strong>';
                $createdAt = ($shortDate->timeAgo((new DateTime($createdAt))->format('Y-m-d H:i:s')));
                $releaseAtShort = $shortDate->shortDate($releaseAt);
                $returnAtShort = $shortDate->shortDate($returnAt);

                //Day status calculation
                $dayStatus = 0;
                $today = strtotime(\Carbon\Carbon::now('Pacific/Auckland')->format('Y-m-d'));

                if($type == 3) {
                    if(strtotime(((new DateTime($from))->format('Y-m-d'))) == $today) {
                        $dayStatus = 1;
                        $fromShort = substr($to, -8).' <strong style="color: #0059cc">Today</strong>';
                    }
                    else if(strtotime(((new DateTime($from))->format('Y-m-d'))) < $today) {
                        $dayStatus = 3;
                        $fromShort = substr($from, -8).' <strong style="color: #0059cc">'.($shortDate->timeAgo((new DateTime($from))->format('Y-m-d H:i:s'))).'</strong>';
                    }
                }
                else if($type == 2) {
                    if(strtotime(((new DateTime($to))->format('Y-m-d'))) == $today) {
                        $dayStatus = 1;
                        $fromShort = ($shortDate->timeAgo((new DateTime($from))->format('Y-m-d H:i:s')));
                        $toShort = substr($to, -8).' <strong style="color: #0059cc">Today</strong>';
                    }
                    else if(strtotime(((new DateTime($to))->format('Y-m-d'))) < $today) {
                        $dayStatus = 2;
                        $fromShort = ($shortDate->timeAgo((new DateTime($from))->format('Y-m-d H:i:s')));
                        $toShort = substr($to, -8).' <strong style="color: #7f0c03;">'.($shortDate->timeAgo((new DateTime($to))->format('Y-m-d H:i:s'))).'</strong>';
                    }
                }


                $ret[] = array(
                    'booking_id' => str_pad($bookingId, 5, '0', STR_PAD_LEFT),
                    'plate' => $plate,
                    'from' => $fromShort,
                    'to' => $toShort,
                    'fname' => $fName,
                    'lname' => $lName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'created_at' => $createdAt,
                    'day_status' => $dayStatus,
                    'release_at' => $releaseAtShort,
                    'return_at' => $returnAtShort

                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getInternalBookingList', $ex);

            return 0;
        }
    }

    public function getSingleVehicleBookingList()
    {
        $vehicleId = Input::get('vehicle_id');
        $ret = "";
        try {
            $bookingList = VehicleBooking::where('vehicle_id', $vehicleId)->where('status', '!=', 0)->where('status', '!=', 4)->lists('id');

            foreach ($bookingList as $bookingId) {
                $jobId = VehicleBooking::where('id', $bookingId)->pluck('job_id');
                $infoId = BookingJob::where('id', $jobId)->pluck('info_id');
                $fName = UserInfo::where('id', $infoId)->pluck('fname');
                $lName = UserInfo::where('id', $infoId)->pluck('lname');
                $title = $fName . ' ' . $lName;
                $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');
                $className = Vehicles::where('id', $vehicleId)->pluck('display_class');

                $ret[] = array(
                    'title' => $title,
                    'start' => $from,
                    'end' => $to,
                    'className' => $className
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getSingleVehicleBookingList', $ex);

            return 0;
        }
    }

    public function getSingleVehicleDetails()
    {
        $vehicleId = Input::get('vehicle_id');

        try {
            $plateNumber = Vehicles::where('id', $vehicleId)->pluck('plate');
            $modal = Vehicles::where('id', $vehicleId)->pluck('modal');
            $year = Vehicles::where('id', $vehicleId)->pluck('year');
            $rate = Vehicles::where('id', $vehicleId)->pluck('rate');
            $description = Vehicles::where('id', $vehicleId)->pluck('description');
            $status = Vehicles::where('id', $vehicleId)->pluck('status');

            $ret = array(
                'plate' => $plateNumber,
                'modal' => $modal,
                'year' => $year,
                'rate' => $rate,
                'description' => $description,
                'status' => $status
            );

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getSingleVehicleDetails', $ex);

            return 0;
        }
    }

    public function enableDisableVehicle() {
        $status = Input::get('status');
        $vehicleId = Input::get('vehicle_id');

        try {
            if(UserOperationAccess::where('user_id', Auth::user()->id)->where('operation_id', 3)->where('status', 1)->pluck('id') || User::where('id', Auth::user()->id)->pluck('roll') < 2) {
                Vehicles::where('id', $vehicleId)->update(array(
                    'status' => $status
                ));

                return 1;
            }
            else {
                $operation = sprintf("Try to %s a vehicle when access is not granted. Operation code = 3.", ($status == 1 ? 'Enable' : 'Disable'));
                $intrusion = new UserManageController();
                $intrusion->systemIntrusions('VehicleBookingController', 'enableDisableVehicle', $operation);
                return -1;
            }

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'enableDisableVehicle', $ex);

            return 0;
        }
    }

    public function getAllBookingList()
    {
        $ret = "";
        try {
            $bookingList = VehicleBooking::where('status','!=', 0)->where('status', '!=', 4)->lists('id');

            foreach ($bookingList as $bookingId) {
                $jobId = VehicleBooking::where('id', $bookingId)->pluck('job_id');
                $infoId = BookingJob::where('id', $jobId)->pluck('info_id');
                $fName = UserInfo::where('id', $infoId)->pluck('fname');
                $lName = UserInfo::where('id', $infoId)->pluck('lname');
                $plate = Vehicles::where('id', VehicleBooking::where('id', $bookingId)->pluck('vehicle_id'))->pluck('plate');
                $title = $fName.' '.$lName.' - '.$plate;
                $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');
                $className = Vehicles::where('id', VehicleBooking::where('id', $bookingId)->pluck('vehicle_id'))->pluck('display_class');

                $ret[] = array(
                    'title' => $title,
                    'start' => $from,
                    'end' => $to,
                    'className' => $className
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getAllBookingList', $ex);

            return 0;
        }
    }

    public function getVehiclePlateList() {
        try {
            $plateList = Vehicles::where('status', 1)->lists('id');
            $arr = null;

            foreach ($plateList as $plateId) {
                $arr[] = array(
                    'id' => $plateId,
                    'plate' => Vehicles::where('id', $plateId)->pluck('plate')
                );

            }

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getVehiclePlateList', $ex);

            return 0;
        }
    }

    public function getAvailableVehicles() {
        $from = Input::get('from');
        $to = Input::get('to');

        try {
            $from = DateTime::createFromFormat('d/m/Y H:i', $from);
            $from = date_format($from, 'Y-m-d H:i:s');

            $to = DateTime::createFromFormat('d/m/Y H:i', $to);
            $to = date_format($to, 'Y-m-d H:i:s');

            $vehicleList = Vehicles::where('status', 1)->lists('id');
            $bookedVehicleList1 = VehicleBooking::whereBetween('booking_from', array($from, $to))->where('status', '!=', 0)->lists('vehicle_id');
            $bookedVehicleList2 = VehicleBooking::whereBetween('booking_to', array($from, $to))->where('status', '!=', 0)->lists('vehicle_id');
            $bookedVehicleList = array_unique(array_merge($bookedVehicleList1, $bookedVehicleList2));

            $availableList = array_diff($vehicleList, $bookedVehicleList);

            $arr = null;

            foreach ($availableList as $plateId) {
                $arr[] = array(
                    'id' => $plateId,
                    'plate' => Vehicles::where('id', $plateId)->pluck('plate'),
                    'list1' => $bookedVehicleList1,
                    'list2' => $bookedVehicleList2,
                    'bookedlist' => $bookedVehicleList,
                    'available' => $availableList
                );

            }

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getAvailableVehicles', $ex);

            return 0;
        }
    }

    public function getBookingPrice() {
        $vehicleId = Input::get('vehicle_id');
        $from = Input::get('from');
        $to = Input::get('to');

        try {

            $from = DateTime::createFromFormat('d/m/Y H:i', $from);
            $from = date_format($from, 'Y-m-d H:i:s');

            $to = DateTime::createFromFormat('d/m/Y H:i', $to);
            $to = date_format($to, 'Y-m-d H:i:s');

            //get required data from models
            $dayRate = Vehicles::where('id', $vehicleId)->pluck('rate');

            //calculation

            $diff = strtotime($to) - strtotime($from);
            $hours = $diff / 3600;


            $days = $hours / 24;
            $restHours = $hours % 24;

            if ($restHours > 0) {
                $days++;
            }

            $days = (int)$days;
            $price = $days * $dayRate;

            $ret = array(
                'price' => $price,
                'days' => $days,
                'rate' => $dayRate
            );

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getBookingPrice', $ex);

            return 0;
        }
    }

    public function saveVehicleBooking() {
        $vehicleId = Input::get('vehicle_id');
        $from = Input::get('from');
        $to = Input::get('to');
        $fName = Input::get('fname');
        $lName = Input::get('lname');
        $email = Input::get('email');
        $telephone = Input::get('telephone');

        try {
            $from = DateTime::createFromFormat('d/m/Y H:i', $from);
            $from = date_format($from, 'Y-m-d H:i:s');

            $to = DateTime::createFromFormat('d/m/Y H:i', $to);
            $to = date_format($to, 'Y-m-d H:i:s');

            $plate = Vehicles::where('id', $vehicleId)->pluck('plate');

            $customer = new UserInfo();
            $customer->fname = $fName;
            $customer->lname = $lName;
            $customer->email = $email;
            $customer->telephone = $telephone;
            $customer->status = 1;
            $customer->save();
            $customerInfoId = $customer->id;

            $booking = new VehicleBooking();
            $booking->vehicle_id = $vehicleId;
            $booking->info_id = $customerInfoId;
            $booking->booking_from = $from;
            $booking->booking_to = $to;
            $booking->status = 3;
            $booking->created_at = (new DateTime("now", new DateTimeZone('Pacific/Auckland')))->format('Y-m-d H:i:s');
            $booking->save();
            $bookingId = $booking->id;

            if ($booking) {
                Mail::send('emails.vehicle-booking-customer', array(
                    'fName' => $fName,
                    'plate' => $plate,
                    'from' => $from,
                    'to' => $to),
                    function ($message) use ($fName, $lName, $email) {
                        $message->to($email, sprintf("%s %s", $fName, $lName))->subject('Giant Rentals vehicle booking confirmation!');
                    });
            }

            //create activity for the activity log
            $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname') . ' ' . UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');
            $activity = sprintf("%s Added new vehicle Booking under booking ID %s.", $userName, str_pad($bookingId, 5, '0', STR_PAD_LEFT));
            $addActivity = new UserManageController();
            $addActivity->addActivity($activity);

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'saveVehicleBooking', $ex);

            return 0;
        }
    }

    public function changeVehicleStatus() {
        $bookingId = Input::get("id");
        $status = Input::get('status');

        try {
            VehicleBooking::where('id', $bookingId)->update(array(
                'status' => $status
            ));

            if($status == 2) {
                VehicleBooking::where('id', $bookingId)->update(array(
                    'release_at' => \Carbon\Carbon::now('Pacific/Auckland')
                ));
            }
            else {
                VehicleBooking::where('id', $bookingId)->update(array(
                    'return_at' => \Carbon\Carbon::now('Pacific/Auckland')
                ));
            }

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'changeVehicleStatus', $ex);

            return 0;
        }
    }

    public function getBookingSliderInitiatingData() {
        $fromDay = Input::get('from_day');
        $fromMonth = Input::get('from_month');

        try {
            //create from date
            $fromDate = sprintf("%s-%s-%s 00:00:00", date("Y"), str_pad($fromMonth, 2, '0', STR_PAD_LEFT), str_pad($fromDay, 2, '0', STR_PAD_LEFT));
            $fromDate = Carbon::createFromFormat('Y-m-d H:i:s', $fromDate);
            $toDate = Carbon::createFromFormat('Y-m-d H:i:s', $fromDate);
            $toDate = $toDate->addDays(80);

            $plateList = Vehicles::where('status', 1)->lists('plate');

            $bookingList = VehicleBooking::where('status', '!=', 0)->where('status', '!=', 4)->whereBetween('booking_from', [$fromDate, $toDate])->whereBetween('booking_to', [$fromDate, $toDate])->orderBy('booking_from', 'asc')->lists('id');

            $ret = null;

            $ret[] = array(
                'plate' => $plateList
            );

            foreach ($bookingList as $bookingId) {
                $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');

                $jobId = VehicleBooking::where('id', $bookingId)->pluck('job_id');
                $infoId = BookingJob::where('id', $jobId)->pluck('info_id');

                $plate = Vehicles::where('id', VehicleBooking::where('id', $bookingId)->pluck('vehicle_id'))->pluck('plate');
                $name = sprintf('%s %s', UserInfo::where('id', $infoId)->pluck('fname'), UserInfo::where('id', $infoId)->pluck('lname'));
                $telephone = UserInfo::where('id', $infoId)->pluck('telephone');
                $color = Vehicles::where('id', VehicleBooking::where("id", $bookingId)->pluck('vehicle_id'))->pluck('display_color');
                $status = VehicleBooking::where('id', $bookingId)->pluck('status');
                $time = sprintf("%s to %s", substr_replace(substr($from, -8), "", -3), substr_replace(substr($to, -8), "", -3));

                //calculations
                $from = (new DateTime($from))->format('Y-m-d 00:00:00');
                $to = (new DateTime($to))->format('Y-m-d 01:00:00');
                $diff = strtotime($to) - strtotime($from);
                $hours = $diff / 3600;


                $days = $hours / 24;
                $restHours = $hours % 24;

                if ($restHours > 0) {
                    $days++;
                }

                $days = (int)$days;

                $ret[] = array(
                    'id' => $bookingId,
                    'booking_id' => str_pad($bookingId, 5, '0', STR_PAD_LEFT),
                    'from' => $from,
                    'to' => $to,
                    'plate' => $plate,
                    'days' => $days,
                    'name' => $name,
                    'telephone' => $telephone,
                    'color' => $color,
                    'status' => $status,
                    'time' => $time
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('VehicleBookingController', 'getBookingSliderInitiatingData', $ex);

            return 0;
        }
    }
    //endregion

}