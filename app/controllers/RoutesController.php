<?php 
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 29/08/2016 10:17.
 */
 
class RoutesController extends BaseController {

    public function getSecureRoute($pageId) {

        if(UserPageAccess::where('page_id', $pageId)->where('user_id', Auth::User()->id)->where('status', 1)->pluck('id') || Auth::user()->roll == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    public function getHome() {
        return View::make('frontend.home', array(
            'page' => 'Home'
        ));
    }

    public function getSignIn() {
        return View::make('frontend.signup', array(
            'page' => 'Sign In'
        ));
    }

    public function getFrontEndLogin() {
        return View::make('frontend.login');
    }

    public function getVehicle() {
        return View::make('frontend.vehicle', array(
            'page' => 'Vehicle'
        ));
    }

    public function getScaffold() {
        return View::make('frontend.scaffold', array(
            'page' => 'Scaffold'
        ));
    }

    public function getEvent() {
        return View::make('frontend.event', array(
            'page' => 'Event Gear'
        ));
    }

    public function getMyAccount() {
        return View::make('frontend.my-account', array(
            'page' => 'My Account'
        ));
    }

    public function getContactUs() {
        return View::make('frontend.contact-us', array(
            'page' => 'Contact Us'
        ));
    }

    public function getBookingInfo() {
        return View::make('frontend.book-confirm', array(
            'page' => 'Confirm Booking'
        ));
    }

    public function getBookConfirm(){
        return View::make('frontend.book-confirm', array(
            'page' => 'Confirm Booking'
        ));
    }

    public function getBookRecorded() {
        return View::make('frontend.book-recorded', array(
            'page' => 'Booking Recorded'
        ));
    }

    public function getRentVehicle() {
        return View::make('frontend.rent-vehicle');
    }

    public function getTest() {
        return View::make('testing');
    }

    public function getLogin() {
        return View::make('login');
    }

    public function getInternal() {
        return View::make('internal.default', array(
            'menuName' => 'Home',
            'pageName' => 'Home'
        ));
    }

    public function getInternalSystemException() {
        $access = $this->getSecureRoute(7);

        if($access == true) {
            return View::make('internal.system.exception', array(
                'menuName' => 'System',
                'pageName' => 'Exception'
            ));
        }
        else {
            return View::make('internal.others.404', array(
                'menuName' => null,
                'pageName' => null
            ));
        }

    }

    public function getInternalSystemIntrusion() {
        $access = $this->getSecureRoute(9);

        if($access == true) {
            return View::make('internal.system.intrusions', array(
                'menuName' => 'System',
                'pageName' => 'Intrusion'
            ));
        }
        else {
            return View::make('internal.others.404', array(
                'menuName' => null,
                'pageName' => null
            ));
        }

    }

    public function getUserManage() {
        $access = $this->getSecureRoute(5);

        if($access == true) {
            return View::make('internal.users.manage-users', array(
                'menuName' => 'Users',
                'pageName' => 'User Manage'
            ));
        }
        else {
            return View::make('internal.others.404', array(
                'menuName' => null,
                'pageName' => null
            ));
        }
    }

    public function getInternalVehicleManageBooking() {
        return View::make('internal.vehicles.manage-booking', array(
            'menuName' => 'Vehicles',
            'pageName' => 'Vehicle Bookings'
        ));
    }

    public function getManageVehicles() {
        return View::make('internal.vehicles.manage-vehicles', array(
            'menuName' => 'Vehicles',
            'pageName' => 'Vehicle Manage'
        ));
    }

    public function getScaffoldingBookings() {
        return View::make('internal.scaffolding.manage-booking', array(
            'menuName' => 'Scaffolding',
            'pageName' => 'Scaffolding Booking'
        ));
    }

    public function getScaffoldingMange() {
        return View::make('internal.scaffolding.manage-scaffolding', array(
            'menuName' => 'Scaffolding',
            'pageName' => 'Manage Scaffolding'
        ));
    }




    public function getInternalLogin() {
        return View::make('internal.login');
    }
}
