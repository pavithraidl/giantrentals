<?php

/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/08/2016 14:21.
 */
class EventsController extends BaseController
{

    //region Frontend
    public function getAddEvent() {
        $pickupd = Input::get('pickupd');
        $pickupt = Input::get('pickupt');
        $returnd = Input::get('returnd');
        $returnt = Input::get('returnt');
        $typeId = Input::get('type');
        $qty = Input::get('qty');
        $ret = null;

        try {
            $from = sprintf('%s %s:00:00', $pickupd, str_pad($pickupt, 2, '0', STR_PAD_LEFT));
            $to = sprintf('%s %s:00:00', $returnd, str_pad($returnt, 2, '0', STR_PAD_LEFT));

            $dayRate = EventItemDetails::where('id', $typeId)->pluck('day_rate');
            $gearName = EventItemDetails::where('id', $typeId)->pluck('name');

            $eventIdFrom = EventCart::whereBetween('booking_from', array($from, $to))->where('item_detail_id', $typeId)->where('status', '!=', 0)->lists('id');
            $eventIdTo = EventCart::whereBetween('booking_to', array($from, $to))->where('item_detail_id', $typeId)->where('status', '!=', 0)->lists('id');
            $eventIdDifference1 = EventCart::where('booking_from', '<', $from)->where('item_detail_id', $typeId)->where('booking_to', '>', $to)->lists('id');
            $eventIdDifference2 = EventCart::where('booking_from', '>', $from)->where('item_detail_id', $typeId)->where('booking_to', '<', $to)->lists('id');

            $eventIdList = array_merge($eventIdFrom, $eventIdTo, $eventIdDifference1, $eventIdDifference2);
            $eventIdList = array_unique($eventIdList);
            $bookedEventItemList[0] = null;
            $counter = 0;

            foreach ($eventIdList as $eventId) {
                $eventItemIds = EventCartItems::where('cart_id', $eventId)->where('status', '!=', 0)->lists('item_id');
                foreach ($eventItemIds as $itemId) {
                    $bookedEventItemList[$counter] = $itemId;
                    $counter++;
                }
            }

            $bookedEventItemList = array_unique($bookedEventItemList);

            $allEventList = EventItems::where('detail_id', $typeId)->where('status', 1)->lists('id');

            $availableList = array_diff($allEventList, $bookedEventItemList);

            if($availableList > $qty) {
                $gearList = array_slice($availableList, 0, $qty);

                //calculation
                $diff = strtotime($to) - strtotime($from);
                $hours = $diff / 3600;


                $days = $hours / 24;
                $restHours = $hours % 24;

                if ($restHours > 0) {
                    $days++;
                }

                $userId = null;
                if(Auth::check()) {
                    $userId = Auth::user()->id;
                }

                session_start();
                if(isset($_SESSION["session_id"])) {
                    $sessionId = $_SESSION["session_id"];
                    $jobId = BookingJob::where('session_id', $sessionId)->pluck('id');
                }
                else {
                    $sessionId = str_random(120);
                    $_SESSION["session_id"] = $sessionId;

                    $job = new BookingJob();
                    $job -> session_id = $sessionId;
                    $job -> user_id = $userId;
                    $job -> info_id = null;
                    $job -> licence = null;
                    $job -> status = 2;
                    $job -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $job -> save();
                    $jobId = $job -> id;
                }

                if(!$jobId) {
                    $job = new BookingJob();
                    $job -> session_id = $sessionId;
                    $job -> user_id = $userId;
                    $job -> info_id = null;
                    $job -> licence = null;
                    $job -> status = 2;
                    $job -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                    $job -> save();
                    $jobId = $job -> id;
                }

                $cart = new EventCart();
                $cart -> job_id = $jobId;
                $cart -> item_detail_id = $typeId;
                $cart -> booking_from = $from;
                $cart -> booking_to = $to;
                $cart -> status = 4;
                $cart -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $cart -> save();
                $cartId = $cart -> id;

                foreach ($gearList as $gearId) {
                    $cartItem = new EventCartItems();
                    $cartItem -> cart_id = $cartId;
                    $cartItem -> item_id = $gearId;
                    $cartItem -> status = 1;
                    $cartItem -> save();
                }

                //processing data
                $shortDate = new CalculationController();

                $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';


                $days = (int)$days;
                $total = $days * $dayRate*$qty;

                $ret = array(
                    'rate' => number_format($dayRate, 2, '.', ''),
                    'days' => $days,
                    'name' => $gearName,
                    'total' => number_format($total, 2, '.', '' ),
                    'total_int' => $total,
                    'cart_id' => $cartId,
                    'from' => $fromShort,
                    'to' => $toShort
                );

                return json_encode($ret);
            }
            else {
                return -1;
            }

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventsController', 'getAddEvent', $ex);

            return 0;
        }
    }

    public function removeGear() {
        $cartId = Input::get('cart_id');

        try {
            $gearList = EventCartItems::where('cart_id', $cartId)->lists('item_id');

            foreach ($gearList as $gearId) {
                EventCartItems::where('id', $gearId)->delete();
            }

            EventCart::where('id', $cartId)->delete();

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventsController', 'removeGear', $ex);

            return 0;
        }
    }

    public function getSessionList() {
        try {
            session_start();
            if(isset($_SESSION["session_id"])) {
                $sessionId = $_SESSION['session_id'];
                $jobId = BookingJob::where('session_id', $sessionId)->pluck('id');
                $gearCartList = EventCart::where('job_id', $jobId)->lists('id');
                $ret = null;

                foreach ($gearCartList as $cartId) {
                    $qty = EventCartItems::where('cart_id', $cartId)->count('id');

                    $detailId = EventCart::where('id', $cartId)->pluck('item_detail_id');
                    $gearName = EventItemDetails::where('id', $detailId)->pluck('name');
                    $dayRate = EventItemDetails::where('id', $detailId)->pluck('day_rate');

                    $from = EventCart::where('id', $cartId)->pluck('booking_from');
                    $to = EventCart::where('id', $cartId)->pluck('booking_to');

                    //processing data
                    $shortDate = new CalculationController();

                    $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                    $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';

                    //calculation
                    $diff = strtotime($to) - strtotime($from);
                    $hours = $diff / 3600;


                    $days = $hours / 24;
                    $restHours = $hours % 24;

                    if ($restHours > 0) {
                        $days++;
                    }

                    $days = (int)$days;
                    $total = $days * $dayRate * $qty;

                    $ret[] = array(
                        'rate' => number_format($dayRate, 2, '.', ''),
                        'days' => $days,
                        'name' => $gearName,
                        'qty' => $qty,
                        'total' => number_format($total, 2, '.', '' ),
                        'total_int' => $total,
                        'cart_id' => $cartId,
                        'from' => $fromShort,
                        'to' => $toShort
                    );
                }

                return json_encode($ret);
            }


        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventsController', 'getSessionList', $ex);

            return 0;
        }
    }

    public function addCartItems()
    {
        $itemDetailId = Input::get('item_detail_id');
        $quantity = Input::get('quantity');
        $from = Input::get('from');
        $to = Input::get('to');

        $availableQuantity = $this->checkAvailability($itemDetailId, $from, $to);

        if ($availableQuantity > $quantity) {
            $sessionId = Session::get('session_id');
            $bookingId = null;
            $categoryId = EventItemDetails::where('id', $itemDetailId)->pluck('category_id');

            if ($sessionId == '') {
                $sessionId = str_random(60);
                Session::put('session_id', $sessionId);

                $booking = new EventBooking();
                $booking->session_id = $sessionId;
                $booking->status = 4;
                $booking->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $booking->save();
                $bookingId = $booking->id;
            } else if ($bookingId == '' || $bookingId == null) {
                $booking = new EventBooking();
                $booking->session_id = $sessionId;
                $booking->status = 4;
                $booking->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $booking->save();
                $bookingId = $booking->id;
            } else {
                $bookingId = EventBooking::where('session_id', $sessionId)->pluck('id');
            }

            $eventItemList = EventItems::where('detail_id', $itemDetailId)->where('status', '!=', 0)->orderBy('status', 'desc')->lists('id');

            for ($counter = 0; $quantity > $counter; $counter++) {
                $cart = new EventCart();
                $cart->booking_id = $bookingId;
                $cart->event_category_id = $categoryId;
                $cart->item_detail_id = $itemDetailId;
                $cart->event_item_id = $eventItemList[$counter];
                $cart->booking_from = $from;
                $cart->booking_to = $to;
                $cart->status = 3;
                $cart->release_at = null;
                $cart->return_at = null;
                $cart->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $cart->save();

                EventItems::where('id', $eventItemList[$counter])->update(array(
                    'status' => 2
                ));
            }

            return 1;

        } else {
            return json_encode('No Enough Stock Available!');
        }
    }

    public function bookEvents()
    {
        $fName = Input::get('first_name');
        $lName = Input::get('last_name');
        $email = Input::get('email');
        $telephone = Input::get('telephone');


        if (Auth::check()) {
            $customerInfoId = UserInfo::where('user_id', Auth::user()->id)->where('status', 1)->pluck('id');
        } else {
            $customer = new UserInfo();
            $customer->fname = $fName;
            $customer->lname = $lName;
            $customer->email = $email;
            $customer->telephone = $telephone;
            $customer->status = 1;
            $customer->save();
            $customerInfoId = $customer->id;
        }

        $sessionId = Session::get('session_id');

        if ($sessionId == '' || $sessionId == null) {
            return json_encode('Session is expired!');
        } else {
            $bookingId = EventBooking::where('session_id', $sessionId)->pluck('id');

            $booking = EventBooking::where('id', $bookingId)->update(array(
                'customer_info_id' => $customerInfoId,
                'status' => 3,
                'created_at' => \Carbon\Carbon::now('Pacific/Auckland')
            ));
        }

        if ($booking) {
            Mail::send('emails.events-booking-customer', array(
                'fName' => $fName),
                function ($message) use ($fName, $lName, $email) {
                    $message->to($email, sprintf("%s %s", $fName, $lName))->subject('Giant Rentals vehicle booking confirmation!');
                });

            Mail::send('emails.events-booking-admin', array(
                'fName' => $fName,
                'telephone' => $telephone,
                'email' => $email),
                function ($message) use ($fName, $lName, $email) {
                    $message->to('pavithraisuru@gmail.com', sprintf("%s %s", $fName, $lName))->subject('Giant Rentals new vehicle booking');
                });
        }

        return 1;
    }
    //endregion

    //region Backend
    public function addEventCategory()
    { //tested and ok
        $name = Input::get('name');
        $description = Input::get('description');

        try {
            $newCategory = new EventCategory();
            $newCategory->name = $name;
            $newCategory->description = $description;
            $newCategory->status = 1;
            $newCategory->created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $newCategory->save();

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventController', 'addEventCategory', $ex);

            return 0;
        }
    }

    public function addEventItems()
    { //tested and ok
        $name = Input::get('name');
        $categoryId = Input::get('category_id');
        $dayRate = Input::get('day_rate');
        $quantity = Input::get('quantity');

        try {
            $newDetail = new EventItemDetails();
            $newDetail->name = $name;
            $newDetail->category_id = $categoryId;
            $newDetail->day_rate = $dayRate;
            $newDetail->status = 1;
            $newDetail->created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $newDetail->save();
            $detailId = $newDetail->id;

            for ($counter = 0; $counter < $quantity; $counter++) {
                $newItem = new EventItems();
                $newItem->detail_id = $detailId;
                $newItem->status = 1;
                $newItem->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $newItem->save();
            }

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventController', 'addEventDetail', $ex);

            return 0;
        }
    }

    public function updateItemQuantity()
    { //tested and ok
        $detailId = Input::get('detail_id');
        $quantity = Input::get('quantity');

        try {
            for ($counter = 0; $quantity > $counter; $counter++) {
                $updateItems = new EventItems();
                $updateItems->detail_id = $detailId;
                $updateItems->status = 1;
                $updateItems->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $updateItems->save();
            }

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventController', 'addEventDetail', $ex);

            return 0;
        }
    }

    public function getPendingBookingList()
    { //tested and ok

        try {
            $bookingList = EventBooking::where('status', 3)->lists('id');
            $cart[] = null;
            $ret[] = null;

            foreach ($bookingList as $bookingId) {
                $customerInfoId = EventBooking::where('id', $bookingId)->pluck('customer_info_id');
                $userInfo = UserInfo::where('id', $customerInfoId);
                $fName = $userInfo->pluck('fname');
                $lName = $userInfo->pluck('lname');
                $email = $userInfo->pluck('email');
                $telephone = $userInfo->pluck('telephone');
                $created_at = EventBooking::where('id', $bookingId)->pluck('created_at');

                $cartList = EventCart::where('booking_id', $bookingId)->where('status', 3)->lists('id');


                foreach ($cartList as $cartId) {
                    $eventCart = EventCart::where('id', $cartId);
                    $detailId = $eventCart->pluck('item_detail_id');
                    $from = $eventCart->pluck('booking_from');
                    $to = $eventCart->pluck('booking_to');
                    $name = EventItemDetails::where('id', $detailId)->pluck('name');

                    $cart[] = array(
                        'detail_id' => $detailId,
                        'item_name' => $name,
                        'booking_from' => $from,
                        'booking_to' => $to
                    );
                }

                $ret[] = array(
                    'f_name' => $fName,
                    'l_name' => $lName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'created_at' => $created_at,
                    'cart' => $cart
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventsController', 'getPendingBookingList', $ex);

            return 0;
        }
    }

    public function releaseEventItems()
    { //tested and ok
        $cartIdList = Input::get('cart_ids'); //need to be an array. release item cart ids

        try {
            foreach ($cartIdList as $cartId) {
                EventCart::where('id', $cartId)->update(array(
                    'status' => 2,
                    'release_at' => \Carbon\Carbon::now('Pacific/Auckland')
                ));
            }

            EventBooking::where('id', EventCart::where('id', $cartIdList[0])->pluck('booking_id'))->update(array(
                'status' => 2
            ));

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventsController', 'releaseEventItems', $ex);

            return 0;
        }
    }

    public function getCurrentBookingList()
    { //tested and ok

        try {
            $bookingList = EventBooking::where('status', 2)->lists('id');
            $cart[] = null;
            $ret[] = null;

            foreach ($bookingList as $bookingId) {
                $customerInfoId = EventBooking::where('id', $bookingId)->pluck('customer_info_id');
                $fName = UserInfo::where('id', $customerInfoId)->pluck('fname');
                $lName = UserInfo::where('id', $customerInfoId)->pluck('lname');
                $email = UserInfo::where('id', $customerInfoId)->pluck('email');
                $telephone = UserInfo::where('id', $customerInfoId)->pluck('telephone');
                $created_at = EventBooking::where('id', $bookingId)->pluck('created_at');

                $cartList = EventCart::where('booking_id', $bookingId)->where('status', 2)->lists('id');


                foreach ($cartList as $cartId) {
                    $detailId = EventCart::where('id', $cartId)->pluck('item_detail_id');
                    $name = EventItemDetails::where('id', $detailId)->pluck('name');
                    $from = EventCart::where('id', $cartId)->pluck('booking_from');
                    $to = EventCart::where('id', $cartId)->pluck('booking_to');


                    $cart[] = array(
                        'detail_id' => $detailId,
                        'item_name' => $name,
                        'booking_from' => $from,
                        'booking_to' => $to
                    );
                }

                $ret[] = array(
                    'f_name' => $fName,
                    'l_name' => $lName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'created_at' => $created_at,
                    'cart' => $cart
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventController', 'geCurrentBookingList', $ex);

            return 0;
        }

    }

    public function returnEventItems()
    { //tested and ok
        $cartIdList = Input::get('cart_ids'); //need to be an array. returned item cart ids

        try {
            foreach ($cartIdList as $cartId) {
                EventCart::where('id', $cartId)->update(array(
                    'status' => 1,
                    'return_at' => \Carbon\Carbon::now('Pacific/Auckland')
                ));

                $eventItemId = EventCart::where('id', $cartId)->pluck('event_item_id');
                EventItems::where('id', $eventItemId)->update(array(
                    'status' => 1
                ));
            }

            EventBooking::where('id', EventCart::where('id', $cartIdList[0])->pluck('booking_id'))->update(array(
                'status' => 1
            ));

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventsController', 'returnEventItems', $ex);

            return 0;
        }
    }

    public function getBookingHistory()
    { //tested and ok

        try {
            $bookingList = EventBooking::where('status', 1)->lists('id');
            $cart[] = null;
            $ret[] = null;

            foreach ($bookingList as $bookingId) {
                $customerInfoId = EventBooking::where('id', $bookingId)->pluck('customer_info_id');
                $fName = UserInfo::where('id', $customerInfoId)->pluck('fname');
                $lName = UserInfo::where('id', $customerInfoId)->pluck('lname');
                $email = UserInfo::where('id', $customerInfoId)->pluck('email');
                $telephone = UserInfo::where('id', $customerInfoId)->pluck('telephone');
                $created_at = EventBooking::where('id', $bookingId)->pluck('created_at');

                $cartList = EventCart::where('booking_id', $bookingId)->where('status', 1)->lists('id');


                foreach ($cartList as $cartId) {
                    $detailId = EventCart::where('id', $cartId)->pluck('item_detail_id');
                    $name = EventItemDetails::where('id', $detailId)->pluck('name');
                    $from = EventCart::where('id', $cartId)->pluck('booking_from');
                    $to = EventCart::where('id', $cartId)->pluck('booking_to');


                    $cart[] = array(
                        'detail_id' => $detailId,
                        'item_name' => $name,
                        'booking_from' => $from,
                        'booking_to' => $to
                    );
                }

                $ret[] = array(
                    'f_name' => $fName,
                    'l_name' => $lName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'created_at' => $created_at,
                    'cart' => $cart
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventsController', 'getBookingHistory', $ex);

            return 0;
        }

    }

    public function getSingleBookingElement()
    { //tested and ok

        $bookingListType = Input::get('booking_list_type'); //require to send the status of the booking list

        try {
            $bookingList = EventBooking::where('status', $bookingListType)->lists('id');
            $cart[] = null;
            $ret[] = null;

            foreach ($bookingList as $bookingId) {
                $customerInfoId = EventBooking::where('id', $bookingId)->pluck('customer_info_id');
                $fName = UserInfo::where('id', $customerInfoId)->pluck('fname');
                $lName = UserInfo::where('id', $customerInfoId)->pluck('lname');
                $email = UserInfo::where('id', $customerInfoId)->pluck('email');
                $telephone = UserInfo::where('id', $customerInfoId)->pluck('telephone');
                $created_at = EventBooking::where('id', $bookingId)->pluck('created_at');

                $cartList = EventCart::where('booking_id', $bookingId)->where('status', $bookingListType)->lists('id');


                foreach ($cartList as $cartId) {
                    $detailId = EventCart::where('id', $cartId)->pluck('item_detail_id');
                    $name = EventItemDetails::where('id', $detailId)->pluck('name');
                    $from = EventCart::where('id', $cartId)->pluck('booking_from');
                    $to = EventCart::where('id', $cartId)->pluck('booking_to');


                    $cart[] = array(
                        'cart_id' => $cartId,
                        'detail_id' => $detailId,
                        'item_name' => $name,
                        'booking_from' => $from,
                        'booking_to' => $to
                    );
                }

                $ret[] = array(
                    'booking_id' => $bookingId,
                    'f_name' => $fName,
                    'l_name' => $lName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'created_at' => $created_at,
                    'cart' => $cart
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventController', 'getSingleBookingElement', $ex);

            return 0;
        }
    }

    public function getBookingTitles()
    {
        $bookingListType = Input::get('booking_list_type'); //require to send the status of the booking list

        try {
            $bookingList = EventCart::where('status', $bookingListType)->lists('booking_id');
            $bookingList = array_unique($bookingList);
            $ret[] = null;

            foreach ($bookingList as $bookingId) {
                $customerInfoId = EventBooking::where('id', $bookingId)->pluck('customer_info_id');
                $user_info = UserInfo::where('id', $customerInfoId);
                $fName = UserInfo::where('id', $customerInfoId)->pluck('fname');
                $lName = UserInfo::where('id', $customerInfoId)->pluck('lname');
                $email = UserInfo::where('id', $customerInfoId)->pluck('email');
                $telephone = UserInfo::where('id', $customerInfoId)->pluck('telephone');
                $created_at = EventBooking::where('id', $bookingId)->pluck('created_at');

                $cartListCount = EventCart::where('booking_id', $bookingId)->where('status', $bookingListType)->count('id');


                $ret[] = array(
                    'booking_id' => $bookingId,
                    'f_name' => $fName,
                    'l_name' => $lName,
                    'email' => $email,
                    'telephone' => $telephone,
                    'created_at' => $created_at,
                    'cart_list_count' => $cartListCount
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventController', 'getBookingTitles', $ex);

            return 0;
        }
    }

    public function getSingleBookingCartList()
    {
        $bookingListType = Input::get('booking_list_type'); //require to send the status of the booking list
        $bookingId = Input::get('booking_id');

        try {
            $cartList = EventCart::where('booking_id', $bookingId)->where('status', $bookingListType)->lists('id');
            $ret[] = null;

            foreach ($cartList as $cartId) {
                $categoryId = EventCart::where('id', $cartId)->pluck('event_category_id');
                $categoryName = EventCategory::where('id', $categoryId)->pluck('name');
                $itemDetailId = EventCart::where('id', $cartId)->pluck('item_detail_id');
                $itemName = EventItemDetails::where('id', $itemDetailId)->pluck('name');
                $from = EventCart::where('id', $cartId)->pluck('booking_from');
                $to = EventCart::where('id', $cartId)->pluck('booking_to');


                $ret[] = array(
                    'cart_id' => $cartId,
                    'category_id' => $categoryId,
                    'category_name' => $categoryName,
                    'item_detail_id' => $itemDetailId,
                    'item_name' => $itemName,
                    'from' => $from,
                    'to' => $to
                );
            }

            return json_encode($ret);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception->saveExceptionDetails('EventsController', 'getSingleBookingCartList', $ex);

            return 0;
        }
    }
    //endregion

}
