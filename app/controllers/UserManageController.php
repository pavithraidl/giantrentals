<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 6/09/16
 * Time: 11:03 AM
 */

class UserManageController extends BaseController {

    public function getUserList() {
        //send the basic user details for display in the user list
        try{
            $userRollId = Auth::user()->roll;
            $userList = User::where('status', '!=', 0)->where('roll','>=',$userRollId)->lists('id');
            $arr = [];

            foreach($userList as $userId) {
                //get values from database
                $infoId = User::where('id', $userId)->pluck('info_id');
                if(DB::table('user_info')->where('id', $userId)->pluck('status') != 0){
                    $fullName = sprintf("%s %s", DB::table('user_info')->where('id', $infoId)->pluck('fname'), DB::table('user_info')->where('id', $infoId)->pluck('lname'));
                    $title = DB::table('user_rolls')->where('id', DB::table('users')->where('id', $userId)->pluck('roll'))->pluck('name');
                    $active = DB::table('users')->where('id', $userId)->pluck('active');
                    $status = 1;

                    //setup the user avatar if user account is active
                    $userAvatar = UserInfo::where('id', $infoId)->pluck('avatar');
                    if ($userAvatar == 0) {
                        $gender = DB::table('user_info')->where('id', $infoId)->pluck('gender');
                        if ($gender == null) {
                            $avatarId = "default";
                        }
                        else {
                            $avatarId = sprintf("default-%s", $gender);
                        }
                    }
                }
                else{
                    $fullName = DB::table('user_info')->where('id', $userId)->pluck('fname');
                    $title = DB::table('user_rolls')->where('id', DB::table('users')->where('id', $userId)->pluck('roll'))->pluck('name');
                    $avatarId = 'default'; //setup the default image for the user avatar
                    $status = 0;
                }

                //make array to send data to the front end
                $arr[]= array(
                    "userid" => $userId,
                    "name" => $fullName,
                    "title" => $title,
                    "active" => $active,
                    "avatarid" => $avatarId,
                    "status" => $status
                );
            }

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'getUserList', $ex);
            return 0;
        }
    }

    public function getUserDisplayInfo() {
        //get the details of a single user to display when select from the user list
        $userId = Input::get('userid');

        try{
            //get values form database
            $infoId = User::where('id', $userId)->pluck('info_id');

            //setup the user avatar
            $userAvatar = UserInfo::where('id', $infoId)->pluck('avatar');
            if ($userAvatar == 0) {
                $gender = DB::table('user_info')->where('id', $infoId)->pluck('gender');
                if ($gender == null) {
                    $userAvatar = "default";
                }
                else {
                    $userAvatar = sprintf("default-%s", $gender);
                }
            }

            //get the other required data from the database to display under user
            $firstName = DB::table('user_info')->where('id', $infoId)->pluck('fname');
            $lastName = DB::table('user_info')->where('id', $infoId)->pluck('lname');
            $email = DB::table('user_info')->where('id', $infoId)->pluck('email');
            $telephone = DB::table('user_info')->where('id', $infoId)->pluck('telephone');
            $roll = DB::table('user_rolls')->where('id', DB::table('users')->where('id', $userId)->pluck('roll'))->pluck('name');
            $active = DB::table('users')->where('id', $userId)->pluck('active');
            $timeSince = new CalculationController();
            $createdAt = DB::table('users')->where('id',$userId)->pluck('created_at');
            $joinAt = ($timeSince->timeAgo((new DateTime($createdAt))->format('Y-m-d H:i:s')));
            $rollId = DB::table('users')->where('id', $userId)->pluck('roll');

            //make the array to send data to the front-end
            $arr = array(
                'avatar' => $userAvatar,
                'firstname' => $firstName,
                'lastname' => $lastName,
                'roll' => $roll,
                'rollid' => $rollId,
                'active' => $active,
                'email' => $email,
                'telephone' => $telephone,
                'createdat' => $createdAt,
                'joinat' => $joinAt
            );

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'getUserDisplayInfo', $ex);
            return 0;
        }
    }

    public function getUsersAccess() {
        //get the list of user access pages and operations to set up the front end access tab
        $userId = Input::get('userid');
        $arr = [];

        try{
            //get the required data from the database to send the front-end
            $allowedPages = UserPageAccess::where('user_id', $userId)->where('status', 1)->lists('page_id');
            $allPages = UserPageAccess::where('user_id', Auth::user()->id)->where('status', 1)->lists('page_id');
            $operationList = UserOperationAccess::where('user_id', $userId)->where('status', 1)->lists('operation_id');
            $allOperations = SystemOperations::where('status', 1)->lists('id');

            //crete an array to send data to the front-end
            $arr = array(
                'pages' => $allowedPages,
                'allpages' => $allPages,
                'operations' => $operationList,
                'alloperations' => $allOperations,
            );

            return json_encode($arr);

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'getUsersAccess', $ex);
            return 0;
        }
    }

    //manage the user's operation access
    public function allowDenyOperationAccess() {
        $userId = Input::get('userid');
        $status = Input::get('status');
        $opId = Input::get('opid');

        try {
            $accessId = UserOperationAccess::where('user_id', $userId)->where('operation_id', $opId)->pluck('id');
            if($accessId != null) {
                UserOperationAccess::where('id', $accessId)
                    ->update(array
                        (
                            'status' => $status
                        )
                    );
            }
            else {
                UserOperationAccess::insert(
                    array(
                        'operation_id' => $opId,
                        'user_id' => $userId,
                        'status' => $status,
                        'created_by' => Auth::user()->id,
                        'created_at' => (new DateTime("now", new DateTimeZone('Asia/Colombo')))->format('Y-m-d H:i:s')
                    )
                );
            }

            $actionUserName = sprintf("%s %s", UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname'), UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname'));
            $action = ($status == 1 ? 'Allowed' : 'Denied');
            $operation = SystemOperations::where('id', $opId)->pluck('description');
            $UserName = sprintf("%s %s", UserInfo::where('id', User::where('id', $userId)->pluck('info_id'))->pluck('fname'), UserInfo::where('id', User::where('id', $userId)->pluck('info_id'))->pluck('lname'));

            $activity = (sprintf("%s %s %s's access for %s", $actionUserName, $action, $UserName, $operation));
            $this->addActivity($activity);

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'allowDenyOperationAccess', $ex);
            return 0;
        }
    }

    public function allowDenySystemPages() {
        //change the user access status depending on the check boxes in the front-end access tab list
        $userId = Input::get('userid');
        $status = Input::get('status');
        $pageId = Input::get('pageid');

        try {
            //update the relevant access status
            $accessId = UserPageAccess::where('user_id', $userId)->where('page_id', $pageId)->pluck('id');
            if($accessId != null) {
                UserPageAccess::where('id', $accessId)
                    ->update(array
                        (
                            'status' => $status
                        )
                    );
            }
            else {
                //for the first time when granting the access it creates a new row in the UserPageAccess model
                UserPageAccess::insert(
                    array(
                        'page_id' => $pageId,
                        'user_id' => $userId,
                        'status' => $status,
                        'created_at' => \Carbon\Carbon::now('Pacific/Auckland'),
                        'created_by' => Auth::user()->id
                    )
                );
            }

            //create activity for the activity log
            $objectName = UserInfo::where('id', User::where('id', $userId)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', $userId)->pluck('info_id'))->pluck('lname');
            $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');
            $pageName = SystemPageList::where('id', $pageId)->pluck('name');
            if($status == 1) {
                $status = 'Allowed';
            }
            else {
                $status = 'Denied';
            }

            $activity = sprintf("%s %s %s to access in %s page", $userName, $status, $objectName, $pageName);
            $this->addActivity($activity);

            return 1;
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'allowDenySystemPages', $ex);
            return 0;
        }
    }

    public function activeDeactiveUser() {
        //change the status of a user to active or deactive
        $userId = Input::get('userid');
        $status = Input::get('status');

        try{
            //if account is an admin one, function doesn't allow to change admin status
            $userRoll = User::where('id', $userId)->pluck('roll');
            if($userRoll != 1) {
                //update the active deactive status
                User::where('id', $userId)
                    ->update(['active' => $status]);

                //create activity for the activity log
                $objectName = UserInfo::where('id', User::where('id', $userId)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', $userId)->pluck('info_id'))->pluck('lname');
                $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');
                if($status == 1) {
                    $status = 'Activate';
                }
                else {
                    $status = 'Deactivate';
                }

                $activity = sprintf("%s %s %s's User Account.", $userName, $status, $objectName);
                $this->addActivity($activity);

                return 1;
            }
            else {
                return -1;
            }



        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'allowDenySystemPages', $ex);
            return 0;
        }
    }

    public function deleteUser() {
        //delete a particular user. For reuse the email if required when deleting function changes the email by adding random characters to the front
        $userId = Input::get('userid');

        try{
            $userRoll = User::where('id', $userId)->pluck('roll');
            if($userRoll != 1) {
                $email = User::where('id', $userId)->pluck('email');
                $email = sprintf("temp%s-%s", str_random(5), $email);
                User::where('id', $userId)
                    ->update(['status' => 0, 'email'=>$email]);

                //create activity for the activity log
                $objectName = UserInfo::where('id', User::where('id', $userId)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', $userId)->pluck('info_id'))->pluck('lname');
                $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');

                $activity = sprintf("%s Deleted %s's User Account.", $userName, $objectName);
                $this->addActivity($activity);

                return 1;
            }
            else {
                return -1;
            }
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'allowDenySystemPages', $ex);
            return 0;
        }
    }

    public function addNewUser() {
        //add new user to the system
        $email = Input::get('email');
        $fname = Input::get('fname');
        $lname = Input::get('lname');

        try{
            if(DB::table('users')->where('email', $email)->pluck('id')){
                return -1;
            }
            else{
                //generate the activation code
                $code = str_random(60);

                //insert id to user avatar
                $info = new UserInfo();
                $info -> fname = $fname;
                $info -> lname = $lname;
                $info -> email = $email;
                $info -> save();

                $infoId = $info -> id;

                //setup the user account
                $user = new User;
                $user->email = $email;
                $user->active = 0;
                $user->code = $code;
                $user->roll = 2;
                $user->status = 2;
                $user->info_id = $infoId;
                $user->created_at = \Carbon\Carbon::now('Pacific/Auckland');
                $user->updated_at = \Carbon\Carbon::now('Pacific/Auckland');
                $user->save();


                if ($user) {
                    Mail::send('emails.internal-welcome-email', array(
                        'link' => URL::route('account-activate', $code),
                        'username' => sprintf("%s %s", $fname, $lname),
                        ), function ($message) use ($user, $email, $fname) {
                        $message->to($email, $fname)->subject('Activate your account');
                    });
                }

                //create activity for the activity log
                $objectName = UserInfo::where('id', $infoId)->pluck('fname').' '.UserInfo::where('id', $infoId)->pluck('lname');
                $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');

                $activity = sprintf("%s Send an account request to %s.", $userName, $objectName);
                $this->addActivity($activity);

                return 1;
            }
        }  catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'addNewUser', $ex);
            return 0;
        }
    }

    public function resendActivationEmail() {
        //if the activation email is not received, this functions allows to resend the activation email
        $userId = Input::get('userid');

        try{
            $infoId = User::where('id', $userId)->pluck('info_id');
            $fname = UserInfo::where('id', $infoId)->pluck('fname');
            $lname = UserInfo::where('id', $infoId)->pluck('lname');
            $code = User::where('id', $userId)->pluck('code');
            $email = User::where('id', $userId)->pluck('email');
            //send email
            Mail::send('emails.internal-welcome-email', array(
                'link' => URL::route('account-activate', $code),
                'username' => sprintf("%s %s", $fname, $lname),
            ), function ($message) use ($email, $fname) {
                $message->to($email, $fname)->subject('Activate your account');
            });

            //create activity for the activity log
            $objectName = $fname.' '.$lname;
            $userName = UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('fname').' '.UserInfo::where('id', User::where('id', Auth::user()->id)->pluck('info_id'))->pluck('lname');

            $activity = sprintf("%s Resend the Activation Email to %s.", $userName, $objectName);
            $this->addActivity($activity);

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'resendActivationEmail', $ex);
            return 0;
        }
    }

    public function getActivityList() {
        $paginate = Input::get('paginate');
        $userId = Input::get('userid');
        $arr = [];

        try {
            $activityList = UserActivityLog::where('user_id', $userId)->orderBy('created_at', 'desc')->forPage($paginate, 50)->lists('id');

            if(sizeof($activityList) > 0){
                //creating objects
                $timeSince = new CalculationController();

                foreach ($activityList as $activityId) {
                    //get values form database
                    $activity = UserActivityLog::where('id', $activityId)->pluck('activity');
                    $createdAt = UserActivityLog::where('id', $activityId)->pluck('created_at');
                    $createdAt = (new DateTime($createdAt))->format('Y-m-d H:i:s');

                    //calculations
                    $setCreatedAt = ($timeSince->timeAgo((new DateTime($createdAt))->format('Y-m-d H:i:s')));

                    //return json
                    $arr[] = array(
                        'activity' => $activity,
                        'timeago' => $setCreatedAt,
                        'createdat' => $createdAt
                    );
                }
                return json_encode($arr);
            }
            else{
                return -1;
            }
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'getActivityList', $ex);
            return 0;
        }
    }

    public function addActivity($activity) {
        //records the users activities in the database
        try {
            $newActivity = new UserActivityLog();
            $newActivity -> activity = $activity;
            $newActivity -> user_id = Auth::user()->id;
            $newActivity -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $newActivity -> save();

            return 1;
        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'addActivity', $ex);
            return 0;
        }
    }

    public function systemIntrusions($controller, $func, $msg) {
        try {
            $intrusion = new SystemIntrusions();
            $intrusion -> controller = $controller;
            $intrusion -> func = $func;
            $intrusion -> msg = $msg;
            $intrusion -> user_id = Auth::user()->id;
            $intrusion -> status = 2;
            $intrusion -> created_at = \Carbon\Carbon::now('Pacific/Auckland');
            $intrusion -> save();

            return 1;

        } catch (Exception $ex) {
            $exception = new ErrorController();
            $exception -> saveExceptionDetails('UserManageController', 'systemIntrusions', $ex);
            return 0;
        }
    }
}
