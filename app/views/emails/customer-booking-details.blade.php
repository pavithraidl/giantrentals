<?php
/************************| customer-booking-details.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 12/11/16 09:38.
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 18/10/16
 * Time: 11:54
 */
?>

<!-- / Full width container -->
<style type="text/css">
    .bs-callout {
        padding: 20px;
        margin: 20px 0;
        border: 1px solid #eee;
        border-left-width: 5px;
        border-radius: 3px;
    }
    .bs-callout-top {
        padding: 20px;
        margin: 20px 0;
        border: 1px solid #eee;
        border-top-width: 5px;
        border-radius: 3px;
    }
    .bs-callout h4 {
        margin-top: 0;
        margin-bottom: 5px;
    }
    .bs-callout p:last-child {
        margin-bottom: 0;
    }
    .bs-callout code {
        border-radius: 3px;
    }
    .bs-callout + .bs-callout {
        margin-top: -5px;
    }
    .bs-callout-default {
        border-left-color: #777;
    }
    .bs-callout-top-default {
        border-top-color: #777;
    }
    .bs-callout-default h4 {
        color: #777;
    }
    .bs-callout-primary {
        border-left-color: #428bca;
    }
    .bs-callout-top-primary {
        border-top-color: #428bca;
    }
    .bs-callout-primary h4 {
        color: #428bca;
    }
    .bs-callout-success {
        border-left-color: #f96421;
    }
    .bs-callout-top-success {
        border-top-color: #f96421;
    }
    .bs-callout-success h4 {
        color: #f96421;
    }
    .bs-callout-danger {
        border-left-color: #d9534f;
    }
    .bs-callout-top-danger {
        border-top-color: #d9534f;
    }
    .bs-callout-danger h4 {
        color: #d9534f;
    }
    .bs-callout-warning {
        border-left-color: #f0ad4e;
    }
    .bs-callout-top-warning {
        border-top-color: #f0ad4e;
    }
    .bs-callout-warning h4 {
        color: #f0ad4e;
    }
    .bs-callout-info {
        border-left-color: #5bc0de;
    }
    .bs-callout-top-info {
        border-top-color: #5bc0de;
    }
    .bs-callout-info h4 {
        color: #5bc0de;
    }
    .booking-list-container {
        -webkit-box-shadow: 1px 2px 15px -2px rgba(0, 0, 0, 0.51);
        -moz-box-shadow: 1px 2px 15px -2px rgba(0, 0, 0, 0.51);
        box-shadow: 1px 2px 15px -2px rgba(0, 0, 0, 0.51);
        padding: 20px;
        border-radius: 10px;
        margin-top: 20px;
        margin-bottom: 10px;
    }
</style>
<table class="full-width-container" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#eeeeee" style="width: 100%; height: 100%; padding: 30px 0 30px 0; min-width: 500px; overflow: auto;">
    <tr>
        <td align="center" valign="top">
            <!-- / 700px container -->
            <table class="container" border="0" cellpadding="0" cellspacing="0" width="80%" bgcolor="#ffffff" style="width: 80%;">
                <tr>
                    <td align="center" valign="top">
                        <!-- / Header -->
                        <table class="container header" border="0" cellpadding="0" cellspacing="0" width="80%" style="width: 80%;">
                            <tr>
                                <td style="padding: 30px 0 30px 0; border-bottom: solid 1px #eeeeee;" align="left">
                                    <a href="{{URL::Route('/')}}" style="font-size: 30px; text-decoration: none; text-align: center;">
                                        <p style="text-align: center">
                                            <img src="{{URL::To('/')}}/assets/images/logo.png" style="height: 80px; width: auto;"/>
                                        </p>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <!-- /// Header -->

                        <!-- / Hero subheader -->
                        <table class="container hero-subheader" border="0" cellpadding="0" cellspacing="0" width="80%" style="width: 80%;">
                            <tr>
                                <td class="hero-subheader__title" style="font-size: 43px; font-weight: bold; padding: 80px 0 15px 0; color: #de5e21" align="left">Booking Job Sheet</td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="color: #828282; line-height: 22px;">
                                        <span style="font-size: 20px;">Job ID: <strong>{{str_pad($jobId, 5, '0', STR_PAD_LEFT)}}</strong><br/></span>
                                        <span style="font-size: 20px;">Customer Name: <strong>{{UserInfo::where('id', $infoId)->pluck('fname')}} {{UserInfo::where('id', $infoId)->pluck('lname')}}</strong><br/></span>
                                        <span style="font-size: 16px; font-weight: 200;">Phone:<strong> {{UserInfo::where('id', $infoId)->pluck('telephone')}}</strong><br/></span>
                                        <span style="font-size: 16px; font-weight: 200;">Drivers Licence: <strong>{{BookingJob::where('id', $jobId)->pluck('licence')}}</strong><br/></span>
                                        <span style="font-size: 16px; font-weight: 200;">Email: <strong>{{UserInfo::where('id', $infoId)->pluck('email')}}</strong></span>
                                    </div>
                                </td>
                                <td>

                                </td>
                            </tr>
                            <tr>
                                <td class="hero-subheader__content" style="font-size: 16px; line-height: 27px; color: #969696; padding: 0 60px 90px 0;" align="left">

                                    {{--booking table data--}}
                                    <div>
                                        <?php
                                        $vehicleList = VehicleBooking::where('job_id', $jobId)->where('status', 2)->lists('id');
                                        $scaffoldList = ScaffoldingCart::where('job_id', $jobId)->where('status', 2)->lists('id');
                                        $eventList = EventCart::where('job_id', $jobId)->where('status', 2)->lists('id');
                                        $fullTotal = 0;
                                        $vehicleTotal = 0;
                                        $scaffoldTotal = 0;
                                        $eventTotal = 0;
                                        ?>
                                        @if(sizeof($vehicleList) > 0)
                                            <div class="booking-list-container" id="vehicle-booking-list-container">
                                                <h4 style="color: #d76300;">Vehicle Booking List</h4>

                                                <div class="bs-callout bs-callout-success">
                                                    <div class="table-responsive bottommargin">
                                                        <table id="booking-table" class="table cart" style="color: #555555; width: 100%; min-width: 500px; overflow: auto">
                                                            <thead>
                                                            <tr>
                                                                <th class="cart-product-name" align="left">Vehicle Plate</th>
                                                                <th>From</th>
                                                                <th>To</th>
                                                                <th class="cart-product-price">Day Rate</th>
                                                                <th class="cart-product-quantity">Days</th>
                                                                <th align="right" class="cart-product-subtotal">Total</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="vehicle-result">
                                                            @foreach($vehicleList as $bookingId)
                                                                <?php
                                                                $vehicleId = VehicleBooking::where('id', $bookingId)->pluck('vehicle_id');
                                                                $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                                                                $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');
                                                                $status = VehicleBooking::where('id', $bookingId)->pluck('status');
                                                                $vehiclePlate = Vehicles::where('id', $vehicleId)->pluck('plate');
                                                                $dayRate = VehicleBooking::where('id', $bookingId)->pluck('rate');
                                                                $from = VehicleBooking::where('id', $bookingId)->pluck('booking_from');
                                                                $to = VehicleBooking::where('id', $bookingId)->pluck('booking_to');
                                                                //processing data
                                                                $shortDate = new CalculationController();
                                                                $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                                                                $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';
                                                                //calculation
                                                                $diff = strtotime($to) - strtotime($from);
                                                                $hours = $diff / 3600;
                                                                $days = $hours / 24;
                                                                $restHours = $hours % 24;
                                                                if ($restHours > 0) {
                                                                    $days++;
                                                                }
                                                                $days = (int)$days;
                                                                $total = $days * $dayRate;
                                                                $vehicleTotal += $total;
                                                                ?>
                                                                <tr class="cart_item" id="vehicle-booking-row-82">
                                                                    <td class="cart-product-name">
                                                                        <a href="#">{{$vehiclePlate}}</a>
                                                                    </td>
                                                                    <td align="center">{{$fromShort}}</td>
                                                                    <td align="center">{{$toShort}}</td>
                                                                    <td align="center" class="cart-product-price">
                                                                        <span class="amount">${{number_format($dayRate, 2, '.', '')}}</span>
                                                                    </td>
                                                                    <td align="center" class="cart-product-quantity">{{$days}}</td>
                                                                    <td align="right" class="cart-product-subtotal">
                                                                        <span id="vehicle-total-amount-82" class="amount">${{number_format($total, 2, '.', '')}}</span>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <h4 style="color: #d76300; text-align: right; margin-bottom: -10px; margin-top: 20px;">Total: &nbsp;&nbsp;<strong id="vehicle-inbox-total">$ {{number_format($vehicleTotal, 2, '.', '')}}</strong></h4>
                                                </div>
                                            </div>
                                        @endif

                                        @if(sizeof($scaffoldList) > 0)
                                            <div class="booking-list-container" id="scaffold-booking-list-container">
                                                <h4 style="color: #d76300;">Scaffold Booking List</h4>
                                                <div class="bs-callout bs-callout-success">
                                                    <div class="table-responsive bottommargin">
                                                        <table id="booking-table" class="table cart" style="color: #555555; width: 100%;">
                                                            <thead>
                                                            <tr>
                                                                <th class="cart-product-name" align="left">Scaffold Type</th>
                                                                <th>From</th>
                                                                <th>To</th>
                                                                <th class="cart-product-price">Day Rate</th>
                                                                <th>Quantity</th>
                                                                <th class="cart-product-quantity">Days</th>
                                                                <th align="right" class="cart-product-subtotal">Total</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="vehicle-result">
                                                            @foreach($scaffoldList as $cartId)
                                                                <?php
                                                                $qty = ScaffoldingCart::where('id', $cartId)->pluck('qty');

                                                                $typeId = ScaffoldingCart::where('id', $cartId)->pluck('scaffolding_type_id');
                                                                $typeName = ScaffoldingType::where('id', $typeId)->pluck('name');
                                                                $dayRate = ScaffoldingCart::where('id', $cartId)->pluck('day_rate');

                                                                $from = ScaffoldingCart::where('id', $cartId)->pluck('booking_from');
                                                                $to = ScaffoldingCart::where('id', $cartId)->pluck('booking_to');

                                                                //processing data
                                                                $shortDate = new CalculationController();

                                                                $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                                                                $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';

                                                                //calculation
                                                                $diff = strtotime($to) - strtotime($from);
                                                                $hours = $diff / 3600;


                                                                $days = $hours / 24;
                                                                $restHours = $hours % 24;

                                                                if ($restHours > 0) {
                                                                    $days++;
                                                                }

                                                                $days = (int)$days;
                                                                $total = $days * $dayRate * $qty;
                                                                $scaffoldTotal += $total;
                                                                ?>
                                                                <tr class="cart_item" id="vehicle-booking-row-82">
                                                                    <td class="cart-product-name">
                                                                        <a href="#">{{$typeName}}</a>
                                                                    </td>
                                                                    <td align="center">{{$fromShort}}</td>
                                                                    <td align="center">{{$toShort}}</td>
                                                                    <td align="center" class="cart-product-price">
                                                                        <span class="amount">${{number_format($dayRate, 2, '.', '')}}</span>
                                                                    </td>
                                                                    <td align="center">{{$qty}}</td>
                                                                    <td align="center">{{$days}}</td>
                                                                    <td align="right" class="cart-product-subtotal">
                                                                        <span id="vehicle-total-amount-82" class="amount">${{number_format($total, 2, '.', '')}}</span>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <h4 style="color: #d76300; text-align: right; margin-bottom: -10px; margin-top: 20px;">Total: &nbsp;&nbsp;<strong id="scaffold-inbox-total">${{number_format($scaffoldTotal, 2, '.', '')}}</strong></h4>
                                                </div>
                                            </div>
                                        @endif

                                        @if(sizeof($eventList))
                                            <div class="booking-list-container" id="event-booking-list-container">
                                                <h4 style="color: #d76300;">Event Gear Booking List</h4>
                                                <div class="bs-callout bs-callout-success">
                                                    <div class="table-responsive bottommargin">
                                                        <table id="booking-table" class="table cart" style="color: #555555; width: 100%;">
                                                            <thead>
                                                            <tr>
                                                                <th class="cart-product-name" align="left">Gear Name</th>
                                                                <th>From</th>
                                                                <th>To</th>
                                                                <th class="cart-product-price">Day Rate</th>
                                                                <th align="center">Quantity</th>
                                                                <th class="cart-product-quantity">Days</th>
                                                                <th align="right" class="cart-product-subtotal">Total</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="vehicle-result">
                                                            @foreach($eventList as $cartId)
                                                            <?php
                                                                $qty = EventCartItems::where('cart_id', $cartId)->count('id');

                                                                $detailId = EventCart::where('id', $cartId)->pluck('item_detail_id');
                                                                $gearName = EventItemDetails::where('id', $detailId)->pluck('name');
                                                                $dayRate = EventItemDetails::where('id', $detailId)->pluck('day_rate');

                                                                $from = EventCart::where('id', $cartId)->pluck('booking_from');
                                                                $to = EventCart::where('id', $cartId)->pluck('booking_to');

                                                                //processing data
                                                                $shortDate = new CalculationController();

                                                                $fromShort = substr(substr($from, -8), 0, -3).' <strong>'.$shortDate->shortDate($from).'</strong>';
                                                                $toShort = substr(substr($to, -8), 0, -3).' <strong>'.$shortDate->shortDate($to).'</strong>';

                                                                //calculation
                                                                $diff = strtotime($to) - strtotime($from);
                                                                $hours = $diff / 3600;


                                                                $days = $hours / 24;
                                                                $restHours = $hours % 24;

                                                                if ($restHours > 0) {
                                                                    $days++;
                                                                }

                                                                $days = (int)$days;
                                                                $total = $days * $dayRate * $qty;
                                                                $eventTotal += $total;
                                                            ?>
                                                            <tr class="cart_item" id="vehicle-booking-row-82">
                                                                <td class="cart-product-name">
                                                                    <a href="#">{{$gearName}}</a>
                                                                </td>
                                                                <td align="center">{{$fromShort}}</td>
                                                                <td align="center">{{$toShort}}</td>
                                                                <td align="center" class="cart-product-price">
                                                                    <span class="amount">${{number_format($dayRate, 2, '.', '')}}</span>
                                                                </td>
                                                                <td align="center">{{$qty}}</td>
                                                                <td align="center">{{$days}}</td>
                                                                <td align="right" class="cart-product-subtotal">
                                                                    <span id="vehicle-total-amount-82" class="amount">${{number_format($total, 2, '.', '')}}</span>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <h4 style="color: #d76300; text-align: right; margin-bottom: -10px; margin-top: 20px;">Total: &nbsp;&nbsp;<strong id="event-inbox-total">${{number_format($eventTotal, 2, '.', '')}}</strong></h4>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    {{--End booking table data--}}
                                    <div class="table-responsive" style="margin-bottom: 50px;">
                                        <h4 style="color: #de5e21; font-size: 18px; margin-right: 15%;">Booking Totals</h4>
                                        <table class="table cart" style="margin-right: 15%;">
                                            <tbody>
                                            @if(sizeof($vehicleList) > 0)
                                            <tr id="vehicle-booking-sub-total" class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong style="color: #343434;">Vehicle Booking Subtotal</strong>
                                                </td>
                                                <td class="cart-product-name">
                                                    <span id="vehicle-total-amount" class="amount" style="color: #343434;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${{number_format($vehicleTotal, 2, '.', '')}}</span>
                                                </td>
                                            </tr>
                                            @endif
                                            @if(sizeof($scaffoldList) > 0)
                                            <tr id="scaffold-booking-sub-total" class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong style="color: #343434;">Scaffold Booking Subtotal</strong>
                                                </td>
                                                <td class="cart-product-name">
                                                    <span id="scaffold-total-amount" class="amount" style="color: #343434;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${{number_format($scaffoldTotal, 2, '.', '')}}</span>
                                                </td>
                                            </tr>
                                            @endif
                                            @if(sizeof($eventList))
                                            <tr id="event-booking-sub-total" class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong style="color: #343434;">Events Booking Subtotal</strong>
                                                </td>

                                                <td class="cart-product-name">
                                                    <span id="event-total-amount" class="amount" style="color: #343434;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${{number_format($eventTotal, 2, '.', '')}}</span>
                                                </td>
                                            </tr>
                                            @endif
                                            <tr class="cart_item">
                                                <td class="cart-product-name">
                                                    <strong style="color: #f96421; font-size: 20px">Total</strong>
                                                </td>
                                                <td class="cart-product-name">
                                                    <span class="amount color lead" style="color: #f96421; font-size: 24px"><strong id="full-total">${{number_format($vehicleTotal+$scaffoldTotal+$eventTotal, 2, '.', '')}}</strong></span>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!-- /// Hero subheader -->

                        <!-- / Divider -->
                        <table class="container" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-top: 25px;" align="center">
                            <tr>
                                <td align="center">
                                    <table class="container" border="0" cellpadding="0" cellspacing="0" width="620" align="center" style="border-bottom: solid 1px #eeeeee; width: 620px;">
                                        <tr>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- /// Divider -->

                        <!-- / CTA Block -->
                        <table class="container cta-block" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top">
                                    <table class="container" border="0" cellpadding="0" cellspacing="0" width="620" style="width: 620px;">
                                        <tr>
                                            <td class="cta-block__title" style="padding: 35px 0 0 0; font-size: 26px; text-align: center; color: #f96421">About Us</td>
                                        </tr>
                                        <tr>
                                            <td class="cta-block__content" style="padding: 20px 0 27px 0; font-size: 16px; line-height: 27px; color: #969696; text-align: center;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                                consequat.</td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <table class="container" border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td class="cta-block__button" width="230" align="center" style="width: 200px;">
                                                            <a href="http://slicejack.com/" style="border: 3px solid #ff8d50; color: #f96421; text-decoration: none; padding: 15px 45px; text-transform: uppercase; display: block; text-align: center; font-size: 16px;">Visit Us</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- /// CTA Block -->

                        <!-- / Divider -->
                        <table class="container" border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-top: 25px;" align="center">
                            <tr>
                                <td align="center">
                                    <table class="container" border="0" cellpadding="0" cellspacing="0" width="620" align="center" style="border-bottom: solid 1px #eeeeee; width: 620px;">
                                        <tr>
                                            <td align="center">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- /// Divider -->

                        <!-- / Info Bullets -->
                        <table class="container info-bullets" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                            <tr>
                                <td align="center">
                                    <table class="container" border="0" cellpadding="0" cellspacing="0" width="620" align="center" style="width: 620px;">
                                        <tr>
                                            <td class="info-bullets__block" style="padding: 30px 30px 15px 30px;" align="center">
                                                <table class="container" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                        <td class="info-bullets__icon" style="padding: 0 15px 0 0;">
                                                            <img src="http://dev2.slicejack.com/portfolio-email/img/img13.png">
                                                        </td>

                                                        <td class="info-bullets__content" style="color: #969696; font-size: 16px;">info@giantrentals.co.nz</td>
                                                    </tr>
                                                </table>
                                            </td>

                                            <td class="info-bullets__block" style="padding: 30px 30px 15px 30px;" align="center">
                                                <table class="container" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                        <td class="info-bullets__icon" style="padding: 0 15px 0 0;">
                                                            <img src="http://dev2.slicejack.com/portfolio-email/img/img11.png">
                                                        </td>

                                                        <td class="info-bullets__content" style="color: #969696; font-size: 16px;">0800 002 759</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="info-bullets__block" style="padding: 30px;" align="center"colspan="2" >
                                                <table class="container" border="0" cellpadding="0" cellspacing="0" align="center">
                                                    <tr>
                                                        <td class="info-bullets__icon" align="center" style="padding: 0 15px 0 0; text-align: center;">
                                                            <img src="http://dev2.slicejack.com/portfolio-email/img/img12.png">
                                                        </td>

                                                        <td class="info-bullets__content" style="color: #969696; font-size: 16px;">111 Newton Road, Eaden Terrace, Auckland</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- /// Info Bullets -->

                        <!-- / Social nav -->
                        <table class="container" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                            <tr>
                                <td align="center">
                                    <table class="container" border="0" cellpadding="0" cellspacing="0" width="620" align="center" style="border-top: 1px solid #eeeeee; width: 620px;">
                                        <tr>
                                            <td align="center" style="padding: 30px 0 30px 0;">
                                                <a href="#">
                                                    <img src="http://dev2.slicejack.com/portfolio-email/img/img7.png" border="0">
                                                </a>
                                            </td>

                                            <td align="center" style="padding: 30px 0 30px 0;">
                                                <a href="#">
                                                    <img src="http://dev2.slicejack.com/portfolio-email/img/img8.png" border="0">
                                                </a>
                                            </td>

                                            <td align="center" style="padding: 30px 0 30px 0;">
                                                <a href="#">
                                                    <img src="http://dev2.slicejack.com/portfolio-email/img/img9.png" border="0">
                                                </a>
                                            </td>

                                            <td align="center" style="padding: 30px 0 30px 0;">
                                                <a href="#">
                                                    <img src="http://dev2.slicejack.com/portfolio-email/img/img10.png" border="0">
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- /// Social nav -->

                        <!-- / Footer -->
                        <table class="container" border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                            <tr>
                                <td align="center">
                                    <table class="container" border="0" cellpadding="0" cellspacing="0" width="620" align="center" style="border-top: 1px solid #eeeeee; width: 620px;">
                                        <tr>
                                            <td style="text-align: center; padding: 50px 0 10px 0;">
                                                <a href="#" style="font-size: 28px; text-decoration: none; color: #d5d5d5;">Giant Rentals</a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="middle">
                                                <table width="60" height="2" border="0" cellpadding="0" cellspacing="0" style="width: 60px; height: 2px;">
                                                    <tr>
                                                        <td align="middle" width="60" height="2" style="background-color: #eeeeee; width: 60px; height: 2px; font-size: 1px;"><img src="http://dev2.slicejack.com/portfolio-email/img/img16.jpg"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="color: #d5d5d5; text-align: center; font-size: 15px; padding: 10px 0 60px 0; line-height: 22px;">Copyright &copy; {{date('Y')}} <a href="http://giantrentals.co.nz/" target="_blank" style="text-decoration: none; border-bottom: 1px solid #d5d5d5; color: #d5d5d5;">Giant Rentals</a>. <br />All rights reserved.</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <!-- /// Footer -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>