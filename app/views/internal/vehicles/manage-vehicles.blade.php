<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 7/09/16
 * Time: 12:14 PM
 */
?>

@extends('internal.master')

@section('content')
    @include('internal.include.add-new-vehicle')
    @include('internal.include.add-new-vehicle-booking')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header transparent">
                    <h2><strong>Vehicle</strong> Manage Booking</h2>
                    <div class="additional-btn">
                        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <div class="data-table-toolbar">
                        <div class="row">
                            <div class="col-md-2">
                                <form role="form">
                                    <input id="vehicle-booking-search" type="text" onkeyup="tableSearch(this)" class="form-control" placeholder="Search...">
                                </form>
                            </div>
                            <div class="col-md-1 pull-right">
                                @if(UserOperationAccess::where('user_id', Auth::user()->id)->where('operation_id', 1)->where('status', 1)->pluck('id') || User::where('id', Auth::user()->id)->pluck('roll') < 2)
                                    <button id="btn-add-new-vehicle" onclick="addVehicleConfig();" class="btn btn-info btn-sm md-trigger" data-modal="mm-v-m-add-new-vehicle"><i class="fa fa-plus"></i> Vehicle</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 bs-callout bs-callout-primary" style="max-height: 700px; overflow-y: auto;">
                        <h4 style="text-align: center; margin-bottom: 30px;"><strong>Vehicle List</strong></h4>
                        <div class="table-responsive">
                            <table data-sortable class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Plate&nbsp;Number</th>
                                    <th>Modal</th>
                                    <th>Year</th>
                                    <th>Rate</th>
                                </tr>
                                </thead>
                                <tbody id="vehicle-table-data" class="search-filter">
                                {{--Get Required Data--}}
                                <?php
                                    $vehicleList = Vehicles::where('status', '!=', 0)->orderBy('status', 'asc')->orderBy('priority', 'asc')->lists('id');
                                    $counter = 0;
                                    $selectedId = null;
                                ?>
                                @foreach($vehicleList as $vehicleId)
                                    <?php
                                        $highlight = "";
                                        if($counter == 0) {
                                            $highlight = 'row-highlight';
                                            $selectedId = $vehicleId;
                                        }
                                        $counter++;
                                    ?>
                                    <tr id="vehicle-{{$vehicleId}}" class="{{$highlight}}" onclick="selectVehicle({{$vehicleId}})" style="cursor: pointer; border-left: 6px solid {{Vehicles::where('id', $vehicleId)->pluck('display_color')}}; @if(Vehicles::where('id', $vehicleId)->pluck('status') == 2) opacity: 0.3; @endif">
                                        <td>{{Vehicles::where('id', $vehicleId)->pluck('plate')}}</td>
                                        <td>{{Vehicles::where('id', $vehicleId)->pluck('modal')}}</td>
                                        <td>{{Vehicles::where('id', $vehicleId)->pluck('year')}}</td>
                                        <td>{{Vehicles::where('id', $vehicleId)->pluck('rate')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <input type="hidden" id="selected-vehicle-id" value="{{$selectedId}}" />
                        </div>
                    </div>
                    <div class="col-md-8 bs-callout bs-callout-info pull-right" style="margin-top: 20px;">
                        <div class="row">
                            <h3 style="text-align: center; margin-bottom: 30px;"><strong id="vehicle-description-header">ALT507</strong></h3>
                        </div>
                        <div class="col-md-12">
                            <ul id="tbvehicle" class="nav nav-tabs nav-simple">
                                <li class="active">
                                    <a href="#tbvehicle-booking" data-toggle="tab"><i class="fa fa-calendar"></i> Bookings</a>
                                </li>
                                <li class="">
                                    <a href="#tbvehicle-home" data-toggle="tab"><i class="fa fa-info"></i> Info</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="tbvehicle-booking">
                                    <div class="widget-content padding">
                                        <div class="col-md-12">
                                            <div class="widget bg-white">
                                                <div class="widget-body">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div id="calendar"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- / .tab-pane -->
                                <div class="tab-pane fade" id="tbvehicle-home">
                                    <div class="col-md-10 col-md-offset-1" style="">
                                        <h4 style="text-align: center; margin-top: 20px; margin-bottom: 10px;"><strong>Vehicle Info</strong></h4>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p style="text-align: right; margin-top: -35px;">
                                                    @if(UserOperationAccess::where('user_id', Auth::user()->id)->where('operation_id', 2)->where('status', 1)->pluck('id') || User::where('id', Auth::user()->id)->pluck('roll') < 2)
                                                        <button class="btn btn-default btn-sm md-trigger" data-modal="mm-v-m-add-new-vehicle"  onclick="updateVehicleInfo();"><i class="fa fa-list-alt"></i> Edit Info</button>
                                                    @endif
                                                    @if(UserOperationAccess::where('user_id', Auth::user()->id)->where('operation_id', 3)->where('status', 1)->pluck('id') || User::where('id', Auth::user()->id)->pluck('roll') < 2)
                                                        <input type = "checkbox" data-toggle = "tooltip" title = "Disable Vehicle" data-id = "disable-enable-vehicle" class = "ios-switch ios-switch-info ios-switch-sm pull-left" checked/>
                                                    @endif
                                                </p>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table data-sortable class="table">
                                                <thead>
                                                <tbody>
                                                <tr>
                                                    <td>Plate Number</td>
                                                    <td>-</td>
                                                    <td><strong id="vehicle-plate">-</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Modal</td>
                                                    <td>-</td>
                                                    <td><strong id="vehicle-modal">-</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Modal Year</td>
                                                    <td>-</td>
                                                    <td><strong id="vehicle-year">-</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Day Rent Rate</td>
                                                    <td>-</td>
                                                    <td><strong id="vehicle-rate">-</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>-</td>
                                                    <td><strong id="vehicle-description">-</strong></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div> <!-- / .tab-pane -->
                            </div> <!-- / .tab-content -->
                        </div>
                    </div>
                    <p style="text-align: center">
                        <img id="booking-list-loading" src="{{URL::To('/')}}/internal/assets/img/loading/loading.gif" style="display: none;" />
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{HTML::script('internal/assets/js/custom/vehicles/vehicles.js')}}
    {{HTML::script('/internal/assets/libs/fullcalendar/fullcalendar.min.js')}}
    {{HTML::script('/internal/assets/js/custom/calendar.js')}}
@endsection