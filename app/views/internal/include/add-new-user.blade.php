<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 5/09/16
 * Time: 3:21 PM
 */
?>

<div class = "md-modal md-fade-in-scale-up" id = "mm-u-m-add-new-user" style = "z-index: 50;max-height: 90%;overflow-y: auto;box-shadow: -3px -3px 3px rgba(136, 136, 136, 0.76);" ng-app>
    <div class = "md-content">
        <h3 id = "model-supplier-contact-popup-header">Add New User</h3>
        <div class = "row">
            <div class = "form-group">
                <label>User Email</label>
                <input id = "mm-u-m-new-user-email" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);emailValidator(this);" type = "email" class = "form-control" placeholder = "someone&#64;example.com" maxlength = "64" required>
            </div>
            <div class = "form-group">
                <label>First Name</label>
                <input id = "mm-u-m-new-user-fname" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);" type = "text" class = "form-control" placeholder = "Jhone" maxlength = "30">
            </div>
            <div class = "form-group">
                <label>Last Name</label>
                <input id = "mm-u-m-new-user-lname" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);" type = "text" class = "form-control" placeholder = "Doe" maxlength = "30">
            </div>
            <p style = "margin-left: 30%">
                <button id = "mm-u-m-new-user-add" onclick="addUser();" type = "button" class = "btn btn-primary">Send Request</button>
                <button id = "mm-u-m-new-user-cancel" class = "btn btn-danger md-close">Cancel</button>
                {{HTML::image('internal/assets/img/loading/loading.gif', 'loading image', array('id' => 'mm-u-m-add-new-user-loading'))}}
</p>
</div>
</div>
<!-- End div .md-content -->
</div><!-- End div .md-modal .md-3d-flip-vertical -->
