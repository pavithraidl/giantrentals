<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 30/09/2016 AD
 * Time: 15:04
 */
?>

<div class = "md-modal md-fade-in-scale-up" id = "mm-s-m-release-return-scaffolding" style = "z-index: 50;max-height: 90%;overflow-y: auto;box-shadow: -3px -3px 3px rgba(136, 136, 136, 0.76);">
    <div class = "md-content">
        <div class="widget">
            <div class="widget-header transparent">
                <h2><strong id="mm-s-m-release-return-title"></strong> Scaffolding</h2>
                <div class="additional-btn">
                    <a id="manage-booking-widget-reload" href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                </div>
            </div>
            <div class = "row">
                <div class="col-md-12">
                    <div class="table-responsive" style="min-height: 400px;">
                        <table data-sortable class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Item&nbsp;ID</th>
                                <th>Item&nbsp;Name</th>
                                <th>Status</th>
                            </tr>
                            </thead>

                            <tbody id="booking-table-cart-item-list" class="search-filter">
                            {{--JQuery Append--}}
                            </tbody>
                        </table>
                    </div>
                </div>
                <p style = "text-align: center;">
                    <button id = "mm-s-m-btn-return-release-item-cancel" class = "btn btn-danger md-close">Cancel</button>
                    <button id="mm-s-m-btn-return-release-item" onclick="updateItemCartStatus();" disabled="disabled" class="btn btn-success"></button>
                </p>
            </div>
        </div>
    </div>
</div><!-- End div .md-modal .md-3d-flip-vertical -->
