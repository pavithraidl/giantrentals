<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 7/09/16
 * Time: 12:30 PM
 */
?>

<!-- BEGIN MODAL -->
<div class="modal fade" id="event-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add New <strong>Booking</strong></h4>
            </div>
            <div class="modal-body">
                <div class="row booking-time">
                    <div class="col-md-12"> 
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Booking From</label>
                                <input class="form-control b-from" id="booking-from" type="datetime" value="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Booking To</label>
                                <input class="form-control b-to" id="booking-to" type="datetime" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row not-available-msg">
                    <div class='col-md-12 not-available-msg'><h4 style='text-align: center; color: #a70e00; margin-top: 50px; margin-bottom: 50px;'><strong>Sorry!</strong> No Vehicles Available For That Time Period.</h4></div>
                </div>
                <div class="row plate-list">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                            <label class="control-label">Vehicle Plate Number</label>
                            <select onchange="getBookingPrice();" id="mm-v-b-plate" class="form-control plate">
                                {{--Jquery Append--}}
                            </select>
                        </div>
                   </div>
                </div>
                <div class="row price-label">
                    <div class="col-md-10 col-md-offset-1">
                        <h4 style="text-align: center;">Booking Price:
                            <strong class="booking-price">
                                {{--JQuery Append--}}
                            </strong>
                        </h4>
                    </div>
                </div>
                <div class="row customer-booking-details">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">First Name</label>
                                <input class="form-control fname" id="mm-v-b-fname" type="text" onblur="requiredValidator(this)" maxlength="16">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Last Name</label>
                                <input class="form-control lname" id="mm-v-b-lname" type="text" onblur="requiredValidator(this)" maxlength="16">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input class="form-control email" id="mm-v-b-email" type="text" onblur="requiredValidator(this);" maxlength="128">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Telephone Number</label>
                                <input class="form-control telephone only-numbers" id="mm-v-b-telephone" type="text" onblur="requiredValidator(this)" maxlength="12">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row loading-img" id="mm-loading-img">
                    <p class="img-loading" style="text-align: center;"><img src="{{URL::To('/')}}/internal/assets/img/loading/loading.gif"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default go-back pull-left" onclick="goBackBooking();"><i class="fa fa-arrow-left"></i> Go Back </button>
                <button type="button" class="btn btn-info show-available-vehicles" onclick="checkAvailability();">Show Available Vehicles <i class="fa fa-arrow-right"></i> </button>
                <button type="button" class="btn btn-default close-booking" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success save-booking" onclick="addBooking();">Add Booking</button>
                <button type="button" class="btn btn-danger delete-booking" data-dismiss="modal">Delete</button>
                <p class="lbl-wait" style="text-align: center">
                    Please Wait...
                </p>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->