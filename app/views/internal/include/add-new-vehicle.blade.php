<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 7/09/16
 * Time: 11:38 AM
 */
?>

<div class = "md-modal md-fade-in-scale-up" id = "mm-v-m-add-new-vehicle" style = "z-index: 50;max-height: 90%;overflow-y: auto;box-shadow: -3px -3px 3px rgba(136, 136, 136, 0.76);">
    <div class = "md-content">
        <h3 id = "model-supplier-contact-popup-header">Add New Vehicle</h3>

        <div class = "row">
            <div class = "form-group">
                <label for="mm-v-m-plate">Plate Number</label>
                <input id = "mm-v-m-plate" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);" type = "text" class = "form-control" placeholder = "ALT507" maxlength = "6" required>
            </div>
            <div class = "form-group">
                <label for="mm-v-m-modal">Modal</label>
                <input id = "mm-v-m-modal" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);" type = "text" class = "form-control" placeholder = "BMW" maxlength = "16">
            </div>
            <div class = "form-group">
                <label for="mm-v-m-year">Year</label>
                <input id = "mm-v-m-year" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);" type = "number" class = "form-control" placeholder = "2012" maxlength = "4">
            </div>
            <div class = "form-group">
                <label for="mm-v-m-rate">Day Rent Rate</label>
                <input id = "mm-v-m-rate" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);" type = "number" class = "form-control" placeholder = "62.00" maxlength = "6">
            </div>
            <div class="form-group">
                <lable for="mm-v-m-color">Vehicle Color</lable>
                <select id="mm-v-m-color" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);" class="form-control">
                    <option value="bg-pink-1">pink-1</option>
                    <option value="bg-pink-2">pink-2</option>
                    <option value="bg-green-1">green-1</option>
                    <option value="bg-green-2">green-2</option>
                    <option value="bg-green-3">green-3</option>
                    <option value="bg-orange-1">orange-1</option>
                    <option value="bg-orange-2">orange-2</option>
                    <option value="bg-orange-3">orange-3</option>
                    <option value="bg-orange-4">orange-4</option>
                    <option value="bg-light-blue-1">lightblue-1</option>
                    <option value="bg-light-blue-2">lightblue-2</option>
                    <option value="bg-blue-1">blue-1</option>
                    <option value="bg-blue-2">blue-2</option>
                    <option value="bg-blue-3">blue-3</option>
                    <option value="bg-darkblue-1">darkblue-1</option>
                    <option value="bg-darkblue-2">darkblue-2</option>
                    <option value="bg-darkblue-3">darkblue-3</option>
                    <option value="bg-red-1">red-1</option>
                    <option value="bg-yellow-1">yellow-1</option>
                    <option value="bg-yellow-2">yellow-2</option>
                </select>
            </div>
            <div class = "form-group">
                <label for="mm-v-m-description">Description</label>
                <input id = "mm-v-m-description" onblur="requiredValidator(this);" onkeyup="requiredValidator(this);" type = "text" class = "form-control" placeholder = "description..." maxlength = "500">
            </div>
            <p style = "margin-left: 30%">
                <button id = "mm-v-m-new-vehicle-add" onclick="addVehicle();" type = "button" class = "btn btn-primary">Add Vehicle</button>
                <button id="mm-v-m-update-vehicle" onclick="updateVehicle();" type="button" class="btn btn-primary" style="display: none;">Update Vehicle Info</button>
                <button id = "mm-v-m-new-vehicle-cancel" class = "btn btn-danger md-close">Cancel</button>
                {{HTML::image('internal/assets/img/loading/loading.gif', 'loading image', array('id' => 'mm-v-m-loading-img'))}}
            </p>
        </div>
    </div>
    <!-- End div .md-content -->
</div><!-- End div .md-modal .md-3d-flip-vertical -->
