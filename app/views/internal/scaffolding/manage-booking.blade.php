<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 2/09/16
 * Time: 4:12 PM
 */
?>

@extends('internal.master')

@section('content')
    @include('internal.include.release-return-scaffolding-items')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header main-widget-header transparent">
                    <h2><strong>Scaffolding</strong> Manage Booking </h2>
                    <div class="additional-btn">
                        <a id="manage-booking-widget-reload" href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                    </div>
                </div>
                <button style="display: none;" id="btn-show-cart-item-list" class="btn btn-info btn-sm md-trigger" data-modal="mm-s-m-release-return-scaffolding"></button>
                <div class="widget-content">
                    {{--Tabs Header--}}
                    <ul id="demo5" class="nav nav-tabs nav-simple">
                        <li class="active">
                            <a id="tab-booking-table-container" href="#booking-table-container" data-toggle="tab"><i class="fa fa-list"></i> Invoices</a>
                        </li>
                        {{--<li class="">--}}
                            {{--<a id="tab-booking-slider-container" href="#booking-slider-container" data-toggle="tab"><i class="fa fa-tasks"></i> Slider</a>--}}
                        {{--</li>--}}
                        {{--<li class="">--}}
                            {{--<a id="tab-booking-calender-container" href="#booking-calendar-container" data-toggle="tab"><i class="fa fa-calendar"></i> Calendar</a>--}}
                        {{--</li>--}}
                    </ul>
                    {{--Tabs Header End--}}
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="booking-table-container">
                            <div class="data-table-toolbar">
                                <div class="row">
                                    <div class="col-md-2">
                                        <form role="form">
                                            <input id="scaffolding-booking-search" type="text" onkeyup="searchAll(this, 'span')" class="form-control" placeholder="Search...">
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <h3 id="booking-type-title" style="text-align: center;">Pending</h3>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <select id="vehicle-booking-type" class="form-control">
                                                <option value="3">Current Bookings List</option>
                                                <option value="2">Rented List</option>
                                                <option value="1">Booking History</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-info btn-md pull-right" onclick="showBookingModal();"><i class="fa fa-plus"></i> Booking</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="all-booking-container" class="row" style="min-height: 550px;">
                                    {{--Jquery append--}}
                                    <div id="s-i-btn-add-new-invoice" class="circle-md hidden-sm hidden-xs" style="background-color: #db4437; position: fixed; top: 81%; left: 94%"><i id="s-i-add-invoice-icon" style="font-size: 30px; font-weight: 200" class="icon-plus-3"></i> </div>
                                    <div id="s-i-btn-add-new-invoice" class="circle-md hidden-xs hidden-md hidden-lg" style="background-color: #db4437; position: fixed; top: 81%; left: 90%; z-index: 100;"><i id="s-i-add-invoice-icon" style="font-size: 30px; font-weight: 200" class="icon-plus-3"></i> </div>
                                    <div id="s-i-btn-add-new-invoice" class="circle-md hidden-sm hidden-md hidden-lg" style="background-color: #db4437; position: fixed; top: 81%; left: 78%; z-index: 100;"><i id="s-i-add-invoice-icon" style="font-size: 30px; font-weight: 200" class="icon-plus-3"></i> </div>

                                    <input type="hidden" id="s-i-delete-access" value="0" />
                                    <input type="hidden" id="s-i-open-invoice-id" value="0" />
                                </div>
                            </div>
                        </div> <!-- / .tab-pane -->
                        <div class="tab-pane fade" id="booking-slider-container">
                            <div class="col-md-12" style="margin-top: 20px;">
                                <h2 style="text-align: center;" onclick="testFunction();">Booking Slider</h2>
                                <div class="col-xs-4 col-md-2 col-lg-1" style="position: relative;">
                                    <table id="slider-table-freeze-column" data-sortable class="table booking-slider" style="position: absolute; right: 0px; top: 15px; width: auto;">
                                        <thead>
                                        <th class="right-border-1 blink" style="text-align: center;">Plate&nbsp;No</th>
                                        </thead>
                                        <tbody id="slider-table-freeze-column-plate-list">
                                        {{--JQuery Append--}}
                                        </tbody>
                                    </table>
                                </div>
                                <div id="slider-cell-container" class="col-xs-8 col-md-10 col-lg-11 pull-right inner-shadow" style="overflow-x: auto; min-height: 350px; margin-bottom: 100px;">
                                    <table id="slider-cell-table" data-sortable class="table booking-slider">
                                        <thead id="slider-table-thead-list">
                                        {{--JQuery Append--}}
                                        </thead>

                                        <tbody id="slider-table-tbody-cells">
                                        {{--JQuery Append--}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> <!-- / .tab-pane -->
                        <div class="tab-pane fade" id="booking-calendar-container">
                            <div class="col-md-12" style="margin-top: 50px;">
                                <div class="widget-content padding">
                                    <div class="col-md-12">
                                        <div class="widget bg-white">
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div id="calendar"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- / .tab-pane -->
                    </div> <!-- / .tab-content -->
                    <p style="text-align: center">
                        <img id="booking-list-loading" src="{{URL::To('/')}}/internal/assets/img/loading/loading.gif" style="display: none;" />
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{HTML::style('/internal/assets/libs/datetime-picker/css/bootstrap-material-datetimepicker.css')}}
    {{HTML::script('/internal/assets/libs/fullcalendar/fullcalendar.min.js')}}
    {{HTML::script('/internal/assets/js/custom/calendar.js')}}
    {{HTML::script('/internal/assets/js/custom/scaf/booking.js')}}
    {{HTML::script('/internal/assets/libs/datetime-picker/js/moment.min.js')}}
    {{HTML::script('/internal/assets/libs/datetime-picker/js/bootstrap-material-datetimepicker.js')}}
@endsection