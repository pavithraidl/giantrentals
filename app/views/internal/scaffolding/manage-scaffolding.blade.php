<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 4/10/2016 AD
 * Time: 14:52
 */
?>

@extends('internal.master')

@section('content')
    @include('internal.include.release-return-scaffolding-items')
    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header main-widget-header transparent">
                    <h2><strong>Scaffolding</strong> Manage</h2>
                    <div class="additional-btn">
                        <a id="manage-booking-widget-reload" href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                    </div>
                </div>
                <button style="display: none;" id="btn-show-cart-item-list" class="btn btn-info btn-sm md-trigger" data-modal="mm-s-m-release-return-scaffolding"></button>
                <div class="widget-content">
                    <div class="data-table-toolbar">
                        <div class="row">
                            <div class="col-md-2">
                                <form role="form">
                                    <input id="vehicle-booking-search" type="text" onkeyup="tableSearch(this)" class="form-control" placeholder="Search...">
                                </form>
                            </div>
                            <div class="col-md-6">
                                <h3 id="booking-type-title" style="text-align: center;">Base Collars</h3>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?php
                                        $partList = ScaffoldingPart::lists('id');
                                    ?>
                                    <select id="vehicle-booking-type" class="form-control">
                                        @foreach($partList as $partId)
                                            <option value="{{$partId}}">{{ScaffoldingPart::where('id', $partId)->pluck('name')}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-info btn-md pull-right" onclick="showBookingModal();"><i class="fa fa-plus"></i> New Part</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 bs-callout bs-callout-info" style="margin-left: 30px; background-image: linear-gradient(to bottom, transparent, white);">
                            <div class="table-responsive" style="min-height: 400px; max-height: 600px; overflow: auto;">
                                <table data-sortable class="table table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th>Type&nbsp;ID</th>
                                        <th>Status</th>
                                        <th>Created&nbsp;At</th>
                                    </tr>
                                    </thead>

                                    <tbody id="scaffolding-table-data" class="search-filter">
                                    {{--JQuery Append--}}
                                    </tbody>
                                </table>
                                <div id="bottom_fade" style="background: url('{{URL::To('/')}}/assets/images/bottom-fade.png') bottom center no-repeat; z-index: 999; width: 100%; position: absolute; bottom: -200px; left: 0px; height: 100px; "></div>
                            </div>
                            <p style="text-align: center">
                                <img id="booking-list-loading" src="{{URL::To('/')}}/internal/assets/img/loading/loading.gif" style="display: none;" />
                            </p>
                        </div>
                        <div class="col-md-8">

                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{HTML::script('/internal/assets/js/custom/scaf/scaffolding.js')}}
@endsection
