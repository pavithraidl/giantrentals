<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 2/09/16
 * Time: 4:15 PM
 */
?>

@extends('internal.master')

@section('content')
<div class = "content" style = "background-color: white;">
@include('internal.include.add-new-user')
<!-- Page Heading Start -->
<div class = "page-heading">
    <div class="row" style="margin-right: 20px;">
        <h1 class="col-lg-push-1" style="margin-left: 20px !important;"><i class = 'fa icon-retweet-2'></i> Users Manage</h1>
        <button class="pull-right btn btn-blue-3 btn-sm md-trigger" style="margin-top: 20px;" data-modal="mm-u-m-add-new-user"><i class="fa fa-plus-circle"></i> Add User</button>
    </div>

</div>
<div class = "page-content">
    <div class = "col-md-12">
        <div class = "widget">
            <div class = "col-md-3 bs-callout bs-callout-info" style = "border-radius: 10px; margin-top: -10px;">
                <div class = "row">
                    <div class = "col-md-12">
                        <div class = "form-group form-search search-box has-feedback">
                            <span id = "searchSpan">
                                <input type = "text" class = "form-control full-rounded" onkeyup = "searchFilter(this);" id = "searchText" ng-model = "searchText" placeholder = "Search..">
                            </span>
                        </div>
                    </div>
                </div>
                <div id = "u-m-user-list-container" class = "row" style = "margin-top: 20px;">
                    <ul id = "u-m-user-list-ul" class = "search-filter" style = "list-style-type: none; margin-left: -30px;">
                        {{--user list jquery append--}}
                    </ul>
                </div>
            </div>
            <div class = "col-md-3" style = "padding: 5px;">
                <div class = " shadow-pane-1" style = "margin-top: -10px;">
                    <div class = "row">
                        <div class = "col-md-12">
                            <?php
                            $infoId = User::where('id', Auth::user()->id)->pluck('info_id');
                            $userAvatar = UserInfo::where('id', $infoId)->pluck('avatar');
                            if ($userAvatar == 0) {
                                $gender = DB::table('user_info')->where('id', $infoId)->pluck('gender');
                                if($gender == null) {
                                    $userAvatar = "default";
                                }
                                else {
                                    $userAvatar = sprintf("default-%s", $gender);
                                }
                            }
                            ?>
                            <div id = "u-m-active-deactive-container" data-toggle = "tooltip" title = "Active/Deactive user" style = "visibility: hidden; position: absolute;">
                                <input id = "u-m-active-deactive" data-id = "user-enable-disable" type = "checkbox" class = "ios-switch ios-switch-primary ios-switch-sm pull-right" checked hidden = "hidden"/>
                            </div>

                            <p style = "text-align: center;">
                                {{HTML::image(sprintf('internal/assets/img/users/%s/user-100.jpg', $userAvatar), 'User Avatar', array('class' => 'img-circle', 'id' => 'u-m-user-avatar'))}}
                            </p>

                        </div>
                    </div>
                    <div class = "row">
                        <h4 class = "text-blue-3" style = "text-align: center; font-size: 18px;">
                            <strong id = "u-m-full-name">{{UserInfo::where('id', $infoId)->pluck('fname')}} {{DB::table('user_info')->where('id', $infoId)->pluck('lname')}}</strong>
                        </h4>
                    </div>
                    <div class = "row" style = "margin-top: -10px;">
                        <p style = "text-align: center; font-weight: 600; color: rgba(0, 0, 0, 0.30)" id = "u-m-roll">Admin</p>
                    </div>
                    <div class = "row">
                        <ul id = "u-m-quick-link-list" style = "list-style-type: none; margin-left: -20px;margin-top: 20px;">
                            {{--<li style = "margin-top: 5px;">--}}
                            {{--<a href = "#"> <i class = "fa fa-circle text-green-1"></i> &nbsp;Online</a>--}}
                            {{--</li>--}}
                            <li style = "margin-top: 5px;">
                                <a id = "u-m-account-active-status" href = "#" style="cursor: text">
                                    <i id = "u-m-account-active-status-icon" class = "fa fa-bookmark text-green-2"></i>
                                    &nbsp;<span id = "u-m-account-active-status-text">Account is Active</span></a>
                            </li>
                            <li style = "margin-top: 5px;">
                                <a id = "u-m-view-activity-log" href = "#">
                                    <i class = "fa fa-align-left text-darkblue-3"></i> &nbsp;View Activity
                                    Log</a>
                            </li>
                            <li style = "margin-top: 5px;">
                                <a id = "u-m-user-mail-to" href = "mailto:{{UserInfo::where('id', $infoId)->pluck('email')}}">
                                    <i class = "fa fa-envelope-o text-blue-3"></i> &nbsp;Send Email</a>
                            </li>
                            <li style = "margin-top: 5px;">
                                <a id = "u-m-user-telephone" href = "tel:{{UserInfo::where('id', $infoId)->pluck('telephone')}}">
                                    <i class = "fa fa-phone-square" style = "color: #02db11"></i> &nbsp;Call
                                    Phone</a>
                            </li>
                            <li style = "margin-top: 15px;">
                                <button id = "u-m-delete-user" onclick = "deleteUser();" class = "btn btn-danger" style = "width: 90%;text-align: center; visibility: hidden;">
                                    <i class = "fa fa-trash"></i> Delete User
                                </button>
                            </li>
                        </ul>
                        <ul id = "u-m-not-activated-quick-links" style = "list-style-type: none; margin-left: -20px; visibility: hidden;">
                            <li style = "margin-top: 25px;">
                                <button id = "u-m-resend-activation-email" class = "btn btn-default" style = "width: 90%;text-align: center;">
                                    <i class = "fa fa-envelope"></i> Resend Activation Email
                                </button>
                            </li>
                            <li style = "margin-top: 15px;">
                                <button onclick = "deleteUser();" class = "btn btn-danger" style = "width: 90%;text-align: center;">
                                    <i class = "fa fa-trash"></i> Delete User
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class = "col-md-6">
                <ul id = "u-m-user-manage-container" class = "nav nav-tabs nav-simple">
                    <li class = "active">
                        <a href = "#u-m-user-manage-container-info" data-toggle = "tab"><i class = "fa fa-info-circle text-blue-3"></i>
                            Info</a>
                    </li>
                    <li class = "">
                        <a href = "#u-m-user-manage-container-access" data-toggle = "tab"><i class = "icon-lock-alt text-orange-3"></i>
                            Access</a>
                    </li>
                    <li class = "">
                        <a id = "u-m-tab-activity-log" href = "#u-m-user-manage-container-activity-log" data-toggle = "tab"><i class = "fa fa-bullhorn text-green-3"></i>
                            Activity Log</a>
                    </li>

                </ul>

                <div class = "tab-content" style = "padding: 20px;">
                    <div class = "tab-pane fade active in" id = "u-m-user-manage-container-info">
                        <div id = "u-m-user-info-container" class = "row">
                            <div class = "row" style = "padding-left: 15px;">
                                <?php
                                $timeScence = new CalculationController();
                                $createdAt = DB::table('users')->where('id', Auth::user()->id)->pluck('created_at');
                                $joinAt = ($timeScence->timeAgo((new DateTime($createdAt))->format('Y-m-d H:i:s')));
                                ?>
                                <i class = "fa fa-circle" style = "color: #663268"></i>
                                <span style = "font-weight: 600; color: rgba(0, 0, 0, 0.51)" title = "{{$createdAt}}" data-toggle = "tooltip"><span id = "u-m-join-at">{{$joinAt}}
                                        Join with the system</span></span>
                            </div>
                            <h4><strong>Basic Info</strong></h4>

                            <div class = "row" style = "padding-left: 35px;">
                                <div class = "row" style = "margin-top: 5px;">
                                    <div class = "col-md-3">
                                        <span style = "color: #070068">First Name </span>
                                    </div>
                                    <div class = "col-md-1">
                                        -
                                    </div>
                                    <div class = "col-md-7">
                                        <span id = "u-m-first-name" class = "pull-left" style = "margin-left: 10px;">{{DB::table('user_info')->where('id', $infoId)->pluck('fname')}}</span>
                                    </div>
                                </div>
                                <div class = "row" style = "margin-top: 5px;">
                                    <div class = "col-md-3">
                                        <span style = "color: #070068">Last Name </span>
                                    </div>
                                    <div class = "col-md-1">
                                        -
                                    </div>
                                    <div class = "col-md-7">
                                        <span id = "u-m-last-name" class = "pull-left" style = "margin-left: 10px;">{{DB::table('user_info')->where('id', $infoId)->pluck('lname')}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id = "u-m-account-not-activated-container" class = "row" style = "margin-left: 30px;"></div>
                    </div>
                    <!-- / .tab-pane -->
                    <div class = "tab-pane fade" id = "u-m-user-manage-container-access">
                        <div id="u-m-access-admin-account" style="@if(Auth::user()->rollid > 4) opacity: 0; @endif" class="row">
                            <span>This is the admin account. This account has all the privileges. You can't restrict admin account access levels.</span>
                        </div>
                        <?php
                        $categoryList = SystemPageCategory::where('status', 1)->orderBy('id', 'asc')->lists('id');
                        ?>

                        <div id="u-m-access-control-container" style="margin-top: -40px; @if(Auth::user()->rollid < 3) opacity: 0; @endif" class = "row">
                            @foreach($categoryList as $categoryId)
                                <?php
                                $pageId = null;
                                $systemPages = SystemPageList::where('category_id', $categoryId)->orderBy('id', 'desc')->lists('id');
                                foreach ($systemPages as $systemPageId) {
                                    if (UserPageAccess::where('page_id', $systemPageId)->where('user_id', Auth::user()->id)->pluck('id')) {
                                        $pageId = $systemPageId;
                                    }
                                }
                                ?>
                                @if(UserPageAccess::where('page_id', $pageId)->where('user_id', Auth::user()->id)->pluck('id') || Auth::user()->roll == 1)

                                    <h3>
                                        <i class = "fa {{SystemPageCategory::where('id', $categoryId)->pluck('icon')}}"></i> {{SystemPageCategory::where('id', $categoryId)->pluck('name')}}
                                    </h3>
                                    <div class = "panel-group accordion-toggle" id = "parent-accordion-{{$categoryId}}">
                                        @foreach($systemPages as $systemPageId)
                                            @if(UserPageAccess::where('page_id', $systemPageId)->where('status', 1)->where('user_id', Auth::user()->id)->pluck('id') || Auth::user()->roll == 1)
                                                <div id="u-m-tab-container-{{$systemPageId}}" class = "panel panel-lightblue-2">
                                                    <div class = "panel-heading">
                                                        <h4 class = "panel-title">
                                                            <a data-toggle = "collapse" class = "collapsed" data-parent = "#parent-accordion-{{$categoryId}}" href = "#accordion-{{$systemPageId}}">
                                                                <i class = "fa {{SystemPageList::where('id', $systemPageId)->pluck('icon')}}"></i> {{SystemPageList::where('id', $systemPageId)->pluck('name')}}
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id = "accordion-{{$systemPageId}}" class = "panel-collapse collapse">
                                                        <div class = "panel-body" style = "padding: 20px;">
                                                            <div class = "row">
                                                                <div class = "col-md-6">
                                                                    <label>Active Page</label>
                                                                </div>
                                                                <div class = "col-md-6">
                                                                    <input id="u-m-page-allow-{{$systemPageId}}" type = "checkbox" data-toggle = "tooltip" title = "Allow Access" data-id = "pages-{{$systemPageId}}" class = "ios-switch ios-switch-primary ios-switch-md pull-left"/>
                                                                </div>
                                                            </div>
                                                            <div id = "u-m-sub-access-{{$systemPageId}}">
                                                                <?php
                                                                $operationList = SystemOperations::where('page_id', $systemPageId)->lists('id');
                                                                ?>
                                                                @if(count($operationList) > 0)
                                                                    <div class = "row" style = "margin-top: 10px;">
                                                                        <h5>
                                                                            <strong class = "text-darkblue-2">Operation Access</strong>
                                                                        </h5>
                                                                    </div>
                                                                @endif

                                                                @foreach($operationList as $operationId)
                                                                    <div class = "row" style = "margin-top: 5px;">
                                                                        <div class = "col-md-6">
                                                                            <label>{{SystemOperations::where('id', $operationId)->pluck('name')}}</label>
                                                                        </div>
                                                                        <div class = "col-md-6">
                                                                            <input type = "checkbox" data-toggle = "tooltip" title = "Allow Access" data-id = "op-{{$operationId}}" class = "ios-switch ios-switch-warning ios-switch-sm pull-left" checked/>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <!-- / .tab-pane -->
                    <div class = "tab-pane fade" id = "u-m-user-manage-container-activity-log" style = "max-height: 400px; overflow-y: auto; overflow-x: hidden;">
                        <input id = "u-m-selected-user" type = "hidden" value = "{{Auth::user()->id}}"/>

                        <div id = "u-m-activity-list-container" class = "row">
                            <ul id = "u-m-activity-list" class = "media-list">

                            </ul>
                        </div>
                    </div>
                    <!-- / .tab-pane -->
                </div>
                <!-- / .tab-content -->
                <hr>
            </div>
        </div>
    </div>
</div>
<div class = "row" style = "width: 100%;">
</div>
</div>
@endsection

@section('scripts')
    {{HTML::script('internal/assets/js/custom/users/manage.js')}}
@endsection