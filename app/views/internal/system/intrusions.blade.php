<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 16/09/2016 AD
 * Time: 11:27
 */
?>
@extends('internal.master')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="widget">
                <div class="widget-header transparent">
                    <h2><strong>System Intrusions</strong> Table</h2>
                    <div class="additional-btn">
                        <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                    </div>
                </div>
                <div class="widget-content">
                    <div class="data-table-toolbar">
                        <div class="row">
                            <div class="col-md-4">
                                <form role="form">
                                    <input id="intrusions-search" type="text" onkeyup="tableSearch(this)" class="form-control" placeholder="Search...">
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table data-sortable class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Row&nbsp;ID</th>
                                <th>Controller</th>
                                <th>Function</th>
                                <th>User</th>
                                <th>Intrusion&nbsp;Msg</th>
                                <th data-sortable="false">Status</th>
                                <th>Occurred&nbsp;At</th>
                            </tr>
                            </thead>

                            <tbody id="intrusion-table-data" class="search-filter">
                            {{--JQuery Append--}}
                            </tbody>
                        </table>
                    </div>
                    <p style="text-align: center">
                        <img id="intrusions-list-loading" src="{{URL::To('/')}}/internal/assets/img/loading/loading.gif" style="display: none;" />
                    </p>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    {{HTML::script('internal/assets/js/custom/system/intrusion.js')}}
@endsection