<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 2/09/16
 * Time: 2:53 PM
 */
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Giant Rentals | {{$pageName}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="description" content="Giant Rentals Control Panel">
    <meta name="keywords" content="giant rentals vehicle renting, scaffolding, scaffolds for rent, events items for rent,">
    <meta name="author" content="The Core">

    <!-- Base Css Files -->
{{HTML::style('internal/assets/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css')}}
{{HTML::style('internal/assets/libs/bootstrap/css/bootstrap.min.css')}}
{{HTML::style('internal/assets/libs/font-awesome/css/font-awesome.min.css')}}
{{HTML::style('internal/assets/libs/fontello/css/fontello.css')}}
{{HTML::style('internal/assets/libs/animate-css/animate.min.css')}}
{{HTML::style('internal/assets/libs/nifty-modal/css/component.css')}}
{{HTML::style('internal/assets/libs/magnific-popup/magnific-popup.css')}}
{{HTML::style('internal/assets/libs/ios7-switch/ios7-switch.css')}}
{{HTML::style('internal/assets/libs/pace/pace.css')}}
{{HTML::style('internal/assets/libs/sortable/sortable-theme-bootstrap.css')}}
{{HTML::style('internal/assets/libs/bootstrap-datepicker/css/datepicker.css')}}
{{HTML::style('internal/assets/libs/jquery-icheck/skins/all.css')}}
<!-- Code Highlighter for Demo -->
{{HTML::style('internal/assets/libs/prettify/github.css')}}

<!-- Extra CSS Libraries Start -->
{{HTML::style('internal/assets/libs/rickshaw/rickshaw.min.css')}}
{{HTML::style('internal/assets/libs/morrischart/morris.css')}}
{{HTML::style('internal/assets/libs/jquery-jvectormap/css/jquery-jvectormap-1.2.2.css')}}
{{HTML::style('internal/assets/libs/jquery-clock/clock.css')}}
{{HTML::style('internal/assets/libs/bootstrap-calendar/css/bic_calendar.css')}}
{{HTML::style('internal/assets/libs/sortable/sortable-theme-bootstrap.css')}}
{{HTML::style('internal/assets/libs/jquery-weather/simpleweather.css')}}
{{HTML::style('internal/assets/libs/bootstrap-xeditable/css/bootstrap-editable.css')}}
{{HTML::style('internal/assets/css/style.css')}}
<!-- Extra CSS Libraries End -->
{{HTML::style('internal/assets/css/style-responsive.css')}}
{{HTML::style('internal/assets/libs/fullcalendar/fullcalendar.css')}}

    @yield('styles')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="{{URL::To('/')}}/internal/assets/img/favicon.ico">
    <link rel="apple-touch-icon" href="{{URL::To('/')}}/internal/assets/img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::To('/')}}/internal/assets/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::To('/')}}/internal/assets/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::To('/')}}/internal/assets/img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{URL::To('/')}}/internal/assets/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="{{URL::To('/')}}/internal/assets/img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="{{URL::To('/')}}/internal/assets/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="{{URL::To('/')}}/internal/assets/img/apple-touch-icon-152x152.png" />
</head>
<body class="fixed-left">
{{--Required data--}}
<?php
    $infoId = User::where('id', Auth::user()->id)->pluck('info_id');
    $fname = UserInfo::where('id', $infoId)->pluck('fname');
    $lname = UserInfo::where('id', $infoId)->pluck('lname');
    $userAvatar = UserInfo::where('id', $infoId)->pluck('avatar');
    if ($userAvatar == 0) {
        $gender = DB::table('user_info')->where('id', $infoId)->pluck('gender');
        if($gender == null) {
            $userAvatar = "default";
        }
        else {
            $userAvatar = sprintf("default-%s", $gender);
        }

    }
?>
<!--Required data end-->

<!-- Modal Logout -->
<div class="md-modal md-just-me" id="logout-modal">
    <div class="md-content">
        <h3><strong>Logout</strong> Confirmation</h3>
        <div>
            <p class="text-center">Are you sure want to logout from Giant Rentals Control Panel?</p>
            <p class="text-center">
                <button class="btn btn-danger md-close">Nope!</button>
                <a href="{{URL::Route('get-internal-logout')}}" class="btn btn-success md-close">Yeah, I'm sure</a>
            </p>
        </div>
    </div>
</div>        <!-- Modal End -->

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">
        <div class="topbar-left">
            <div class="logo">
                <h1><a href="#"><img src="{{URL::To('/')}}/internal/assets/img/logo.png" alt="Logo"></a></h1>
            </div>
            <button class="button-menu-mobile open-left">
                <i class="fa fa-bars"></i>
            </button>
        </div>
        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-collapse2">
                    <ul class="nav navbar-nav hidden-xs">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-th"></i></a>
                            <div class="dropdown-menu grid-dropdown">
                                <div class="row stacked">
                                    <div class="col-xs-4">
                                        <a href="javascript:;" data-app="notes-app" data-status="active"><i class="icon-edit"></i>Notes</a>
                                    </div>
                                    <div class="col-xs-4">
                                        <a href="javascript:;" data-app="calc" data-status="inactive"><i class="fa fa-calculator"></i>Calculator</a>
                                    </div>
                                    <div class="col-xs-4">
                                        <a href="javascript:;" data-app="calendar-widget2" data-status="active"><i class="icon-calendar"></i>Calendar</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right top-navbar">
                        <li class="dropdown iconify hide-phone">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i><span class="label label-danger absolute">4</span></a>
                            <ul class="dropdown-menu dropdown-message">
                                <li class="dropdown-header notif-header"><i class="icon-bell-2"></i> New Notifications<a class="pull-right" href="#"><i class="fa fa-cog"></i></a></li>
                                <li class="unread">
                                    <a href="#">
                                        <p><strong>John Doe</strong> Uploaded a photo <strong>&#34;DSC000254.jpg&#34;</strong>
                                            <br /><i>2 minutes ago</i>
                                        </p>
                                    </a>
                                </li>
                                <li class="unread">
                                    <a href="#">
                                        <p><strong>John Doe</strong> Created an photo album  <strong>&#34;Fappening&#34;</strong>
                                            <br /><i>8 minutes ago</i>
                                        </p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <p><strong>John Malkovich</strong> Added 3 products
                                            <br /><i>3 hours ago</i>
                                        </p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <p><strong>Sonata Arctica</strong> Send you a message <strong>&#34;Lorem ipsum dolor...&#34;</strong>
                                            <br /><i>12 hours ago</i>
                                        </p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <p><strong>Johnny Depp</strong> Updated his avatar
                                            <br /><i>Yesterday</i>
                                        </p>
                                    </a>
                                </li>
                                <li class="dropdown-footer">
                                    <div class="btn-group btn-group-justified">
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-sm btn-primary"><i class="icon-ccw-1"></i> Refresh</a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-sm btn-danger"><i class="icon-trash-3"></i> Clear All</a>
                                        </div>
                                        <div class="btn-group">
                                            <a href="#" class="btn btn-sm btn-success">See All <i class="icon-right-open-2"></i></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown iconify hide-phone">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i><span class="label label-danger absolute">3</span></a>
                            <ul class="dropdown-menu dropdown-message">
                                <li class="dropdown-header notif-header"><i class="icon-mail-2"></i> New Messages</li>
                                <li class="unread">
                                    <a href="#" class="clearfix">
                                        <img src="images/users/chat/2.jpg" class="xs-avatar ava-dropdown" alt="Avatar">
                                        <strong>John Doe</strong><i class="pull-right msg-time">5 minutes ago</i><br />
                                        <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                    </a>
                                </li>
                                <li class="unread">
                                    <a href="#" class="clearfix">
                                        <img src="images/users/chat/1.jpg" class="xs-avatar ava-dropdown" alt="Avatar">
                                        <strong>Sandra Kraken</strong><i class="pull-right msg-time">22 minutes ago</i><br />
                                        <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="clearfix">
                                        <img src="images/users/chat/3.jpg" class="xs-avatar ava-dropdown" alt="Avatar">
                                        <strong>Zoey Lombardo</strong><i class="pull-right msg-time">41 minutes ago</i><br />
                                        <p>Duis autem vel eum iriure dolor in hendrerit ...</p>
                                    </a>
                                </li>
                                <li class="dropdown-footer"><div class=""><a href="#" class="btn btn-sm btn-block btn-primary"><i class="fa fa-share"></i> See all messages</a></div></li>
                            </ul>
                        </li>
                        <li class="dropdown iconify hide-phone"><a href="#" onclick="javascript:toggle_fullscreen()"><i class="icon-resize-full-2"></i></a></li>
                        <li class="dropdown topbar-profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="rounded-image topbar-profile-image"><img src="{{URL::To('/')}}/internal/assets/img/users/{{$userAvatar}}/user-35.jpg"></span> {{$fname}} <strong>{{$lname}}</strong> <i class="fa fa-caret-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">My Profile</a></li>
                                <li><a href="#">Change Password</a></li>
                                <li><a href="#">Account Setting</a></li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="icon-help-2"></i> Help</a></li>
                                <li><a href="lockscreen.html"><i class="icon-lock-1"></i> Lock me</a></li>
                                <li><a href="javascript:void(0);" class="md-trigger" data-modal="logout-modal"><i class="icon-logout-1"></i> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
    </div>
    <!-- Top Bar End -->
    <!-- Left Sidebar Start -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <div class="clearfix" style="margin-top: 20px;"></div>
            <!--- Profile -->
            <div class="profile-info">
                <div class="col-xs-4">
                    <a href="profile.html" class="rounded-image profile-image"><img src="{{URL::To('/')}}/internal/assets/img/users/{{$userAvatar}}/user-100.jpg"></a>
                </div>
                <div class="col-xs-8">
                    <div class="profile-text">Welcome <b>{{$lname}}</b></div>
                    <div class="profile-buttons">
                        <a href="{{URL::Route('get-internal-logout')}}" title="Sign Out"><i class="fa fa-power-off text-red-1"></i></a>
                    </div>
                </div>
            </div>
            <!--- Divider -->
            <div class="clearfix" style="margin-top: 10px;"></div>
            <hr class="divider" />
            <div class="clearfix" style="margin-top: 10px;"></div>
            <!--- Divider -->
            <div id="sidebar-menu">
                <?php
                    $categoryList = SystemPageCategory::where('status', 1)->lists('id');
                ?>
                <ul>
                @foreach($categoryList as $categoryId)
                    @if(SystemPageCategory::where('id', $categoryId)->pluck('has_sub') == 1)
                        <?php
                            $pageList = SystemPageList::where('category_id', $categoryId)->where('status', 1)->lists('id');
                        ?>

                        @if(UserPageAccess::where('page_id', SystemPageList::where('category_id', $categoryId)->pluck('id'))->where('user_id', Auth::user()->id)->pluck('id') || Auth::user()->roll == 1)
                            <li class='has_sub'>
                                <a href='{{URL::Route(SystemPageCategory::where('id', $categoryId)->pluck('url'))}}' @if($menuName == SystemPageCategory::where('id', $categoryId)->pluck('name')) class="subdrop" @endif>
                                    <i class='{{SystemPageCategory::where('id', $categoryId)->pluck('icon')}}'></i><span>{{SystemPageCategory::where('id', $categoryId)->pluck('name')}}</span> <span class="pull-right"><i class="fa fa-angle-down"></i></span>
                                </a>
                                <ul>
                                    @foreach($pageList as $pageId)
                                        @if(UserPageAccess::where('page_id', $pageId)->where('user_id', Auth::user()->id)->where('status', 1)->pluck('id') || Auth::user()->roll == 1)
                                            <li>
                                                <a href='{{URL::Route(SystemPageList::where('id', $pageId)->pluck('url'))}}'><span><i class="{{SystemPageList::where('id', $pageId)->pluck('icon')}}"></i> {{SystemPageList::where('id', $pageId)->pluck('name')}}</span></a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @else
                        @if(UserPageAccess::where('page_id', SystemPageList::where('category_id', $categoryId)->pluck('id'))->where('user_id', Auth::user()->id)->pluck('id') || Auth::user()->roll == 1)
                            <li>
                                <a href='{{SystemPageCategory::where('id', $categoryId)->pluck('url')}}' @if($menuName == SystemPageCategory::where('id', $categoryId)->pluck('name')) class="subdrop" @endif>
                                    <i class='{{SystemPageCategory::where('id', $categoryId)->pluck('icon')}}'></i><span>{{SystemPageCategory::where('id', $categoryId)->pluck('name')}}</span>
                                </a>
                            </li>
                        @endif
                    @endif
                @endforeach
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div><br><br><br>
        </div>
    </div>
    <!-- Left Sidebar End -->
    <!-- Notifications -->
    <div id="notification-container">
        {{--JQuery Append - public-functions.js--}}
    </div>

    <!-- End notifications -->
    <!-- Start right content -->
    <div class="content-page">
        <!-- ============================================================== -->
        <!-- Start Content here -->
        <!-- ============================================================== -->
        <div class="content">

            @yield('content')

            <!-- Footer Start -->
                <footer>
                    The Core Creative &copy; {{date('Y')}}
                    <div class="footer-links pull-right">
                        <a href="#">About</a><a href="#">Support</a><a href="#">Terms of Service</a><a href="#">Legal</a><a href="#">Help</a><a href="#">Contact Us</a>
                    </div>
                </footer>
                <!-- Footer End -->
        </div>
        <!-- ============================================================== -->
        <!-- End content here -->
        <!-- ============================================================== -->

    </div>
    <!-- End right content -->

</div>
<div id="contextMenu" class="dropdown clearfix">
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display:block;position:static;margin-bottom:5px;">
        <li><a tabindex="-1" href="javascript:;" data-priority="high"><i class="fa fa-circle-o text-red-1"></i> High Priority</a></li>
        <li><a tabindex="-1" href="javascript:;" data-priority="medium"><i class="fa fa-circle-o text-orange-3"></i> Medium Priority</a></li>
        <li><a tabindex="-1" href="javascript:;" data-priority="low"><i class="fa fa-circle-o text-yellow-1"></i> Low Priority</a></li>
        <li><a tabindex="-1" href="javascript:;" data-priority="none"><i class="fa fa-circle-o text-lightblue-1"></i> None</a></li>
    </ul>
</div>
<!-- End of page -->
<!-- the overlay modal element -->
<div class="md-overlay"></div>
<!-- End of eoverlay modal -->
<script>
    var resizefunc = [];
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->


{{HTML::script('internal/assets/libs/jquery/jquery-1.11.1.min.js')}}
{{HTML::script('internal/assets/libs/bootstrap/js/bootstrap.min.js')}}
{{HTML::script('internal/assets/libs/jqueryui/jquery-ui-1.10.4.custom.min.js')}}
{{HTML::script('internal/assets/libs/jquery-ui-touch/jquery.ui.touch-punch.min.js')}}
{{HTML::script('internal/assets/libs/jquery-detectmobile/detect.js')}}
{{HTML::script('internal/assets/libs/jquery-animate-numbers/jquery.animateNumbers.js')}}
{{HTML::script('internal/assets/libs/ios7-switch/ios7.switch.js')}}
{{HTML::script('internal/assets/libs/fastclick/fastclick.js')}}
{{HTML::script('internal/assets/libs/jquery-blockui/jquery.blockUI.js')}}
{{HTML::script('internal/assets/libs/bootstrap-bootbox/bootbox.min.js')}}
{{HTML::script('internal/assets/libs/jquery-slimscroll/jquery.slimscroll.js')}}
{{HTML::script('internal/assets/libs/jquery-sparkline/jquery-sparkline.js')}}
{{HTML::script('internal/assets/libs/nifty-modal/js/classie.js')}}
{{HTML::script('internal/assets/libs/nifty-modal/js/modalEffects.js')}}
{{HTML::script('internal/assets/libs/sortable/sortable.min.js')}}
{{HTML::script('internal/assets/libs/bootstrap-fileinput/bootstrap.file-input.js')}}
{{HTML::script('internal/assets/libs/bootstrap-select/bootstrap-select.min.js')}}
{{HTML::script('internal/assets/libs/bootstrap-select2/select2.min.js')}}
{{HTML::script('internal/assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}
{{HTML::script('internal/assets/libs/pace/pace.min.js')}}
{{HTML::script('internal/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.js')}}
{{HTML::script('internal/assets/libs/jquery-icheck/icheck.min.js')}}
{{HTML::script('internal/assets/js/moment.min.js')}}

{{HTML::script('internal/assets/js/custom/constant.js')}}
{{HTML::script('internal/assets/js/custom/public-functions.js')}}
<!-- Demo Specific JS Libraries -->
{{HTML::script('internal/assets/libs/prettify/prettify.js')}}

{{HTML::script('internal/assets/js/init.js')}}
<!-- Page Specific JS Libraries -->
{{HTML::script('internal/assets/js/apps/calculator.js')}}
{{HTML::script('internal/assets/js/apps/notes.js')}}
{{HTML::script('internal/assets/libs/jquery-notifyjs/notify.min.js')}}
{{HTML::script('internal/assets/libs/jquery-notifyjs/styles/metro/notify-metro.js')}}
{{HTML::script('/internal/assets/js/pages/notifications.js')}}
{{HTML::script('/internal/assets/js/custom/validator.js')}}
@yield('scripts')
</body>
</html>


