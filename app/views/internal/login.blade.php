<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 5/09/16
 * Time: 3:38 PM
 */
?>
        <!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login | Giant Rentals Control Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="author" content="The Core">

    <!-- Base Css Files -->
{{ HTML::style('internal/assets/libs/jqueryui/ui-lightness/jquery-ui-1.10.4.custom.min.css') }}
{{ HTML::style('internal/assets/libs/bootstrap/css/bootstrap.min.css') }}
{{ HTML::style('internal/assets/libs/font-awesome/css/font-awesome.min.css') }}
{{ HTML::style('internal/assets/libs/fontello/css/fontello.css') }}
{{ HTML::style('internal/assets/libs/animate-css/animate.min.css') }}
{{ HTML::style('internal/assets/libs/nifty-modal/css/component.css') }}
{{ HTML::style('internal/assets/libs/magnific-popup/magnific-popup.css') }}
{{ HTML::style('internal/assets/libs/ios7-switch/ios7-switch.css') }}
{{ HTML::style('internal/assets/libs/pace/pace.css') }}
{{ HTML::style('internal/assets/libs/sortable/sortable-theme-bootstrap.css') }}
{{ HTML::style('internal/assets/libs/bootstrap-datepicker/css/datepicker.css') }}
{{ HTML::style('internal/assets/libs/jquery-icheck/skins/all.css') }}
<!-- Code Highlighter for Demo -->
{{ HTML::style('internal/assets/libs/prettify/github.css') }}

<!-- Extra CSS Libraries Start -->
{{ HTML::style('internal/assets/css/style.css') }}
<!-- Extra CSS Libraries End -->
{{ HTML::style('internal/assets/css/style-responsive.css') }}

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ HTML::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
    {{ HTML::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}
    <![endif]-->

    <link rel="shortcut icon" href="internal/assets/img/favicon.ico">
    <link rel="apple-touch-icon" href="internal/assets/img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="internal/assets/img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="internal/assets/img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="internal/assets/img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="internal/assets/img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="internal/assets/img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="internal/assets/img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="internal/assets/img/apple-touch-icon-152x152.png" />
</head>
<body class="fixed-left login-page">


<!-- Begin page -->
<div class="container">
    <div class="full-content-center">
        <p class="text-center"><a href="#"><img src="internal/assets/img/login-logo.png" alt="Logo"></a></p>
        <div class="login-wrap animated flipInX">
            <form role="form" action="{{ URL::route('post-internal-login') }}" method="post" id="registerForm">
                <div class="login-block">
                    <img src="internal/assets/img/oms-logo.png" class="img-circle not-logged-avatar">
                    @if( Session::has('global') )
                        <p style="color:@if($errors->has('email') || Session::has('global') && Session::get('global') != 'Activated! you can now sign in' ) red @else #00ca6d @endif">* {{ Session::get('global') }}</p>
                    @endif
                    <form role="form" action="index.html">
                        <div class="form-group login-input">
                            <i class="fa fa-user overlay"></i>
                            <input id="email" name="email" type="email" class="form-control text-input" placeholder="Email" required="true">
                            @if($errors->has('email'))
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $emailerror=$errors->first('email'); }}</label>
                            @endif
                        </div>
                        <div class="form-group login-input">
                            <i class="fa fa-key overlay"></i>
                            <input id="password" name="password"  type="password" class="form-control text-input" placeholder="********" required="true">
                            @if($errors->has('password'))
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> {{ $emailerror=$errors->first('password'); }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="remember" id="remember"/>
                            <label for="remember">Remember me</label>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-lg-push-3">
                                <button type="submit" id="register" class="btn btn-success btn-block">LOGIN</button>
                            </div>
                        </div>
                    </form>
                </div>
            </form>

        </div>

    </div>
</div>

<!-- the overlay modal element -->


<div class="md-overlay"></div>
<!-- End of eoverlay modal -->
<script>
    var resizefunc = [];
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
{{ HTML::script('internal/assets/libs/jquery/jquery-1.11.1.min.js') }}
{{ HTML::script('internal/assets/libs/bootstrap/js/bootstrap.min.js') }}
{{ HTML::script('internal/assets/libs/jqueryui/jquery-ui-1.10.4.custom.min.js') }}
{{ HTML::script('internal/assets/libs/jquery-ui-touch/jquery.ui.touch-punch.min.js') }}
{{ HTML::script('internal/assets/libs/jquery-detectmobile/detect.js') }}
{{ HTML::script('internal/assets/libs/jquery-animate-numbers/jquery.animateNumbers.js') }}
{{ HTML::script('internal/assets/libs/ios7-switch/ios7.switch.js') }}
{{ HTML::script('internal/assets/libs/fastclick/fastclick.js') }}
{{ HTML::script('internal/assets/libs/jquery-blockui/jquery.blockUI.js') }}
{{ HTML::script('internal/assets/libs/bootstrap-bootbox/bootbox.min.js') }}
{{ HTML::script('internal/assets/libs/jquery-slimscroll/jquery.slimscroll.js') }}
{{ HTML::script('internal/assets/libs/jquery-sparkline/jquery-sparkline.js') }}
{{ HTML::script('internal/assets/libs/nifty-modal/js/classie.js') }}
{{ HTML::script('internal/assets/libs/nifty-modal/js/modalEffects.js') }}
{{ HTML::script('internal/assets/libs/sortable/sortable.min.js') }}
{{ HTML::script('internal/assets/libs/bootstrap-fileinput/bootstrap.file-input.js') }}
{{ HTML::script('internal/assets/libs/bootstrap-select/bootstrap-select.min.js') }}
{{ HTML::script('internal/assets/libs/bootstrap-select2/select2.min.js') }}
{{ HTML::script('internal/assets/libs/magnific-popup/jquery.magnific-popup.min.js') }}
{{ HTML::script('internal/assets/libs/pace/pace.min.js') }}
{{ HTML::script('internal/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.js') }}
{{ HTML::script('internal/assets/libs/jquery-icheck/icheck.min.js') }}

<!-- Demo Specific JS Libraries -->
{{ HTML::script('internal/assets/libs/prettify/prettify.js') }}

{{ HTML::script('internal/assets/js/init.js') }}
</body>
</html>