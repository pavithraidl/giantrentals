<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 30/08/2016 15:15.
 */
?>

<html>
<head>
    <title>Testing functions</title>
</head>
<body>
<h4>Vehicle Booking</h4><br/>
<button onclick="addVehicle();">Add Vehicle</button><br/>
<button onclick="calculatePrice();">Calculate Price</button><br/>
<button onclick="vehicleBooking();">Vehicle Booking</button><br/>
<button onclick="getPendingBookingList();">Vehicle Pending Booking</button><br/>
<button onclick="releaseVehicle();">Release Vehicle</button><br/>
<button onclick="getCurrentRentedList();">Current Rented Vehicles</button><br/>
<button onclick="returnVehicle();">Return Vehicle</button><br/>
<button onclick="getPendingBookingHistory();">Vehicle Booking History</button><br/>
<button onclick="deleteVehicleBooking();">Delete Vehicle Booking</button><br/>

<br/><br/>
<h4>Scaffoldings Booking</h4><br/>
<button onclick="addScaffoldingPart();">Add Scaffolding Part</button><br/>
<button onclick="addScaffoldingType();">Add Scaffolding Type</button><br/>
<button onclick="calculateScaffoldingTypePrice();">Calculate Price</button><br/>
<button onclick="addScaffoldingCartItems();">Add To Cart</button><br/>
<button onclick="bookScaffolding();">Book Scaffolding</button><br/>
<button onclick="getPendingScaffoldingBookingList();">Pending Scaffolding Booking List</button><br/>
<button onclick="releaseScaffolding();">Release Scaffolding</button><br/>
<button onclick="scaffoldingCurrentBookingList();">Current Booking List</button><br/>
<button onclick="returnScaffolding();">Return Scaffolding</button><br/>
<button onclick="scaffoldingHistory();">Scaffolding History</button><br/>
<button onclick="getSingleBookingElement();">Get Single Booking Element</button><br/>
<button onclick="getScaffoldingBookingTitles();">Get Booking Titles</button><br/>
<button onclick="getScaffoldingBookingCartList();">Get Single Booking Cart List</button><br/>

<br/><br/>
<h4>Users</h4>
<button onclick="postSignUp()">Sign Up</button><br/>
<button onclick="postActive()">Active</button><br/>
<button onclick="postLogin()">Login</button><br/>
<button onclick="getLogOut()">Log Out</button>

<br/><br/>
<h4>Events</h4>
<button onclick="addEventCategory()">Add Event Category</button><br/>
<button onclick="addEventItems()">Add Event Items</button><br/>
<button onclick="calculateEventItemPrice()">Calculate Event Item Price</button><br/>
<button onclick="addEventCartItems()">Add event cart items</button><br/>
<button onclick="bookEvent()">Book Event</button><br/>
<button onclick="pendingEventBookingList()">Pending Booking List</button><br/>
<button onclick="releaseEventItems()">Release Event Items</button><br/>
<button onclick="getCurrentEventsBookingList()">Current Event Booking List</button><br/>
<button onclick="returnEventItems()">Return Event Items</button><br/>
<button onclick="getEventBookingHistory()">Event Booking History</button><br/>
<button onclick="getSingleEventBookingElement()">Single Event Booking Element</button><br/>
<button onclick="getEventBookingTitles()">Event Booking Titles</button><br/>
<button onclick="getEventBookingCartList()">Event Booking Cart List</button><br/>

{{HTML::script('assets/js/jquery-3.1.0.min.js')}}
{{HTML::script('assets/js/testing.js')}}
</body>
</html>
