<?php
/************************| book-recorded.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 11/11/16 10:20.
 */
?>

@extends('frontend.master')

@section('content')
    <section id="content">
        <div class="section parallax dark nomargin noborder" style="background-color: #ffffff; margin-bottom: 50px !important;" data-stellar-background-ratio="0.2">
            <div class="row">
                <div class="col-md-12">
                    <h3 style="text-align: center; color: #de5e21; margin-top: 100px;">Thank you for your booking. We have recorded it successfully!</h3>
                    <p style="text-align: center;">
                        <a href="{{URL::Route('/')}}">
                            <button class="btn btn-lg" style="background-color: #f96421; color: #ffffff;">Home</button>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
