<?php
/************************| vehicle-booking-list.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/11/16 10:19.
 */
?>

<div class="booking-list-container" id="vehicle-booking-list-container" style="display: none; background-color: #eef1f8">
    <h4 style="color: #d76300;">Vehicle Booking List</h4>

    <div class="bs-callout bs-callout-success">
        <div class="table-responsive bottommargin">
            <table id="booking-table" class="table cart" style="color: #555555;">
                <thead>
                <tr>
                    <th class="cart-product-remove">&nbsp;</th>
                    <th class="cart-product-name">Vehicle Plate</th>
                    <th>From</th>
                    <th>To</th>
                    <th class="cart-product-price">Day Rate</th>
                    <th class="cart-product-quantity">Days</th>
                    <th class="cart-product-subtotal">Total</th>
                </tr>
                </thead>
                <tbody id="vehicle-result">
                {{--JQuery Append--}}
                </tbody>
            </table>
        </div>
        <h4 style="color: #d76300; text-align: right; margin-bottom: -10px; margin-top: -40px;">Total: &nbsp;&nbsp;<strong id="vehicle-inbox-total">$50.00</strong></h4>
    </div>
    <p id="add-another-vehicle-container" style="text-align: center; margin-bottom: -10px;">
        <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);" onclick="addMore();">Add Another Vehicle</button>
    </p>
</div>





