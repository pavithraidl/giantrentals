<?php
/************************| booking-totals.blade.php |***********************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 9/11/16 10:23.
 */
?>

<div class="col-md-6 clearfix pull-right">
    <div class="table-responsive">
        <h4 style="color: #343434;">Booking Totals</h4>

        <table class="table cart">
            <tbody>
            <tr id="vehicle-booking-sub-total" class="cart_item" style="display: none;">
                <td class="cart-product-name">
                    <strong style="color: #343434;">Vehicle Booking Subtotal</strong>
                </td>

                <td class="cart-product-name">
                    <span id="vehicle-total-amount" class="amount" style="color: #343434;">$106.94</span>
                </td>
            </tr>
            <tr id="scaffold-booking-sub-total" class="cart_item" style="display: none;">
                <td class="cart-product-name">
                    <strong style="color: #343434;">Scaffold Booking Subtotal</strong>
                </td>

                <td class="cart-product-name">
                    <span id="scaffold-total-amount" class="amount" style="color: #343434;">$106.94</span>
                </td>
            </tr>
            <tr id="event-booking-sub-total" class="cart_item" style="display: none;">
                <td class="cart-product-name">
                    <strong style="color: #343434;">Events Booking Subtotal</strong>
                </td>

                <td class="cart-product-name">
                    <span id="event-total-amount" class="amount" style="color: #343434;">$106.94</span>
                </td>
            </tr>
            <tr class="cart_item">
                <td class="cart-product-name">
                    <strong style="color: #343434;">Total</strong>
                </td>

                <td class="cart-product-name">
                    <span class="amount color lead" style="color: #343434;"><strong id="full-total">$106.94</strong></span>
                </td>
            </tr>
            </tbody>

        </table>

    </div>
</div>
