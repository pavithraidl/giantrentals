<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 16/10/16
 * Time: 10:17
 */
?>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    {{HTML::style('assets/css/bootstrap.css')}}
    {{HTML::style('assets/style.css')}}
    {{HTML::style('assets/css/dark.css')}}
    {{HTML::style('assets/css/font-icons.css')}}
    {{HTML::style('assets/css/animate.css')}}
    {{HTML::style('assets/css/magnific-popup.css')}}
    {{HTML::style('assets/css/custom.css')}}

    {{HTML::style('assets/css/responsive.css')}}
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script src="//code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    {{HTML::style('assets/css/components/radio-checkbox.css')}}
    <![endif]-->

    <!-- External JavaScripts
============================================= -->
    {{HTML::script('assets/js/jquery.js')}}
    {{HTML::script('assets/js/plugins.js')}}


    <!-- Document Title
    ============================================= -->
    <title>{{$page}} - Giant Rentals</title>

    <style type="text/css">
        .bs-callout {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }

        .bs-callout-top {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-top-width: 5px;
            border-radius: 3px;
        }

        .bs-callout h4 {
            margin-top: 0;
            margin-bottom: 5px;
        }

        .bs-callout p:last-child {
            margin-bottom: 0;
        }

        .bs-callout code {
            border-radius: 3px;
        }

        .bs-callout + .bs-callout {
            margin-top: -5px;
        }

        .bs-callout-default {
            border-left-color: #777;
        }

        .bs-callout-top-default {
            border-top-color: #777;
        }

        .bs-callout-default h4 {
            color: #777;
        }

        .bs-callout-primary {
            border-left-color: #428bca;
        }

        .bs-callout-top-primary {
            border-top-color: #428bca;
        }

        .bs-callout-primary h4 {
            color: #428bca;
        }

        .bs-callout-success {
            border-left-color: #f96421;
        }

        .bs-callout-top-success {
            border-top-color: #f96421;
        }

        .bs-callout-success h4 {
            color: #f96421;
        }

        .bs-callout-danger {
            border-left-color: #d9534f;
        }

        .bs-callout-top-danger {
            border-top-color: #d9534f;
        }

        .bs-callout-danger h4 {
            color: #d9534f;
        }

        .bs-callout-warning {
            border-left-color: #f0ad4e;
        }

        .bs-callout-top-warning {
            border-top-color: #f0ad4e;
        }

        .bs-callout-warning h4 {
            color: #f0ad4e;
        }

        .bs-callout-info {
            border-left-color: #5bc0de;
        }

        .bs-callout-top-info {
            border-top-color: #5bc0de;
        }

        .bs-callout-info h4 {
            color: #5bc0de;
        }
    </style>
</head>

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    <header id="header" class="transparent-header">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">
                    <a href="index.html" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="{{URL::To('/')}}/assets/images/logo.png" alt="Giant Rentals Logo"></a>
                    <a href="index.html" class="retina-logo" data-dark-logo="{{URL::To('/')}}/assets/images/logo-dark@2x.png"><img src="{{URL::To('/')}}/assets/images/logo@2x.png" alt="Canvas Logo"></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu"@if($page == 'Home') class="home"@endif>

                    <ul>
                        <li @if($page == 'Home')class="current"@endif><a href="{{URL::Route('/')}}"><div>Home</div></a></li>
                        <li @if($page == 'Vehicle')class="current"@endif><a href="{{URL::Route('book/vehicle')}}"><div>Vehicle</div></a></li>
                        <li @if($page == 'Scaffold')class="current"@endif><a href="{{URL::Route('book/scaffolding')}}"><div>Scaffold</div></a></li>
                        <li @if($page == 'Event Gear')class="current"@endif><a href="{{URL::Route('book/event-gear')}}"><div>Events Gear</div></a></li>
                        <li @if($page == 'My Account')class="current"@endif><a href="{{URL::Route('my-account')}}"><div>My Account</div></a></li>
                        <li @if($page == 'Contact Us')class="current"@endif><a href="{{URL::Route('contact-us')}}"><div>Contact Us</div></a></li>
                    </ul>

                    <!-- Top Cart
                    ============================================= -->
                    <div id="top-cart">
                        <a href="#" id="top-cart-trigger"></a>
                        <div class="top-cart-content">
                            <div class="top-cart-title">
                                <h4>Booking Cart</h4>
                            </div>
                            <div id="cart-items" class="top-cart-items">
                                <p id="cart-loading-image" style="text-align: center; margin-top: 20px;">
                                    <img src="{{URL::To('/')}}/assets/images/loading/default.gif" />
                                </p>
                            </div>
                            <div class="top-cart-action clearfix">
                                <span id="total-price" class="fleft top-checkout-price"></span>
                                <button onclick="confirmBooking();" class="button button-3d button-small nomargin fright">Confirm Booking</button>
                            </div>
                        </div>
                    </div><!-- #top-cart end -->

                    {{--<!-- Top Search--}}
                    {{--============================================= -->--}}
                    <div id="top-search">
                        <a href="#"><i class="icon-user"></i><i class="icon-line-cross"></i></a>
                    </div><!-- #top-search end -->

                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header><!-- #header end -->

    @yield('content')

<!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

        <div class="container">

            <!-- Footer Widgets
            ============================================= -->
            <div class="footer-widgets-wrap clearfix">

                <div class="col_two_third">

                    <div class="col_one_third">

                        <div class="widget clearfix">

                            <img src="{{URL::To('/')}}/assets/images/footer-logo.png" style="margin-left: -30px; margin-top: 0; margin-bottom: 10px;" alt="" class="footer-logo">

                            <p>We believe in <strong>Simple</strong>, <strong>Creative</strong> &amp; <strong>Flexible</strong> Design Standards.</p>

                            <div style="background: url('{{URL::To('/')}}/assets/images/world-map.png') no-repeat center center; background-size: 100%;">
                                <address>
                                    <strong>Location:</strong><br>
                                    111 Newton Road<br>
                                    Eden terrace, Auckland<br>
                                </address>
                                <strong>Phone:</strong> <a href="tel:0800002386">0800 002 386</a><br>
                                <strong>Email:</strong> <a href="mailto:info@giantrentals.com">info@giantrentals.com</a>
                            </div>

                        </div>

                    </div>

                    <div class="col_one_third">

                        <div class="widget widget_links clearfix">
                            <h4>Quick Links</h4>
                            <ul>
                                <li><a href="http://codex.wordpress.org/">Home</a></li>
                                <li><a href="http://wordpress.org/support/forum/requests-and-feedback">Vehicle</a></li>
                                <li><a href="http://wordpress.org/extend/plugins/">Scaffold</a></li>
                                <li><a href="http://wordpress.org/support/">Events</a></li>
                                <li><a href="http://wordpress.org/extend/themes/">My Account</a></li>
                                <li><a href="http://wordpress.org/news/">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div><!-- .footer-widgets-wrap end -->

        </div>

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights">

            <div class="container clearfix">

                <div class="col_half">
                    Copyrights &copy; {{date('Y')}} All Rights Reserved by Giant Rentals.<br>
                </div>

                <div class="col_half col_last tright">
                    <div class="fright clearfix">
                        <a href="#" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-gplus">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-pinterest">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-vimeo">
                            <i class="icon-vimeo"></i>
                            <i class="icon-vimeo"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-github">
                            <i class="icon-github"></i>
                            <i class="icon-github"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-yahoo">
                            <i class="icon-yahoo"></i>
                            <i class="icon-yahoo"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-linkedin">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                    </div>

                    <div class="clear"></div>

                    <i class="icon-envelope2"></i> info@giantrentals.co.nz <span class="middot">&middot;</span> <i class="icon-headphones"></i> +64-80-000-2759 <span class="middot">&middot;</span> <i class="icon-skype2"></i> GiantOnSkype
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->


<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- Footer Scripts
============================================= -->
{{HTML::script('assets/js/functions.js')}}
{{HTML::script('internal/assets/js/custom/constant.js')}}
{{HTML::script('assets/js/custom/validator.js')}}
{{HTML::script('assets/js/custom/frontend/public.js')}}
@yield('script')

</body>
</html>


