<?php
/**
 * Created by PhpStorm.
 * User: pavit
 * Date: 02/11/2016
 * Time: 00:57
 */
?>

@extends('frontend.master')

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h2 style="text-align: center; color: #F06421 !important;">My Account</h2>
            <div class="tabs nobottommargin clearfix" id="sidebar-tabs">

                <ul class="tab-nav clearfix">
                    <li><a href="#tabs-1">Job Sheets</a></li>
                    <li><a href="#tabs-2">Vehicles</a></li>
                    <li><a href="#tabs-3">Scaffold</a></li>
                    <li><a href="#tabs-4">Events</a></li>
                </ul>

                <div class="tab-container">

                    <div class="tab-content clearfix" id="tabs-1">
                        <div id="popular-post-list-sidebar">
                            <div class="job-sheet-list-container">
                                <div class="job-sheet-ul">
                                    <div class="job-sheet-li">
                                        <div class="col-md-12">
                                            <div class="col-xs-2">
                                                <div class="col-md-1 col-sm-1 col-xs-3 flip3D">
                                                    <div id="s-i-circle-tag-22" class="circle-sm front" style="background-color: #2DB42D">
                                                        <p style="text-align: center;">A</p>
                                                    </div>
                                                    <div class="circle-sm back" onclick="deleteInvoice();" data-toggle="tooltip" title="" style="background-color: #ff3d35" data-original-title="Delete Invoice">
                                                        <p style="text-align: center;"><i class="icon-trash"></i></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 job-number">
                                                JN-0008892
                                            </div>
                                            <div class="col-xs-5 job-total" style="width: 100% !important;">
                                                $1200.00
                                            </div>
                                        </div>
                                        <div class="job-detail-container">

                                        </div>
                                    </div>
                                    <div class="job-sheet-li">
                                        <div class="col-md-12">
                                            <div class="col-xs-2">
                                                <div class="col-md-1 col-sm-1 col-xs-3 flip3D">
                                                    <div id="s-i-circle-tag-22" class="circle-sm front" style="background-color: #2DB42D">
                                                        <p style="text-align: center;">A</p>
                                                    </div>
                                                    <div class="circle-sm back" data-toggle="tooltip" title="" style="background-color: #ff3d35" data-original-title="Delete Invoice">
                                                        <p style="text-align: center;"><i class="icon-trash"></i></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 job-number">
                                                JN-0008892
                                            </div>
                                            <div class="col-xs-5 job-total" style="width: 100% !important;">
                                                $1200.00
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content clearfix" id="tabs-2">
                        <div id="recent-post-list-sidebar">
                            test vehicle tab
                        </div>
                    </div>
                    <div class="tab-content clearfix" id="tabs-3">
                        <div id="recent-post-list-sidebar">
                            test scaffold tab
                        </div>
                    </div>
                    <div class="tab-content clearfix" id="tabs-4">
                        <div id="recent-post-list-sidebar">
                            test event tab
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
