<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 16/10/16
 * Time: 10:17
 */

?>

@extends('frontend.master')


@section('content')
    <section id="slider" class="slider-parallax with-header clearfix" style="height:640px">

        <div class="slider-parallax-inner" style="min-height: 580px !important; background: url('/assets/images/auckland.jpg') center no-repeat; background-size: cover;">

            <div class="force-full-screen"> {{--background: url('{{URL::To('/')}}/assets/images/parallax/home/9.jpg') center center no-repeat; --}}

                <div class="container clearfix">
                    <h3 class="slider-header" style="font-size: 64px;">Rentals Made Easy</h3>
                    <div class="cell-container" style="margin-top: 90px;">
                        <div class="hero-item">
                            <div class="hero-icon"></div>
                            <a href="{{URL::Route('book/vehicle')}}"><button class="btn btn-hero">Vehicles</button></a>

                        </div>
                        <div class="hero-item">
                            <div class="hero-icon"></div>
                            <a href="{{URL::Route('book/scaffolding')}}"><button class="btn btn-hero">Scaffolding</button></a>
                        </div>
                        <div class="hero-item">
                            <div class="hero-icon"></div>
                            <a href="{{URL::Route('book/event-gear')}}"><button class="btn btn-hero">Event Gear</button></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>

    <div class="clear"></div>

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap nopadding">

            <div class="section parallax dark nomargin noborder" style="background-color: #ffffff; margin-bottom: 50px !important;" data-stellar-background-ratio="0.4">
                <div class="container clearfix">
                    <div class="col-md-10" style="text-align: left;">
                        <h3 class="section-title">Vehicles</h3>
                        <p class="section-content">
                            We provide a large fleet of Toyota HiAce vans
                        </p>
                    </div>
                    <div class="col-md-10 col-md-offset-2" style="text-align: right; margin-top:-50px">
                        <h3 class="section-title">Scaffolding</h3>
                        <p class="section-content">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </p>
                    </div>
                    <div class="col-md-10" style="text-align: left; margin-top:-50px">
                        <h3 class="section-title">Event Gear</h3>
                        <p class="section-content">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                        </p>
                    </div>
                            {{-- <button class="btn btn-lg" style="background-color: #f06421; color: #ffffff; padding-left: 50px; padding-right: 50px; font-weight: 600; margin-top: 10px;">Informative button is here</button> --}}
                    </div>
                </div>
            </div>

            <div class="col-md-12 nopadding common-height">

                <div class="col-md-4 dark col-padding ohidden" style="background-color: #F06421;">
                    <div>
                        <h3 class="uppercase" style="font-weight: 600;">Why choose Us</h3>
                        <p style="line-height: 1.8;">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
                        <a href="#" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
                        <i class="icon-bulb bgicon"></i>
                    </div>
                </div>

                <div class="col-md-4 dark col-padding ohidden" style="background-color: #34495e;">
                    <div>
                        <h3 class="uppercase" style="font-weight: 600;">Our Mission</h3>
                        <p style="line-height: 1.8;">Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
                        <a href="#" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
                        <i class="icon-cog bgicon"></i>
                    </div>
                </div>

                <div class="col-md-4 dark col-padding ohidden" style="background-color: #e74c3c;">
                    <div>
                        <h3 class="uppercase" style="font-weight: 600;">What you get</h3>
                        <p style="line-height: 1.8;">Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
                        <a href="#" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
                        <i class="icon-thumbs-up bgicon"></i>
                    </div>
                </div>

                <div class="clear"></div>

            </div>

            <div class="clear"></div>

            <div class="section nomargin noborder" style="background-image: url('{{URL::To('/')}}/assets/images/parallax/3.jpg');">
                <div class="heading-block center nobottomborder nobottommargin">
                    <h2>"Some large title is appear here as well for display purpose"</h2>
                </div>
            </div>
        </div>

    </section><!-- #content end -->

@endsection

@section('')