<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 18/10/16
 * Time: 11:54
 */
?>

@extends('frontend.master')

@section('content')
    <section id="content">
        <div class="section parallax dark nomargin noborder" style="background-color: #ffffff; margin-bottom: 50px !important;" data-stellar-background-ratio="0.2">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="rent-form-container" style="min-width: 500px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="tab-event" class="col-md-12 active-booking booking-vehicle">
                                        Sign In
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-input col-md-6" style="margin-bottom: -35px;">
                                    <label>First Name</label>
                                    <input type="text" id="txt-first-name" onblur="firstNameValidator(); firstNameMinLengthValidator();" placeholder="First Name..."/>
                                </div>
                                <div class=" form-input col-md-6 col_last">
                                    <label>Last Name</label>
                                    <input type="text" id="txt-last-name" onblur="lastNameValidator(); lastNameMinLengthValidator();" placeholder="Last Name..."/>
                                </div>
                                <div class="form-input col-md-6" style="margin-top: -10px;">
                                    <label>Phone</label>
                                    <input type="text" id="txt-phone-number" onblur="phoneValidator();" placeholder="Phone Number..."/>
                                </div>
                                <div class=" form-input col-md-6 col_last" style="margin-top: -10px;">
                                    <label>Drivers Licence</label>
                                    <input type="text" id="txt-licence" onblur="licenceValidator();" placeholder="Drivers Licence ID"/>
                                </div>
                                <div class=" form-input col-md-12" style="margin-top: -10px;">
                                    <label>Email Address</label>
                                    <input type="email" id="txt-email" onblur="emailValidator(this); emailAddressValidator();" placeholder="Email..."/>
                                </div>
                                <div class=" form-input col-md-12" style="margin-top: -10px;">
                                    <label>Password</label>
                                    <input type="password" id="txt-password" onblur="passwordValidator(); minLengthValidator();" placeholder="Password..."/>
                                </div>
                                <div class=" form-input col-md-12" style="margin-top: -10px;">
                                    <label>Confim Password</label>
                                    <input type="password" onkeyup="matchPassword(this, 'txt-password');" id="txt-confirm-password" onblur="confirmPasswordValidator();" placeholder="Confirm Password..."/>
                                </div>
                                <div class="col-md-12">
                                    <p style="text-align: center; margin-top: 20px;">
                                        <button id="btn-event" class="btn btn-lg" style="background-color: #F06421; margin-bottom: -10px; display: inline-block;" onclick="createAccount();">Create Account</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('script')
    {{HTML::script('assets/js/custom/frontend/signup.js')}}
@endsection
