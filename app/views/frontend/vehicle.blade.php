@extends('frontend.master')

@section('content')
	<section id="content">
		<div class="section parallax dark nomargin noborder" style="background-color: #ffffff; margin-bottom: 50px !important;" data-stellar-background-ratio="0.2">
			<div class="container clearfix">
				<div class="col-md-10 col-md-offset-1" style="margin-top: -100px;">
					<div class="col-md-6">
						<div class="rent-form-container">
							<div class="row">
								<div class="col-md-12">
									<div id="tab-vehicle" class="col-md-12 active-booking booking-vehicle" onclick="selectVehicle();">
										Book Vehicle
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-input" style="margin-bottom: -35px;">
									<label>Vehicle Type</label>
									<select>
										<option>Toyota HiAce Van</option>
									</select>
								</div>
								<div class="form-input">
									<label>Pick-up Date</label>
									<div class="col-md-8">
										<input id="pickup-date" type="date" value="{{date('Y-m-d')}}" min="{{date('Y-m-d')}}"/>
									</div>
									<div class="col-md-4">
										<select id="pickup-time" class="time-select">
											<option id="vehicle-pickup-time-8" value="8">08:00</option>
											<option id="vehicle-pickup-time-9"  value="9">09:00</option>
											<option id="vehicle-pickup-time-10"  value="10">10:00</option>
											<option id="vehicle-pickup-time-11"  value="11">11:00</option>
											<option id="vehicle-pickup-time-12"  value="12">12:00</option>
											<option id="vehicle-pickup-time-13"  value="13">13:00</option>
											<option id="vehicle-pickup-time-14"  value="14">14:00</option>
											<option id="vehicle-pickup-time-15"  value="15">15:00</option>
											<option id="vehicle-pickup-time-16"  value="16">16:00</option>
											<option id="vehicle-pickup-time-17"  value="17">17:00</option>
											<option id="vehicle-pickup-time-18"  value="18">18:00</option>
										</select>
										<i class="icon-time input-time-icon"></i>
									</div>
								</div>
								<div class="form-input">
									<label>Return Date</label>
									<div class="col-md-8">
										<input id="return-date" type="date" value="{{date('Y-m-d')}}" min="{{date('Y-m-d')}}"/>
									</div>
									<div class="col-md-4">
										<select id="return-time" class="time-select">
											<option class="booking-time-option" id="vehicle-return-time-8" disabled  value="8">08:00</option>
											<option class="booking-time-option" id="vehicle-return-time-9" value="9">09:00</option>
											<option class="booking-time-option" id="vehicle-return-time-10" value="10">10:00</option>
											<option class="booking-time-option" id="vehicle-return-time-11" value="11">11:00</option>
											<option class="booking-time-option" id="vehicle-return-time-12" value="12">12:00</option>
											<option class="booking-time-option" id="vehicle-return-time-13" value="13">13:00</option>
											<option class="booking-time-option" id="vehicle-return-time-14" value="14">14:00</option>
											<option class="booking-time-option" id="vehicle-return-time-15" value="15">15:00</option>
											<option class="booking-time-option" id="vehicle-return-time-16" value="16">16:00</option>
											<option class="booking-time-option" id="vehicle-return-time-17" value="17">17:00</option>
											<option class="booking-time-option" id="vehicle-return-time-18" value="18">18:00</option>
										</select>
										<i class="icon-time input-time-icon"></i>
									</div>
								</div>
								<div class="form-input col-md-5" id="age-container" style="margin-bottom: -20px;">
									<label>Driver's Age</label>
									<select class="age-select">
										<option>21 and Over</option>
										<option>Under 21</option>
									</select>
								</div>
								<div class="form-input col_half col_last col-md-offset-1" id="licence-number" style="margin-bottom: 10px; display: inline-block;">
									<input id="licence" class="checkbox-style" name="checkbox-10" type="checkbox" style="position: absolute; top: 5px; left: -15px;">
									<label for="checkbox-10" class="checkbox-style-3-label" style="display: inline-block;">I have NZ Full Driver's Licence</label>
								</div>
								<div class="col-md-12">
									<p style="text-align: center; margin-top: 20px;">
										<button id="btn-vehicle" class="btn btn-lg" style="background-color: #F06421; margin-top: -10px; display: inline-block;" onclick="findMyVehicle();">Add Vehicle</button>
									</p>
								</div>
							</div>
						</div>
					</div>
						<div class="col-md-6 hidden-sm hidden-xs">
							<img src="{{URL::To('/')}}/assets/images/dragon.png" style="max-width: 150% !important;width: 120% !important;margin-top: 120px;margin-left: 40px;" />
						</div>
				</div>
			</div>
			<div class="row">
				<p style="text-align: center;">
					<img id="booking-table-loading-img" src="{{URL::To('/')}}/assets/images/loading.gif" style="margin-top: 50px; margin-bottom: -100px; display: none;"/>
				</p>
				<div class="col-md-10 col-md-offset-1 booking-results" style="display: none;" id="booking-result">
					<div class="content-wrap">
						<div class="container clearfix">
							@include('frontend.include.vehicle-booking-list')
							@include('frontend.include.scaffold-booking-list')
							@include('frontend.include.event-booking-list')
							@include('frontend.include.booking-totals')
						</div>
						<p style="text-align: right;">
							<a style="color: rgb(255, 255, 255);" href="{{URL::Route('book/scaffolding')}}">
								<button id="btn-book-event" class="btn btn-lg" style="background-color: rgba(249, 100, 33, 0.7);">Book Scaffold</button>
							</a>
							<a style="color: rgb(255, 255, 255);" href="{{URL::Route('book/event-gear')}}">
								<button id="btn-book-event" class="btn btn-lg" style="background-color: rgba(249, 100, 33, 0.7);">Book Event Gear</button>
							</a>
							<a style="color: #ffffff;" href="{{URL::Route('book/confirm')}}">
								<button id="btn-book-this-event" class="btn btn-lg btn-success" style="background-color: #F06421; border-color: #b75c00">
									Place Booking &nbsp;&nbsp;<i style="font-size: 18px;" class="icon icon-angle-right"></i>
								</button>
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('script')
	{{HTML::script('assets/js/custom/frontend/vehicle.js')}}
@endsection