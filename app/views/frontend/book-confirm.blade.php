<?php
/**
 * Created by PhpStorm.
 * User: Isuru
 * Date: 18/10/16
 * Time: 11:54
 */
?>

@extends('frontend.master')

@section('content')
    <section id="content">
        <?php
            $pageType = 1;
        if(!isset($_SESSION))
        {
            session_start();
        }
        if(isset($_SESSION['session_id'])) {
            $sessionId = $_SESSION['session_id'];
            $jobId = BookingJob::where('session_id', $sessionId)->pluck('id');
        }
        if($jobId) {

        }
        else {
            $pageType = 0;
        }
        ?>
        @if($pageType == 1)
        <div class="row">
            <h2 style="text-align: center; color: #de5e21; margin-bottom: -50px; margin-top: 30px;">Confirm Booking</h2>
        </div>
        <div class="content-wrap">
            <div id="booking-result" class="col-md-10 col-md-offset-1" style="margin-bottom: 100px; !important;">
                <div class="col-md-8">
                    @include('frontend.include.vehicle-booking-list')
                    @include('frontend.include.scaffold-booking-list')
                    @include('frontend.include.event-booking-list')
                </div>
                <div class="col-md-4"  style="margin-top: 30px;">
                    <div class="table-responsive" style="margin-bottom: 50px;">
                        <h4 style="color: #343434;">Booking Totals</h4>
                        <table class="table cart">
                            <tbody>
                            <tr id="vehicle-booking-sub-total" class="cart_item" style="display: none;">
                                <td class="cart-product-name">
                                    <strong style="color: #343434;">Vehicle Booking Subtotal</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span id="vehicle-total-amount" class="amount" style="color: #343434;">$106.94</span>
                                </td>
                            </tr>
                            <tr id="scaffold-booking-sub-total" class="cart_item" style="display: none;">
                                <td class="cart-product-name">
                                    <strong style="color: #343434;">Scaffold Booking Subtotal</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span id="scaffold-total-amount" class="amount" style="color: #343434;">$106.94</span>
                                </td>
                            </tr>
                            <tr id="event-booking-sub-total" class="cart_item" style="display: none;">
                                <td class="cart-product-name">
                                    <strong style="color: #343434;">Events Booking Subtotal</strong>
                                </td>

                                <td class="cart-product-name">
                                    <span id="event-total-amount" class="amount" style="color: #343434;">$106.94</span>
                                </td>
                            </tr>
                            <tr class="cart_item">
                                <td class="cart-product-name">
                                    <strong style="color: #343434;">Total</strong>
                                </td>
                                <td class="cart-product-name">
                                    <span class="amount color lead" style="color: #343434;"><strong id="full-total">$106.94</strong></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="test-id" class="col_half">
                        <label for="register-form-fname">First Name:</label>
                        <input type="text" id="guest-form-fname" onblur="requiredValidator(this);" value="" class="form-control" />
                    </div>
                    <div class="col_half col_last">
                        <label for="register-form-lname">Last Name:</label>
                        <input type="text" id="guest-form-lname"onblur="requiredValidator(this);"  value="" class="form-control" />
                    </div>
                    <div class="col_half">
                        <label for="register-form-phone">Phone:</label>
                        <input type="text" id="guest-form-phone" onblur="requiredValidator(this);" value="" class="form-control" />
                    </div>
                    <div class="col_half col_last">
                        <label for="register-drivers-licence">Drivers licence ID:</label>
                        <input class="form-control" id="guest-form-licence" type="text" onblur="requiredValidator(this);" placeholder="" />
                    </div>
                    <div class="col_full">
                        <label for="register-form-email">Email Address:</label>
                        <input type="text" id="guest-form-email" value="" onblur="requiredValidator(this); emailValidator(this);" class="form-control" />
                    </div>
                    <div class="col_full nobottommargin" style="text-align: center;">
                        <button class="button button-3d button-black nomargin" onclick="submitGuestForm();" id="register-form-submit">Confirm Booking <i class="icon-angle-right"></i> </button>
                        <img id="confirm-booking-loading-img" src="{{URL::Route('/')}}/assets/images/loading.gif" style="display: none;" />
                    </div>

                    <input type="hidden" id="jobid" value="{{$jobId}}" />
                </div>
            </div>
        </div>
        @else
            <?php
                    $url = URL::Route('/');
                header("Location: $url"); /* Redirect browser */
                exit();
            ?>
        @endif
    </section><!-- #content end -->
@endsection

@section('script')
    {{HTML::script('assets/js/custom/booking-info.js')}}
@endsection