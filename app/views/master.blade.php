<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 29/08/2016 10:41.
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Giant Rentals | {{$pageName}}</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    {{HTML::style('assets/booking/css/booking.css')}}
    {{HTML::style('assets/css/camera.css')}}
    {{HTML::style('assets/css/owl.carousel.css')}}
    {{HTML::style('assets/css/style.css')}}
    {{HTML::script('assets/js/jquery.js')}}
    {{HTML::script('assets/js/jquery-migrate-1.2.1.js')}}
    {{HTML::script('assets/js/script.js')}}
    {{HTML::script('assets/js/superfish.js')}}
    {{HTML::script('assets/js/jquery.ui.totop.js')}}
    {{HTML::script('assets/js/jquery.equalheights.js')}}
    {{HTML::script('assets/js/jquery.mobilemenu.js')}}
    {{HTML::script('assets/js/jquery.easing.1.3.js')}}
    {{HTML::script('assets/js/owl.carousel.js')}}
    {{HTML::script('assets/js/camera.js')}}
            <!--[if (gt IE 9)|!(IE)]><!-->
    {{HTML::script('assets/js/jquery.mobile.customized.min.js')}}
            <!--<![endif]-->
    {{HTML::script('assets/booking/js/booking.js')}}

    {{--tab icon--}}
    <link rel="icon" href="{{URL::To('/')}}/assets/images/fav-icon.png">

    <script>
        $(document).ready(function(){
            jQuery('#camera_wrap').camera({
                loader: false,
                pagination: false ,
                minHeight: '444',
                thumbnails: false,
                height: '28.28125%',
                caption: true,
                navigation: true,
                fx: 'mosaic'
            });
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
    <!--[if lt IE 9]>
    {{HTML::script('assets/js/html5shiv.js')}}
    {{HTML::style('assets/css/ie.css')}}
    <![endif]-->
</head>
<body class="page1" id="top">
<div class="main">
    <!--==============================header=================================-->
    <header>
        <div class="menu_block ">
            <div class="container_12">
                <div class="grid_12">
                    <h1 style="height: 100px; width: auto; position: absolute !important; top: -50px; left: -20px;">
                        <a href="index.html">
                            <img src="{{URL::To('/')}}/assets/images/logo-text.png" alt="Giant Rentals Logo">
                        </a>
                    </h1>
                    <nav class="horizontal-nav full-width horizontalNav-notprocessed">
                        <ul class="sf-menu" style="margin-left: 300px;">
                            <li @if($pageName == 'Home')class="current"@endif><a href="index.html">Home</a></li>
                            <li @if($pageName == 'Scaffoldings')class="current"@endif><a href="index-1.html">Scaffoldings</a></li>
                            <li @if($pageName == 'Vehicles')class="current"@endif><a href="index-2.html">Vehicles</a></li>
                            <li @if($pageName == 'Events')class="current"@endif><a href="index-3.html">Events</a></li>
                            <li @if($pageName == 'About Us')class="current"@endif><a href="index-3.html">About Us</a></li>
                            <li @if($pageName == 'Contacts')class="current"@endif><a href="index-4.html">Contacts</a></li>
                        </ul>
                    </nav>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        @if($pageName == 'Home')
        <div class="container_12">
            <div class="grid_12">
                <h1>
                    <a href="index.html">
                        <img src="{{URL::To('/')}}/assets/images/logo.png" alt="Giant Rentals Logo">
                    </a>
                </h1>
            </div>
        </div>
        <div class="clear"></div>
        @endif

    </header>

    {{--Yield Content Here--}}
        @yield('content')
    {{--Yield Content end here--}}

            <!--==============================footer=================================-->
    <footer>
        <div class="container_12">
            <div class="grid_12">
                <div class="f_phone"><span>Call Us:</span><a href="tel:0800002759"> 0800 002 759</a></div>
                <div class="socials">
                    <a href="#" class="fa fa-twitter"></a>
                    <a href="#" class="fa fa-facebook"></a>
                    <a href="#" class="fa fa-google-plus"></a>
                </div>
                <div class="copy">
                    <div class="st1">
                        <div class="brand"><span class="color1">G</span>iant <span class="color1">R</span>entals </div>
                        &copy; {{date('Y')}}	| <a href="#">Privacy Policy</a> </div> Website designed by <a href="http://www.https://www.thecore.co.nz/" rel="nofollow">The Core</a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </footer>
    <script>
        $(function (){
            $('#bookingForm').bookingForm({
                ownerEmail: '#'
            });
        })
        $(function() {
            $('#bookingForm input, #bookingForm textarea').placeholder();
        });
    </script>
</body>
</html>
