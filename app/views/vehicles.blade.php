<?php
/*************************************************************************
 *
 * CONFIDENTIAL
 * __________________
 *
 *  [2014] - [2024]
 *  All Rights Reserved.
 *
 * Developed by - Pavithra Isuru
 * Created on - 29/08/2016 10:51.
 */
?>

@extends('master')

@section('content')
    <!--==============================Content=================================-->
    <div class="content">
        <div class="container_12">
            <div class="grid_5">
                <h3>Booking Form</h3>
                <form id="bookingForm">
                    <strong>Plate Number</strong>
                    <?php
                        $vehicleList = Vehicles::where('status', 1)->lists('id');
                    ?>
                    <select class="tmSelect" style="width: 80%;" data-class="tmSelect tmSelect2" data-constraints="">
                        @foreach($vehicleList as $vehicleId)
                            <option value="{{$vehicleId}}">{{Vehicles::where('id', $vehicleId)->pluck('plate')}}</option>
                        @endforeach
                    </select>
                    <div class="fl1">
                        <div class="tmInput">
                            <input placeHolder="First Name:" type="text" >
                        </div>
                        <div class="tmInput">
                            <input placeHolder="Drivers Age:" type="text" >
                        </div>
                    </div>
                    <div class="fl1">
                        <div class="tmInput">
                            <input placeHolder="Last Name:" type="text" >
                        </div>
                        <div class="tmInput mr0">
                            <input placeHolder="Email" type="text" >
                        </div>
                    </div>
                    <div class="clear"></div>
                    <strong>From</strong>
                    <div class="tmDatepicker">
                        <input placeHolder="18/08/2016" type="datetime" >
                    </div>
                    <div class="clear"></div>
                    <strong>To</strong>
                    <label class="tmDatepicker">
                        <input type="datetime"	placeHolder='20/08/2016' >
                    </label>
                    <div class="clear"></div>
                    <div class="tmRadio">
                        <p>Comfort</p>
                        <input name="Comfort" type="radio" id="tmRadio0" data-constraints='@RadioGroupChecked(name="Comfort", groups=[RadioGroup])' checked/>
                        <span>Cheap</span>
                        <input name="Comfort" type="radio" id="tmRadio1" data-constraints='@RadioGroupChecked(name="Comfort", groups=[RadioGroup])' />
                        <span>Standart</span>
                        <input name="Comfort" type="radio" id="tmRadio2" data-constraints='@RadioGroupChecked(name="Comfort", groups=[RadioGroup])' />
                        <span>Lux</span>
                    </div>
                    <div class="clear"></div>
                    <div class="fl1 fl2">
                        <em>Adults</em>
                        <select name="Adults" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="">
                            <option>1</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                        <div class="clear height1"></div>
                    </div>
                    <div class="fl1 fl2">
                        <em>Children</em>
                        <select name="Children" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="">
                            <option>0</option>
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                        </select>
                    </div>
                    <div class="clear"></div>
                    <div class="tmTextarea">
                        <textarea name="Message" placeHolder="Message" data-constraints='@NotEmpty @Required @Length(min=20,max=999999)'></textarea>
                    </div>
                    <a href="#" class="btn" data-type="submit">Submit</a>
                </form>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    </div>
@endsection