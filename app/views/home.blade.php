@extends('layout.layout')

@section('content')
	<div class="hero hero-home">
		<div class="multi-choice">
			<div class="item">
				<div class="title">Vehicles</div>
			</div>
			<div class="item">
				<div class="title">Events</div>
			</div>
			<div class="item">
				<div class="title">Scaffolding</div>
			</div>
		</div>
	</div>
@endsection