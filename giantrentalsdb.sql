-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 07, 2017 at 11:11 PM
-- Server version: 5.6.28
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `giantrentals`
--
CREATE DATABASE IF NOT EXISTS `giantrentals` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `giantrentals`;

-- --------------------------------------------------------

--
-- Table structure for table `booking_job`
--

DROP TABLE IF EXISTS `booking_job`;
CREATE TABLE `booking_job` (
  `id` int(11) NOT NULL,
  `session_id` varchar(120) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `info_id` int(11) DEFAULT NULL,
  `licence` varchar(10) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_job`
--

INSERT INTO `booking_job` (`id`, `session_id`, `user_id`, `info_id`, `licence`, `status`, `created_at`) VALUES
(34, '', 2, 93, 'KD0343242', 2, '2017-02-07 21:40:57');

-- --------------------------------------------------------

--
-- Table structure for table `event_cart`
--

DROP TABLE IF EXISTS `event_cart`;
CREATE TABLE `event_cart` (
  `id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `item_detail_id` int(11) DEFAULT NULL,
  `day_rate` float DEFAULT NULL,
  `booking_from` datetime DEFAULT NULL,
  `booking_to` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `release_at` datetime DEFAULT NULL,
  `return_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_cart`
--

INSERT INTO `event_cart` (`id`, `job_id`, `item_detail_id`, `day_rate`, `booking_from`, `booking_to`, `status`, `release_at`, `return_at`, `created_at`) VALUES
(40, 4, 1, NULL, '2016-11-04 08:00:00', '2016-11-04 09:00:00', 4, NULL, NULL, '2016-11-04 03:11:39'),
(86, 6, 1, NULL, '2016-11-09 08:00:00', '2016-11-09 09:00:00', 4, NULL, NULL, '2016-11-09 23:05:12'),
(87, 6, 2, NULL, '2016-11-09 08:00:00', '2016-11-09 09:00:00', 4, NULL, NULL, '2016-11-09 23:05:18'),
(90, 7, 1, NULL, '2016-11-10 08:00:00', '2016-11-10 09:00:00', 4, NULL, NULL, '2016-11-10 01:48:07'),
(91, 8, 1, NULL, '2016-11-10 08:00:00', '2016-11-10 09:00:00', 4, NULL, NULL, '2016-11-10 20:42:53'),
(92, 8, 4, NULL, '2016-11-10 08:00:00', '2016-11-10 09:00:00', 4, NULL, NULL, '2016-11-10 20:43:02'),
(95, 13, 1, 8, '2016-11-15 08:00:00', '2016-11-15 09:00:00', 4, NULL, NULL, '2016-11-15 04:37:46'),
(96, 14, 1, NULL, '2016-11-18 08:00:00', '2016-11-22 12:00:00', 3, NULL, NULL, '2016-11-18 00:38:02'),
(97, 14, 4, NULL, '2016-11-18 08:00:00', '2016-11-22 12:00:00', 3, NULL, NULL, '2016-11-18 00:38:12'),
(98, 15, 1, NULL, '2016-11-18 08:00:00', '2016-11-18 09:00:00', 3, NULL, NULL, '2016-11-18 00:52:30'),
(99, 15, 4, NULL, '2016-11-18 08:00:00', '2016-11-18 09:00:00', 3, NULL, NULL, '2016-11-18 00:52:33'),
(100, 18, 1, NULL, '2016-11-18 08:00:00', '2016-11-18 09:00:00', 3, NULL, NULL, '2016-11-18 02:41:38'),
(101, 20, 4, NULL, '2016-11-30 08:00:00', '2016-12-02 09:00:00', 3, NULL, NULL, '2016-11-18 02:55:41'),
(102, 22, 1, NULL, '2016-11-20 08:00:00', '2016-11-20 09:00:00', 3, NULL, NULL, '2016-11-20 19:46:24'),
(103, 22, 4, NULL, '2016-11-20 08:00:00', '2016-11-20 09:00:00', 3, NULL, NULL, '2016-11-20 19:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `event_cart_items`
--

DROP TABLE IF EXISTS `event_cart_items`;
CREATE TABLE `event_cart_items` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_cart_items`
--

INSERT INTO `event_cart_items` (`id`, `cart_id`, `item_id`, `status`) VALUES
(11, 25, 1, 1),
(12, 26, 2, 1),
(13, 27, 1, 1),
(14, 28, 6, 1),
(15, 29, 7, 1),
(16, 30, 8, 1),
(17, 31, 1, 1),
(18, 32, 9, 1),
(19, 33, 10, 1),
(20, 34, 6, 1),
(24, 35, 184, 1),
(25, 35, 185, 1),
(26, 35, 186, 1),
(27, 35, 187, 1),
(28, 36, 7, 1),
(29, 37, 101, 1),
(30, 38, 131, 1),
(31, 38, 132, 1),
(32, 39, 21, 1),
(33, 40, 1, 1),
(34, 41, 211, 1),
(35, 41, 1, 1),
(36, 42, 2, 1),
(37, 43, 3, 1),
(38, 44, 101, 1),
(39, 44, 102, 1),
(40, 44, 103, 1),
(41, 45, 1, 1),
(42, 46, 2, 1),
(43, 47, 104, 1),
(44, 47, 105, 1),
(45, 47, 106, 1),
(46, 48, 131, 1),
(47, 48, 132, 1),
(48, 48, 133, 1),
(49, 49, 3, 1),
(50, 50, 4, 1),
(51, 51, 5, 1),
(52, 52, 181, 1),
(53, 53, 1, 1),
(54, 54, 181, 1),
(55, 54, 182, 1),
(56, 54, 183, 1),
(57, 54, 184, 1),
(58, 54, 185, 1),
(59, 55, 21, 1),
(60, 55, 22, 1),
(61, 55, 23, 1),
(62, 41, 1, 1),
(63, 42, 101, 1),
(64, 42, 102, 1),
(65, 42, 103, 1),
(66, 43, 2, 1),
(67, 44, 1, 1),
(68, 45, 1, 1),
(69, 46, 1, 1),
(70, 47, 3, 1),
(71, 48, 4, 1),
(72, 49, 5, 1),
(73, 50, 6, 1),
(74, 51, 7, 1),
(75, 52, 8, 1),
(76, 53, 3, 1),
(77, 55, 1, 1),
(78, 56, 1, 1),
(79, 60, 1, 1),
(80, 64, 2, 1),
(81, 65, 1, 1),
(82, 66, 1, 1),
(83, 67, 2, 1),
(84, 68, 3, 1),
(85, 69, 4, 1),
(86, 70, 5, 1),
(87, 71, 2, 1),
(88, 72, 3, 1),
(89, 73, 4, 1),
(90, 74, 5, 1),
(91, 75, 6, 1),
(92, 76, 7, 1),
(93, 77, 1, 1),
(94, 78, 1, 1),
(95, 79, 1, 1),
(96, 80, 2, 1),
(97, 81, 131, 1),
(98, 82, 181, 1),
(99, 82, 182, 1),
(100, 82, 183, 1),
(104, 85, 1, 1),
(105, 86, 1, 1),
(106, 86, 2, 1),
(107, 86, 3, 1),
(108, 87, 21, 1),
(109, 87, 22, 1),
(110, 87, 23, 1),
(111, 88, 1, 1),
(112, 89, 101, 1),
(113, 89, 102, 1),
(114, 89, 103, 1),
(115, 90, 1, 1),
(116, 91, 2, 1),
(117, 91, 3, 1),
(118, 91, 4, 1),
(119, 92, 131, 1),
(120, 92, 132, 1),
(121, 93, 1, 1),
(122, 93, 2, 1),
(123, 94, 101, 1),
(124, 94, 102, 1),
(125, 95, 1, 1),
(126, 96, 1, 1),
(127, 97, 131, 1),
(128, 97, 132, 1),
(129, 98, 2, 1),
(130, 98, 3, 1),
(131, 99, 133, 1),
(132, 99, 134, 1),
(133, 100, 4, 1),
(134, 101, 131, 1),
(135, 102, 2, 1),
(136, 102, 3, 1),
(137, 103, 133, 1),
(138, 103, 134, 1),
(139, 103, 135, 1),
(140, 104, 101, 1),
(141, 104, 102, 1),
(142, 104, 103, 1),
(143, 105, 2, 1),
(144, 104, 1, 1),
(145, 105, 2, 1),
(146, 106, 1, 1),
(147, 107, 2, 1),
(148, 108, 3, 1),
(149, 108, 4, 1),
(150, 108, 5, 1),
(151, 108, 6, 1),
(152, 109, 1, 1),
(153, 110, 1, 1),
(154, 111, 2, 1),
(155, 112, 3, 1),
(156, 113, 1, 1),
(157, 114, 21, 1),
(158, 115, 1, 1),
(159, 116, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `event_category`
--

DROP TABLE IF EXISTS `event_category`;
CREATE TABLE `event_category` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_category`
--

INSERT INTO `event_category` (`id`, `name`, `description`, `status`, `created_at`) VALUES
(1, 'Unit', 'testing description', 1, '2016-09-02 05:48:18'),
(2, 'Locations', 'testing Location descrioption', 1, '2016-09-02 05:57:33'),
(4, 'testing', 'testing Location descrioption', 1, '2016-09-02 08:48:05');

-- --------------------------------------------------------

--
-- Table structure for table `event_items`
--

DROP TABLE IF EXISTS `event_items`;
CREATE TABLE `event_items` (
  `id` int(11) NOT NULL,
  `detail_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_items`
--

INSERT INTO `event_items` (`id`, `detail_id`, `status`, `created_at`) VALUES
(1, 1, 1, '2016-09-02 05:53:23'),
(2, 1, 1, '2016-09-02 05:53:23'),
(3, 1, 1, '2016-09-02 05:53:23'),
(4, 1, 1, '2016-09-02 05:53:24'),
(5, 1, 1, '2016-09-02 05:53:24'),
(6, 1, 1, '2016-09-02 05:53:25'),
(7, 1, 1, '2016-09-02 05:53:25'),
(8, 1, 1, '2016-09-02 05:53:26'),
(9, 1, 1, '2016-09-02 05:53:26'),
(10, 1, 1, '2016-09-02 05:53:27'),
(11, 1, 1, '2016-09-02 05:53:27'),
(12, 1, 1, '2016-09-02 05:53:27'),
(13, 1, 1, '2016-09-02 05:53:28'),
(14, 1, 1, '2016-09-02 05:53:28'),
(15, 1, 1, '2016-09-02 05:53:29'),
(16, 1, 1, '2016-09-02 05:53:29'),
(17, 1, 1, '2016-09-02 05:53:30'),
(18, 1, 1, '2016-09-02 05:53:30'),
(19, 1, 1, '2016-09-02 05:53:30'),
(20, 1, 1, '2016-09-02 05:53:31'),
(21, 2, 1, '2016-09-02 05:54:45'),
(22, 2, 1, '2016-09-02 05:54:45'),
(23, 2, 1, '2016-09-02 05:54:46'),
(24, 2, 1, '2016-09-02 05:54:46'),
(25, 2, 1, '2016-09-02 05:54:47'),
(26, 2, 1, '2016-09-02 05:54:47'),
(27, 2, 1, '2016-09-02 05:54:48'),
(28, 2, 1, '2016-09-02 05:54:48'),
(29, 2, 1, '2016-09-02 05:54:48'),
(30, 2, 1, '2016-09-02 05:54:49'),
(31, 2, 1, '2016-09-02 05:54:49'),
(32, 2, 1, '2016-09-02 05:54:50'),
(33, 2, 1, '2016-09-02 05:54:50'),
(34, 2, 1, '2016-09-02 05:54:50'),
(35, 2, 1, '2016-09-02 05:54:51'),
(36, 2, 1, '2016-09-02 05:54:51'),
(37, 2, 1, '2016-09-02 05:54:52'),
(38, 2, 1, '2016-09-02 05:54:52'),
(39, 2, 1, '2016-09-02 05:54:53'),
(40, 2, 1, '2016-09-02 05:54:53'),
(41, 2, 1, '2016-09-02 05:54:53'),
(42, 2, 1, '2016-09-02 05:54:54'),
(43, 2, 1, '2016-09-02 05:54:54'),
(44, 2, 1, '2016-09-02 05:54:55'),
(45, 2, 1, '2016-09-02 05:54:55'),
(46, 2, 1, '2016-09-02 05:54:55'),
(47, 2, 1, '2016-09-02 05:54:56'),
(48, 2, 1, '2016-09-02 05:54:56'),
(49, 2, 1, '2016-09-02 05:54:57'),
(50, 2, 1, '2016-09-02 05:54:57'),
(51, 2, 1, '2016-09-02 05:54:57'),
(52, 2, 1, '2016-09-02 05:54:58'),
(53, 2, 1, '2016-09-02 05:54:58'),
(54, 2, 1, '2016-09-02 05:54:59'),
(55, 2, 1, '2016-09-02 05:54:59'),
(56, 2, 1, '2016-09-02 05:54:59'),
(57, 2, 1, '2016-09-02 05:55:00'),
(58, 2, 1, '2016-09-02 05:55:00'),
(59, 2, 1, '2016-09-02 05:55:01'),
(60, 2, 1, '2016-09-02 05:55:01'),
(61, 2, 1, '2016-09-02 05:55:01'),
(62, 2, 1, '2016-09-02 05:55:02'),
(63, 2, 1, '2016-09-02 05:55:02'),
(64, 2, 1, '2016-09-02 05:55:03'),
(65, 2, 1, '2016-09-02 05:55:03'),
(66, 2, 1, '2016-09-02 05:55:03'),
(67, 2, 1, '2016-09-02 05:55:04'),
(68, 2, 1, '2016-09-02 05:55:04'),
(69, 2, 1, '2016-09-02 05:55:05'),
(70, 2, 1, '2016-09-02 05:55:05'),
(71, 2, 1, '2016-09-02 05:55:05'),
(72, 2, 1, '2016-09-02 05:55:06'),
(73, 2, 1, '2016-09-02 05:55:06'),
(74, 2, 1, '2016-09-02 05:55:07'),
(75, 2, 1, '2016-09-02 05:55:07'),
(76, 2, 1, '2016-09-02 05:55:07'),
(77, 2, 1, '2016-09-02 05:55:08'),
(78, 2, 1, '2016-09-02 05:55:08'),
(79, 2, 1, '2016-09-02 05:55:09'),
(80, 2, 1, '2016-09-02 05:55:09'),
(81, 2, 1, '2016-09-02 05:55:09'),
(82, 2, 1, '2016-09-02 05:55:10'),
(83, 2, 1, '2016-09-02 05:55:10'),
(84, 2, 1, '2016-09-02 05:55:11'),
(85, 2, 1, '2016-09-02 05:55:11'),
(86, 2, 1, '2016-09-02 05:55:11'),
(87, 2, 1, '2016-09-02 05:55:12'),
(88, 2, 1, '2016-09-02 05:55:12'),
(89, 2, 1, '2016-09-02 05:55:13'),
(90, 2, 1, '2016-09-02 05:55:13'),
(91, 2, 1, '2016-09-02 05:55:14'),
(92, 2, 1, '2016-09-02 05:55:14'),
(93, 2, 1, '2016-09-02 05:55:14'),
(94, 2, 1, '2016-09-02 05:55:15'),
(95, 2, 1, '2016-09-02 05:55:15'),
(96, 2, 1, '2016-09-02 05:55:16'),
(97, 2, 1, '2016-09-02 05:55:16'),
(98, 2, 1, '2016-09-02 05:55:17'),
(99, 2, 1, '2016-09-02 05:55:17'),
(100, 2, 1, '2016-09-02 05:55:17'),
(101, 3, 1, '2016-09-02 05:55:53'),
(102, 3, 1, '2016-09-02 05:55:54'),
(103, 3, 1, '2016-09-02 05:55:54'),
(104, 3, 1, '2016-09-02 05:55:55'),
(105, 3, 1, '2016-09-02 05:55:55'),
(106, 3, 1, '2016-09-02 05:55:55'),
(107, 3, 1, '2016-09-02 05:55:56'),
(108, 3, 1, '2016-09-02 05:55:56'),
(109, 3, 1, '2016-09-02 05:55:57'),
(110, 3, 1, '2016-09-02 05:55:57'),
(111, 3, 1, '2016-09-02 05:55:58'),
(112, 3, 1, '2016-09-02 05:55:58'),
(113, 3, 1, '2016-09-02 05:55:58'),
(114, 3, 1, '2016-09-02 05:55:59'),
(115, 3, 1, '2016-09-02 05:55:59'),
(116, 3, 1, '2016-09-02 05:56:00'),
(117, 3, 1, '2016-09-02 05:56:00'),
(118, 3, 1, '2016-09-02 05:56:00'),
(119, 3, 1, '2016-09-02 05:56:01'),
(120, 3, 1, '2016-09-02 05:56:01'),
(121, 3, 1, '2016-09-02 05:56:02'),
(122, 3, 1, '2016-09-02 05:56:02'),
(123, 3, 1, '2016-09-02 05:56:02'),
(124, 3, 1, '2016-09-02 05:56:03'),
(125, 3, 1, '2016-09-02 05:56:03'),
(126, 3, 1, '2016-09-02 05:56:04'),
(127, 3, 1, '2016-09-02 05:56:04'),
(128, 3, 1, '2016-09-02 05:56:04'),
(129, 3, 1, '2016-09-02 05:56:05'),
(130, 3, 1, '2016-09-02 05:56:05'),
(131, 4, 1, '2016-09-02 05:56:44'),
(132, 4, 1, '2016-09-02 05:56:45'),
(133, 4, 1, '2016-09-02 05:56:45'),
(134, 4, 1, '2016-09-02 05:56:45'),
(135, 4, 1, '2016-09-02 05:56:46'),
(136, 4, 1, '2016-09-02 05:56:46'),
(137, 4, 1, '2016-09-02 05:56:47'),
(138, 4, 1, '2016-09-02 05:56:47'),
(139, 4, 1, '2016-09-02 05:56:47'),
(140, 4, 1, '2016-09-02 05:56:48'),
(141, 4, 1, '2016-09-02 05:56:48'),
(142, 4, 1, '2016-09-02 05:56:49'),
(143, 4, 1, '2016-09-02 05:56:49'),
(144, 4, 1, '2016-09-02 05:56:49'),
(145, 4, 1, '2016-09-02 05:56:50'),
(146, 4, 1, '2016-09-02 05:56:50'),
(147, 4, 1, '2016-09-02 05:56:51'),
(148, 4, 1, '2016-09-02 05:56:51'),
(149, 4, 1, '2016-09-02 05:56:51'),
(150, 4, 1, '2016-09-02 05:56:52'),
(151, 4, 1, '2016-09-02 05:56:52'),
(152, 4, 1, '2016-09-02 05:56:53'),
(153, 4, 1, '2016-09-02 05:56:53'),
(154, 4, 1, '2016-09-02 05:56:54'),
(155, 4, 1, '2016-09-02 05:56:54'),
(156, 4, 1, '2016-09-02 05:56:54'),
(157, 4, 1, '2016-09-02 05:56:55'),
(158, 4, 1, '2016-09-02 05:56:55'),
(159, 4, 1, '2016-09-02 05:56:56'),
(160, 4, 1, '2016-09-02 05:56:56'),
(161, 4, 1, '2016-09-02 05:56:56'),
(162, 4, 1, '2016-09-02 05:56:57'),
(163, 4, 1, '2016-09-02 05:56:57'),
(164, 4, 1, '2016-09-02 05:56:58'),
(165, 4, 1, '2016-09-02 05:56:58'),
(166, 4, 1, '2016-09-02 05:56:58'),
(167, 4, 1, '2016-09-02 05:56:59'),
(168, 4, 1, '2016-09-02 05:56:59'),
(169, 4, 1, '2016-09-02 05:57:00'),
(170, 4, 1, '2016-09-02 05:57:00'),
(171, 4, 1, '2016-09-02 05:57:00'),
(172, 4, 1, '2016-09-02 05:57:01'),
(173, 4, 1, '2016-09-02 05:57:01'),
(174, 4, 1, '2016-09-02 05:57:02'),
(175, 4, 1, '2016-09-02 05:57:02'),
(176, 4, 1, '2016-09-02 05:57:02'),
(177, 4, 1, '2016-09-02 05:57:03'),
(178, 4, 1, '2016-09-02 05:57:03'),
(179, 4, 1, '2016-09-02 05:57:04'),
(180, 4, 1, '2016-09-02 05:57:04'),
(181, 5, 1, '2016-09-02 05:58:38'),
(182, 5, 1, '2016-09-02 05:58:39'),
(183, 5, 1, '2016-09-02 05:58:39'),
(184, 5, 1, '2016-09-02 05:58:39'),
(185, 5, 1, '2016-09-02 05:58:40'),
(186, 5, 1, '2016-09-02 05:58:40'),
(187, 5, 1, '2016-09-02 05:58:41'),
(188, 5, 1, '2016-09-02 05:58:41'),
(189, 5, 1, '2016-09-02 05:58:41'),
(190, 5, 1, '2016-09-02 05:58:42'),
(191, 5, 1, '2016-09-02 05:58:42'),
(192, 5, 1, '2016-09-02 05:58:43'),
(193, 5, 1, '2016-09-02 05:58:43'),
(194, 5, 1, '2016-09-02 05:58:44'),
(195, 5, 1, '2016-09-02 05:58:44'),
(196, 5, 1, '2016-09-02 05:58:45'),
(197, 5, 1, '2016-09-02 05:58:45'),
(198, 5, 1, '2016-09-02 05:58:45'),
(199, 5, 1, '2016-09-02 05:58:46'),
(200, 5, 1, '2016-09-02 05:58:46'),
(201, 5, 1, '2016-09-02 05:58:47'),
(202, 5, 1, '2016-09-02 05:58:47'),
(203, 5, 1, '2016-09-02 05:58:48'),
(204, 5, 1, '2016-09-02 05:58:48'),
(205, 5, 1, '2016-09-02 05:58:48'),
(206, 5, 1, '2016-09-02 05:58:49'),
(207, 5, 1, '2016-09-02 05:58:49'),
(208, 5, 1, '2016-09-02 05:58:50'),
(209, 5, 1, '2016-09-02 05:58:50'),
(210, 5, 1, '2016-09-02 05:58:50'),
(211, 6, 1, '2016-09-02 05:59:37'),
(212, 6, 1, '2016-09-02 05:59:37'),
(213, 6, 1, '2016-09-02 05:59:38'),
(214, 6, 1, '2016-09-02 05:59:38'),
(215, 6, 1, '2016-09-02 05:59:38'),
(216, 6, 1, '2016-09-02 05:59:39'),
(217, 6, 1, '2016-09-02 05:59:39'),
(218, 6, 1, '2016-09-02 05:59:40'),
(219, 6, 1, '2016-09-02 05:59:40'),
(220, 6, 1, '2016-09-02 05:59:40'),
(221, 6, 1, '2016-09-02 05:59:41'),
(222, 6, 1, '2016-09-02 05:59:41'),
(223, 6, 1, '2016-09-02 05:59:42'),
(224, 6, 1, '2016-09-02 05:59:42'),
(225, 6, 1, '2016-09-02 05:59:42'),
(226, 6, 1, '2016-09-02 05:59:43'),
(227, 6, 1, '2016-09-02 05:59:43'),
(228, 6, 1, '2016-09-02 05:59:44'),
(229, 6, 1, '2016-09-02 05:59:44'),
(230, 6, 1, '2016-09-02 05:59:44'),
(231, 6, 1, '2016-09-02 05:59:45'),
(232, 6, 1, '2016-09-02 05:59:45'),
(233, 6, 1, '2016-09-02 05:59:46'),
(234, 6, 1, '2016-09-02 05:59:46'),
(235, 6, 1, '2016-09-02 05:59:46'),
(236, 6, 1, '2016-09-02 05:59:47'),
(237, 6, 1, '2016-09-02 05:59:47'),
(238, 6, 1, '2016-09-02 05:59:48'),
(239, 6, 1, '2016-09-02 05:59:48'),
(240, 6, 1, '2016-09-02 05:59:48'),
(241, 6, 1, '2016-09-02 05:59:49'),
(242, 6, 1, '2016-09-02 05:59:49'),
(243, 6, 1, '2016-09-02 05:59:50'),
(244, 6, 1, '2016-09-02 05:59:50'),
(245, 6, 1, '2016-09-02 05:59:50'),
(246, 6, 1, '2016-09-02 05:59:51'),
(247, 6, 1, '2016-09-02 05:59:51'),
(248, 6, 1, '2016-09-02 05:59:52'),
(249, 6, 1, '2016-09-02 05:59:52'),
(250, 6, 1, '2016-09-02 05:59:52'),
(251, 6, 1, '2016-09-02 05:59:53'),
(252, 6, 1, '2016-09-02 05:59:53'),
(253, 6, 1, '2016-09-02 05:59:54'),
(254, 6, 1, '2016-09-02 05:59:54'),
(255, 6, 1, '2016-09-02 05:59:54'),
(256, 6, 1, '2016-09-02 05:59:55'),
(257, 6, 1, '2016-09-02 05:59:55'),
(258, 6, 1, '2016-09-02 05:59:56'),
(259, 6, 1, '2016-09-02 05:59:56'),
(260, 6, 1, '2016-09-02 05:59:56'),
(261, 6, 1, '2016-09-02 05:59:57'),
(262, 6, 1, '2016-09-02 05:59:57'),
(263, 6, 1, '2016-09-02 05:59:58'),
(264, 6, 1, '2016-09-02 05:59:58'),
(265, 6, 1, '2016-09-02 05:59:58'),
(266, 6, 1, '2016-09-02 05:59:59'),
(267, 6, 1, '2016-09-02 05:59:59'),
(268, 6, 1, '2016-09-02 06:00:00'),
(269, 6, 1, '2016-09-02 06:00:00'),
(270, 6, 1, '2016-09-02 06:00:00'),
(271, 7, 1, '2016-09-02 08:48:48'),
(272, 7, 1, '2016-09-02 08:48:49'),
(273, 7, 1, '2016-09-02 08:48:49'),
(274, 7, 1, '2016-09-02 08:48:50'),
(275, 7, 1, '2016-09-02 08:48:50'),
(276, 7, 1, '2016-09-02 08:48:50'),
(277, 7, 1, '2016-09-02 08:48:51'),
(278, 7, 1, '2016-09-02 08:48:51'),
(279, 7, 1, '2016-09-02 08:48:52'),
(280, 7, 1, '2016-09-02 08:48:53'),
(281, 7, 1, '2016-09-02 08:48:53'),
(282, 7, 1, '2016-09-02 08:48:53'),
(283, 7, 1, '2016-09-02 08:48:54'),
(284, 7, 1, '2016-09-02 08:48:54'),
(285, 7, 1, '2016-09-02 08:48:55'),
(286, 7, 1, '2016-09-02 08:48:55'),
(287, 7, 1, '2016-09-02 08:48:56'),
(288, 7, 1, '2016-09-02 08:48:56'),
(289, 7, 1, '2016-09-02 08:48:56'),
(290, 7, 1, '2016-09-02 08:48:57'),
(291, 7, 1, '2016-09-02 08:48:57'),
(292, 7, 1, '2016-09-02 08:48:58'),
(293, 7, 1, '2016-09-02 08:48:58'),
(294, 7, 1, '2016-09-02 08:48:58'),
(295, 7, 1, '2016-09-02 08:48:59'),
(296, 7, 1, '2016-09-02 08:48:59'),
(297, 7, 1, '2016-09-02 08:49:00'),
(298, 7, 1, '2016-09-02 08:49:00'),
(299, 7, 1, '2016-09-02 08:49:01'),
(300, 7, 1, '2016-09-02 08:49:01'),
(301, 7, 1, '2016-09-02 08:49:02'),
(302, 7, 1, '2016-09-02 08:49:02'),
(303, 7, 1, '2016-09-02 08:49:02'),
(304, 7, 1, '2016-09-02 08:49:03'),
(305, 7, 1, '2016-09-02 08:49:03'),
(306, 7, 1, '2016-09-02 08:49:04'),
(307, 7, 1, '2016-09-02 08:49:04'),
(308, 7, 1, '2016-09-02 08:49:04'),
(309, 7, 1, '2016-09-02 08:49:05'),
(310, 7, 1, '2016-09-02 08:49:05'),
(311, 7, 1, '2016-09-02 08:49:06'),
(312, 7, 1, '2016-09-02 08:49:06'),
(313, 7, 1, '2016-09-02 08:49:07'),
(314, 7, 1, '2016-09-02 08:49:07'),
(315, 7, 1, '2016-09-02 08:49:07'),
(316, 7, 1, '2016-09-02 08:49:08'),
(317, 7, 1, '2016-09-02 08:49:08'),
(318, 7, 1, '2016-09-02 08:49:09'),
(319, 7, 1, '2016-09-02 08:49:09'),
(320, 7, 1, '2016-09-02 08:49:10');

-- --------------------------------------------------------

--
-- Table structure for table `event_item_details`
--

DROP TABLE IF EXISTS `event_item_details`;
CREATE TABLE `event_item_details` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `day_rate` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_item_details`
--

INSERT INTO `event_item_details` (`id`, `name`, `category_id`, `day_rate`, `status`, `created_at`) VALUES
(1, 'Heater Fan', 1, 8, 1, '2016-09-02 05:53:20'),
(2, 'Agency Chairs', 1, 3, 1, '2016-09-02 05:54:43'),
(3, 'Air	Con/Dehumidifier', 1, 15, 1, '2016-09-02 05:55:51'),
(4, 'Bucket', 1, 3, 1, '2016-09-02 05:56:42'),
(5, 'Barrier Arms', 2, 2, 1, '2016-09-02 05:58:36'),
(6, 'Broom Large Interior', 2, 4, 1, '2016-09-02 05:59:35'),
(7, 'test item', 4, 6, 1, '2016-09-02 08:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `exceptions`
--

DROP TABLE IF EXISTS `exceptions`;
CREATE TABLE `exceptions` (
  `id` int(11) NOT NULL,
  `controller` varchar(32) DEFAULT NULL,
  `func` varchar(32) DEFAULT NULL,
  `exception` varchar(512) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exceptions`
--

INSERT INTO `exceptions` (`id`, `controller`, `func`, `exception`, `status`, `created_at`) VALUES
(1, 'VehicleBookingController', 'addVehicle', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'day_rate\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `ve...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-08-31 17:35:16'),
(2, 'VehicleBookingController', 'vehicleBooking', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'drivers_age\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `ve...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\', 1, '2016-08-31 17:54:50'),
(3, 'VehicleBookingController', 'vehicleBooking', 'ErrorException: Undefined property: Illuminate\\Database\\Eloquent\\Builder::$id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:114\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(114): Illuminate\\Exception\\Handler->handleError(8, \'Undefined prope...\', \'/Users/Isuru/Pr...\', 114, Array)\n#1 [internal function]: VehicleBookingController->vehicleBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate', 1, '2016-08-31 18:02:48'),
(4, 'VehicleBookingController', 'releaseVehicle', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'release_at\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:389\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(389): PDO->prepare(\'update `vehicle...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\D', 1, '2016-08-31 18:04:32'),
(5, 'VehicleBookingController', 'returnVehicle', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'return_at\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:389\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(389): PDO->prepare(\'update `vehicle...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Da', 1, '2016-08-31 18:06:42'),
(6, 'ScaffoldController', 'addScaffoldings', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'quantity\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `sc...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-08-31 18:23:56'),
(7, 'ScaffoldController', 'addScaffoldings', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'quantity\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `sc...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-08-31 18:25:06'),
(8, 'ScaffoldController', 'checkAvailability', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'quntity\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select count(`q...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-08-31 19:35:50'),
(9, 'ScaffoldController', 'checkAvailability', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'quntity\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select count(`q...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-08-31 19:51:15'),
(10, 'ScaffoldController', 'checkAvailability', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'booking_from\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select count(`q...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illumina', 1, '2016-08-31 19:52:03'),
(11, 'ScaffoldController', 'addScaffoldingPartType', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'scaffolding_part_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `sc...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Ill', 1, '2016-09-01 17:08:17'),
(12, 'ScaffoldController', 'addScaffoldingTypesPartTypes', 'ErrorException: Invalid argument supplied for foreach() in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:267\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(267): Illuminate\\Exception\\Handler->handleError(2, \'Invalid argumen...\', \'/Users/Isuru/Pr...\', 267, Array)\n#1 [internal function]: ScaffoldController->addScaffoldingTypes()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_', 1, '2016-09-01 18:16:33'),
(13, 'ScaffoldController', 'addScaffoldingTypesPartTypes', 'ErrorException: Invalid argument supplied for foreach() in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:267\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(267): Illuminate\\Exception\\Handler->handleError(2, \'Invalid argumen...\', \'/Users/Isuru/Pr...\', 267, Array)\n#1 [internal function]: ScaffoldController->addScaffoldingTypes()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_', 1, '2016-09-01 18:18:01'),
(14, 'ScaffoldController', 'calculatePrice', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'scaffolding_part_id\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select count(`i...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->I', 1, '2016-09-01 19:09:32'),
(15, 'ScaffoldController', 'calculatePrice', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'scaffodling_part_id\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select count(*)...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->I', 1, '2016-09-01 19:10:47'),
(16, 'UserController', 'postSignIn', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'user_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `us...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-09-01 23:28:45'),
(17, 'UserController', 'postSignIn', 'InvalidArgumentException: Route [account-activate] not defined. in /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php:5989\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(3269): Illuminate\\Routing\\UrlGenerator->route(\'account-activat...\', Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(54): Illuminate\\Support\\Facades\\Facade::__callStatic(\'route\', Array)\n#2 [internal function]: UserController->postSignUp()\n#3 /Users/Isuru/Projects/giantrentals/', 1, '2016-09-01 23:34:21'),
(18, 'EventController', 'getSingleBookingElement', 'PDOException: SQLSTATE[42S02]: Base table or view not found: 1146 Table \'giantrentals.events_cart\' doesn\'t exist in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connectio', 1, '2016-09-03 20:36:47'),
(19, 'ErrorController', 'updateInternalSystemExceptionSta', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'statu\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:389\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(389): PDO->prepare(\'update `excepti...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-09-04 23:01:15'),
(20, 'ErrorController', 'updateInternalSystemExceptionSta', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'statu\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:389\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(389): PDO->prepare(\'update `excepti...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-09-04 23:01:38'),
(21, 'UserManageController', 'getUserList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'rollid\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-09-05 23:34:11'),
(22, 'UserManageController', 'getUserList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'rollid\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-09-05 23:34:48'),
(23, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:17:49'),
(24, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:18:33'),
(25, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:19:41'),
(26, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:20:00'),
(27, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:20:08'),
(28, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:21:28'),
(29, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:21:30'),
(30, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:21:32'),
(31, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pageid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pageid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 00:24:23'),
(32, 'UserManageController', 'getUsersAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'allow\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-09-06 00:28:42'),
(33, 'UserManageController', 'addNewUser', 'InvalidArgumentException: Route [account-activate] not defined. in /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php:5989\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(3269): Illuminate\\Routing\\UrlGenerator->route(\'account-activat...\', Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php(259): Illuminate\\Support\\Facades\\Facade::__callStatic(\'route\', Array)\n#2 [internal function]: UserManageController->addNewUser()\n#3 /Users/Isuru/Projects/', 1, '2016-09-06 04:05:38'),
(34, 'UserManageController', 'addNewUser', 'ErrorException: Undefined variable: userName in /Users/Isuru/Projects/giantrentals/app/storage/views/9bbad1c6775c98fa3f965d0da2f7d9e1:9\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/storage/views/9bbad1c6775c98fa3f965d0da2f7d9e1(9): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 9, Array)\n#1 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr...\')\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illumi', 1, '2016-09-06 04:08:10'),
(35, 'UserManageController', 'addNewUser', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'created_at\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `us...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\D', 1, '2016-09-06 04:13:03'),
(36, 'UserManageController', 'addNewUser', 'ErrorException: Undefined variable: name in /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php:263\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php(263): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 263, Array)\n#1 [internal function]: UserManageController->addNewUser()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Ar', 1, '2016-09-06 04:30:55'),
(37, 'UserManageController', 'addNewUser', 'ErrorException: Undefined variable: name in /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php:264\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php(264): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 264, Array)\n#1 [internal function]: UserManageController->addNewUser()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Ar', 1, '2016-09-06 04:32:18'),
(38, 'UserManageController', 'getUserList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'tempname\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `tempnam...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-09-06 04:59:28'),
(39, 'UserManageController', 'getUserList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'rollid\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `rollid`...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-06 05:00:19'),
(40, 'UserManageController', 'getActivityList', 'ErrorException: date() expects parameter 2 to be integer, object given in /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php:348\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'date() expects ...\', \'/Users/Isuru/Pr...\', 348, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php(348): date(\'Y-m-d H:i:s\', Object(Carbon\\Carbon))\n#2 [internal function]: UserManageController->getActivityList()\n#3 /Users/Isuru/Projects/gia', 1, '2016-09-06 22:04:22'),
(41, 'UserManageController', 'getActivityList', 'ErrorException: date() expects parameter 2 to be integer, object given in /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php:348\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'date() expects ...\', \'/Users/Isuru/Pr...\', 348, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/UserManageController.php(348): date(\'Y-m-d H:i:s\', Object(Carbon\\Carbon))\n#2 [internal function]: UserManageController->getActivityList()\n#3 /Users/Isuru/Projects/gia', 1, '2016-09-06 22:05:03'),
(42, 'ErrorController', 'updateInternalSystemExceptionSta', 'BadMethodCallException: Method [addActivity] does not exist. in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php:268\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ErrorController.php(84): Illuminate\\Routing\\Controller->__call(\'addActivity\', Array)\n#1 [internal function]: ErrorController->updateInternalSystemExceptionStatus()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-09-06 22:30:12'),
(43, 'VehicleBookingController', 'addVehicle', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'day_rate\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDO->prepare(\'insert into `ve...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-09-07 00:08:44'),
(44, 'VehicleBookingController', 'getSingleVehicleBookingList', 'BadMethodCallException: Call to undefined method Illuminate\\Database\\Query\\Builder::list() in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php:2184\nStack trace:\n#0 [internal function]: Illuminate\\Database\\Query\\Builder->__call(\'list\', Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(972): call_user_func_array(Array, Array)\n#2 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBooking', 1, '2016-09-07 04:48:43'),
(45, 'VehicleBookingController', 'getSingleVehicleBookingList', 'BadMethodCallException: Call to undefined method Illuminate\\Database\\Query\\Builder::list() in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php:2184\nStack trace:\n#0 [internal function]: Illuminate\\Database\\Query\\Builder->__call(\'list\', Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(972): call_user_func_array(Array, Array)\n#2 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBooking', 1, '2016-09-07 04:48:52'),
(46, 'VehicleBookingController', 'getPendingBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'customer_info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `custome...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illumi', 1, '2016-09-07 22:28:33'),
(47, 'VehicleBookingController', 'getSingleVehicleDetails', 'ErrorException: Use of undefined constant icleId - assumed \'icleId\' in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:285\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(285): Illuminate\\Exception\\Handler->handleError(8, \'Use of undefine...\', \'/Users/Isuru/Pr...\', 285, Array)\n#1 [internal function]: VehicleBookingController->getSingleVehicleDetails()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/', 1, '2016-09-07 23:26:39'),
(48, 'VehicleBookingController', 'getBookingPrice', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'day_rate\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `day_rat...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-09-09 02:55:01'),
(49, 'VehicleBookingController', 'getBookingPrice', 'Exception: DateTime::__construct(): Failed to parse time string (28/08/2016 08:00) at position 0 (2): Unexpected character in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:400\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(400): DateTime->__construct(\'28/08/2016 08:0...\')\n#1 [internal function]: VehicleBookingController->getBookingPrice()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Co', 1, '2016-09-09 03:21:26'),
(50, 'VehicleBookingController', 'getBookingPrice', 'Exception: DateTime::__construct(): Failed to parse time string (28/08/2016 08:00) at position 0 (2): Unexpected character in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:400\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(400): DateTime->__construct(\'28/08/2016 08:0...\')\n#1 [internal function]: VehicleBookingController->getBookingPrice()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Co', 1, '2016-09-09 03:22:08'),
(51, 'VehicleBookingController', 'getBookingPrice', 'Exception: DateTime::__construct(): Failed to parse time string (28/08/2016 08:00) at position 0 (2): Unexpected character in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:400\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(400): DateTime->__construct(\'28/08/2016 08:0...\')\n#1 [internal function]: VehicleBookingController->getBookingPrice()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Co', 1, '2016-09-09 03:23:28'),
(52, 'VehicleBookingController', 'getBookingPrice', 'ErrorException: date_format() expects parameter 1 to be DateTimeInterface, boolean given in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:401\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'date_format() e...\', \'/Users/Isuru/Pr...\', 401, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(401): date_format(false, \'Y-m-d H:i:s\')\n#2 [internal function]: VehicleBookingController->getBookingPrice()\n#3 /Use', 1, '2016-09-09 03:24:26'),
(53, 'VehicleBookingController', 'getBookingPrice', 'ErrorException: date_format() expects parameter 1 to be DateTimeInterface, boolean given in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:401\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'date_format() e...\', \'/Users/Isuru/Pr...\', 401, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(401): date_format(false, \'Y-m-d H:i:s\')\n#2 [internal function]: VehicleBookingController->getBookingPrice()\n#3 /Use', 1, '2016-09-09 03:33:13'),
(54, 'VehicleBookingController', 'getBookingPrice', 'ErrorException: date_format() expects parameter 1 to be DateTimeInterface, boolean given in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:404\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'date_format() e...\', \'/Users/Isuru/Pr...\', 404, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(404): date_format(false, \'Y-m-d H:i:s\')\n#2 [internal function]: VehicleBookingController->getBookingPrice()\n#3 /Use', 1, '2016-09-09 04:07:43'),
(55, 'VehicleBookingController', 'getBookingPrice', 'ErrorException: date_format() expects parameter 1 to be DateTimeInterface, boolean given in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:404\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'date_format() e...\', \'/Users/Isuru/Pr...\', 404, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(404): date_format(false, \'Y-m-d H:i:s\')\n#2 [internal function]: VehicleBookingController->getBookingPrice()\n#3 /Use', 1, '2016-09-09 04:32:48'),
(56, 'VehicleBookingController', 'getBookingPrice', 'ErrorException: date_format() expects parameter 1 to be DateTimeInterface, boolean given in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:412\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'date_format() e...\', \'/Users/Isuru/Pr...\', 412, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(412): date_format(false, \'Y-m-d H:i:s\')\n#2 [internal function]: VehicleBookingController->getBookingPrice()\n#3 /Use', 1, '2016-09-11 21:30:24'),
(57, 'VehicleBookingController', 'getBookingSliderInitiatingData', 'ErrorException: substr_replace() expects at least 3 parameters, 1 given in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:627\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'substr_replace(...\', \'/Users/Isuru/Pr...\', 627, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(627): substr_replace(\'08:00:00\')\n#2 [internal function]: VehicleBookingController->getBookingSliderInitiatingData()\n#3 /Users/Isuru/', 1, '2016-09-14 04:28:01'),
(58, 'VehicleBookingController', 'getBookingSliderInitiatingData', 'InvalidArgumentException: Unexpected data found.\nUnexpected data found.\nUnexpected data found.\nData missing in /Users/Isuru/Projects/giantrentals/vendor/nesbot/carbon/src/Carbon/Carbon.php:387\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/nesbot/carbon/src/Carbon/Carbon.php(330): Carbon\\Carbon::createFromFormat(\'Y-n-j G:i:s\', \'-9-14 23:31:43\', NULL)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(613): Carbon\\Carbon::create(false)\n#2 [internal function]: Vehicl', 1, '2016-09-14 23:31:43'),
(59, 'VehicleBookingController', 'getBookingSliderInitiatingData', 'ErrorException: Object of class Carbon\\Carbon could not be converted to int in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:664\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(664): Illuminate\\Exception\\Handler->handleError(8, \'Object of class...\', \'/Users/Isuru/Pr...\', 664, Array)\n#1 [internal function]: VehicleBookingController->getBookingSliderInitiatingData()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/', 1, '2016-09-15 00:12:57'),
(60, 'VehicleBookingController', 'getBookingSliderInitiatingData', 'InvalidArgumentException: Trailing data in /Users/Isuru/Projects/giantrentals/vendor/nesbot/carbon/src/Carbon/Carbon.php:387\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/nesbot/carbon/src/Carbon/Carbon.php(330): Carbon\\Carbon::createFromFormat(\'Y-n-j G:i:s\', \'2016-09-05 00:0...\', NULL)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(614): Carbon\\Carbon::create(Object(Carbon\\Carbon))\n#2 [internal function]: VehicleBookingController->getBookingSliderInitiatingDa', 1, '2016-09-15 00:30:17'),
(61, 'UserManageController', 'allowDenyOperationAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'userid\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `id` fro...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-09-15 22:34:02'),
(62, 'UserManageController', 'allowDenyOperationAccess', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'firstname\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `firstna...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Da', 1, '2016-09-15 22:37:48'),
(63, 'ErrorController', 'getInternalSystemIntrusionList', 'ErrorException: Object of class Illuminate\\Database\\Eloquent\\Builder could not be converted to string in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Support/helpers.php:900\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(4096, \'Object of class...\', \'/Users/Isuru/Pr...\', 900, Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Support/helpers.php(900): preg_replace(\'/\\\\?/\', Object(Illuminate\\Database\\Eloquent\\Build', 1, '2016-09-16 00:10:52'),
(64, 'ErrorController', 'getInternalSystemIntrusionList', 'ErrorException: Object of class Illuminate\\Database\\Eloquent\\Builder could not be converted to string in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Support/helpers.php:900\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(4096, \'Object of class...\', \'/Users/Isuru/Pr...\', 900, Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Support/helpers.php(900): preg_replace(\'/\\\\?/\', Object(Illuminate\\Database\\Eloquent\\Build', 1, '2016-09-16 00:13:27'),
(65, 'ErrorController', 'getInternalSystemIntrusionList', 'ErrorException: Object of class Illuminate\\Database\\Eloquent\\Builder could not be converted to string in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Support/helpers.php:900\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(4096, \'Object of class...\', \'/Users/Isuru/Pr...\', 900, Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Support/helpers.php(900): preg_replace(\'/\\\\?/\', Object(Illuminate\\Database\\Eloquent\\Build', 1, '2016-09-16 00:14:51'),
(66, 'ErrorController', 'getInternalSystemIntrusionList', 'ErrorException: Object of class Illuminate\\Database\\Eloquent\\Builder could not be converted to string in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Support/helpers.php:900\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(4096, \'Object of class...\', \'/Users/Isuru/Pr...\', 900, Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Support/helpers.php(900): preg_replace(\'/\\\\?/\', Object(Illuminate\\Database\\Eloquent\\Build', 1, '2016-09-16 00:15:12'),
(67, 'ScaffoldController', 'getBookingList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:334\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(334): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 334, Array)\n#1 [internal function]: ScaffoldController->getBookingList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-09-22 23:23:10'),
(68, 'ScaffoldController', 'getBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-09-23 00:32:29'),
(69, 'ScaffoldController', 'getBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'bookking_id\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select * from `...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminat', 1, '2016-09-23 01:58:15'),
(70, 'ScaffoldController', 'getBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'from\' in \'order clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select * from `...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-23 01:58:38'),
(71, 'ScaffoldController', 'getBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'from\' in \'order clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select * from `...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Datab', 1, '2016-09-23 01:59:11'),
(72, 'ScaffoldController', 'getBookingList', 'ErrorException: Trying to get property of non-object in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:345\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(345): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/Isuru/Pr...\', 345, Array)\n#1 [internal function]: ScaffoldController->getBookingList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_fun', 1, '2016-09-23 02:02:28'),
(73, 'ScaffoldController', 'getBookingList', 'ErrorException: Trying to get property of non-object in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:331\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(331): Illuminate\\Exception\\Handler->handleError(8, \'Trying to get p...\', \'/Users/Isuru/Pr...\', 331, Array)\n#1 [internal function]: ScaffoldController->getBookingList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_fun', 1, '2016-09-23 02:07:53'),
(74, 'ScaffoldController', 'getSingleBookingDetails', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'qty\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `qty` fr...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database', 1, '2016-09-27 02:36:58'),
(75, 'ScaffoldController', 'getSingleBookingDetails', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'from\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `from` f...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databas', 1, '2016-09-27 03:18:26'),
(76, 'ScaffoldController', 'getCartItemList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'scaffolding_part_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `scaffol...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Ill', 1, '2016-10-01 20:42:15'),
(77, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Undefined variable: vehicleListDifference in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:55\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(55): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 55, Array)\n#1 [internal function]: VehicleBookingController->getFindMyVehicle()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.p', 1, '2016-10-16 22:39:18'),
(78, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Undefined variable: vehicleListDifference in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:55\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(55): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 55, Array)\n#1 [internal function]: VehicleBookingController->getFindMyVehicle()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.p', 1, '2016-10-16 22:39:20'),
(79, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Undefined variable: vehicleListDifference in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:55\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(55): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 55, Array)\n#1 [internal function]: VehicleBookingController->getFindMyVehicle()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.p', 1, '2016-10-16 22:39:22'),
(80, 'UserController', 'postBookingInfo', 'BadMethodCallException: Call to undefined method Illuminate\\Database\\Query\\Builder::id() in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php:2184\nStack trace:\n#0 [internal function]: Illuminate\\Database\\Query\\Builder->__call(\'id\', Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(972): call_user_func_array(Array, Array)\n#2 [internal function]: Illuminate\\Database\\Eloquent\\Builder->__call(\'id', 1, '2016-10-19 21:47:38'),
(81, 'UserController', 'postBookingInfo', 'BadMethodCallException: Call to undefined method Illuminate\\Database\\Query\\Builder::id() in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php:2184\nStack trace:\n#0 [internal function]: Illuminate\\Database\\Query\\Builder->__call(\'id\', Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(972): call_user_func_array(Array, Array)\n#2 [internal function]: Illuminate\\Database\\Eloquent\\Builder->__call(\'id', 1, '2016-10-19 21:50:14'),
(82, 'UserController', 'postBookingInfo', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:335\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(335): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 335, Array)\n#1 [internal function]: UserController->postBookingInfo()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-10-19 21:50:55');
INSERT INTO `exceptions` (`id`, `controller`, `func`, `exception`, `status`, `created_at`) VALUES
(83, 'UserController', 'postBookingInfo', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:335\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(335): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 335, Array)\n#1 [internal function]: UserController->postBookingInfo()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-10-19 21:59:06'),
(84, 'UserController', 'postBookingInfo', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:335\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(335): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 335, Array)\n#1 [internal function]: UserController->postBookingInfo()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-10-19 21:59:39'),
(85, 'UserController', 'postBookingInfo', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:335\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(335): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 335, Array)\n#1 [internal function]: UserController->postBookingInfo()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-10-19 22:02:13'),
(86, 'VehicleBookingController', 'removeBooking', 'BadMethodCallException: Call to undefined method Illuminate\\Database\\Query\\Builder::remove() in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php:2184\nStack trace:\n#0 [internal function]: Illuminate\\Database\\Query\\Builder->__call(\'remove\', Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(972): call_user_func_array(Array, Array)\n#2 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBoo', 1, '2016-10-21 03:29:00'),
(87, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'job_id\' cannot be null in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-10-21 04:21:50'),
(88, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'job_id\' cannot be null in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-10-21 04:21:55'),
(89, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'job_id\' cannot be null in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-10-21 04:22:05'),
(90, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'job_id\' cannot be null in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-10-21 04:22:13'),
(91, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'job_id\' cannot be null in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-10-21 20:33:33'),
(92, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'job_id\' cannot be null in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-10-21 20:33:34'),
(93, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'job_id\' cannot be null in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-10-21 20:33:42'),
(94, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'job_id\' cannot be null in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:369\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(369): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databa', 1, '2016-10-21 20:33:53'),
(95, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 00:22:41'),
(96, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 00:23:14'),
(97, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 00:30:30'),
(98, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 00:49:41'),
(99, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:09:41'),
(100, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:10:08'),
(101, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:10:33'),
(102, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:11:21'),
(103, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:20:25'),
(104, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:21:52'),
(105, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:22:12'),
(106, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:23:17'),
(107, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:23:39'),
(108, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:24:22'),
(109, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:24:34'),
(110, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:25:11'),
(111, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:26:02'),
(112, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 01:26:23'),
(113, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:134\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(134): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 134, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-10-27 19:57:50'),
(114, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:172\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(172): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 172, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-27 20:27:00'),
(115, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:04:52'),
(116, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:14:54'),
(117, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:16:39'),
(118, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:18:33'),
(119, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:29:13'),
(120, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:30:17'),
(121, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:30:52'),
(122, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:31:14'),
(123, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:33:28'),
(124, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-30 20:36:07'),
(125, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-31 23:54:58'),
(126, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-10-31 23:59:28'),
(127, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-01 00:00:44'),
(128, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:174\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(174): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 174, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-01 00:01:37'),
(129, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:135\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(135): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 135, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-01 00:06:01'),
(130, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-01 00:37:07'),
(131, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-01 00:37:23'),
(132, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-01 00:40:04'),
(133, 'VehicleBookingController', 'getBookingSliderInitiatingData', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-01 04:21:54'),
(134, 'VehicleBookingController', 'getAllBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-01 04:21:54'),
(135, 'VehicleBookingController', 'getAllBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-01 19:47:57'),
(136, 'VehicleBookingController', 'getBookingSliderInitiatingData', 'ErrorException: Object of class Illuminate\\Database\\Eloquent\\Builder could not be converted to string in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:852\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(4096, \'Object of class...\', \'/Users/Isuru/Pr...\', 852, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(852): sprintf(\'%s %s\', Object(Illuminate\\Database\\Eloquent\\Builder))\n#2 [internal function]: Vehicl', 1, '2016-11-01 19:47:57'),
(137, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-01 19:57:00'),
(138, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-01 20:46:23'),
(139, 'VehicleBookingController', 'getSessionList', 'exception \'ErrorException\' with message \'Undefined index: session_id\' in /Users/olivercooper/Documents/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/olivercooper/Documents/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/oliverco...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/olivercooper/Documents/giantrentals/vendor/laravel/framewor', 1, '2016-11-01 22:22:39'),
(140, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:199\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(199): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 199, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-02 03:44:19'),
(141, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:199\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(199): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 199, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-02 03:47:57'),
(142, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:199\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(199): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 199, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-02 03:51:32'),
(143, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:199\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(199): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 199, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-02 03:51:55'),
(144, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-02 03:54:24'),
(145, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'event_item_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `event_i...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminat', 1, '2016-11-03 21:03:15'),
(146, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: array_diff(): Argument #2 is not an array in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_diff(): A...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, NULL)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/R', 1, '2016-11-03 21:22:30'),
(147, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:26:22'),
(148, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:27:32'),
(149, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:28:42'),
(150, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:28:44'),
(151, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:28:50'),
(152, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:29:05'),
(153, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:29:38'),
(154, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:36:13'),
(155, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:36:50'),
(156, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:39:48'),
(157, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:40:39'),
(158, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:41:21'),
(159, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:42:20'),
(160, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:48\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 48, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(48): array_diff(Array, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Control', 1, '2016-11-03 21:44:03'),
(161, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: array_push() expects parameter 1 to be array, integer given in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:44\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_push() ex...\', \'/Users/Isuru/Pr...\', 44, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(44): array_push(1, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/s', 1, '2016-11-03 22:00:15'),
(162, 'VehicleBookingController', 'getFindMyVehicle', 'ErrorException: array_push() expects parameter 1 to be array, integer given in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:44\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_push() ex...\', \'/Users/Isuru/Pr...\', 44, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(44): array_push(1, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/s', 1, '2016-11-03 22:01:09'),
(163, 'EventsController', 'getAddEvent', 'ErrorException: array_push() expects parameter 1 to be array, integer given in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:44\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_push() ex...\', \'/Users/Isuru/Pr...\', 44, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(44): array_push(1, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/s', 1, '2016-11-03 22:52:47'),
(164, 'EventsController', 'getAddEvent', 'ErrorException: array_push() expects parameter 1 to be array, integer given in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:44\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_push() ex...\', \'/Users/Isuru/Pr...\', 44, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(44): array_push(1, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/s', 1, '2016-11-03 23:41:13');
INSERT INTO `exceptions` (`id`, `controller`, `func`, `exception`, `status`, `created_at`) VALUES
(165, 'EventsController', 'getAddEvent', 'ErrorException: array_push() expects parameter 1 to be array, integer given in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:44\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_push() ex...\', \'/Users/Isuru/Pr...\', 44, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(44): array_push(3, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/s', 1, '2016-11-03 23:42:17'),
(166, 'EventsController', 'getAddEvent', 'ErrorException: array_push() expects parameter 1 to be array, null given in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:44\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_push() ex...\', \'/Users/Isuru/Pr...\', 44, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(44): array_push(NULL, Array)\n#2 [internal function]: EventsController->getAddEvent()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/s', 1, '2016-11-03 23:56:39'),
(167, 'EventsController', 'getSessionList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'rate\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `rate` f...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Databas', 1, '2016-11-04 02:38:01'),
(168, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:226\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(226): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 226, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-04 02:44:51'),
(169, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:226\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(226): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 226, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-04 02:47:30'),
(170, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:226\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(226): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 226, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-04 02:48:02'),
(171, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-04 03:11:35'),
(172, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-04 03:11:35'),
(173, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-04 03:11:48'),
(174, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-04 03:11:54'),
(175, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-06 01:30:40'),
(176, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-06 02:07:04'),
(177, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-06 02:07:48'),
(178, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-06 02:11:49'),
(179, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'vehicle_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `vehicle...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\D', 1, '2016-11-06 03:25:05'),
(180, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'booking_from\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `cart_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illumina', 1, '2016-11-06 03:26:22'),
(181, 'VehicleBookingController', 'getFindMyVehicle', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'booking_from\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `cart_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illumina', 1, '2016-11-06 03:27:50'),
(182, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'booking_from\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `cart_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illumina', 1, '2016-11-06 03:42:29'),
(183, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'cart_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `cart_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-06 03:47:16'),
(184, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Undefined offset: 40 in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:55\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(55): Illuminate\\Exception\\Handler->handleError(8, \'Undefined offse...\', \'/Users/Isuru/Pr...\', 55, Array)\n#1 [internal function]: ScaffoldController->getAddScaffold()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-06 03:49:52'),
(185, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Undefined variable: allBookedCartList in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:65\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(65): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 65, Array)\n#1 [internal function]: ScaffoldController->getAddScaffold()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_', 1, '2016-11-06 21:19:25'),
(186, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Undefined variable: allBookedCartList in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:65\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(65): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 65, Array)\n#1 [internal function]: ScaffoldController->getAddScaffold()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_', 1, '2016-11-06 21:20:13'),
(187, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_diff(Array, Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routin', 1, '2016-11-07 23:09:31'),
(188, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_diff(Array, Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routin', 1, '2016-11-08 00:06:54'),
(189, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_diff(Array, Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routin', 1, '2016-11-08 00:13:12'),
(190, 'getAddScaffold', 'ScaffoldController', 'ErrorException: array_diff(): at least 2 parameters are required, 1 given in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_diff(): a...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_diff(Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framewo', 1, '2016-11-08 00:16:13'),
(191, 'getAddScaffold', 'ScaffoldController', 'ErrorException: array_diff(): at least 2 parameters are required, 1 given in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_diff(): a...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_diff(Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framewo', 1, '2016-11-08 00:16:32'),
(192, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_diff(Array, Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routin', 1, '2016-11-08 00:18:18'),
(193, 'getAddScaffold', 'ScaffoldController', 'ErrorException: array_diff(): Argument #1 is not an array in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_diff(): A...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_diff(NULL, NULL)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illu', 1, '2016-11-08 00:18:53'),
(194, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Array to string conversion in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'Array to string...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_diff(Array, Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routin', 1, '2016-11-08 00:19:34'),
(195, 'getAddScaffold', 'ScaffoldController', 'ErrorException: Undefined variable: requiredQty in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:63\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(63): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 63, Array)\n#1 [internal function]: ScaffoldController->getAddScaffold()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(', 1, '2016-11-08 00:41:24'),
(196, 'getAddScaffold', 'ScaffoldController', 'ErrorException: array_push() expects at least 2 parameters, 1 given in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:55\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_push() ex...\', \'/Users/Isuru/Pr...\', 55, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(55): array_push(Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src', 1, '2016-11-08 00:47:15'),
(197, 'getAddScaffold', 'ScaffoldController', 'ErrorException: array_push() expects parameter 1 to be array, null given in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:50\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_push() ex...\', \'/Users/Isuru/Pr...\', 50, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(50): array_push(NULL, Array)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/fr', 1, '2016-11-08 01:22:42'),
(198, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-08 01:42:41'),
(199, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-08 01:43:35'),
(200, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-08 01:43:37'),
(201, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 02:27:51'),
(202, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 02:29:31'),
(203, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 02:29:42'),
(204, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 02:44:39'),
(205, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 02:48:24'),
(206, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 02:48:30'),
(207, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 02:50:12'),
(208, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 02:50:25'),
(209, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 03:33:47'),
(210, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 03:34:31'),
(211, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 03:35:39'),
(212, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 04:01:25'),
(213, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 21:49:03'),
(214, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 22:24:50'),
(215, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 22:28:36'),
(216, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 22:28:38'),
(217, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 22:31:30'),
(218, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:18:58'),
(219, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:19:06'),
(220, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:22:37'),
(221, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:24:19'),
(222, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:24:47'),
(223, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:24:47'),
(224, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:24:57'),
(225, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:24:57'),
(226, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:25:02'),
(227, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:25:02'),
(228, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:27:15'),
(229, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:27:15'),
(230, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:39:54'),
(231, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:39:54'),
(232, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:46:39'),
(233, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:46:39'),
(234, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:46:47'),
(235, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:46:47'),
(236, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:47:39'),
(237, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:47:39'),
(238, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:52:24'),
(239, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-08 23:52:24'),
(240, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:52:24'),
(241, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-08 23:52:28'),
(242, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:52:28'),
(243, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:52:28'),
(244, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:52:54'),
(245, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:52:55'),
(246, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-08 23:57:00'),
(247, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:57:01');
INSERT INTO `exceptions` (`id`, `controller`, `func`, `exception`, `status`, `created_at`) VALUES
(248, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:57:01'),
(249, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-08 23:57:16'),
(250, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-08 23:57:16'),
(251, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:00:19'),
(252, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:00:19'),
(253, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:00:32'),
(254, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:00:32'),
(255, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:00:32'),
(256, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:01:22'),
(257, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:01:22'),
(258, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:01:22'),
(259, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:02:32'),
(260, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:02:32'),
(261, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:02:40'),
(262, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:02:40'),
(263, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:02:40'),
(264, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:03:45'),
(265, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:03:45'),
(266, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:03:52'),
(267, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:03:52'),
(268, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:03:52'),
(269, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:04:55'),
(270, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:04:55'),
(271, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:04:56'),
(272, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:05:09'),
(273, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:05:09'),
(274, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:05:24'),
(275, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:05:24'),
(276, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:05:24'),
(277, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:05:58'),
(278, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:05:58'),
(279, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:07:20'),
(280, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:07:20'),
(281, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:08:04'),
(282, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:08:04'),
(283, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:08:05'),
(284, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:08:14'),
(285, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:08:14'),
(286, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:44:33'),
(287, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:44:33'),
(288, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:48:02'),
(289, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:48:02'),
(290, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:48:11'),
(291, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:48:11'),
(292, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:48:11'),
(293, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:52:06'),
(294, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:52:06'),
(295, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:52:06'),
(296, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 00:58:58'),
(297, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 00:58:58'),
(298, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 00:58:58'),
(299, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 20:21:22'),
(300, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 20:21:23'),
(301, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 20:21:24'),
(302, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 20:26:05'),
(303, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 20:26:05'),
(304, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 20:27:00'),
(305, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 20:55:24'),
(306, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 20:56:20'),
(307, 'ScaffoldController', 'getSessionList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'cart_id\' in \'where clause\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select count(`q...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Da', 1, '2016-11-09 20:58:36'),
(308, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 20:58:37'),
(309, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 21:20:38'),
(310, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 21:20:52'),
(311, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:28:29'),
(312, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:29:16'),
(313, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:30:22'),
(314, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:31:10'),
(315, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:31:50'),
(316, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:34:35'),
(317, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:34:35'),
(318, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:34:43'),
(319, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:35:14'),
(320, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:35:14'),
(321, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:35:25'),
(322, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:35:25'),
(323, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:35:30'),
(324, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:37:14'),
(325, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:37:14'),
(326, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:37:20'),
(327, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:39:28'),
(328, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:39:28'),
(329, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:40:00'),
(330, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:40:00');
INSERT INTO `exceptions` (`id`, `controller`, `func`, `exception`, `status`, `created_at`) VALUES
(331, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:40:09'),
(332, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:41:52'),
(333, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:41:52'),
(334, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:43:22'),
(335, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:43:22'),
(336, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:43:58'),
(337, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:43:58'),
(338, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:44:14'),
(339, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:44:14'),
(340, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:44:29'),
(341, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:44:29'),
(342, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:46:37'),
(343, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:46:44'),
(344, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:46:44'),
(345, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:47:31'),
(346, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:47:38'),
(347, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:47:38'),
(348, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:48:10'),
(349, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:48:27'),
(350, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:48:27'),
(351, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-09 21:48:45'),
(352, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:49:16'),
(353, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:49:40'),
(354, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:50:10'),
(355, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:50:10'),
(356, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:50:28'),
(357, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:50:28'),
(358, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:50:51'),
(359, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:50:51'),
(360, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 21:51:03'),
(361, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:51:03'),
(362, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:51:03'),
(363, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 21:51:15'),
(364, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:51:15'),
(365, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:51:15'),
(366, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:51:35'),
(367, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:51:35'),
(368, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:51:58'),
(369, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:51:58'),
(370, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:53:38'),
(371, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:53:39'),
(372, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:56:43'),
(373, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:56:43'),
(374, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 21:56:50'),
(375, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:56:50'),
(376, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:56:50'),
(377, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 21:56:56'),
(378, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 21:56:56'),
(379, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 22:00:00'),
(380, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 22:00:01'),
(381, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 22:00:11'),
(382, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 22:00:11'),
(383, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 22:00:42'),
(384, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 22:00:43'),
(385, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:225\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(225): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 225, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 22:00:47'),
(386, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 22:00:47'),
(387, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 22:00:47'),
(388, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 22:01:04'),
(389, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 22:01:04'),
(390, 'EventsController', 'getAddEvent', 'ErrorException: Undefined offset: 0 in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:110\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(110): Illuminate\\Exception\\Handler->handleError(8, \'Undefined offse...\', \'/Users/Isuru/Pr...\', 110, Array)\n#1 [internal function]: EventsController->getAddEvent()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /', 1, '2016-11-09 22:01:18'),
(391, 'EventsController', 'getAddEvent', 'ErrorException: Undefined offset: 0 in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:110\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(110): Illuminate\\Exception\\Handler->handleError(8, \'Undefined offse...\', \'/Users/Isuru/Pr...\', 110, Array)\n#1 [internal function]: EventsController->getAddEvent()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /', 1, '2016-11-09 22:01:19'),
(392, 'EventsController', 'getAddEvent', 'ErrorException: Undefined offset: 0 in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:110\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(110): Illuminate\\Exception\\Handler->handleError(8, \'Undefined offse...\', \'/Users/Isuru/Pr...\', 110, Array)\n#1 [internal function]: EventsController->getAddEvent()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /', 1, '2016-11-09 22:01:23'),
(393, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 22:01:25'),
(394, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 22:01:25'),
(395, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:209\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(209): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 209, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array', 1, '2016-11-09 22:01:35'),
(396, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:198\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(198): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 198, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_use', 1, '2016-11-09 22:01:35'),
(397, 'EventsController', 'getSessionList', 'ErrorException: Undefined variable: ret in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:224\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(224): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 224, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Arra', 1, '2016-11-09 22:01:35'),
(398, 'EventsController', 'getAddEvent', 'ErrorException: Undefined offset: 0 in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:110\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(110): Illuminate\\Exception\\Handler->handleError(8, \'Undefined offse...\', \'/Users/Isuru/Pr...\', 110, Array)\n#1 [internal function]: EventsController->getAddEvent()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /', 1, '2016-11-09 22:01:40'),
(399, 'EventsController', 'getAddEvent', 'ErrorException: Undefined offset: 0 in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:110\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(110): Illuminate\\Exception\\Handler->handleError(8, \'Undefined offse...\', \'/Users/Isuru/Pr...\', 110, Array)\n#1 [internal function]: EventsController->getAddEvent()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /', 1, '2016-11-09 22:06:48'),
(400, 'EventsController', 'getAddEvent', 'ErrorException: Undefined offset: 0 in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:110\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(110): Illuminate\\Exception\\Handler->handleError(8, \'Undefined offse...\', \'/Users/Isuru/Pr...\', 110, Array)\n#1 [internal function]: EventsController->getAddEvent()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3 /', 1, '2016-11-09 22:06:58'),
(401, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 00:40:03'),
(402, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 00:40:09'),
(403, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 00:40:18'),
(404, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 00:40:36'),
(405, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 00:41:45'),
(406, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 00:41:45'),
(407, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:161\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(161): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 161, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 00:41:45'),
(408, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:161\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(161): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 161, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 00:41:50'),
(409, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 00:41:50'),
(410, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 00:41:50'),
(411, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 00:42:44'),
(412, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 00:42:44'),
(413, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:161\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(161): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 161, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 00:42:44');
INSERT INTO `exceptions` (`id`, `controller`, `func`, `exception`, `status`, `created_at`) VALUES
(414, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 00:43:06'),
(415, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 00:45:06'),
(416, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 01:06:45'),
(417, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 01:10:08'),
(418, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 01:10:13'),
(419, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 01:13:28'),
(420, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 01:16:00'),
(421, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 01:16:37'),
(422, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 01:16:39'),
(423, 'getAddScaffold', 'ScaffoldController', 'PDOException: SQLSTATE[HY000]: General error: 2031  in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:301\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(301): PDOStatement->execute(Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{closure}(Object(Illuminate\\Database\\MySqlCo', 1, '2016-11-10 01:20:53'),
(424, 'getAddScaffold', 'ScaffoldController', 'ErrorException: array_diff(): Argument #2 is not an array in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:54\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'array_diff(): A...\', \'/Users/Isuru/Pr...\', 54, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(54): array_diff(Array, NULL)\n#2 [internal function]: ScaffoldController->getAddScaffold()\n#3 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Ill', 1, '2016-11-10 01:26:16'),
(425, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 20:41:33'),
(426, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 20:41:33'),
(427, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 20:41:33'),
(428, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 20:42:09'),
(429, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 20:42:09'),
(430, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 20:42:09'),
(431, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 20:42:41'),
(432, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 20:42:41'),
(433, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 20:42:41'),
(434, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 20:42:44'),
(435, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 20:42:44'),
(436, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 20:42:44'),
(437, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 20:42:48'),
(438, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 20:42:48'),
(439, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 20:42:48'),
(440, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 21:17:53'),
(441, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 21:17:53'),
(442, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 21:17:53'),
(443, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 21:17:59'),
(444, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 21:17:59'),
(445, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 21:17:59'),
(446, 'VehicleBookingController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:148\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(148): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 148, Array)\n#1 [internal function]: VehicleBookingController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call', 1, '2016-11-10 21:33:18'),
(447, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 21:33:18'),
(448, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 21:33:18'),
(449, 'UserController', 'postBookingInfo', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:349\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(349): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 349, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#3', 1, '2016-11-10 21:34:14'),
(450, 'ScaffoldController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:167\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(167): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 167, Array)\n#1 [internal function]: ScaffoldController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(A', 1, '2016-11-10 21:37:30'),
(451, 'EventsController', 'getSessionList', 'ErrorException: Undefined index: session_id in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:175\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(175): Illuminate\\Exception\\Handler->handleError(8, \'Undefined index...\', \'/Users/Isuru/Pr...\', 175, Array)\n#1 [internal function]: EventsController->getSessionList()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, ', 1, '2016-11-10 21:37:30'),
(452, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:40:02'),
(453, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:40:11'),
(454, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:40:47'),
(455, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:41:12'),
(456, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:41:15'),
(457, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:41:27'),
(458, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:41:36'),
(459, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:41:42'),
(460, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:41:50'),
(461, 'VehicleBookingController', 'getSessionList', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php:149\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 149, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/VehicleBookingController.php(149): session_start()\n#2 [internal function]: VehicleBookingController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals', 1, '2016-11-10 21:41:56'),
(462, 'UserController', 'postBookingInfo', 'ErrorException: Undefined variable: page in /Users/Isuru/Projects/giantrentals/app/storage/views/c6a42c4566b9dc25225b677034ebfae5:55\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/storage/views/c6a42c4566b9dc25225b677034ebfae5(55): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 55, Array)\n#1 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr...\')\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illumin', 1, '2016-11-11 21:01:44'),
(463, 'UserController', 'postBookingInfo', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:356\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(356): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 356, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-11 21:03:03'),
(464, 'UserController', 'postBookingInfo', 'Swift_TransportException: Expected response code 250 but got code "535", with message "535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials u23sm17216535pfg.86 - gsmtp\r\n" in /Users/Isuru/Projects/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php:383\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(281): Swi', 1, '2016-11-11 21:23:54'),
(465, 'UserController', 'postBookingInfo', 'Swift_TransportException: Expected response code 250 but got code "535", with message "535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials l11sm17314441pfb.28 - gsmtp\r\n" in /Users/Isuru/Projects/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php:383\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(281): Swi', 1, '2016-11-11 21:31:19'),
(466, 'ScaffoldController', 'getBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-15 02:29:53'),
(467, 'ScaffoldController', 'getBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-15 02:29:58'),
(468, 'UserController', 'postBookingInfo', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 03:07:07'),
(469, 'UserController', 'postBookingInfo', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 04:38:18'),
(470, 'UserController', 'postBookingInfo', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 04:40:02'),
(471, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:339\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(339): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 339, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-15 04:42:06'),
(472, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 04:43:16'),
(473, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 04:43:17'),
(474, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 04:46:09'),
(475, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 04:46:23'),
(476, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 04:47:50'),
(477, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:173\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 173, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(173): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-15 04:49:03'),
(478, 'UserController', 'postBookingInfo', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:147\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 147, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(147): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-17 22:29:28'),
(479, 'UserController', 'postBooking', 'ErrorException: session_start(): Cannot send session cache limiter - headers already sent (output started at /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:2) in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:286\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'session_start()...\', \'/Users/Isuru/Pr...\', 286, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(286): session_start()\n#2 [internal functio', 1, '2016-11-17 22:31:44'),
(480, 'UserController', 'postBooking', 'ErrorException: session_start(): Cannot send session cache limiter - headers already sent (output started at /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:2) in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:286\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'session_start()...\', \'/Users/Isuru/Pr...\', 286, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(286): session_start()\n#2 [internal functio', 1, '2016-11-17 22:31:47'),
(481, 'UserController', 'postBooking', 'ErrorException: session_start(): Cannot send session cache limiter - headers already sent (output started at /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:2) in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:286\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(2, \'session_start()...\', \'/Users/Isuru/Pr...\', 286, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(286): session_start()\n#2 [internal functio', 1, '2016-11-17 22:33:09'),
(482, 'EventsController', 'getSessionList', 'ErrorException: ob_end_flush(): failed to delete and flush buffer. No buffer to delete or flush in /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php:223\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'ob_end_flush():...\', \'/Users/Isuru/Pr...\', 223, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/EventsController.php(223): ob_end_flush()\n#2 [internal function]: EventsController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals/vendor', 1, '2016-11-18 00:03:50'),
(483, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:147\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 147, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(147): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-18 00:08:09'),
(484, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:147\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 147, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(147): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-18 00:09:46'),
(485, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:147\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 147, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(147): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-18 00:09:58'),
(486, 'UserController', 'postBooking', 'ErrorException: A session had already been started - ignoring session_start() in /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c:147\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'A session had a...\', \'/Users/Isuru/Pr...\', 147, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/storage/views/5539593465476f78efd8bad98f64948c(147): session_start()\n#2 /Users/Isuru/Projects/giantrentals/bootstrap/compiled.php(9973): include(\'/Users/Isuru/Pr', 1, '2016-11-18 00:10:47'),
(487, 'ScaffoldController', 'getSessionList', 'ErrorException: ob_end_flush(): failed to delete and flush buffer. No buffer to delete or flush in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:216\nStack trace:\n#0 [internal function]: Illuminate\\Exception\\Handler->handleError(8, \'ob_end_flush():...\', \'/Users/Isuru/Pr...\', 216, Array)\n#1 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(216): ob_end_flush()\n#2 [internal function]: ScaffoldController->getSessionList()\n#3 /Users/Isuru/Projects/giantrentals/', 1, '2016-11-18 00:12:31'),
(488, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:312\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(312): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 312, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-18 01:47:00'),
(489, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:312\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(312): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 312, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-18 01:54:56'),
(490, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:322\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(322): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 322, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-18 02:05:23'),
(491, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:322\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(322): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 322, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-18 02:08:40'),
(492, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:323\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(323): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 323, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-18 02:13:32'),
(493, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:323\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(323): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 323, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-18 02:15:35'),
(494, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:325\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(325): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 325, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-18 02:24:37'),
(495, 'UserController', 'postBooking', 'ErrorException: Undefined variable: _SESSION in /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php:325\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/UserController.php(325): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 325, Array)\n#1 [internal function]: UserController->postBooking()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_array(Array, Array)\n#', 1, '2016-11-18 02:35:57'),
(496, 'ScaffoldController', 'getBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-19 20:50:34');
INSERT INTO `exceptions` (`id`, `controller`, `func`, `exception`, `status`, `created_at`) VALUES
(497, 'ScaffoldController', 'getBookingList', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-19 20:51:48'),
(498, 'ScaffoldController', 'getSingleBookingDetails', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'info_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `info_id...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Data', 1, '2016-11-20 01:58:48'),
(499, 'ScaffoldController', 'getSingleBookingDetails', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'pavement\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `pavemen...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Dat', 1, '2016-11-20 02:00:31'),
(500, 'ScaffoldController', 'getSingleBookingDetails', 'ErrorException: Undefined variable: cart in /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php:654\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldController.php(654): Illuminate\\Exception\\Handler->handleError(8, \'Undefined varia...\', \'/Users/Isuru/Pr...\', 654, Array)\n#1 [internal function]: ScaffoldController->getSingleBookingDetails()\n#2 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Routing/Controller.php(231): call_user_func_a', 1, '2016-11-20 02:00:58'),
(501, 'ScaffoldController', 'getSingleBookingDetails', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'scaffolding_type_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `scaffol...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Ill', 1, '2016-11-20 04:19:40'),
(502, 'ScaffoldController', 'getSingleBookingDetails', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'scaffolding_type_id\' in \'field list\' in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:299\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(299): PDO->prepare(\'select `scaffol...\')\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Ill', 1, '2016-11-20 19:26:14'),
(503, 'ScaffoldController', 'updateCartStatus', 'BadMethodCallException: Call to undefined method Illuminate\\Database\\Query\\Builder::status() in /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Query/Builder.php:2184\nStack trace:\n#0 [internal function]: Illuminate\\Database\\Query\\Builder->__call(\'status\', Array)\n#1 /Users/Isuru/Projects/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php(972): call_user_func_array(Array, Array)\n#2 /Users/Isuru/Projects/giantrentals/app/controllers/ScaffoldCo', 1, '2016-11-20 23:44:54'),
(504, 'UserController', 'postFrontendSingIn', 'Swift_TransportException: Expected response code 250 but got code "535", with message "535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials c128sm89718934pfc.39 - gsmtp\r\n" in /Users/Isuru/Projects/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php:383\nStack trace:\n#0 /Users/Isuru/Projects/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(281): Sw', 1, '2016-11-28 23:30:50'),
(505, 'UserController', 'postBooking', 'Swift_TransportException: Expected response code 250 but got code "535", with message "535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials w17sm67272086pgm.18 - gsmtp\r\n" in /Users/isuru/project/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php:383\nStack trace:\n#0 /Users/isuru/project/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(281): Swift', 1, '2016-12-11 01:43:15'),
(506, 'UserController', 'postBooking', 'Swift_TransportException: Expected response code 250 but got code "535", with message "535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials y6sm67262483pge.16 - gsmtp\r\n" in /Users/isuru/project/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php:383\nStack trace:\n#0 /Users/isuru/project/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(281): Swift_', 1, '2016-12-11 01:45:11'),
(507, 'UserController', 'postBooking', 'Swift_TransportException: Expected response code 250 but got code "535", with message "535-5.7.8 Username and Password not accepted. Learn more at\r\n535 5.7.8  https://support.google.com/mail/?p=BadCredentials h17sm13768503pfh.62 - gsmtp\r\n" in /Users/isuru/project/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php:383\nStack trace:\n#0 /Users/isuru/project/giantrentals/vendor/swiftmailer/swiftmailer/lib/classes/Swift/Transport/AbstractSmtpTransport.php(281): Swift', 1, '2017-02-07 21:11:01'),
(508, 'UserController', 'postBooking', 'PDOException: SQLSTATE[42S22]: Column not found: 1054 Unknown column \'0\' in \'field list\' in /Users/isuru/project/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php:389\nStack trace:\n#0 /Users/isuru/project/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(389): PDO->prepare(\'update `vehicle...\')\n#1 /Users/isuru/project/giantrentals/vendor/laravel/framework/src/Illuminate/Database/Connection.php(617): Illuminate\\Database\\Connection->Illuminate\\Database\\{clo', 2, '2017-02-07 21:31:14');

-- --------------------------------------------------------

--
-- Table structure for table `scaffolding_booking`
--

DROP TABLE IF EXISTS `scaffolding_booking`;
CREATE TABLE `scaffolding_booking` (
  `id` int(11) NOT NULL,
  `job_id` int(11) DEFAULT NULL,
  `discount` float NOT NULL DEFAULT '0',
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scaffolding_booking`
--

INSERT INTO `scaffolding_booking` (`id`, `job_id`, `discount`, `status`, `created_at`) VALUES
(15, 9, 0, 3, '2016-09-01 10:46:02'),
(16, 2, 0, 3, '2016-09-09 10:46:02'),
(17, 15, 0, 3, '2016-09-09 10:46:02'),
(18, 20, 0, 3, '2016-09-09 10:46:02'),
(19, 21, 0, 3, '2016-09-09 10:46:02');

-- --------------------------------------------------------

--
-- Table structure for table `scaffolding_cart`
--

DROP TABLE IF EXISTS `scaffolding_cart`;
CREATE TABLE `scaffolding_cart` (
  `id` int(11) NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `job_id` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `scaffolding_type_id` int(11) DEFAULT NULL,
  `booking_from` datetime DEFAULT NULL,
  `booking_to` datetime DEFAULT NULL,
  `price` float DEFAULT NULL,
  `discount` float DEFAULT '0',
  `payment` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `release_at` datetime DEFAULT NULL,
  `return_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `day_rate` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scaffolding_cart`
--

INSERT INTO `scaffolding_cart` (`id`, `booking_id`, `job_id`, `qty`, `scaffolding_type_id`, `booking_from`, `booking_to`, `price`, `discount`, `payment`, `status`, `release_at`, `return_at`, `created_at`, `day_rate`) VALUES
(854, NULL, 22, 1, 3, '2016-11-20 08:00:00', '2016-11-23 09:00:00', 48, 10, 100, 1, NULL, NULL, '2016-11-20 19:46:05', 12),
(855, NULL, 22, 3, 3, '2016-11-20 08:00:00', '2016-11-23 09:00:00', 144, 10, 100, 1, NULL, NULL, '2016-11-20 19:46:11', 12),
(856, NULL, 23, 4, 3, '2016-11-23 08:00:00', '2016-11-26 15:00:00', 192, 8, NULL, 1, NULL, NULL, '2016-11-20 21:09:53', 12),
(857, NULL, 23, 2, 4, '2016-11-23 08:00:00', '2016-11-26 15:00:00', 128, 8, NULL, 1, NULL, NULL, '2016-11-20 21:10:02', 16),
(858, NULL, 23, 1, 5, '2016-11-23 08:00:00', '2016-11-26 15:00:00', 80, 8, NULL, 1, NULL, NULL, '2016-11-20 21:10:12', 20),
(859, NULL, 24, 3, 3, '2016-11-24 08:00:00', '2016-11-26 09:00:00', 108, 14, NULL, 2, NULL, NULL, '2016-11-20 23:48:14', 12),
(860, NULL, 24, 4, 4, '2016-11-24 08:00:00', '2016-11-26 09:00:00', 192, 14, NULL, 2, NULL, NULL, '2016-11-20 23:48:22', 16),
(869, NULL, 27, 1, 3, '2016-11-23 08:00:00', '2016-11-23 09:00:00', 12, 0, NULL, 4, NULL, NULL, '2016-11-23 23:14:18', 12),
(870, NULL, 30, 5, 3, '2017-02-08 08:00:00', '2017-02-10 09:00:00', 180, 0, NULL, 4, NULL, NULL, '2017-02-07 02:36:37', 12);

-- --------------------------------------------------------

--
-- Table structure for table `scaffolding_cart_items`
--

DROP TABLE IF EXISTS `scaffolding_cart_items`;
CREATE TABLE `scaffolding_cart_items` (
  `id` int(11) NOT NULL,
  `scaffolding_part_each_id` int(11) DEFAULT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '3',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scaffolding_cart_items`
--

INSERT INTO `scaffolding_cart_items` (`id`, `scaffolding_part_each_id`, `cart_id`, `status`, `created_at`) VALUES
(237, 42, 844, 1, '2016-11-10 01:48:41'),
(238, 43, 844, 1, '2016-11-10 01:48:41'),
(239, 44, 844, 1, '2016-11-10 01:48:41'),
(240, 45, 844, 1, '2016-11-10 01:48:41'),
(241, 46, 845, 1, '2016-11-10 21:33:46'),
(242, 47, 845, 1, '2016-11-10 21:33:46'),
(243, 48, 845, 1, '2016-11-10 21:33:46'),
(244, 49, 845, 1, '2016-11-10 21:33:46'),
(245, 50, 845, 1, '2016-11-10 21:33:46'),
(246, 51, 845, 1, '2016-11-10 21:33:46'),
(247, 52, 845, 1, '2016-11-10 21:33:46'),
(248, 53, 845, 1, '2016-11-10 21:33:46'),
(249, 54, 845, 1, '2016-11-10 21:33:46'),
(250, 55, 845, 1, '2016-11-10 21:33:46'),
(251, 56, 845, 1, '2016-11-10 21:33:46'),
(252, 57, 845, 1, '2016-11-10 21:33:46'),
(261, 42, 848, 1, '2016-11-15 04:37:41'),
(262, 43, 848, 1, '2016-11-15 04:37:41'),
(263, 44, 848, 1, '2016-11-15 04:37:41'),
(264, 45, 848, 1, '2016-11-15 04:37:41'),
(265, 42, 849, 1, '2016-11-18 00:37:36'),
(266, 43, 849, 1, '2016-11-18 00:37:36'),
(267, 44, 849, 1, '2016-11-18 00:37:36'),
(268, 45, 849, 1, '2016-11-18 00:37:36'),
(269, 46, 849, 1, '2016-11-18 00:37:36'),
(270, 47, 849, 1, '2016-11-18 00:37:36'),
(271, 48, 849, 1, '2016-11-18 00:37:36'),
(272, 49, 849, 1, '2016-11-18 00:37:36'),
(273, 50, 850, 1, '2016-11-18 00:52:22'),
(274, 51, 850, 1, '2016-11-18 00:52:22'),
(275, 52, 850, 1, '2016-11-18 00:52:22'),
(276, 53, 850, 1, '2016-11-18 00:52:22'),
(277, 54, 851, 1, '2016-11-18 02:38:49'),
(278, 55, 851, 1, '2016-11-18 02:38:49'),
(279, 56, 851, 1, '2016-11-18 02:38:49'),
(280, 57, 851, 1, '2016-11-18 02:38:49'),
(281, 58, 852, 1, '2016-11-18 02:50:13'),
(282, 59, 852, 1, '2016-11-18 02:50:13'),
(283, 60, 852, 1, '2016-11-18 02:50:13'),
(284, 61, 852, 1, '2016-11-18 02:50:13'),
(285, 42, 853, 1, '2016-11-18 02:55:11'),
(286, 43, 853, 1, '2016-11-18 02:55:11'),
(287, 44, 853, 1, '2016-11-18 02:55:11'),
(288, 45, 853, 1, '2016-11-18 02:55:11'),
(289, 46, 853, 1, '2016-11-18 02:55:11'),
(290, 47, 853, 1, '2016-11-18 02:55:11'),
(291, 48, 853, 1, '2016-11-18 02:55:11'),
(292, 49, 853, 1, '2016-11-18 02:55:11'),
(293, 50, 853, 1, '2016-11-18 02:55:11'),
(294, 51, 853, 1, '2016-11-18 02:55:11'),
(295, 52, 853, 1, '2016-11-18 02:55:11'),
(296, 53, 853, 1, '2016-11-18 02:55:11'),
(297, 54, 853, 1, '2016-11-18 02:55:11'),
(298, 55, 853, 1, '2016-11-18 02:55:11'),
(299, 56, 853, 1, '2016-11-18 02:55:11'),
(300, 57, 853, 1, '2016-11-18 02:55:11'),
(301, 50, 854, 1, '2016-11-20 19:46:05'),
(302, 51, 854, 1, '2016-11-20 19:46:05'),
(303, 52, 854, 1, '2016-11-20 19:46:05'),
(304, 53, 854, 1, '2016-11-20 19:46:05'),
(305, 54, 855, 1, '2016-11-20 19:46:11'),
(306, 55, 855, 1, '2016-11-20 19:46:11'),
(307, 56, 855, 1, '2016-11-20 19:46:11'),
(308, 57, 855, 1, '2016-11-20 19:46:11'),
(309, 58, 855, 1, '2016-11-20 19:46:11'),
(310, 59, 855, 1, '2016-11-20 19:46:11'),
(311, 60, 855, 1, '2016-11-20 19:46:11'),
(312, 61, 855, 1, '2016-11-20 19:46:11'),
(313, 62, 855, 1, '2016-11-20 19:46:11'),
(314, 63, 855, 1, '2016-11-20 19:46:11'),
(315, 64, 855, 1, '2016-11-20 19:46:11'),
(316, 65, 855, 1, '2016-11-20 19:46:11'),
(317, 42, 856, 1, '2016-11-20 21:09:53'),
(318, 43, 856, 1, '2016-11-20 21:09:53'),
(319, 44, 856, 1, '2016-11-20 21:09:53'),
(320, 45, 856, 1, '2016-11-20 21:09:53'),
(321, 46, 856, 1, '2016-11-20 21:09:53'),
(322, 47, 856, 1, '2016-11-20 21:09:53'),
(323, 48, 856, 1, '2016-11-20 21:09:53'),
(324, 49, 856, 1, '2016-11-20 21:09:53'),
(325, 66, 856, 1, '2016-11-20 21:09:53'),
(326, 67, 856, 1, '2016-11-20 21:09:53'),
(327, 68, 856, 1, '2016-11-20 21:09:53'),
(328, 69, 856, 1, '2016-11-20 21:09:53'),
(329, 70, 856, 1, '2016-11-20 21:09:53'),
(330, 71, 856, 1, '2016-11-20 21:09:53'),
(331, 72, 856, 1, '2016-11-20 21:09:53'),
(332, 73, 856, 1, '2016-11-20 21:09:53'),
(333, 42, 857, 1, '2016-11-20 21:10:02'),
(334, 43, 857, 1, '2016-11-20 21:10:02'),
(335, 44, 857, 1, '2016-11-20 21:10:02'),
(336, 45, 857, 1, '2016-11-20 21:10:02'),
(337, 192, 858, 1, '2016-11-20 21:10:12'),
(338, 193, 858, 1, '2016-11-20 21:10:12'),
(339, 50, 859, 1, '2016-11-20 23:48:14'),
(340, 51, 859, 1, '2016-11-20 23:48:14'),
(341, 52, 859, 1, '2016-11-20 23:48:14'),
(342, 53, 859, 1, '2016-11-20 23:48:14'),
(343, 54, 859, 1, '2016-11-20 23:48:14'),
(344, 55, 859, 1, '2016-11-20 23:48:14'),
(345, 56, 859, 1, '2016-11-20 23:48:14'),
(346, 57, 859, 1, '2016-11-20 23:48:14'),
(347, 58, 859, 1, '2016-11-20 23:48:14'),
(348, 59, 859, 1, '2016-11-20 23:48:14'),
(349, 60, 859, 1, '2016-11-20 23:48:14'),
(350, 61, 859, 1, '2016-11-20 23:48:14'),
(351, 46, 860, 1, '2016-11-20 23:48:22'),
(352, 47, 860, 1, '2016-11-20 23:48:22'),
(353, 48, 860, 1, '2016-11-20 23:48:22'),
(354, 49, 860, 1, '2016-11-20 23:48:22'),
(355, 50, 860, 1, '2016-11-20 23:48:22'),
(356, 51, 860, 1, '2016-11-20 23:48:22'),
(357, 52, 860, 1, '2016-11-20 23:48:22'),
(358, 53, 860, 1, '2016-11-20 23:48:22'),
(387, 74, 869, 1, '2016-11-23 23:14:18'),
(388, 75, 869, 1, '2016-11-23 23:14:18'),
(389, 76, 869, 1, '2016-11-23 23:14:18'),
(390, 77, 869, 1, '2016-11-23 23:14:18'),
(391, 42, 870, 1, '2017-02-07 02:36:37'),
(392, 43, 870, 1, '2017-02-07 02:36:37'),
(393, 44, 870, 1, '2017-02-07 02:36:37'),
(394, 45, 870, 1, '2017-02-07 02:36:37'),
(395, 46, 870, 1, '2017-02-07 02:36:37'),
(396, 47, 870, 1, '2017-02-07 02:36:37'),
(397, 48, 870, 1, '2017-02-07 02:36:37'),
(398, 49, 870, 1, '2017-02-07 02:36:37'),
(399, 50, 870, 1, '2017-02-07 02:36:37'),
(400, 51, 870, 1, '2017-02-07 02:36:37'),
(401, 52, 870, 1, '2017-02-07 02:36:37'),
(402, 53, 870, 1, '2017-02-07 02:36:37'),
(403, 54, 870, 1, '2017-02-07 02:36:37'),
(404, 55, 870, 1, '2017-02-07 02:36:37'),
(405, 56, 870, 1, '2017-02-07 02:36:37'),
(406, 57, 870, 1, '2017-02-07 02:36:37'),
(407, 58, 870, 1, '2017-02-07 02:36:37'),
(408, 59, 870, 1, '2017-02-07 02:36:37'),
(409, 60, 870, 1, '2017-02-07 02:36:37'),
(410, 61, 870, 1, '2017-02-07 02:36:37');

-- --------------------------------------------------------

--
-- Table structure for table `scaffolding_part`
--

DROP TABLE IF EXISTS `scaffolding_part`;
CREATE TABLE `scaffolding_part` (
  `id` int(11) NOT NULL,
  `part_code` int(11) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scaffolding_part`
--

INSERT INTO `scaffolding_part` (`id`, `part_code`, `name`, `created_at`) VALUES
(7, 638009, 'Base Collars', '2016-09-01 05:13:53'),
(8, 638376, 'Standards', '2016-09-01 05:14:30'),
(9, 638354, 'Ledgers', '2016-09-01 05:15:52'),
(10, 638837, 'Decks', '2016-09-01 05:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `scaffolding_part_each`
--

DROP TABLE IF EXISTS `scaffolding_part_each`;
CREATE TABLE `scaffolding_part_each` (
  `id` int(11) NOT NULL,
  `scaffolding_part_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scaffolding_part_each`
--

INSERT INTO `scaffolding_part_each` (`id`, `scaffolding_part_id`, `status`, `created_at`) VALUES
(42, 7, 1, '0000-00-00 00:00:00'),
(43, 7, 1, '0000-00-00 00:00:00'),
(44, 7, 1, '0000-00-00 00:00:00'),
(45, 7, 1, '0000-00-00 00:00:00'),
(46, 7, 1, '0000-00-00 00:00:00'),
(47, 7, 1, '0000-00-00 00:00:00'),
(48, 7, 1, '0000-00-00 00:00:00'),
(49, 7, 1, '0000-00-00 00:00:00'),
(50, 7, 1, '0000-00-00 00:00:00'),
(51, 7, 1, '0000-00-00 00:00:00'),
(52, 7, 1, '0000-00-00 00:00:00'),
(53, 7, 1, '0000-00-00 00:00:00'),
(54, 7, 1, '0000-00-00 00:00:00'),
(55, 7, 1, '0000-00-00 00:00:00'),
(56, 7, 1, '0000-00-00 00:00:00'),
(57, 7, 1, '0000-00-00 00:00:00'),
(58, 7, 1, '0000-00-00 00:00:00'),
(59, 7, 1, '0000-00-00 00:00:00'),
(60, 7, 1, '0000-00-00 00:00:00'),
(61, 7, 1, '0000-00-00 00:00:00'),
(62, 7, 1, '0000-00-00 00:00:00'),
(63, 7, 1, '0000-00-00 00:00:00'),
(64, 7, 1, '0000-00-00 00:00:00'),
(65, 7, 1, '0000-00-00 00:00:00'),
(66, 7, 1, '0000-00-00 00:00:00'),
(67, 7, 1, '0000-00-00 00:00:00'),
(68, 7, 1, '0000-00-00 00:00:00'),
(69, 7, 1, '0000-00-00 00:00:00'),
(70, 7, 1, '0000-00-00 00:00:00'),
(71, 7, 1, '0000-00-00 00:00:00'),
(72, 7, 1, '0000-00-00 00:00:00'),
(73, 7, 1, '0000-00-00 00:00:00'),
(74, 7, 1, '0000-00-00 00:00:00'),
(75, 7, 1, '0000-00-00 00:00:00'),
(76, 7, 1, '0000-00-00 00:00:00'),
(77, 7, 1, '0000-00-00 00:00:00'),
(78, 7, 1, '0000-00-00 00:00:00'),
(79, 7, 1, '0000-00-00 00:00:00'),
(80, 7, 1, '0000-00-00 00:00:00'),
(81, 7, 1, '0000-00-00 00:00:00'),
(82, 8, 2, '0000-00-00 00:00:00'),
(83, 8, 2, '0000-00-00 00:00:00'),
(84, 8, 2, '0000-00-00 00:00:00'),
(85, 8, 2, '0000-00-00 00:00:00'),
(86, 8, 2, '0000-00-00 00:00:00'),
(87, 8, 2, '0000-00-00 00:00:00'),
(88, 8, 2, '0000-00-00 00:00:00'),
(89, 8, 2, '0000-00-00 00:00:00'),
(90, 8, 2, '0000-00-00 00:00:00'),
(91, 8, 2, '0000-00-00 00:00:00'),
(92, 8, 2, '0000-00-00 00:00:00'),
(93, 8, 2, '0000-00-00 00:00:00'),
(94, 8, 2, '0000-00-00 00:00:00'),
(95, 8, 2, '0000-00-00 00:00:00'),
(96, 8, 2, '0000-00-00 00:00:00'),
(97, 8, 2, '0000-00-00 00:00:00'),
(98, 8, 2, '0000-00-00 00:00:00'),
(99, 8, 2, '0000-00-00 00:00:00'),
(100, 8, 2, '0000-00-00 00:00:00'),
(101, 8, 2, '0000-00-00 00:00:00'),
(102, 8, 2, '0000-00-00 00:00:00'),
(103, 8, 2, '0000-00-00 00:00:00'),
(104, 8, 2, '0000-00-00 00:00:00'),
(105, 8, 2, '0000-00-00 00:00:00'),
(106, 8, 1, '0000-00-00 00:00:00'),
(107, 8, 1, '0000-00-00 00:00:00'),
(108, 8, 1, '0000-00-00 00:00:00'),
(109, 8, 1, '0000-00-00 00:00:00'),
(110, 8, 1, '0000-00-00 00:00:00'),
(111, 8, 1, '0000-00-00 00:00:00'),
(112, 8, 1, '0000-00-00 00:00:00'),
(113, 8, 1, '0000-00-00 00:00:00'),
(114, 8, 1, '0000-00-00 00:00:00'),
(115, 8, 1, '0000-00-00 00:00:00'),
(116, 8, 1, '0000-00-00 00:00:00'),
(117, 8, 1, '0000-00-00 00:00:00'),
(118, 8, 1, '0000-00-00 00:00:00'),
(119, 8, 1, '0000-00-00 00:00:00'),
(120, 8, 1, '0000-00-00 00:00:00'),
(121, 8, 1, '0000-00-00 00:00:00'),
(122, 8, 1, '0000-00-00 00:00:00'),
(123, 8, 1, '0000-00-00 00:00:00'),
(124, 8, 1, '0000-00-00 00:00:00'),
(125, 8, 1, '0000-00-00 00:00:00'),
(126, 8, 1, '0000-00-00 00:00:00'),
(127, 8, 1, '0000-00-00 00:00:00'),
(128, 8, 1, '0000-00-00 00:00:00'),
(129, 8, 1, '0000-00-00 00:00:00'),
(130, 8, 1, '0000-00-00 00:00:00'),
(131, 8, 1, '0000-00-00 00:00:00'),
(132, 8, 1, '0000-00-00 00:00:00'),
(133, 8, 1, '0000-00-00 00:00:00'),
(134, 8, 1, '0000-00-00 00:00:00'),
(135, 8, 1, '0000-00-00 00:00:00'),
(136, 8, 1, '0000-00-00 00:00:00'),
(137, 8, 1, '0000-00-00 00:00:00'),
(138, 8, 1, '0000-00-00 00:00:00'),
(139, 8, 1, '0000-00-00 00:00:00'),
(140, 8, 1, '0000-00-00 00:00:00'),
(141, 8, 1, '0000-00-00 00:00:00'),
(142, 9, 2, '0000-00-00 00:00:00'),
(143, 9, 2, '0000-00-00 00:00:00'),
(144, 9, 2, '0000-00-00 00:00:00'),
(145, 9, 2, '0000-00-00 00:00:00'),
(146, 9, 2, '0000-00-00 00:00:00'),
(147, 9, 2, '0000-00-00 00:00:00'),
(148, 9, 2, '0000-00-00 00:00:00'),
(149, 9, 2, '0000-00-00 00:00:00'),
(150, 9, 2, '0000-00-00 00:00:00'),
(151, 9, 2, '0000-00-00 00:00:00'),
(152, 9, 2, '0000-00-00 00:00:00'),
(153, 9, 2, '0000-00-00 00:00:00'),
(154, 9, 1, '0000-00-00 00:00:00'),
(155, 9, 1, '0000-00-00 00:00:00'),
(156, 9, 1, '0000-00-00 00:00:00'),
(157, 9, 1, '0000-00-00 00:00:00'),
(158, 9, 1, '0000-00-00 00:00:00'),
(159, 9, 1, '0000-00-00 00:00:00'),
(160, 9, 1, '0000-00-00 00:00:00'),
(161, 9, 1, '0000-00-00 00:00:00'),
(162, 9, 1, '0000-00-00 00:00:00'),
(163, 9, 1, '0000-00-00 00:00:00'),
(164, 9, 1, '0000-00-00 00:00:00'),
(165, 9, 1, '0000-00-00 00:00:00'),
(166, 9, 1, '0000-00-00 00:00:00'),
(167, 9, 1, '0000-00-00 00:00:00'),
(168, 9, 1, '0000-00-00 00:00:00'),
(169, 9, 1, '0000-00-00 00:00:00'),
(170, 9, 1, '0000-00-00 00:00:00'),
(171, 9, 1, '0000-00-00 00:00:00'),
(172, 9, 1, '0000-00-00 00:00:00'),
(173, 9, 1, '0000-00-00 00:00:00'),
(174, 9, 1, '0000-00-00 00:00:00'),
(175, 9, 1, '0000-00-00 00:00:00'),
(176, 9, 1, '0000-00-00 00:00:00'),
(177, 9, 1, '0000-00-00 00:00:00'),
(178, 9, 1, '0000-00-00 00:00:00'),
(179, 9, 1, '0000-00-00 00:00:00'),
(180, 9, 1, '0000-00-00 00:00:00'),
(181, 9, 1, '0000-00-00 00:00:00'),
(182, 9, 1, '0000-00-00 00:00:00'),
(183, 9, 1, '0000-00-00 00:00:00'),
(184, 9, 1, '0000-00-00 00:00:00'),
(185, 9, 1, '0000-00-00 00:00:00'),
(186, 9, 1, '0000-00-00 00:00:00'),
(187, 9, 1, '0000-00-00 00:00:00'),
(188, 9, 1, '0000-00-00 00:00:00'),
(189, 9, 1, '0000-00-00 00:00:00'),
(190, 9, 1, '0000-00-00 00:00:00'),
(191, 9, 1, '0000-00-00 00:00:00'),
(192, 10, 2, '0000-00-00 00:00:00'),
(193, 10, 2, '0000-00-00 00:00:00'),
(194, 10, 2, '0000-00-00 00:00:00'),
(195, 10, 2, '0000-00-00 00:00:00'),
(196, 10, 2, '0000-00-00 00:00:00'),
(197, 10, 2, '0000-00-00 00:00:00'),
(198, 10, 1, '0000-00-00 00:00:00'),
(199, 10, 1, '0000-00-00 00:00:00'),
(200, 10, 1, '0000-00-00 00:00:00'),
(201, 10, 1, '0000-00-00 00:00:00'),
(202, 10, 1, '0000-00-00 00:00:00'),
(203, 10, 1, '0000-00-00 00:00:00'),
(204, 10, 1, '0000-00-00 00:00:00'),
(205, 10, 1, '0000-00-00 00:00:00'),
(206, 10, 1, '0000-00-00 00:00:00'),
(207, 10, 1, '0000-00-00 00:00:00'),
(208, 10, 1, '0000-00-00 00:00:00'),
(209, 10, 1, '0000-00-00 00:00:00'),
(210, 10, 1, '0000-00-00 00:00:00'),
(211, 10, 1, '0000-00-00 00:00:00'),
(212, 10, 1, '0000-00-00 00:00:00'),
(213, 10, 1, '0000-00-00 00:00:00'),
(214, 10, 1, '0000-00-00 00:00:00'),
(215, 10, 1, '0000-00-00 00:00:00'),
(216, 10, 1, '0000-00-00 00:00:00'),
(217, 10, 1, '0000-00-00 00:00:00'),
(218, 10, 1, '0000-00-00 00:00:00'),
(219, 10, 1, '0000-00-00 00:00:00'),
(220, 10, 1, '0000-00-00 00:00:00'),
(221, 10, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `scaffolding_type`
--

DROP TABLE IF EXISTS `scaffolding_type`;
CREATE TABLE `scaffolding_type` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `day_rate` float DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scaffolding_type`
--

INSERT INTO `scaffolding_type` (`id`, `name`, `day_rate`, `status`, `created_at`) VALUES
(3, '2M Scaffolding', 12, 1, '2016-09-01 06:19:28'),
(4, '3M Scaffolding', 16, 1, '2016-09-01 06:19:28'),
(5, '4M Scaffolding', 20, 1, '2016-09-01 06:19:28');

-- --------------------------------------------------------

--
-- Table structure for table `scaffolding_type_parts`
--

DROP TABLE IF EXISTS `scaffolding_type_parts`;
CREATE TABLE `scaffolding_type_parts` (
  `id` int(11) NOT NULL,
  `scaffolding_type_id` int(11) DEFAULT NULL,
  `scaffolding_part_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scaffolding_type_parts`
--

INSERT INTO `scaffolding_type_parts` (`id`, `scaffolding_type_id`, `scaffolding_part_id`, `quantity`) VALUES
(1, 3, 7, 4),
(2, 3, 8, 4),
(3, 3, 9, 2),
(4, 3, 10, 1),
(5, 5, 10, 2),
(6, 5, 9, 4),
(7, 5, 8, 8),
(8, 4, 7, 2),
(9, 4, 8, 4),
(10, 4, 9, 2),
(11, 4, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `session_vehicle`
--

DROP TABLE IF EXISTS `session_vehicle`;
CREATE TABLE `session_vehicle` (
  `id` int(11) NOT NULL,
  `session_id` varchar(60) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `booking_from` datetime DEFAULT NULL,
  `booking_to` datetime DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session_vehicle`
--

INSERT INTO `session_vehicle` (`id`, `session_id`, `user_id`, `booking_from`, `booking_to`, `vehicle_id`, `status`, `created_at`) VALUES
(1, '9i6ebtqcHhnDYuJk0mmKkX6RMkQv8j9A9VFo8hRWRTZDx0CHZFYfwVPAbpWR', 2, '2016-10-19 08:00:00', '2016-10-21 15:00:00', 10, 1, '2016-10-19 21:41:45'),
(2, 'LZi2khUGXXzzUD2YI7HPqbcd9DBvGJnt3D5ay9aSBqijvN4LK9Ho2alWUMYX', 2, '2016-10-21 08:00:00', '2016-10-24 13:00:00', 10, 1, '2016-10-19 22:01:23'),
(3, 'LfyijNYq2RnoQmexrhhlwtlXBlDkZfqOxd0ruIobKkx4KeTPOa6gFUz3xeDa', 2, '2016-10-19 08:00:00', '2016-10-22 08:00:00', 10, 1, '2016-10-19 22:08:49'),
(4, 'sryYSlzKFHWiOdEuIdnj3DA1cakRDV5PyX8Y52GxOiZcVbX6gWnnXM8T4qe5', 2, '2016-10-19 08:00:00', '2016-10-22 14:00:00', 10, 1, '2016-10-19 22:14:09'),
(5, 'G6L5IOd7trHePzILWPyOvBCxAPg3jB7otwmL9p7jPjWdCFRWPuva6GtvukWe', 2, '2016-10-19 08:00:00', '2016-10-22 13:00:00', 10, 1, '2016-10-19 22:15:29');

-- --------------------------------------------------------

--
-- Table structure for table `system_config`
--

DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` int(11) NOT NULL,
  `data` varchar(128) DEFAULT NULL,
  `value` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_config`
--

INSERT INTO `system_config` (`id`, `data`, `value`) VALUES
(1, 'invoice-email', 'pavithraisuru@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `system_intrusions`
--

DROP TABLE IF EXISTS `system_intrusions`;
CREATE TABLE `system_intrusions` (
  `id` int(11) NOT NULL,
  `controller` varchar(32) DEFAULT NULL,
  `func` varchar(32) DEFAULT NULL,
  `msg` varchar(256) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_intrusions`
--

INSERT INTO `system_intrusions` (`id`, `controller`, `func`, `msg`, `user_id`, `status`, `created_at`) VALUES
(1, 'testing', 'test func', 'test msg', 2, 1, '2016-09-16 00:25:38'),
(2, 'VehicleBookingController', 'addVehicle', 'Try to add a vehicle when access is not granted. Operation code ', 2, 1, '2016-09-16 00:10:34'),
(3, 'VehicleBookingController', 'addVehicle', 'Try to add a vehicle when access is not granted. Operation code ', 3, 1, '2016-09-16 00:22:45'),
(4, 'VehicleBookingController', 'updateVehicleInfo', 'Try to update a vehicle when access is not granted. Operation co', 3, 1, '2016-09-16 01:55:35'),
(5, 'VehicleBookingController', 'updateVehicleInfo', 'Try to update a vehicle when access is not granted. Operation code = 2.', 3, 1, '2016-09-16 01:55:36'),
(6, 'VehicleBookingController', 'enableDisableVehicle', 'Try to Disable a vehicle when access is not granted. Operation code = 3.', 3, 1, '2016-09-16 02:52:53');

-- --------------------------------------------------------

--
-- Table structure for table `system_operations`
--

DROP TABLE IF EXISTS `system_operations`;
CREATE TABLE `system_operations` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_operations`
--

INSERT INTO `system_operations` (`id`, `name`, `page_id`, `description`, `status`, `created_at`) VALUES
(1, 'Add Vehicle', 8, 'Add a new vehicle to the system', 1, '2016-09-15 22:06:04'),
(2, 'Edit Vehicle', 8, 'Edit current vehicle Info in the system', 1, '2016-09-15 22:06:06'),
(3, 'Enable Disable Vehicles', 8, 'Enable Disable Vehicles', 1, '2016-09-16 02:47:32'),
(4, 'Delete Booking', 3, 'Delete a created booking invoice', 1, '2016-09-22 21:34:28');

-- --------------------------------------------------------

--
-- Table structure for table `system_page_categories`
--

DROP TABLE IF EXISTS `system_page_categories`;
CREATE TABLE `system_page_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `url` varchar(32) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `icon` varchar(32) NOT NULL,
  `has_sub` int(11) NOT NULL DEFAULT '0',
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_page_categories`
--

INSERT INTO `system_page_categories` (`id`, `name`, `url`, `description`, `icon`, `has_sub`, `status`, `created_at`) VALUES
(1, 'Home', 'internal-home', NULL, 'icon-home-3', 0, 1, '2016-09-05 05:01:11'),
(2, 'Vehicles', '/', NULL, 'fa fa-truck', 1, 1, '2016-09-05 05:28:59'),
(3, 'Scaffolding', '/', NULL, 'icon icon-layers-alt', 1, 1, '2016-09-05 05:29:01'),
(4, 'Event Items', '/', NULL, 'fa fa-calendar', 1, 1, '2016-09-05 05:29:03'),
(5, 'Users', '/', NULL, 'fa fa-user', 1, 1, '2016-09-05 05:29:05'),
(6, 'System', '/', NULL, 'fa fa-desktop', 1, 1, '2016-09-05 05:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `system_page_list`
--

DROP TABLE IF EXISTS `system_page_list`;
CREATE TABLE `system_page_list` (
  `id` int(11) NOT NULL,
  `category_id` varchar(32) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `url` varchar(64) DEFAULT '#',
  `icon` varchar(32) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `system_page_list`
--

INSERT INTO `system_page_list` (`id`, `category_id`, `name`, `url`, `icon`, `status`, `created_at`) VALUES
(1, '1', 'Home', 'internal-home', 'icon-home-3', 1, '2016-11-17 22:12:53'),
(2, '2', 'Manage Bookings', 'internal-vehicle-manage-booking', 'fa fa-retweet', 1, '2016-11-17 22:12:59'),
(3, '3', 'Manage Bookings', 'internal-scaffolding', 'fa fa-retweet', 1, '2016-11-17 22:13:03'),
(4, '4', 'Manage Bookings', '/', 'fa fa-retweet', 1, '2016-11-15 02:28:03'),
(5, '5', 'Manage Users', 'get-internal-user-manage', 'fa fa-retweet', 1, '2016-11-17 22:13:08'),
(6, '6', 'System Configurations', '/', 'fa fa-wrench', 1, '2016-09-05 21:31:28'),
(7, '6', 'Exceptions', 'internal-system-exception', 'fa fa-bug', 1, '2016-11-17 22:13:19'),
(8, '2', 'Manage Vehicles', 'internal-vehicle-manage-vehicle', 'icon-list-add', 1, '2016-11-17 22:13:25'),
(9, '6', 'Intrusions', 'internal-system-intrusion', 'fa fa-warning', 1, '2016-11-17 22:13:30'),
(10, '3', 'Manage Scaffolding', 'internal-scaffolding-manage', 'fa fa-wrench', 1, '2016-11-17 22:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(60) DEFAULT NULL,
  `password_temp` varchar(60) DEFAULT NULL,
  `code` varchar(60) DEFAULT NULL,
  `active` int(2) DEFAULT NULL,
  `remember_token` varchar(120) DEFAULT NULL,
  `roll` int(1) DEFAULT NULL,
  `info_id` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `password_temp`, `code`, `active`, `remember_token`, `roll`, `info_id`, `status`, `created_at`, `updated_at`) VALUES
(2, 'pavithraisuru@gmail.com', '$2y$10$T1EzHoFTYuvBJohUqEwQzOXMQqhsamcL.xXZ3.9tbkGnibiWUc3LK', NULL, '', 1, 'Ah0xsBGpBRINsa6oSIgUZo1I3GLVN5BCsyzDg6BoxgiJ3xaqu3uC5mo4Yhat', 1, 9, 1, '2017-02-07 02:50:14', '2017-02-07 02:50:14'),
(3, 'oli@yahoo.com', '$2y$10$T1EzHoFTYuvBJohUqEwQzOXMQqhsamcL.xXZ3.9tbkGnibiWUc3LK', NULL, '', 1, 'kXqVlWF3OKmuqh3iem6Jy7rqYlHxzDVcSxfNknooqxMgWwyAzCvWi1dlfMIs', 2, 3, 1, '2016-11-18 03:52:20', '2016-11-18 03:52:20'),
(9, 'tempBfoLa-tempZxHrj-pavithraidl@yahoo.com', NULL, NULL, 'k7U8TbhUIlDmIxwXc26Shcggrxedl4fWN3MwGGmZWLnZHLFWTaU2BLVQrf0V', 0, NULL, 2, 14, 0, '2016-09-06 04:29:11', '2016-09-06 04:29:11'),
(10, 'tempgmFAu-pavithraidl@yahoo.com', NULL, NULL, 'QMpziSbc39GekUKPkWQ7UMHG0aKYK2AJhtfn7ZO1X4OB2tVqu5SuqOrqcCKm', 0, NULL, 2, 15, 0, '2016-09-06 04:31:58', '2016-09-06 04:31:58'),
(11, 'temprNohe-pavithraidl@yahoo.com', NULL, NULL, 'rMti8onlORHJFE9Gk1cW9JZanP5ROKtzf3m7LvwyRZwoa2BiaCfIFuBlPtu2', 0, NULL, 2, 16, 0, '2016-09-06 04:32:54', '2016-09-06 04:32:54'),
(12, 'tempIoAYN-pavithraidl@yahoo.com', NULL, NULL, 'vaVrkXtj2gXqUIFLYfHVDgXYAzVKzmacXfkxQ7GO3VeuyP7OLSsTVZycN6cF', 0, NULL, 2, 17, 0, '2016-09-06 04:58:48', '2016-09-06 04:58:48'),
(13, 'tempBmU35-pavithraidl@yahoo.com', NULL, NULL, 'mNEUMEEzSM3ySX9okrErBqy2i8QzGpFbVP5L5IcE189kxdHJNt3iZ07sjpiI', 0, NULL, 2, 18, 0, '2016-09-06 22:22:55', '2016-09-06 22:22:55'),
(14, 'pavithraidl@yahoo.com', NULL, NULL, 'J3FNfcoZqnN4VhsssZ6xzRFHd51o95jK9ZGal3imGdOQxESLZkoyZ4f9OyB6', 0, NULL, 2, 19, 2, '2016-09-06 22:26:30', '2016-09-07 10:26:30'),
(15, 'paviearn@gmail.com', '$2y$10$eYj2vLcPR/4u9tnIcS5ZxeikJA75Ap8hCib6m9VKZNhGwiZzHGSWC', NULL, '3Yh9gzrx39e3r7iPkJCZ0ceK9Xiymw7RC9HTZByTDkrxQ91PlxVZYn49dLyY', 0, NULL, NULL, 87, NULL, '2016-11-28 23:30:44', '2016-11-29 12:30:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity_log`
--

DROP TABLE IF EXISTS `user_activity_log`;
CREATE TABLE `user_activity_log` (
  `id` int(11) NOT NULL,
  `activity` varchar(128) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_activity_log`
--

INSERT INTO `user_activity_log` (`id`, `activity`, `user_id`, `created_at`) VALUES
(1, 'Pavithra Isuru Allowed Oliver Dennis to access in Manage Bookings page', 2, '2016-09-06 09:48:57'),
(2, 'Pavithra Isuru Allowed Oliver Dennis to access in Manage Users page', 2, '2016-09-06 10:08:22'),
(3, 'Pavithra Isuru Allowed Oliver Dennis to access in Exceptions page', 2, '2016-09-06 22:10:26'),
(4, 'Pavithra Isuru Activate Oliver Dennis\'s User Account.', 2, '2016-09-06 22:21:13'),
(5, 'Pavithra Isuru Deactivate Oliver Dennis\'s User Account.', 2, '2016-09-06 22:21:29'),
(6, 'Pavithra Isuru Activate Oliver Dennis\'s User Account.', 2, '2016-09-06 22:22:51'),
(7, 'Pavithra Isuru Delete Yahoo Test\'s User Account.', 2, '2016-09-06 22:22:55'),
(8, 'Pavithra Isuru Send an account request to Yahoo Test.', 2, '2016-09-06 22:26:30'),
(9, 'Pavithra Isuru Resend the Activation Email to Yahoo Test.', 2, '2016-09-06 22:28:08'),
(10, 'Pavithra Isuru Solved Exception 42.', 2, '2016-09-06 22:31:22'),
(11, 'Pavithra Isuru Solved Exception 43.', 2, '2016-09-07 00:08:56'),
(12, 'Pavithra Isuru Added new vehicle GEJ887.', 2, '2016-09-07 00:11:48'),
(13, 'Pavithra Isuru Allowed Oliver Dennis to access in Manage Vehicles page', 2, '2016-09-07 03:15:23'),
(14, 'Pavithra Isuru Solved Exception 44.', 2, '2016-09-07 04:49:15'),
(15, 'Pavithra Isuru Solved Exception 45.', 2, '2016-09-07 04:49:16'),
(16, 'Pavithra Isuru Solved Exception 46.', 2, '2016-09-07 22:30:02'),
(17, 'Pavithra Isuru Added new vehicle HHF384.', 2, '2016-09-07 22:37:30'),
(18, 'Pavithra Isuru Added new vehicle AHD837.', 2, '2016-09-07 22:38:20'),
(19, 'Pavithra Isuru Solved Exception 47.', 2, '2016-09-07 23:27:23'),
(20, 'Pavithra Isuru Solved Exception 48.', 2, '2016-09-09 02:55:53'),
(21, 'Pavithra Isuru Solved Exception 49.', 2, '2016-09-09 03:24:52'),
(22, 'Pavithra Isuru Solved Exception 50.', 2, '2016-09-09 03:24:53'),
(23, 'Pavithra Isuru Solved Exception 51.', 2, '2016-09-09 03:25:00'),
(24, 'Pavithra Isuru Solved Exception 52.', 2, '2016-09-09 03:25:26'),
(25, 'Pavithra Isuru Solved Exception 53.', 2, '2016-09-09 04:33:02'),
(26, 'Pavithra Isuru Solved Exception 54.', 2, '2016-09-09 04:33:04'),
(27, 'Pavithra Isuru Solved Exception 55.', 2, '2016-09-09 04:33:21'),
(28, 'Pavithra Isuru Solved Exception 56.', 2, '2016-09-11 21:31:33'),
(29, 'Pavithra Isuru Added new vehicle Booking under booking ID 14.', 2, '2016-09-12 03:07:41'),
(30, 'Pavithra Isuru Added new vehicle HRY883.', 2, '2016-09-12 03:36:35'),
(31, 'Pavithra Isuru Added new vehicle Booking under booking ID 00015.', 2, '2016-09-12 03:37:49'),
(32, 'Pavithra Isuru Added new vehicle Booking under booking ID 00016.', 2, '2016-09-12 04:15:59'),
(33, 'Pavithra Isuru Added new vehicle KJS398.', 2, '2016-09-13 00:55:23'),
(34, 'Pavithra Isuru Added new vehicle KLD043.', 2, '2016-09-13 00:55:53'),
(35, 'Pavithra Isuru Added new vehicle JKD343.', 2, '2016-09-13 00:56:16'),
(36, 'Pavithra Isuru Added new vehicle KKLDHO.', 2, '2016-09-13 00:56:42'),
(37, 'Pavithra Isuru Added new vehicle HJS093.', 2, '2016-09-13 00:57:13'),
(38, 'Pavithra Isuru Solved Exception 57.', 2, '2016-09-14 05:21:34'),
(39, 'Pavithra Isuru Solved Exception 58.', 2, '2016-09-15 00:13:03'),
(40, 'Pavithra Isuru Solved Exception 59.', 2, '2016-09-15 00:13:11'),
(41, 'Pavithra Isuru Deactivate Oliver Dennis\'s User Account.', 2, '2016-09-15 01:36:59'),
(42, 'Pavithra Isuru Activate Oliver Dennis\'s User Account.', 2, '2016-09-15 01:37:02'),
(43, 'Pavithra Isuru Solved Exception 60.', 2, '2016-09-15 01:37:29'),
(44, 'Pavithra Isuru Solved Exception 61.', 2, '2016-09-15 22:37:42'),
(45, 'Pavithra Isuru Solved Exception 62.', 2, '2016-09-15 22:39:13'),
(46, 'Pavithra Isuru Allow access for preform operation  to user Oliver Dennis', 2, '2016-09-15 22:39:28'),
(47, 'Pavithra Isuru Deny access for  to Oliver Dennis', 2, '2016-09-15 22:42:05'),
(48, 'Pavithra Isuru Allow access for  to Oliver Dennis', 2, '2016-09-15 22:42:58'),
(49, 'Pavithra Isuru Allow access for  to Oliver Dennis', 2, '2016-09-15 22:42:59'),
(50, 'Pavithra Isuru Deny access for Add a new vehicle to the system to Oliver Dennis', 2, '2016-09-15 22:44:47'),
(51, 'Pavithra Isuru Allow access for Edit current vehicle Info in the system to Oliver Dennis', 2, '2016-09-15 22:44:48'),
(52, 'Pavithra Isuru Allow access for Add a new vehicle to the system to Oliver Dennis', 2, '2016-09-15 22:45:39'),
(53, 'Pavithra Isuru Deny access for Edit current vehicle Info in the system to Oliver Dennis', 2, '2016-09-15 22:46:22'),
(54, 'Pavithra Isuru Allow access for Add a new vehicle to the system to Oliver Dennis', 2, '2016-09-15 22:48:10'),
(55, 'Pavithra Isuru Allow access for Edit current vehicle Info in the system to Oliver Dennis', 2, '2016-09-15 22:48:38'),
(56, 'Pavithra Isuru Deny access for Add a new vehicle to the system to Oliver Dennis', 2, '2016-09-15 22:51:52'),
(57, 'Pavithra Isuru Allow Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-15 22:53:15'),
(58, 'Pavithra Isuru Denied Oliver Dennis\'s access for Edit current vehicle Info in the system', 2, '2016-09-15 22:54:20'),
(59, 'Pavithra Isuru Denied Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-15 22:55:02'),
(60, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-15 23:19:03'),
(61, 'Pavithra Isuru Denied Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-15 23:19:19'),
(62, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-15 23:21:12'),
(63, 'Pavithra Isuru Denied Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-15 23:21:20'),
(64, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-15 23:23:55'),
(65, 'Pavithra Isuru Denied Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-15 23:24:03'),
(66, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-16 00:02:49'),
(67, 'Pavithra Isuru Denied Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-16 00:03:00'),
(68, 'Pavithra Isuru Solved Exception 63.', 2, '2016-09-16 00:11:48'),
(69, 'Pavithra Isuru Solved Exception 64.', 2, '2016-09-16 00:17:42'),
(70, 'Pavithra Isuru Solved Exception 65.', 2, '2016-09-16 00:17:43'),
(71, 'Pavithra Isuru Solved Exception 66.', 2, '2016-09-16 00:17:43'),
(72, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-16 00:19:39'),
(73, 'Pavithra Isuru Denied Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-16 00:19:52'),
(74, 'Pavithra Isuru Ignored or Solved Intrusion 1.', 2, '2016-09-16 00:24:55'),
(75, 'Pavithra Isuru Ignored or Solved Intrusion 1.', 2, '2016-09-16 00:25:38'),
(76, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-09-16 00:25:53'),
(77, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Edit current vehicle Info in the system', 2, '2016-09-16 00:25:54'),
(78, 'Pavithra Isuru Update vehicle Info ALT507.', 2, '2016-09-16 01:51:38'),
(79, 'Pavithra Isuru Update vehicle Info ALT507.', 2, '2016-09-16 01:53:25'),
(80, 'Pavithra Isuru Update vehicle Info GEJ887.', 2, '2016-09-16 01:53:49'),
(81, 'Pavithra Isuru Denied Oliver Dennis\'s access for Edit current vehicle Info in the system', 2, '2016-09-16 01:54:14'),
(82, 'Pavithra Isuru Ignored or Solved Intrusion 4.', 2, '2016-09-16 01:55:35'),
(83, 'Pavithra Isuru Ignored or Solved Intrusion 5.', 2, '2016-09-16 01:55:36'),
(84, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Enable Disable Vehicles', 2, '2016-09-16 02:48:02'),
(85, 'Pavithra Isuru Denied Oliver Dennis\'s access for Enable Disable Vehicles', 2, '2016-09-16 02:52:28'),
(86, 'Pavithra Isuru Ignored or Solved Intrusion 6.', 2, '2016-09-16 02:52:53'),
(87, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Edit current vehicle Info in the system', 2, '2016-09-16 02:56:48'),
(88, 'Oliver Dennis Update vehicle Info ALT507.', 3, '2016-09-16 02:57:02'),
(89, 'Oliver Dennis Update vehicle Info GEJ887.', 3, '2016-09-16 02:57:28'),
(90, 'Pavithra Isuru Allowed Oliver Dennis to access in Home page', 2, '2016-09-16 02:59:10'),
(91, 'Pavithra Isuru Denied Oliver Dennis to access in Exceptions page', 2, '2016-09-16 02:59:16'),
(92, 'Pavithra Isuru Denied Oliver Dennis to access in Manage Users page', 2, '2016-09-16 02:59:19'),
(93, 'Pavithra Isuru Allowed Oliver Dennis to access in Manage Users page', 2, '2016-09-16 02:59:22'),
(94, 'Oliver Dennis Denied Oliver Dennis to access in Manage Bookings page', 3, '2016-09-16 03:00:19'),
(95, 'Pavithra Isuru Solved Exception 67.', 2, '2016-09-22 23:25:22'),
(96, 'Pavithra Isuru Solved Exception 68.', 2, '2016-09-23 00:33:17'),
(97, 'Pavithra Isuru Solved Exception 69.', 2, '2016-09-23 01:59:07'),
(98, 'Pavithra Isuru Solved Exception 70.', 2, '2016-09-23 01:59:23'),
(99, 'Pavithra Isuru Solved Exception 71.', 2, '2016-09-23 01:59:24'),
(100, 'Pavithra Isuru Solved Exception 72.', 2, '2016-09-23 02:02:36'),
(101, 'Pavithra Isuru Solved Exception 73.', 2, '2016-09-23 04:01:47'),
(102, 'Pavithra Isuru Solved Exception 74.', 2, '2016-09-27 02:38:27'),
(103, 'Pavithra Isuru Solved Exception 75.', 2, '2016-09-27 03:18:46'),
(104, 'Pavithra Isuru Solved Exception 76.', 2, '2016-10-01 20:43:47'),
(105, 'Pavithra Isuru Added new vehicle Booking under booking ID 00017.', 2, '2016-10-02 00:09:37'),
(106, 'Pavithra Isuru Deactivate Oliver Dennis\'s User Account.', 2, '2016-10-04 03:36:55'),
(107, 'Pavithra Isuru Activate Oliver Dennis\'s User Account.', 2, '2016-10-04 03:36:56'),
(108, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Enable Disable Vehicles', 2, '2016-10-11 03:37:43'),
(109, 'Pavithra Isuru Denied Oliver Dennis\'s access for Enable Disable Vehicles', 2, '2016-10-11 03:37:44'),
(110, 'Pavithra Isuru Solved Exception 77.', 2, '2016-10-19 21:48:14'),
(111, 'Pavithra Isuru Solved Exception 78.', 2, '2016-10-19 21:48:15'),
(112, 'Pavithra Isuru Solved Exception 79.', 2, '2016-10-19 21:48:16'),
(113, 'Pavithra Isuru Solved Exception 80.', 2, '2016-10-19 21:49:12'),
(114, 'Pavithra Isuru Solved Exception 81.', 2, '2016-10-19 21:50:51'),
(115, 'Pavithra Isuru Solved Exception 82.', 2, '2016-10-19 21:54:58'),
(116, 'Pavithra Isuru Solved Exception 83.', 2, '2016-10-19 21:59:44'),
(117, 'Pavithra Isuru Solved Exception 84.', 2, '2016-10-19 21:59:48'),
(118, 'Pavithra Isuru Solved Exception 85.', 2, '2016-10-19 22:02:28'),
(119, 'Pavithra Isuru Solved Exception 86.', 2, '2016-10-21 03:30:07'),
(120, 'Pavithra Isuru Solved Exception 87.', 2, '2016-10-21 04:22:23'),
(121, 'Pavithra Isuru Solved Exception 88.', 2, '2016-10-21 04:22:23'),
(122, 'Pavithra Isuru Solved Exception 89.', 2, '2016-10-21 04:22:25'),
(123, 'Pavithra Isuru Solved Exception 90.', 2, '2016-10-21 04:26:17'),
(124, 'Pavithra Isuru Solved Exception 91.', 2, '2016-10-21 20:34:17'),
(125, 'Pavithra Isuru Solved Exception 92.', 2, '2016-10-21 20:34:19'),
(126, 'Pavithra Isuru Solved Exception 93.', 2, '2016-10-21 20:34:20'),
(127, 'Pavithra Isuru Solved Exception 94.', 2, '2016-10-21 20:34:23'),
(128, 'Pavithra Isuru Solved Exception 95.', 2, '2016-10-27 19:55:17'),
(129, 'Pavithra Isuru Solved Exception 96.', 2, '2016-10-27 19:55:17'),
(130, 'Pavithra Isuru Solved Exception 97.', 2, '2016-10-27 19:55:18'),
(131, 'Pavithra Isuru Solved Exception 98.', 2, '2016-10-27 19:55:19'),
(132, 'Pavithra Isuru Solved Exception 99.', 2, '2016-10-27 19:55:20'),
(133, 'Pavithra Isuru Solved Exception 100.', 2, '2016-10-27 19:55:21'),
(134, 'Pavithra Isuru Solved Exception 101.', 2, '2016-10-27 19:55:22'),
(135, 'Pavithra Isuru Solved Exception 102.', 2, '2016-10-27 19:55:24'),
(136, 'Pavithra Isuru Solved Exception 103.', 2, '2016-10-27 19:55:25'),
(137, 'Pavithra Isuru Solved Exception 104.', 2, '2016-10-27 19:55:26'),
(138, 'Pavithra Isuru Solved Exception 105.', 2, '2016-10-27 19:55:26'),
(139, 'Pavithra Isuru Solved Exception 106.', 2, '2016-10-27 19:55:28'),
(140, 'Pavithra Isuru Solved Exception 107.', 2, '2016-10-27 19:55:29'),
(141, 'Pavithra Isuru Solved Exception 108.', 2, '2016-10-27 19:55:30'),
(142, 'Pavithra Isuru Solved Exception 109.', 2, '2016-10-27 19:55:31'),
(143, 'Pavithra Isuru Solved Exception 110.', 2, '2016-10-27 19:55:33'),
(144, 'Pavithra Isuru Solved Exception 111.', 2, '2016-10-27 19:55:34'),
(145, 'Pavithra Isuru Solved Exception 112.', 2, '2016-10-27 19:55:35'),
(146, 'Pavithra Isuru Solved Exception 113.', 2, '2016-11-01 04:21:30'),
(147, 'Pavithra Isuru Solved Exception 114.', 2, '2016-11-01 04:21:30'),
(148, 'Pavithra Isuru Solved Exception 115.', 2, '2016-11-01 04:21:31'),
(149, 'Pavithra Isuru Solved Exception 116.', 2, '2016-11-01 04:21:32'),
(150, 'Pavithra Isuru Solved Exception 117.', 2, '2016-11-01 04:21:33'),
(151, 'Pavithra Isuru Solved Exception 118.', 2, '2016-11-01 04:21:33'),
(152, 'Pavithra Isuru Solved Exception 119.', 2, '2016-11-01 04:21:35'),
(153, 'Pavithra Isuru Solved Exception 120.', 2, '2016-11-01 04:21:35'),
(154, 'Pavithra Isuru Solved Exception 121.', 2, '2016-11-01 04:21:36'),
(155, 'Pavithra Isuru Solved Exception 122.', 2, '2016-11-01 04:21:36'),
(156, 'Pavithra Isuru Solved Exception 123.', 2, '2016-11-01 04:21:38'),
(157, 'Pavithra Isuru Solved Exception 124.', 2, '2016-11-01 04:21:39'),
(158, 'Pavithra Isuru Solved Exception 125.', 2, '2016-11-01 04:21:40'),
(159, 'Pavithra Isuru Solved Exception 126.', 2, '2016-11-01 04:21:40'),
(160, 'Pavithra Isuru Solved Exception 127.', 2, '2016-11-01 04:21:41'),
(161, 'Pavithra Isuru Solved Exception 128.', 2, '2016-11-01 04:21:43'),
(162, 'Pavithra Isuru Solved Exception 129.', 2, '2016-11-01 04:21:44'),
(163, 'Pavithra Isuru Solved Exception 130.', 2, '2016-11-01 04:21:45'),
(164, 'Pavithra Isuru Solved Exception 131.', 2, '2016-11-01 04:21:46'),
(165, 'Pavithra Isuru Solved Exception 132.', 2, '2016-11-01 04:21:46'),
(166, 'Pavithra Isuru Solved Exception 134.', 2, '2016-11-03 20:26:24'),
(167, 'Pavithra Isuru Solved Exception 133.', 2, '2016-11-03 20:26:25'),
(168, 'Pavithra Isuru Solved Exception 135.', 2, '2016-11-03 20:26:26'),
(169, 'Pavithra Isuru Solved Exception 136.', 2, '2016-11-03 20:26:27'),
(170, 'Pavithra Isuru Solved Exception 137.', 2, '2016-11-03 20:26:29'),
(171, 'Pavithra Isuru Solved Exception 138.', 2, '2016-11-03 20:26:30'),
(172, 'Pavithra Isuru Solved Exception 139.', 2, '2016-11-03 20:26:31'),
(173, 'Pavithra Isuru Solved Exception 140.', 2, '2016-11-03 20:26:32'),
(174, 'Pavithra Isuru Solved Exception 141.', 2, '2016-11-03 20:26:34'),
(175, 'Pavithra Isuru Solved Exception 142.', 2, '2016-11-03 20:26:35'),
(176, 'Pavithra Isuru Solved Exception 143.', 2, '2016-11-03 20:26:36'),
(177, 'Pavithra Isuru Solved Exception 144.', 2, '2016-11-03 20:26:37'),
(178, 'Pavithra Isuru Solved Exception 145.', 2, '2016-11-03 21:22:38'),
(179, 'Pavithra Isuru Solved Exception 146.', 2, '2016-11-03 21:23:24'),
(180, 'Pavithra Isuru Solved Exception 147.', 2, '2016-11-03 21:26:53'),
(181, 'Pavithra Isuru Solved Exception 148.', 2, '2016-11-03 21:29:50'),
(182, 'Pavithra Isuru Solved Exception 149.', 2, '2016-11-03 21:29:51'),
(183, 'Pavithra Isuru Solved Exception 150.', 2, '2016-11-03 21:29:51'),
(184, 'Pavithra Isuru Solved Exception 151.', 2, '2016-11-03 21:29:52'),
(185, 'Pavithra Isuru Solved Exception 152.', 2, '2016-11-03 21:29:52'),
(186, 'Pavithra Isuru Solved Exception 153.', 2, '2016-11-03 21:30:04'),
(187, 'Pavithra Isuru Solved Exception 154.', 2, '2016-11-03 21:36:45'),
(188, 'Pavithra Isuru Solved Exception 155.', 2, '2016-11-03 21:36:58'),
(189, 'Pavithra Isuru Solved Exception 156.', 2, '2016-11-03 21:39:59'),
(190, 'Pavithra Isuru Solved Exception 157.', 2, '2016-11-03 21:40:52'),
(191, 'Pavithra Isuru Solved Exception 158.', 2, '2016-11-03 21:41:34'),
(192, 'Pavithra Isuru Solved Exception 159.', 2, '2016-11-03 21:44:11'),
(193, 'Pavithra Isuru Solved Exception 160.', 2, '2016-11-03 21:44:14'),
(194, 'Pavithra Isuru Solved Exception 161.', 2, '2016-11-03 22:00:29'),
(195, 'Pavithra Isuru Solved Exception 162.', 2, '2016-11-03 22:01:22'),
(196, 'Pavithra Isuru Solved Exception 163.', 2, '2016-11-03 22:58:47'),
(197, 'Pavithra Isuru Solved Exception 164.', 2, '2016-11-03 23:42:12'),
(198, 'Pavithra Isuru Solved Exception 165.', 2, '2016-11-03 23:42:24'),
(199, 'Pavithra Isuru Solved Exception 166.', 2, '2016-11-04 02:43:51'),
(200, 'Pavithra Isuru Solved Exception 167.', 2, '2016-11-04 02:43:52'),
(201, 'Pavithra Isuru Solved Exception 168.', 2, '2016-11-05 20:28:23'),
(202, 'Pavithra Isuru Solved Exception 169.', 2, '2016-11-05 20:28:24'),
(203, 'Pavithra Isuru Solved Exception 170.', 2, '2016-11-05 20:28:24'),
(204, 'Pavithra Isuru Solved Exception 172.', 2, '2016-11-05 20:28:25'),
(205, 'Pavithra Isuru Solved Exception 171.', 2, '2016-11-05 20:28:26'),
(206, 'Pavithra Isuru Solved Exception 173.', 2, '2016-11-05 20:28:27'),
(207, 'Pavithra Isuru Solved Exception 174.', 2, '2016-11-05 20:28:28'),
(208, 'Pavithra Isuru Solved Exception 175.', 2, '2016-11-06 03:25:17'),
(209, 'Pavithra Isuru Solved Exception 176.', 2, '2016-11-06 03:25:18'),
(210, 'Pavithra Isuru Solved Exception 177.', 2, '2016-11-06 03:25:18'),
(211, 'Pavithra Isuru Solved Exception 178.', 2, '2016-11-06 03:25:19'),
(212, 'Pavithra Isuru Solved Exception 179.', 2, '2016-11-06 03:25:23'),
(213, 'Pavithra Isuru Solved Exception 180.', 2, '2016-11-06 03:26:34'),
(214, 'Pavithra Isuru Solved Exception 181.', 2, '2016-11-06 03:41:38'),
(215, 'Pavithra Isuru Solved Exception 182.', 2, '2016-11-06 03:43:29'),
(216, 'Pavithra Isuru Solved Exception 182.', 2, '2016-11-06 03:43:29'),
(217, 'Pavithra Isuru Solved Exception 183.', 2, '2016-11-06 03:47:25'),
(218, 'Pavithra Isuru Solved Exception 184.', 2, '2016-11-06 20:45:38'),
(219, 'Pavithra Isuru Solved Exception 185.', 2, '2016-11-06 21:19:48'),
(220, 'Pavithra Isuru Solved Exception 186.', 2, '2016-11-07 23:09:39'),
(221, 'Pavithra Isuru Solved Exception 187.', 2, '2016-11-08 00:06:42'),
(222, 'Pavithra Isuru Solved Exception 188.', 2, '2016-11-08 00:12:48'),
(223, 'Pavithra Isuru Solved Exception 189.', 2, '2016-11-08 00:16:41'),
(224, 'Pavithra Isuru Solved Exception 190.', 2, '2016-11-08 00:16:43'),
(225, 'Pavithra Isuru Solved Exception 191.', 2, '2016-11-08 00:16:44'),
(226, 'Pavithra Isuru Solved Exception 192.', 2, '2016-11-08 00:18:51'),
(227, 'Pavithra Isuru Solved Exception 193.', 2, '2016-11-08 00:19:02'),
(228, 'Pavithra Isuru Solved Exception 194.', 2, '2016-11-08 00:41:36'),
(229, 'Pavithra Isuru Solved Exception 195.', 2, '2016-11-08 00:41:37'),
(230, 'Pavithra Isuru Solved Exception 196.', 2, '2016-11-08 01:22:51'),
(231, 'Pavithra Isuru Solved Exception 197.', 2, '2016-11-08 01:22:52'),
(232, 'Pavithra Isuru Solved Exception 199.', 2, '2016-11-08 01:44:05'),
(233, 'Pavithra Isuru Solved Exception 198.', 2, '2016-11-09 21:18:15'),
(234, 'Pavithra Isuru Solved Exception 200.', 2, '2016-11-09 21:18:16'),
(235, 'Pavithra Isuru Solved Exception 201.', 2, '2016-11-09 21:18:17'),
(236, 'Pavithra Isuru Solved Exception 202.', 2, '2016-11-09 21:18:18'),
(237, 'Pavithra Isuru Solved Exception 203.', 2, '2016-11-09 21:18:19'),
(238, 'Pavithra Isuru Solved Exception 204.', 2, '2016-11-09 21:18:20'),
(239, 'Pavithra Isuru Solved Exception 205.', 2, '2016-11-09 21:18:22'),
(240, 'Pavithra Isuru Solved Exception 206.', 2, '2016-11-09 21:18:22'),
(241, 'Pavithra Isuru Solved Exception 207.', 2, '2016-11-09 21:18:23'),
(242, 'Pavithra Isuru Solved Exception 208.', 2, '2016-11-09 21:18:24'),
(243, 'Pavithra Isuru Solved Exception 209.', 2, '2016-11-09 21:18:26'),
(244, 'Pavithra Isuru Solved Exception 210.', 2, '2016-11-09 21:18:28'),
(245, 'Pavithra Isuru Solved Exception 211.', 2, '2016-11-09 21:18:28'),
(246, 'Pavithra Isuru Solved Exception 212.', 2, '2016-11-09 21:18:29'),
(247, 'Pavithra Isuru Solved Exception 213.', 2, '2016-11-09 21:18:31'),
(248, 'Pavithra Isuru Solved Exception 214.', 2, '2016-11-09 21:18:32'),
(249, 'Pavithra Isuru Solved Exception 215.', 2, '2016-11-09 21:18:32'),
(250, 'Pavithra Isuru Solved Exception 216.', 2, '2016-11-09 21:18:33'),
(251, 'Pavithra Isuru Solved Exception 217.', 2, '2016-11-09 21:18:34'),
(252, 'Pavithra Isuru Solved Exception 218.', 2, '2016-11-09 21:18:35'),
(253, 'Pavithra Isuru Solved Exception 219.', 2, '2016-11-09 21:18:37'),
(254, 'Pavithra Isuru Solved Exception 220.', 2, '2016-11-09 21:18:38'),
(255, 'Pavithra Isuru Solved Exception 221.', 2, '2016-11-09 21:18:39'),
(256, 'Pavithra Isuru Solved Exception 222.', 2, '2016-11-09 21:18:41'),
(257, 'Pavithra Isuru Solved Exception 223.', 2, '2016-11-09 21:18:42'),
(258, 'Pavithra Isuru Solved Exception 224.', 2, '2016-11-09 21:18:43'),
(259, 'Pavithra Isuru Solved Exception 225.', 2, '2016-11-09 21:18:44'),
(260, 'Pavithra Isuru Solved Exception 226.', 2, '2016-11-09 21:18:44'),
(261, 'Pavithra Isuru Solved Exception 227.', 2, '2016-11-09 21:18:45'),
(262, 'Pavithra Isuru Solved Exception 229.', 2, '2016-11-09 21:18:47'),
(263, 'Pavithra Isuru Solved Exception 229.', 2, '2016-11-09 21:18:48'),
(264, 'Pavithra Isuru Solved Exception 229.', 2, '2016-11-09 21:18:49'),
(265, 'Pavithra Isuru Solved Exception 230.', 2, '2016-11-09 21:18:50'),
(266, 'Pavithra Isuru Solved Exception 231.', 2, '2016-11-09 21:18:51'),
(267, 'Pavithra Isuru Solved Exception 233.', 2, '2016-11-09 21:18:51'),
(268, 'Pavithra Isuru Solved Exception 232.', 2, '2016-11-09 21:18:52'),
(269, 'Pavithra Isuru Solved Exception 235.', 2, '2016-11-09 21:18:54'),
(270, 'Pavithra Isuru Solved Exception 234.', 2, '2016-11-09 21:18:55'),
(271, 'Pavithra Isuru Solved Exception 237.', 2, '2016-11-09 21:18:55'),
(272, 'Pavithra Isuru Solved Exception 236.', 2, '2016-11-09 21:18:56'),
(273, 'Pavithra Isuru Solved Exception 240.', 2, '2016-11-09 21:18:58'),
(274, 'Pavithra Isuru Solved Exception 240.', 2, '2016-11-09 21:18:58'),
(275, 'Pavithra Isuru Solved Exception 240.', 2, '2016-11-09 21:19:00'),
(276, 'Pavithra Isuru Solved Exception 239.', 2, '2016-11-09 21:19:01'),
(277, 'Pavithra Isuru Solved Exception 241.', 2, '2016-11-09 21:19:02'),
(278, 'Pavithra Isuru Solved Exception 242.', 2, '2016-11-09 21:19:03'),
(279, 'Pavithra Isuru Solved Exception 243.', 2, '2016-11-09 21:19:04'),
(280, 'Pavithra Isuru Solved Exception 244.', 2, '2016-11-09 21:19:05'),
(281, 'Pavithra Isuru Solved Exception 245.', 2, '2016-11-09 21:19:06'),
(282, 'Pavithra Isuru Solved Exception 246.', 2, '2016-11-09 21:19:07'),
(283, 'Pavithra Isuru Solved Exception 247.', 2, '2016-11-09 21:19:08'),
(284, 'Pavithra Isuru Solved Exception 248.', 2, '2016-11-09 21:19:09'),
(285, 'Pavithra Isuru Solved Exception 249.', 2, '2016-11-09 21:19:10'),
(286, 'Pavithra Isuru Solved Exception 250.', 2, '2016-11-09 21:19:11'),
(287, 'Pavithra Isuru Solved Exception 251.', 2, '2016-11-09 21:19:12'),
(288, 'Pavithra Isuru Solved Exception 252.', 2, '2016-11-09 21:19:13'),
(289, 'Pavithra Isuru Solved Exception 253.', 2, '2016-11-09 21:19:14'),
(290, 'Pavithra Isuru Solved Exception 254.', 2, '2016-11-09 21:19:15'),
(291, 'Pavithra Isuru Solved Exception 255.', 2, '2016-11-09 21:19:16'),
(292, 'Pavithra Isuru Solved Exception 257.', 2, '2016-11-09 21:19:17'),
(293, 'Pavithra Isuru Solved Exception 258.', 2, '2016-11-09 21:19:18'),
(294, 'Pavithra Isuru Solved Exception 256.', 2, '2016-11-09 21:19:19'),
(295, 'Pavithra Isuru Solved Exception 259.', 2, '2016-11-09 21:19:20'),
(296, 'Pavithra Isuru Solved Exception 260.', 2, '2016-11-09 21:19:21'),
(297, 'Pavithra Isuru Solved Exception 261.', 2, '2016-11-09 21:19:21'),
(298, 'Pavithra Isuru Solved Exception 263.', 2, '2016-11-09 21:19:24'),
(299, 'Pavithra Isuru Solved Exception 262.', 2, '2016-11-09 21:19:25'),
(300, 'Pavithra Isuru Solved Exception 264.', 2, '2016-11-09 21:19:25'),
(301, 'Pavithra Isuru Solved Exception 265.', 2, '2016-11-09 21:19:27'),
(302, 'Pavithra Isuru Solved Exception 267.', 2, '2016-11-09 21:19:29'),
(303, 'Pavithra Isuru Solved Exception 266.', 2, '2016-11-09 21:19:29'),
(304, 'Pavithra Isuru Solved Exception 268.', 2, '2016-11-09 21:19:30'),
(305, 'Pavithra Isuru Solved Exception 270.', 2, '2016-11-09 21:19:32'),
(306, 'Pavithra Isuru Solved Exception 269.', 2, '2016-11-09 21:19:33'),
(307, 'Pavithra Isuru Solved Exception 271.', 2, '2016-11-09 21:19:34'),
(308, 'Pavithra Isuru Solved Exception 272.', 2, '2016-11-09 21:19:34'),
(309, 'Pavithra Isuru Solved Exception 273.', 2, '2016-11-09 21:19:36'),
(310, 'Pavithra Isuru Solved Exception 275.', 2, '2016-11-09 21:19:36'),
(311, 'Pavithra Isuru Solved Exception 274.', 2, '2016-11-09 21:19:37'),
(312, 'Pavithra Isuru Solved Exception 276.', 2, '2016-11-09 21:19:38'),
(313, 'Pavithra Isuru Solved Exception 277.', 2, '2016-11-09 21:19:40'),
(314, 'Pavithra Isuru Solved Exception 278.', 2, '2016-11-09 21:19:40'),
(315, 'Pavithra Isuru Solved Exception 279.', 2, '2016-11-09 21:19:41'),
(316, 'Pavithra Isuru Solved Exception 280.', 2, '2016-11-09 21:19:43'),
(317, 'Pavithra Isuru Solved Exception 282.', 2, '2016-11-09 21:19:43'),
(318, 'Pavithra Isuru Solved Exception 281.', 2, '2016-11-09 21:19:44'),
(319, 'Pavithra Isuru Solved Exception 283.', 2, '2016-11-09 21:19:45'),
(320, 'Pavithra Isuru Solved Exception 285.', 2, '2016-11-09 21:19:46'),
(321, 'Pavithra Isuru Solved Exception 284.', 2, '2016-11-09 21:19:47'),
(322, 'Pavithra Isuru Solved Exception 286.', 2, '2016-11-09 21:19:49'),
(323, 'Pavithra Isuru Solved Exception 287.', 2, '2016-11-09 21:19:50'),
(324, 'Pavithra Isuru Solved Exception 288.', 2, '2016-11-09 21:19:51'),
(325, 'Pavithra Isuru Solved Exception 289.', 2, '2016-11-09 21:19:52'),
(326, 'Pavithra Isuru Solved Exception 291.', 2, '2016-11-09 21:19:52'),
(327, 'Pavithra Isuru Solved Exception 290.', 2, '2016-11-09 21:19:54'),
(328, 'Pavithra Isuru Solved Exception 292.', 2, '2016-11-09 21:19:55'),
(329, 'Pavithra Isuru Solved Exception 295.', 2, '2016-11-09 21:19:56'),
(330, 'Pavithra Isuru Solved Exception 293.', 2, '2016-11-09 21:19:56'),
(331, 'Pavithra Isuru Solved Exception 294.', 2, '2016-11-09 21:19:57'),
(332, 'Pavithra Isuru Solved Exception 296.', 2, '2016-11-09 21:19:59'),
(333, 'Pavithra Isuru Solved Exception 298.', 2, '2016-11-09 21:19:59'),
(334, 'Pavithra Isuru Solved Exception 297.', 2, '2016-11-09 21:20:00'),
(335, 'Pavithra Isuru Solved Exception 299.', 2, '2016-11-09 21:20:01'),
(336, 'Pavithra Isuru Solved Exception 300.', 2, '2016-11-09 21:20:02'),
(337, 'Pavithra Isuru Solved Exception 301.', 2, '2016-11-09 21:20:03'),
(338, 'Pavithra Isuru Solved Exception 303.', 2, '2016-11-09 21:20:05'),
(339, 'Pavithra Isuru Solved Exception 302.', 2, '2016-11-09 21:20:06'),
(340, 'Pavithra Isuru Solved Exception 304.', 2, '2016-11-09 21:20:06'),
(341, 'Pavithra Isuru Solved Exception 305.', 2, '2016-11-09 21:20:07'),
(342, 'Pavithra Isuru Solved Exception 306.', 2, '2016-11-09 21:20:08'),
(343, 'Pavithra Isuru Solved Exception 307.', 2, '2016-11-09 21:20:09'),
(344, 'Pavithra Isuru Solved Exception 308.', 2, '2016-11-09 21:20:10'),
(345, 'Pavithra Isuru Solved Exception 228.', 2, '2016-11-09 21:20:16'),
(346, 'Pavithra Isuru Solved Exception 238.', 2, '2016-11-09 21:20:17'),
(347, 'Pavithra Isuru Solved Exception 309.', 2, '2016-11-09 22:02:12'),
(348, 'Pavithra Isuru Solved Exception 310.', 2, '2016-11-09 22:02:14'),
(349, 'Pavithra Isuru Solved Exception 311.', 2, '2016-11-09 22:02:15'),
(350, 'Pavithra Isuru Solved Exception 312.', 2, '2016-11-09 22:02:17'),
(351, 'Pavithra Isuru Solved Exception 313.', 2, '2016-11-09 22:02:18'),
(352, 'Pavithra Isuru Solved Exception 314.', 2, '2016-11-09 22:02:21'),
(353, 'Pavithra Isuru Solved Exception 315.', 2, '2016-11-09 22:02:21'),
(354, 'Pavithra Isuru Solved Exception 316.', 2, '2016-11-09 22:02:22'),
(355, 'Pavithra Isuru Solved Exception 317.', 2, '2016-11-09 22:02:25'),
(356, 'Pavithra Isuru Solved Exception 318.', 2, '2016-11-09 22:02:26'),
(357, 'Pavithra Isuru Solved Exception 320.', 2, '2016-11-09 22:02:27'),
(358, 'Pavithra Isuru Solved Exception 319.', 2, '2016-11-09 22:02:28'),
(359, 'Pavithra Isuru Solved Exception 321.', 2, '2016-11-09 22:02:30'),
(360, 'Pavithra Isuru Solved Exception 322.', 2, '2016-11-09 22:02:30'),
(361, 'Pavithra Isuru Solved Exception 323.', 2, '2016-11-09 22:02:31'),
(362, 'Pavithra Isuru Solved Exception 324.', 2, '2016-11-09 22:02:32'),
(363, 'Pavithra Isuru Solved Exception 325.', 2, '2016-11-09 22:02:33'),
(364, 'Pavithra Isuru Solved Exception 326.', 2, '2016-11-09 22:02:33'),
(365, 'Pavithra Isuru Solved Exception 327.', 2, '2016-11-09 22:02:36'),
(366, 'Pavithra Isuru Solved Exception 328.', 2, '2016-11-09 22:02:37'),
(367, 'Pavithra Isuru Solved Exception 329.', 2, '2016-11-09 22:02:37'),
(368, 'Pavithra Isuru Solved Exception 330.', 2, '2016-11-09 22:02:39'),
(369, 'Pavithra Isuru Solved Exception 331.', 2, '2016-11-09 22:02:40'),
(370, 'Pavithra Isuru Solved Exception 333.', 2, '2016-11-09 22:02:41'),
(371, 'Pavithra Isuru Solved Exception 332.', 2, '2016-11-09 22:02:42'),
(372, 'Pavithra Isuru Solved Exception 335.', 2, '2016-11-09 22:02:43'),
(373, 'Pavithra Isuru Solved Exception 334.', 2, '2016-11-09 22:02:43'),
(374, 'Pavithra Isuru Solved Exception 336.', 2, '2016-11-09 22:02:44'),
(375, 'Pavithra Isuru Solved Exception 337.', 2, '2016-11-09 22:02:48'),
(376, 'Pavithra Isuru Solved Exception 339.', 2, '2016-11-09 22:02:49'),
(377, 'Pavithra Isuru Solved Exception 338.', 2, '2016-11-09 22:02:50'),
(378, 'Pavithra Isuru Solved Exception 341.', 2, '2016-11-09 22:02:51'),
(379, 'Pavithra Isuru Solved Exception 340.', 2, '2016-11-09 22:02:52'),
(380, 'Pavithra Isuru Solved Exception 342.', 2, '2016-11-09 22:02:53'),
(381, 'Pavithra Isuru Solved Exception 344.', 2, '2016-11-09 22:02:55'),
(382, 'Pavithra Isuru Solved Exception 343.', 2, '2016-11-09 22:02:56'),
(383, 'Pavithra Isuru Solved Exception 345.', 2, '2016-11-09 22:02:57'),
(384, 'Pavithra Isuru Solved Exception 347.', 2, '2016-11-09 22:02:58'),
(385, 'Pavithra Isuru Solved Exception 346.', 2, '2016-11-09 22:03:00'),
(386, 'Pavithra Isuru Solved Exception 348.', 2, '2016-11-09 22:03:01'),
(387, 'Pavithra Isuru Solved Exception 349.', 2, '2016-11-09 22:03:02'),
(388, 'Pavithra Isuru Solved Exception 350.', 2, '2016-11-09 22:03:03'),
(389, 'Pavithra Isuru Solved Exception 351.', 2, '2016-11-09 22:03:03'),
(390, 'Pavithra Isuru Solved Exception 352.', 2, '2016-11-09 22:03:06'),
(391, 'Pavithra Isuru Solved Exception 353.', 2, '2016-11-09 22:03:07'),
(392, 'Pavithra Isuru Solved Exception 355.', 2, '2016-11-09 22:03:08'),
(393, 'Pavithra Isuru Solved Exception 354.', 2, '2016-11-09 22:03:08'),
(394, 'Pavithra Isuru Solved Exception 357.', 2, '2016-11-09 22:03:11'),
(395, 'Pavithra Isuru Solved Exception 356.', 2, '2016-11-09 22:03:12'),
(396, 'Pavithra Isuru Solved Exception 358.', 2, '2016-11-09 22:03:14'),
(397, 'Pavithra Isuru Solved Exception 361.', 2, '2016-11-09 22:03:15'),
(398, 'Pavithra Isuru Solved Exception 360.', 2, '2016-11-09 22:03:16'),
(399, 'Pavithra Isuru Solved Exception 362.', 2, '2016-11-09 22:03:18'),
(400, 'Pavithra Isuru Solved Exception 363.', 2, '2016-11-09 22:03:19'),
(401, 'Pavithra Isuru Solved Exception 365.', 2, '2016-11-09 22:03:20'),
(402, 'Pavithra Isuru Solved Exception 364.', 2, '2016-11-09 22:03:21'),
(403, 'Pavithra Isuru Solved Exception 366.', 2, '2016-11-09 22:03:24'),
(404, 'Pavithra Isuru Solved Exception 367.', 2, '2016-11-09 22:03:25'),
(405, 'Pavithra Isuru Solved Exception 369.', 2, '2016-11-09 22:03:25'),
(406, 'Pavithra Isuru Solved Exception 368.', 2, '2016-11-09 22:03:26'),
(407, 'Pavithra Isuru Solved Exception 370.', 2, '2016-11-09 22:03:28'),
(408, 'Pavithra Isuru Solved Exception 371.', 2, '2016-11-09 22:03:29'),
(409, 'Pavithra Isuru Solved Exception 373.', 2, '2016-11-09 22:03:30'),
(410, 'Pavithra Isuru Solved Exception 372.', 2, '2016-11-09 22:03:31'),
(411, 'Pavithra Isuru Solved Exception 375.', 2, '2016-11-09 22:03:33'),
(412, 'Pavithra Isuru Solved Exception 376.', 2, '2016-11-09 22:03:36'),
(413, 'Pavithra Isuru Solved Exception 374.', 2, '2016-11-09 22:03:37'),
(414, 'Pavithra Isuru Solved Exception 377.', 2, '2016-11-09 22:03:38'),
(415, 'Pavithra Isuru Solved Exception 378.', 2, '2016-11-09 22:03:39'),
(416, 'Pavithra Isuru Solved Exception 379.', 2, '2016-11-09 22:03:40'),
(417, 'Pavithra Isuru Solved Exception 380.', 2, '2016-11-09 22:03:41'),
(418, 'Pavithra Isuru Solved Exception 382.', 2, '2016-11-09 22:03:43'),
(419, 'Pavithra Isuru Solved Exception 381.', 2, '2016-11-09 22:03:44'),
(420, 'Pavithra Isuru Solved Exception 383.', 2, '2016-11-09 22:03:44'),
(421, 'Pavithra Isuru Solved Exception 384.', 2, '2016-11-09 22:03:45'),
(422, 'Pavithra Isuru Solved Exception 385.', 2, '2016-11-09 22:03:47'),
(423, 'Pavithra Isuru Solved Exception 387.', 2, '2016-11-09 22:03:48'),
(424, 'Pavithra Isuru Solved Exception 386.', 2, '2016-11-09 22:03:49'),
(425, 'Pavithra Isuru Solved Exception 389.', 2, '2016-11-09 22:03:50'),
(426, 'Pavithra Isuru Solved Exception 390.', 2, '2016-11-09 22:03:52'),
(427, 'Pavithra Isuru Solved Exception 391.', 2, '2016-11-09 22:03:53'),
(428, 'Pavithra Isuru Solved Exception 392.', 2, '2016-11-09 22:03:54'),
(429, 'Pavithra Isuru Solved Exception 394.', 2, '2016-11-09 22:03:54'),
(430, 'Pavithra Isuru Solved Exception 393.', 2, '2016-11-09 22:03:55'),
(431, 'Pavithra Isuru Solved Exception 397.', 2, '2016-11-09 22:03:56'),
(432, 'Pavithra Isuru Solved Exception 395.', 2, '2016-11-09 22:03:57'),
(433, 'Pavithra Isuru Solved Exception 396.', 2, '2016-11-09 22:03:59'),
(434, 'Pavithra Isuru Solved Exception 359.', 2, '2016-11-09 22:04:04'),
(435, 'Pavithra Isuru Solved Exception 388.', 2, '2016-11-09 22:04:05'),
(436, 'Pavithra Isuru Solved Exception 398.', 2, '2016-11-09 22:04:20'),
(437, 'Pavithra Isuru Solved Exception 399.', 2, '2016-11-09 22:07:09'),
(438, 'Pavithra Isuru Solved Exception 400.', 2, '2016-11-09 22:07:11'),
(439, 'Pavithra Isuru Solved Exception 401.', 2, '2016-11-10 00:40:53'),
(440, 'Pavithra Isuru Solved Exception 402.', 2, '2016-11-10 00:40:54'),
(441, 'Pavithra Isuru Solved Exception 403.', 2, '2016-11-10 00:40:55'),
(442, 'Pavithra Isuru Solved Exception 404.', 2, '2016-11-10 00:40:56'),
(443, 'Pavithra Isuru Solved Exception 406.', 2, '2016-11-10 00:43:44'),
(444, 'Pavithra Isuru Solved Exception 407.', 2, '2016-11-10 00:43:45'),
(445, 'Pavithra Isuru Solved Exception 405.', 2, '2016-11-10 00:43:45'),
(446, 'Pavithra Isuru Solved Exception 409.', 2, '2016-11-10 00:43:46'),
(447, 'Pavithra Isuru Solved Exception 410.', 2, '2016-11-10 00:43:47'),
(448, 'Pavithra Isuru Solved Exception 408.', 2, '2016-11-10 00:43:48'),
(449, 'Pavithra Isuru Solved Exception 413.', 2, '2016-11-10 00:43:49'),
(450, 'Pavithra Isuru Solved Exception 412.', 2, '2016-11-10 00:43:49'),
(451, 'Pavithra Isuru Solved Exception 411.', 2, '2016-11-10 00:43:51'),
(452, 'Pavithra Isuru Solved Exception 414.', 2, '2016-11-10 00:43:52'),
(453, 'Pavithra Isuru Solved Exception 416.', 2, '2016-11-10 01:08:53'),
(454, 'Pavithra Isuru Solved Exception 415.', 2, '2016-11-10 01:08:53'),
(455, 'Pavithra Isuru Solved Exception 417.', 2, '2016-11-10 01:10:33'),
(456, 'Pavithra Isuru Solved Exception 418.', 2, '2016-11-10 01:10:35'),
(457, 'Pavithra Isuru Solved Exception 419.', 2, '2016-11-10 01:13:39'),
(458, 'Pavithra Isuru Solved Exception 420.', 2, '2016-11-10 01:16:12'),
(459, 'Pavithra Isuru Solved Exception 421.', 2, '2016-11-10 01:21:05'),
(460, 'Pavithra Isuru Solved Exception 422.', 2, '2016-11-10 01:21:06'),
(461, 'Pavithra Isuru Solved Exception 423.', 2, '2016-11-10 01:21:07'),
(462, 'Pavithra Isuru Solved Exception 424.', 2, '2016-11-10 01:29:45'),
(463, 'Pavithra Isuru Solved Exception 426.', 2, '2016-11-10 21:34:53'),
(464, 'Pavithra Isuru Solved Exception 425.', 2, '2016-11-10 21:34:54'),
(465, 'Pavithra Isuru Solved Exception 427.', 2, '2016-11-10 21:34:54'),
(466, 'Pavithra Isuru Solved Exception 430.', 2, '2016-11-10 21:34:56'),
(467, 'Pavithra Isuru Solved Exception 429.', 2, '2016-11-10 21:34:57'),
(468, 'Pavithra Isuru Solved Exception 431.', 2, '2016-11-10 21:34:59'),
(469, 'Pavithra Isuru Solved Exception 433.', 2, '2016-11-10 21:35:00'),
(470, 'Pavithra Isuru Solved Exception 432.', 2, '2016-11-10 21:35:01'),
(471, 'Pavithra Isuru Solved Exception 436.', 2, '2016-11-10 21:35:01'),
(472, 'Pavithra Isuru Solved Exception 435.', 2, '2016-11-10 21:35:03'),
(473, 'Pavithra Isuru Solved Exception 434.', 2, '2016-11-10 21:35:05'),
(474, 'Pavithra Isuru Solved Exception 439.', 2, '2016-11-10 21:35:06'),
(475, 'Pavithra Isuru Solved Exception 438.', 2, '2016-11-10 21:35:06'),
(476, 'Pavithra Isuru Solved Exception 437.', 2, '2016-11-10 21:35:07'),
(477, 'Pavithra Isuru Solved Exception 442.', 2, '2016-11-10 21:35:10'),
(478, 'Pavithra Isuru Solved Exception 440.', 2, '2016-11-10 21:35:13'),
(479, 'Pavithra Isuru Solved Exception 441.', 2, '2016-11-10 21:35:14'),
(480, 'Pavithra Isuru Solved Exception 443.', 2, '2016-11-10 21:35:15'),
(481, 'Pavithra Isuru Solved Exception 444.', 2, '2016-11-10 21:35:16'),
(482, 'Pavithra Isuru Solved Exception 445.', 2, '2016-11-10 21:35:18'),
(483, 'Pavithra Isuru Solved Exception 448.', 2, '2016-11-10 21:35:19'),
(484, 'Pavithra Isuru Solved Exception 446.', 2, '2016-11-10 21:35:20'),
(485, 'Pavithra Isuru Solved Exception 447.', 2, '2016-11-10 21:35:20'),
(486, 'Pavithra Isuru Solved Exception 449.', 2, '2016-11-10 21:35:25'),
(487, 'Pavithra Isuru Solved Exception 428.', 2, '2016-11-10 21:37:25'),
(488, 'Pavithra Isuru Solved Exception 450.', 2, '2016-11-10 21:39:28'),
(489, 'Pavithra Isuru Solved Exception 451.', 2, '2016-11-10 21:39:29'),
(490, 'Pavithra Isuru Solved Exception 452.', 2, '2016-11-10 21:44:02'),
(491, 'Pavithra Isuru Solved Exception 453.', 2, '2016-11-10 21:44:03'),
(492, 'Pavithra Isuru Solved Exception 454.', 2, '2016-11-10 21:44:04'),
(493, 'Pavithra Isuru Solved Exception 455.', 2, '2016-11-10 21:44:05'),
(494, 'Pavithra Isuru Solved Exception 456.', 2, '2016-11-10 21:44:07'),
(495, 'Pavithra Isuru Solved Exception 457.', 2, '2016-11-10 21:44:07'),
(496, 'Pavithra Isuru Solved Exception 458.', 2, '2016-11-10 21:44:08'),
(497, 'Pavithra Isuru Solved Exception 459.', 2, '2016-11-10 21:44:10'),
(498, 'Pavithra Isuru Solved Exception 460.', 2, '2016-11-10 21:44:11'),
(499, 'Pavithra Isuru Solved Exception 461.', 2, '2016-11-10 21:44:12'),
(500, 'Pavithra Isuru Solved Exception 462.', 2, '2016-11-11 21:02:12'),
(501, 'Pavithra Isuru Solved Exception 463.', 2, '2016-11-11 21:04:36'),
(502, 'Pavithra Isuru Solved Exception 464.', 2, '2016-11-11 21:30:50'),
(503, 'Pavithra Isuru Solved Exception 465.', 2, '2016-11-11 21:36:27'),
(504, 'Pavithra Isuru Solved Exception 466.', 2, '2016-11-15 04:41:32'),
(505, 'Pavithra Isuru Solved Exception 467.', 2, '2016-11-15 04:41:33'),
(506, 'Pavithra Isuru Solved Exception 468.', 2, '2016-11-15 04:42:11'),
(507, 'Pavithra Isuru Solved Exception 469.', 2, '2016-11-15 04:42:12'),
(508, 'Pavithra Isuru Solved Exception 470.', 2, '2016-11-15 04:42:13'),
(509, 'Pavithra Isuru Solved Exception 471.', 2, '2016-11-15 04:43:10'),
(510, 'Pavithra Isuru Solved Exception 472.', 2, '2016-11-15 04:46:12'),
(511, 'Pavithra Isuru Solved Exception 473.', 2, '2016-11-15 04:46:12'),
(512, 'Pavithra Isuru Solved Exception 474.', 2, '2016-11-15 04:46:20'),
(513, 'Pavithra Isuru Solved Exception 475.', 2, '2016-11-15 04:46:31'),
(514, 'Pavithra Isuru Solved Exception 476.', 2, '2016-11-15 04:49:11'),
(515, 'Pavithra Isuru Solved Exception 477.', 2, '2016-11-15 04:49:13'),
(516, 'Pavithra Isuru Solved Exception 478.', 2, '2016-11-17 22:33:29'),
(517, 'Pavithra Isuru Solved Exception 479.', 2, '2016-11-17 22:33:30'),
(518, 'Pavithra Isuru Solved Exception 480.', 2, '2016-11-17 22:33:30'),
(519, 'Pavithra Isuru Solved Exception 481.', 2, '2016-11-17 22:33:31'),
(520, 'Pavithra Isuru Solved Exception 482.', 2, '2016-11-18 00:08:47'),
(521, 'Pavithra Isuru Solved Exception 483.', 2, '2016-11-18 00:08:50'),
(522, 'Pavithra Isuru Solved Exception 484.', 2, '2016-11-18 00:10:06'),
(523, 'Pavithra Isuru Solved Exception 485.', 2, '2016-11-18 00:10:08'),
(524, 'Pavithra Isuru Solved Exception 486.', 2, '2016-11-18 00:10:57'),
(525, 'Pavithra Isuru Solved Exception 487.', 2, '2016-11-18 00:25:41'),
(526, 'Pavithra Isuru Solved Exception 488.', 2, '2016-11-18 02:36:32'),
(527, 'Pavithra Isuru Solved Exception 489.', 2, '2016-11-18 02:36:33'),
(528, 'Pavithra Isuru Solved Exception 490.', 2, '2016-11-18 02:36:33'),
(529, 'Pavithra Isuru Solved Exception 491.', 2, '2016-11-18 02:36:34'),
(530, 'Pavithra Isuru Solved Exception 492.', 2, '2016-11-18 02:36:34'),
(531, 'Pavithra Isuru Solved Exception 493.', 2, '2016-11-18 02:36:35'),
(532, 'Pavithra Isuru Solved Exception 494.', 2, '2016-11-18 02:36:37'),
(533, 'Pavithra Isuru Solved Exception 495.', 2, '2016-11-18 02:36:37'),
(534, 'Pavithra Isuru Denied Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-11-18 03:51:57'),
(535, 'Pavithra Isuru Denied Oliver Dennis\'s access for Edit current vehicle Info in the system', 2, '2016-11-18 03:51:58'),
(536, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Add a new vehicle to the system', 2, '2016-11-18 03:52:02'),
(537, 'Pavithra Isuru Allowed Oliver Dennis\'s access for Edit current vehicle Info in the system', 2, '2016-11-18 03:52:03'),
(538, 'Pavithra Isuru Deactivate Oliver Dennis\'s User Account.', 2, '2016-11-18 03:52:14'),
(539, 'Pavithra Isuru Activate Oliver Dennis\'s User Account.', 2, '2016-11-18 03:52:20'),
(540, 'Pavithra Isuru Solved Exception 496.', 2, '2016-11-20 01:59:13'),
(541, 'Pavithra Isuru Solved Exception 497.', 2, '2016-11-20 01:59:15'),
(542, 'Pavithra Isuru Solved Exception 498.', 2, '2016-11-20 01:59:16'),
(543, 'Pavithra Isuru Solved Exception 499.', 2, '2016-11-20 02:01:03'),
(544, 'Pavithra Isuru Solved Exception 500.', 2, '2016-11-20 02:01:09'),
(545, 'Pavithra Isuru Solved Exception 501.', 2, '2016-11-20 19:25:31'),
(546, 'Pavithra Isuru Solved Exception 502.', 2, '2016-11-20 19:26:25'),
(547, 'Pavithra Isuru Solved Exception 503.', 2, '2016-11-20 23:46:23'),
(548, 'Pavithra Isuru Solved Exception 504.', 2, '2017-02-07 02:39:19'),
(549, 'Pavithra Isuru Solved Exception 505.', 2, '2017-02-07 02:39:20'),
(550, 'Pavithra Isuru Solved Exception 506.', 2, '2017-02-07 02:39:22'),
(551, 'Pavithra Isuru Solved Exception 507.', 2, '2017-02-07 21:14:33');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `fname` varchar(16) DEFAULT NULL,
  `lname` varchar(16) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `telephone` varchar(14) DEFAULT NULL,
  `phontoid-type` varchar(16) NOT NULL,
  `photoid` varchar(24) NOT NULL,
  `avatar` int(11) NOT NULL DEFAULT '0',
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `fname`, `lname`, `gender`, `email`, `telephone`, `phontoid-type`, `photoid`, `avatar`, `status`) VALUES
(2, 'Oliver', 'Dennis', 'M', 'oliver@deny.com', '0222785513', '', '', 0, 1),
(3, 'Oliver', 'Dennis', 'M', 'oliver@deny.com', '0222785513', '', '', 0, 1),
(4, 'Oliver', 'Dennis', 'M', 'oliver@deny.com', '0222785513', '', '', 0, 1),
(5, NULL, NULL, 'M', NULL, NULL, '', '', 0, 1),
(6, NULL, NULL, 'M', NULL, NULL, '', '', 0, 1),
(7, NULL, NULL, 'M', NULL, NULL, '', '', 0, 1),
(8, NULL, NULL, 'M', 'pavithraisuru@gmail.com', '0228787465', '', '', 0, 1),
(9, 'Pavithra', 'Isuru', 'M', 'pavithraisuru@gmail.com', '0228787465', '', '', 0, 1),
(10, 'Pavithra', 'Isuru', 'M', 'pavithraisuru@gmail.com', '0227364744', '', '', 0, 1),
(11, 'Pavithra', 'Isuru', 'M', 'pavithraisuru@gmail.com', '0228787465', '', '', 0, 1),
(12, 'oly', 'cooper', 'M', 'oly@gmail.com', '0228787465', '', '', 0, 1),
(13, 'Yahoo', NULL, NULL, NULL, NULL, '', '', 0, NULL),
(14, 'Yahoo', NULL, NULL, NULL, NULL, '', '', 0, NULL),
(15, 'Yahoo', 'Test', NULL, NULL, NULL, '', '', 0, NULL),
(16, 'Yahoo', 'Test', NULL, NULL, NULL, '', '', 0, NULL),
(17, 'Yahoo', 'Test', NULL, NULL, NULL, '', '', 0, NULL),
(18, 'Yahoo', 'Test', NULL, 'pavithraidl@yahoo.com', NULL, '', '', 0, NULL),
(19, 'Yahoo', 'Test', NULL, 'pavithraidl@yahoo.com', NULL, '', '', 0, NULL),
(20, 'Josh', 'Smith', NULL, 'josh@yahoo.com', '0227873884', '', '', 0, 1),
(21, 'Lyn', 'Pemila', NULL, 'lyn@yahoo.com', '0228839834', '', '', 0, 1),
(22, 'Andrew', 'Jon', NULL, 'jon@gmail.com', '0228774837', '', '', 0, 1),
(23, 'Jshone', 'Peter', NULL, 'peter@gmail.com', '0227766334', '', '', 0, 1),
(24, 'Jonson', 'Clarck', NULL, 'clarck@yahoo.com', '0278878343', '', '', 0, 1),
(25, 'Josh', 'Smith', NULL, 'josh@yahoo.com', '0228938742', '', '', 0, 1),
(26, 'Smith', 'Jone', NULL, 'smith@yahoo.com', '02788323212', '', '', 0, 1),
(27, 'Test', 'Booking', NULL, 'test@gmail.com', '02764783453', '', '', 0, 1),
(28, 'day', 'status', NULL, 'day@da.com', '0278832322', '', '', 0, 1),
(29, 'return', 'test', NULL, 'return@ret.com', '0221231232', '', '', 0, 1),
(30, 'activity', 'test', NULL, 'activity@test.com', '02234242343', '', '', 0, 1),
(31, 'Harry', 'Test', NULL, 'test@harry.com', '02333234242', '', '', 0, 1),
(32, 'edfwe', 'jhg', NULL, 'jhjhg@hjh.com', '02298989', '', '', 0, 1),
(33, 'Oliver', 'Cooper', NULL, 'oliver.cooper@me.com', '123423452353', '', '', 0, 1),
(34, 'Vehicle', 'Booking', NULL, 'pavithraisuru@gmail.com', NULL, '', '', 0, NULL),
(35, 'vehicle', 'booking', NULL, 'bookingtest@gmail.com', NULL, '', '', 0, NULL),
(36, 'vehicle', 'booking', NULL, 'bookingtest@gmail.com', NULL, '', '', 0, NULL),
(37, 'vehicle', 'booking', NULL, 'bookingtest@gmail.com', NULL, '', '', 0, NULL),
(38, 'vehicle', 'booking', NULL, 'bookingtest@gmail.com', NULL, '', '', 0, NULL),
(39, 'vehicle', 'booking', NULL, 'bookingtest@gmail.com', NULL, '', '', 0, NULL),
(40, 'vehicle', 'booking', NULL, 'bookingtest@gmail.com', NULL, '', '', 0, NULL),
(41, 'asdfas', 'hfdfg', NULL, 'test@test.com', NULL, '', '', 0, NULL),
(42, 'test', 'booking', NULL, 'test@booking.com', NULL, '', '', 0, NULL),
(43, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '022989342', '', 'KDLKJF', 0, NULL),
(44, 'Test', 'Two', NULL, 'test@test.com', '022334455', '', 'kdkdkdk', 0, NULL),
(45, 'asdfas', 'ggy', NULL, 'jhjgjhk@yuf.ytuyg', '8907890', '', 'iuhop', 0, NULL),
(46, 'asfdsaf', 'sdfsd', NULL, 'sdfs@sdfs.sfsd', '343223', '', 'sdfsf', 0, NULL),
(47, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222667713', '', 'P883299', 0, NULL),
(48, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'DJ99823', 0, NULL),
(49, 'Pavithra', 'Isuru', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'JD993242', 0, NULL),
(50, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(51, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(52, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(53, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(54, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(55, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(56, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(57, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(58, 'asdfas', 'hgfd', NULL, 'pavithraisuru@gmail.com', '0222435546', '', 'ghfhj', 0, NULL),
(59, 'pavithra', 'Isuru', NULL, 'pavithraisuru@gmail.com', '022283443', '', 'kdjfkd', 0, NULL),
(60, 'sfgds', 'sdfsdf', NULL, 'pavithraisuru@gmail.com', '2324', '', 'sdfs', 0, NULL),
(61, 'sfgds', 'sdfsdf', NULL, 'pavithraisuru@gmail.com', '2324', '', 'sdfs', 0, NULL),
(62, 'sfgds', 'sdfsdf', NULL, 'pavithraisuru@gmail.com', '2324', '', 'sdfs', 0, NULL),
(63, 'isuru', 'liyanage', NULL, 'pavithraisuru@gmail.com', '32233423', '', 'KJ399342', 0, NULL),
(64, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0882313', '', 'kdjfs', 0, NULL),
(65, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0882313', '', 'kdjfs', 0, NULL),
(66, 'isuru', 'liyanage', NULL, 'pavithraisuru@gmail.com', '0222788343', '', 'kjskfs', 0, NULL),
(67, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'HF87773', 0, NULL),
(68, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'HF87773', 0, NULL),
(69, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'HF87773', 0, NULL),
(70, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'HF87773', 0, NULL),
(71, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'HF87773', 0, NULL),
(72, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'HF87773', 0, NULL),
(73, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '0222647713', '', 'HF87773', 0, NULL),
(74, 'asfd', 'safda', NULL, 'sadfsa@sdfds.sdfs', 'asdfas', '', 'asdfsa', 0, NULL),
(75, 'asdfas', 'asdas', NULL, 'asda@adssa.asda', 'ada', '', 'asdsa', 0, NULL),
(76, 'isuru', 'liyanage', NULL, 'pavithraidl@yahoo.com', 'ksdfs', '', 'ksfjsd', 0, NULL),
(77, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '022324234', '', 'slkfs', 0, NULL),
(78, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '022324234', '', 'slkfs', 0, NULL),
(79, 'sdf', 'ladf', NULL, 'pavithraisuru@gmail.com', 'lksdf', '', 'slkdf', 0, NULL),
(80, 'asdfas', 'ghfg', NULL, 'pavithraisuru@gmail.com', 'ffhf', '', 'fghf', 0, NULL),
(81, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '02273422', '', 'ksjfs', 0, NULL),
(82, 'Oliver', 'Cooper', NULL, 'oliver.cooper@me.com', '34567654321', '', 'DSD565656565', 0, NULL),
(83, 'Backend', 'Test', NULL, 'pavithraisuru@gmail.com', '0222778899', '', 'BT99887766', 0, NULL),
(84, 'Scaffold', 'Test', NULL, 'pavithraisuru@gmail.com', '066773424', '', 'DK03424', 0, NULL),
(85, 'Isuru', 'Liyanage', NULL, 'pavithraisuru@gmail.com', '022647713', '', 'HD99342', 0, NULL),
(86, 'date', 'test', NULL, 'pavithraidl@yahoo.com', '0222873364', '', 'KD998342', 0, NULL),
(87, 'pavithra', 'isuru', NULL, 'paviearn@gmail.com', NULL, '', '', 0, 1),
(88, 'sdfs', 'sdfsd', NULL, 'sdfs@sdfs.com', 'sdfs', '', 'sdsdfs', 0, NULL),
(89, 'fgfdg', 'dfgd', NULL, 'sdfs@sdfs.dgdfg', 'sdfss', '', 'sdfs', 0, NULL),
(90, 'Pavithra', 'Isuru', NULL, 'pavithraidl@yahoo.com', '0222647713', '', 'DH8877492', 0, NULL),
(91, 'Pavithra', 'Isuru', NULL, 'pavithraidl@yahoo.com', '0222647713', '', 'KD0934242', 0, NULL),
(92, 'Isuru', 'Liyanage', NULL, 'pavithraidl@yahoo.com', '0222647713', '', 'KD0343242', 0, NULL),
(93, 'Pavithra', 'Isuru', NULL, 'pavithraidl@yahoo.com', '0222647713', '', 'KD0343242', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_operation_access`
--

DROP TABLE IF EXISTS `user_operation_access`;
CREATE TABLE `user_operation_access` (
  `id` int(11) NOT NULL,
  `operation_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_operation_access`
--

INSERT INTO `user_operation_access` (`id`, `operation_id`, `user_id`, `status`, `created_by`, `created_at`) VALUES
(4, 1, 3, 1, 2, '2016-11-18 03:52:02'),
(5, 2, 3, 1, 2, '2016-11-18 03:52:03'),
(6, 3, 3, 0, 2, '2016-10-11 03:37:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_page_access`
--

DROP TABLE IF EXISTS `user_page_access`;
CREATE TABLE `user_page_access` (
  `id` int(11) NOT NULL,
  `page_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_page_access`
--

INSERT INTO `user_page_access` (`id`, `page_id`, `user_id`, `status`, `created_by`, `created_at`) VALUES
(1, 2, 2, 1, 2, '2016-09-05 21:36:18'),
(2, 3, 2, 1, 2, '2016-09-05 21:41:49'),
(3, 5, 2, 1, 2, '2016-09-05 22:49:50'),
(4, 1, 2, 1, 2, '2016-09-05 14:38:04'),
(5, 1, 3, 1, 2, '2016-09-16 02:59:10'),
(6, 2, 3, 1, 2, '2016-09-06 21:48:57'),
(7, 3, 3, 0, 2, '2016-09-16 03:00:19'),
(8, NULL, 3, 0, 2, '2016-09-05 14:49:33'),
(9, 4, 3, 1, 2, '2016-09-05 16:45:04'),
(10, 5, 3, 1, 2, '2016-09-16 02:59:22'),
(11, 7, 3, 0, 2, '2016-09-16 02:59:16'),
(12, 8, 3, 1, 2, '2016-09-07 03:15:23');

-- --------------------------------------------------------

--
-- Table structure for table `user_rolls`
--

DROP TABLE IF EXISTS `user_rolls`;
CREATE TABLE `user_rolls` (
  `id` int(11) NOT NULL,
  `roll_id` int(11) DEFAULT NULL,
  `name` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_rolls`
--

INSERT INTO `user_rolls` (`id`, `roll_id`, `name`) VALUES
(1, 1, 'Admin'),
(2, 2, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) DEFAULT NULL,
  `modal` varchar(16) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `description` text,
  `rate` float DEFAULT NULL,
  `display_class` varchar(16) DEFAULT NULL,
  `display_color` varchar(16) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `plate`, `modal`, `year`, `description`, `rate`, `display_class`, `display_color`, `priority`, `status`, `created_at`) VALUES
(1, 'ALT507', 'Rover', 2012, 'testing description', 60, 'bg-light-blue-2', '#C7D4FF', 1, 1, '2016-08-31 17:51:53'),
(2, 'GEJ887', 'Toyota', 2013, 'testing description for gej887', 8, 'bg-green-2', '#C6FFAE', 2, 1, '2016-09-07 00:11:48'),
(3, 'HHF384', 'Toyota', 2013, 'testing description', 65, 'bg-orange-1', '#FFA490', 3, 1, '2016-09-07 22:37:30'),
(4, 'AHD837', 'Toyota', 2013, 'test description', 69, 'bg-pink-1', '#ff878b', 4, 1, '2016-09-07 22:38:20'),
(5, 'HRY883', 'NISSAN', 2013, 'testing description', 70, 'bg-orange-4', '#cbbb74', 5, 1, '2016-09-12 03:36:35'),
(6, 'KJS398', 'Toyota', 2013, 'test description', 67, 'bg-light-blue-2', '#cbbb74', 6, 1, '2016-09-13 00:55:23'),
(7, 'KLD043', 'Toyota', 2013, 'test description', 77, 'bg-darkblue-3', '#cbbb74', 7, 1, '2016-09-13 00:55:53'),
(8, 'JKD343', 'Toyota', 2014, 'testing', 72, 'bg-pink-1', '#cbbb74', 8, 1, '2016-09-13 00:56:16'),
(9, 'KKLDHO', 'NISSAN', 2012, 'testing', 74, 'bg-orange-1', '#cbbb74', 9, 1, '2016-09-13 00:56:42'),
(10, 'HJS093', 'NISSAN', 2014, 'testing', 75, 'bg-yellow-1', '#cbbb74', 10, 1, '2016-09-13 00:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_booking`
--

DROP TABLE IF EXISTS `vehicle_booking`;
CREATE TABLE `vehicle_booking` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `booking_from` datetime DEFAULT NULL,
  `booking_to` datetime DEFAULT NULL,
  `class_name` varchar(32) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `release_at` datetime DEFAULT NULL,
  `return_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle_booking`
--

INSERT INTO `vehicle_booking` (`id`, `job_id`, `vehicle_id`, `rate`, `booking_from`, `booking_to`, `class_name`, `status`, `release_at`, `return_at`, `created_at`) VALUES
(144, 34, 1, 60, '2017-02-08 08:00:00', '2017-02-12 14:00:00', NULL, 2, NULL, NULL, '2017-02-07 21:40:29'),
(145, 34, 2, 60, '2017-02-08 08:00:00', '2017-02-12 14:00:00', NULL, 2, NULL, NULL, '2017-02-07 21:40:32'),
(146, 34, 3, 60, '2017-02-08 08:00:00', '2017-02-12 14:00:00', NULL, 2, NULL, NULL, '2017-02-07 21:40:35'),
(147, 34, 4, 60, '2017-02-08 08:00:00', '2017-02-12 14:00:00', NULL, 2, NULL, NULL, '2017-02-07 21:40:37');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_config`
--

DROP TABLE IF EXISTS `vehicle_config`;
CREATE TABLE `vehicle_config` (
  `id` int(11) NOT NULL,
  `row` varchar(30) DEFAULT NULL,
  `value` varchar(20) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle_config`
--

INSERT INTO `vehicle_config` (`id`, `row`, `value`, `status`) VALUES
(1, 'Vehicle Hour Rate', '60', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_job`
--
ALTER TABLE `booking_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_cart`
--
ALTER TABLE `event_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`job_id`);

--
-- Indexes for table `event_cart_items`
--
ALTER TABLE `event_cart_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_category`
--
ALTER TABLE `event_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_items`
--
ALTER TABLE `event_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_id` (`detail_id`);

--
-- Indexes for table `event_item_details`
--
ALTER TABLE `event_item_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `exceptions`
--
ALTER TABLE `exceptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scaffolding_booking`
--
ALTER TABLE `scaffolding_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scaffolding_cart`
--
ALTER TABLE `scaffolding_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_id` (`booking_id`);

--
-- Indexes for table `scaffolding_cart_items`
--
ALTER TABLE `scaffolding_cart_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scaffolding_part`
--
ALTER TABLE `scaffolding_part`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scaffolding_part_each`
--
ALTER TABLE `scaffolding_part_each`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scaffodling_part_type_id` (`scaffolding_part_id`);

--
-- Indexes for table `scaffolding_type`
--
ALTER TABLE `scaffolding_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scaffolding_type_parts`
--
ALTER TABLE `scaffolding_type_parts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scaffolding_type_id` (`scaffolding_type_id`),
  ADD KEY `scaffolding_part_type_id` (`scaffolding_part_id`);

--
-- Indexes for table `session_vehicle`
--
ALTER TABLE `session_vehicle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_config`
--
ALTER TABLE `system_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_intrusions`
--
ALTER TABLE `system_intrusions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_operations`
--
ALTER TABLE `system_operations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_page_categories`
--
ALTER TABLE `system_page_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_page_list`
--
ALTER TABLE `system_page_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `info_id` (`info_id`);

--
-- Indexes for table `user_activity_log`
--
ALTER TABLE `user_activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_operation_access`
--
ALTER TABLE `user_operation_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_page_access`
--
ALTER TABLE `user_page_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_rolls`
--
ALTER TABLE `user_rolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_booking`
--
ALTER TABLE `vehicle_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_config`
--
ALTER TABLE `vehicle_config`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_job`
--
ALTER TABLE `booking_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `event_cart`
--
ALTER TABLE `event_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `event_cart_items`
--
ALTER TABLE `event_cart_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT for table `event_category`
--
ALTER TABLE `event_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `event_items`
--
ALTER TABLE `event_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=321;
--
-- AUTO_INCREMENT for table `event_item_details`
--
ALTER TABLE `event_item_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `exceptions`
--
ALTER TABLE `exceptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=509;
--
-- AUTO_INCREMENT for table `scaffolding_booking`
--
ALTER TABLE `scaffolding_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `scaffolding_cart`
--
ALTER TABLE `scaffolding_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=871;
--
-- AUTO_INCREMENT for table `scaffolding_cart_items`
--
ALTER TABLE `scaffolding_cart_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;
--
-- AUTO_INCREMENT for table `scaffolding_part`
--
ALTER TABLE `scaffolding_part`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `scaffolding_part_each`
--
ALTER TABLE `scaffolding_part_each`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=222;
--
-- AUTO_INCREMENT for table `scaffolding_type`
--
ALTER TABLE `scaffolding_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `scaffolding_type_parts`
--
ALTER TABLE `scaffolding_type_parts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `session_vehicle`
--
ALTER TABLE `session_vehicle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `system_config`
--
ALTER TABLE `system_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `system_intrusions`
--
ALTER TABLE `system_intrusions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `system_operations`
--
ALTER TABLE `system_operations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `system_page_categories`
--
ALTER TABLE `system_page_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `system_page_list`
--
ALTER TABLE `system_page_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `user_activity_log`
--
ALTER TABLE `user_activity_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=552;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT for table `user_operation_access`
--
ALTER TABLE `user_operation_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_page_access`
--
ALTER TABLE `user_page_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_rolls`
--
ALTER TABLE `user_rolls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `vehicle_booking`
--
ALTER TABLE `vehicle_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;
--
-- AUTO_INCREMENT for table `vehicle_config`
--
ALTER TABLE `vehicle_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `event_items`
--
ALTER TABLE `event_items`
  ADD CONSTRAINT `event_items_ibfk_1` FOREIGN KEY (`detail_id`) REFERENCES `event_item_details` (`id`);

--
-- Constraints for table `event_item_details`
--
ALTER TABLE `event_item_details`
  ADD CONSTRAINT `event_item_details_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `event_category` (`id`);

--
-- Constraints for table `scaffolding_cart`
--
ALTER TABLE `scaffolding_cart`
  ADD CONSTRAINT `scaffolding_cart_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `scaffolding_booking` (`id`);

--
-- Constraints for table `scaffolding_part_each`
--
ALTER TABLE `scaffolding_part_each`
  ADD CONSTRAINT `scaffolding_part_each_ibfk_1` FOREIGN KEY (`scaffolding_part_id`) REFERENCES `scaffolding_part` (`id`);

--
-- Constraints for table `scaffolding_type_parts`
--
ALTER TABLE `scaffolding_type_parts`
  ADD CONSTRAINT `scaffolding_type_parts_ibfk_1` FOREIGN KEY (`scaffolding_type_id`) REFERENCES `scaffolding_type` (`id`),
  ADD CONSTRAINT `scaffolding_type_parts_ibfk_2` FOREIGN KEY (`scaffolding_part_id`) REFERENCES `scaffolding_part` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`info_id`) REFERENCES `user_info` (`id`);
