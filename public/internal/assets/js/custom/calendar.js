// $(function () {
//     function runCalendar() {
//         var $modal = $('#event-modal');
//         $('#draggable-events div.draggable-event').each(function () {
//             var eventObject = {
//                 title: $.trim($(this).text())
//             };
//
//             $(this).data('eventObject', eventObject);
//
//             $(this).draggable({
//                 zIndex: 999,
//                 revert: true,
//                 revertDuration: 20
//             });
//         });
//         /*  Initialize the calendar  */
//         var date = new Date();
//         var d = date.getDate();
//         var m = date.getMonth();
//         var y = date.getFullYear();
//         var form = '';
//
//         //get booking data
//         jQuery.ajax({
//             type: 'GET',
//             url: window.url + '/internal/vehicle/single-vehicle-booking-list',
//             data:{plate:'ALT507'},
//             success: function (data) {
//                 if (data != 0) {
//                     var bookings = JSON.parse(data);
//
//                     var calendar = $('#calendar').fullCalendar({
//                         slotDuration: '01:00:00', /* If we want to split day time each 1 hour */
//                         minTime: '08:00:00',
//                         maxTime: '19:00:00',
//                         header: {
//                             left: 'prev,next today',
//                             center: 'title',
//                             right: 'month,agendaWeek,agendaDay'
//                         },
//                         events: bookings,
//                         editable: true,
//                         droppable: true,
//                         drop: function (date, allDay) {
//
//                             var originalEventObject = $(this).data('eventObject');
//                             var $categoryClass = $(this).attr('data-class');
//
//                             var copiedEventObject = $.extend({}, originalEventObject);
//
//                             copiedEventObject.start = date;
//                             copiedEventObject.allDay = allDay;
//                             if ($categoryClass)
//                                 copiedEventObject['className'] = [$categoryClass];
//
//                             $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
//
//                             if ($('#drop-remove').is(':checked')) {
//
//                                 $(this).remove();
//                             }
//                         },
//
//                         selectable: true,
//                         eventClick: function (calEvent, jsEvent, view) {
//                             var form = $("<form></form>");
//                             form.append("<label>Change event name</label>");
//                             form.append("<div class='input-group'><input class='form-control' type=text value='" + calEvent.title + "' /><span class='input-group-btn'><button type='submit' class='btn btn-success'><i class='fa fa-check'></i> Save Changes</button></span></div>");
//                             $modal.modal({
//                                 backdrop: 'static'
//                             });
//                             $modal.find('.delete-event').show().end().find('.save-event').hide().end().find('.modal-body').empty().prepend(form).end().find('.delete-event').unbind('click').click(function () {
//                                 calendar.fullCalendar('removeEvents', function (ev) {
//                                     return (ev._id == calEvent._id);
//                                 });
//                                 $modal.modal('hide');
//                             });
//                             $modal.find('form').on('submit', function () {
//                                 calEvent.title = form.find("input[type=text]").val();
//                                 calendar.fullCalendar('updateEvent', calEvent);
//                                 $modal.modal('hide');
//                                 return false;
//                             });
//                         },
//                         select: function (start, end, allDay) {
//                             $modal.modal({
//                                 backdrop: 'static'
//                             });
//                             form = $("<form></form>");
//                             form.append("<div class='row'></div>");
//                             form.find(".row").append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Event Name</label><input class='form-control' placeholder='Insert Event Name' type='text' name='title'/></div></div>").append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Category</label><select class='form-control' name='category'></select></div></div>").find("select[name='category']").append("<option value='bg-red'>Work</option>")
//                                 .append("<option value='bg-green-1'>Entertainment</option>").append("<option value='bg-pink-1'>Meeting</option>").append("<option value='bg-lightblue-1'>Lunch</option>").append("<option value='bg-red-1'>Work</option>").append("<option value='bg-blue-1'>Sport</option>");
//                             $modal.find('.delete-event').hide().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.save-event').unbind('click').click(function () {
//                                 form.submit();
//                             });
//                             $modal.find('form').on('submit', function () {
//                                 title = form.find("input[name='title']").val();
//                                 $categoryClass = form.find("select[name='category'] option:checked").val();
//                                 if (title !== null && title.length != 0) {
//                                     calendar.fullCalendar('renderEvent', {
//                                         title: title,
//                                         start: start,
//                                         end: end,
//                                         allDay: false,
//                                         className: $categoryClass
//                                     }, true);
//                                 }
//                                 else{
//                                     alert('You need a title for the event!');
//                                 }
//                                 $modal.modal('hide');
//                                 return false;
//                             });
//                             calendar.fullCalendar('unselect');
//                         }
//
//                     });
//                 }
//                 else {
//                     dbError();
//                 }
//             },
//             error: function (xhr, textStatus, errorThrown) {
//                 internetError();
//             }
//         });
//     }
//
//     runCalendar();
//
// });