/**
 * Created by Isuru on 6/09/16.
 */
"use strict";


//region Defining Global Variables
window.aLPaginate = 0;
window.uSelecting = 0;
window.emailResend = 0;
window.userChanging = 0;
window.authUser = $('#u-m-selected-user').val();
//endregion


//region Default Settings
$('#u-m-user-list-container').slideUp(100);
$('#u-m-not-activated-quick-links').hide();
$('#u-m-delete-user').hide();
$('#u-m-active-deactive-container').hide();
$('#mm-u-m-add-new-user-loading').hide();
document.getElementById("u-m-not-activated-quick-links").style.visibility = "visible";
document.getElementById("u-m-delete-user").style.visibility = "visible";
document.getElementById("u-m-active-deactive-container").style.visibility = "visible";
//endregion


//region Events
//----------------------------------------------------------------------------------------------------------------------
getUserList();

$('#u-m-tab-activity-log').on('click', function () {
    if (window.aLPaginate == 0) {
        var userId = $('#u-m-selected-user').val();
        getActivityLog(1, userId);
    }
});

$('#u-m-resend-activation-email').on('click', function () {
    if (window.emailResend == 0) {
        window.emailResend = 1;
        $('#u-m-resend-activation-email').fadeTo(250, 0.3);
        resendActivationEmail();
    }

});

$('#u-m-view-activity-log').on('click', function () {
    $('#u-m-tab-activity-log').trigger('click');
});


$('#u-m-user-manage-container-activity-log').scroll(function () {
    var elementHeight = $("#u-m-user-manage-container-activity-log")[0].scrollHeight;
    var scrollPosition = $("#u-m-user-manage-container-activity-log").height() + $("#u-m-user-manage-container-activity-log").scrollTop();
    if (elementHeight - 600 < scrollPosition) {
        if (window.aLPaginate != 0) {
            window.aLPaginate++;
            var userId = $('#u-m-selected-user').val();
            getActivityLog(window.aLPaginate, userId);
        }
    }
});

$('#mm-u-m-new-user-email').on('keyup', function() {
    $('#mm-u-m-add-user-email-validation').fadeOut(200, function() {
        $('#mm-u-m-add-user-email-validation').remove();
    });
});
//endregion


//region Functions
//----------------------------------------------------------------------------------------------------------------------
function getUserList() {
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal-user-manage-get-user-list',
        success: function (data) {
            if (data != 0) {
                var ret = JSON.parse(data);
                var userList = "";
                var userId = $('#u-m-selected-user').val();

                for (var i = 0; i < ret.length; i++) {
                    userList += (
                        '               <li id="u-m-user-list-item-' + ret[i].userid + '" class="user-list-1 ' + (ret[i].userid == userId ? 'user-list-1-selected' : '') + '" onclick="selectUser(' + ret[i].userid + ');" ' + (ret[i].status != 0 ? 'data-status = "active" data-toggle="tooltip" title="Account not activated yet"' : 'data-status = ""') + (ret[i].active == 0 ? 'style="opacity: 0.4;"' : '') + '>' +
                        '                   <div class="row" style="margin-bottom: -15px;">' +
                        '                       <div class="col-md-3 col-xs-4">' +
                        '                           <span class="rounded-image topbar-profile-image">' +
                        '                               <img id="u-m-user-list-avatar-' + ret[i].userid + '" ' + (ret[i].status == 0 ? 'class="gray-scale"' : "") + ' src="' + window.url + '/internal/assets/img/users/' + ret[i].avatarid + '/user-35.jpg">' +
                        '                           </span>' +
                        '                       </div>' +
                        '                       <div class="col-md-9 col-xs-8" style="margin-left: -8px;">' +
                        '                            <div class="row">' +
                        '                               <span id="u-m-user-list-name-' + ret[i].userid + '" class="' + (ret[i].status == 1 ? 'text-blue-3"' : "") + ' pull-left" style="font-size: 15px;"><strong>' + ret[i].name + '</strong></span>' +
                        '                               <span id="u-m-user-list-loading-container-' + ret[i].userid + '"></span>' +
                        '                           </div>' +
                        '                           <div class="row">' +
                        '                               <p id="u-m-user-list-roll-' + ret[i].userid + '" style="color: rgba(100, 100, 100, 0.31);">' + ret[i].title + '</p>' +
                        '                           </div>' +
                        '                       </div> ' +
                        '                   </div>' +
                        '                   <hr/>' +
                        '                </li>'
                    );
                }
                $('#u-m-user-list-ul').html("");
                $('#u-m-user-list-ul').append(userList);
                $('#u-m-user-list-container').slideDown(250);
                selectUser(userId);
            }
            else {
                dbError()
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function selectUser(userId) {
    if (window.userChanging == 0) {
        $('#u-m-user-list-loading-container-' + userId).append('<img id="u-m-user-list-loading" style="position: absolute; left: 85%;top:22px;" src="' + url + 'assets/img/loading/windwheel-blue-sm.gif" />');//
        var title = $('#u-m-user-list-roll-' + userId).text();
        if (title == 'Admin') {
            $('#u-m-access-control-container').fadeTo(200, 0);
            $('#u-m-access-admin-account').fadeTo(200, 1);
        }
        else {
            $('#u-m-access-control-container').fadeTo(200, 1);
            $('#u-m-access-admin-account').fadeTo(200, 0);
        }
        window.userChanging = 1;
        if (userId == window.authUser) {
            $('#u-m-delete-user').fadeOut(200);
            $('#u-m-active-deactive-container').fadeOut(200);
        }
        else {
            $('#u-m-delete-user').fadeIn(200);
            $('#u-m-active-deactive-container').fadeIn(200);
        }
        $('#u-m-not-activated-quick-links').hide();
        $('#u-m-account-not-activated').remove();

        var status = $('#u-m-user-list-item-' + userId).attr('data-status');

        $('#u-m-quick-link-list').fadeIn(200);
        $('#u-m-user-info-container').fadeIn(200);

        if (window.uSelecting == 0) {
            window.uSelecting == 1;
            $('#u-m-activity-list').html("");
            window.aLPaginate = 0;
            $('.user-list-1-selected').removeClass('user-list-1-selected');
            $('#u-m-user-list-item-' + userId).addClass('user-list-1-selected');
            $('#u-m-user-avatar').fadeTo(550, 0.4);

            if (status != "") {
                jQuery.ajax({
                    type: 'GET',
                    url: window.url + '/user-manage-display-info',
                    data: {userid: userId},
                    success: function (data) {
                        $('#master-no-connection').fadeOut(200);
                        if (data != 0) {
                            var ret = JSON.parse(data);
                            //update user avatar
                            $('#u-m-user-avatar').attr('src', url + '/internal/assets/img/users/' + ret.avatar + '/user-100.jpg').removeClass('gray-scale');

                            if (ret.rollid == 1) {
                                $('#u-m-delete-user').fadeOut(200);
                                $('#u-m-active-deactive-container').fadeOut(200);
                            }

                            //set active check box
                            if (ret.active == 0) {
                                $('#u-m-user-avatar').fadeTo(200, 0.4);
                                $('#u-m-account-active-status-icon').addClass('text-red-1').removeClass('text-green-2');
                                $('#u-m-account-active-status-text').text('Account is Deactive').addClass('text-red-1');
                            }
                            else {
                                $('#u-m-user-avatar').fadeTo(200, 1);
                                $('#u-m-account-active-status-icon').removeClass('text-red-1').addClass('text-green-2');
                                $('#u-m-account-active-status-text').text('Account is Active').removeClass('text-red-1');
                            }

                            //update user full name
                            $('#u-m-full-name').fadeOut(200, function () {
                                $('#u-m-full-name').text(ret.firstname + " " + ret.lastname);
                                $('#u-m-full-name').fadeIn(250);
                            });

                            //update user roll
                            $('#u-m-roll').fadeOut(200, function () {
                                $('#u-m-roll').text(ret.roll);
                                $('#u-m-roll').fadeIn(250);
                            });

                            //update user mail to
                            $('#u-m-user-mail-to').attr('href', 'mailto:' + ret.email);
                            $('#u-m-user-telephone').attr('href', 'tel:' + ret.telephone);

                            //update join at
                            $('#u-m-join-at').fadeOut(200, function () {
                                $('#u-m-join-at').text(ret.joinat + ' Join with the system');
                                $('#u-m-join-at').fadeIn(250);
                            });

                            //update first name
                            $('#u-m-first-name').fadeOut(200, function () {
                                $('#u-m-first-name').text(ret.firstname);
                                $('#u-m-first-name').fadeIn(250);
                            });

                            //update last name
                            $('#u-m-last-name').fadeOut(200, function () {
                                $('#u-m-last-name').text(ret.lastname);
                                $('#u-m-last-name').fadeIn(250);
                            });

                            window.uSelecting = 0;
                            getActivityLog(1, userId);
                            window.userChanging = 0;
                            $('#u-m-user-list-loading').fadeOut(100, function () {
                                $('#u-m-user-list-loading').remove();
                            });

                            //change active deactive button status
                            if(ret.active == 0) {
                                $('#is-user-enable-disable').removeClass('on');
                            }
                            else {
                                $('#is-user-enable-disable').addClass('on');
                            }
                        }
                        else {
                            dbError();
                            window.userChanging = 0;
                            $('#u-m-user-list-loading').fadeOut(100, function () {
                                $('#u-m-user-list-loading').remove();
                            });
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        internetError();
                        window.userChanging = 0;
                        $('#u-m-user-list-loading').fadeOut(100, function () {
                            $('#u-m-user-list-loading').remove();
                        });
                    }
                });
            }
            else {
                //update user avatar
                $('#u-m-user-avatar').attr('src', url + '/internal/assets/img/users/default/user-100.jpg').addClass('gray-scale');
                $('#u-m-user-avatar').fadeTo(200, 1);
                var name = $('#u-m-user-list-name-' + userId).text();
                var roll = $('#u-m-user-list-roll-' + userId).text();
                //update user full name
                $('#u-m-full-name').fadeOut(200, function () {
                    $('#u-m-full-name').text(name);
                    $('#u-m-full-name').fadeIn(250);
                });

                //hide active button
                $('#u-m-active-deactive-container').fadeOut(200);

                //update user roll
                $('#u-m-roll').fadeOut(200, function () {
                    $('#u-m-roll').text(roll);
                    $('#u-m-roll').fadeIn(250);
                });
                window.uSelecting = 0;
                getActivityLog(1, userId);

                setTimeout(function () {
                    $('#u-m-not-activated-quick-links').fadeIn(200);
                }, 200);

                $('#u-m-quick-link-list').fadeOut(200);

                jQuery.ajax({
                    type: 'GET',
                    url: window.url + '/user-manage-display-info',
                    data: {userid: userId},
                    success: function (data) {
                        $('#master-no-connection').fadeOut(200);
                        if (data != 0) {
                            var ret = JSON.parse(data);
                            $('#u-m-user-info-container').fadeOut(200, function () {
                                $('#u-m-account-not-activated-container').append('<div id="u-m-account-not-activated" class="row"><h5><i style="margin-left: -5px;" class="fa fa-circle text-blue-3"></i> Account is not activated yet! Activation link has sent to <a href="mailto:' + ret.email + '" style="font-weight:500;">' + ret.email + '</a></h5></div>');
                                window.userChanging = 0;
                                $('#u-m-user-list-loading').fadeOut(100, function () {
                                    $('#u-m-user-list-loading').remove();
                                });
                            });
                        }
                        else {
                            dbError();
                            window.userChanging = 0;
                            $('#u-m-user-list-loading').fadeOut(100, function () {
                                $('#u-m-user-list-loading').remove();
                            });
                        }
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        internetError();
                        window.userChanging = 0;
                        $('#u-m-user-list-loading').fadeOut(100, function () {
                            $('#u-m-user-list-loading').remove();
                        });
                    }
                });
            }
        }
        $('#u-m-selected-user').val(userId);
    }
    getUsersAccess(userId);
}

function getUsersAccess(userId) {
    $('.ios-switch-md').removeClass('on');
    $('.panel').removeClass('panel-blue-2').addClass('panel-lightblue-2');
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/user-manage-get-users-access',
        data: {userid: userId},
        success: function (data) {
            if (data != 0) {
                var res = JSON.parse(data);
                for (var i = 0; i < res.pages.length; i++) {
                    $('#is-pages-' + res.pages[i]).addClass('on');
                    $('#u-m-tab-container-' + res.pages[i]).addClass('panel-blue-2').removeClass('panel-lightblue-2');
                }
                for (var i = 0; i < res.alloperations.length; i++) {
                    $('#is-op-'+res.alloperations[i]).removeClass('on');
                }
                for (var i = 0; i < res.operations.length; i++) {
                    $('#is-op-'+res.operations[i]).addClass('on');
                }
                for (var i = 0; i < res.alloperationlength.length; i++) {
                    $('#u-m-access-levels-option-'+res.alloperationlength[i]+'-a').prop('selected', true);
                }
                for (var i = 0; i < res.oplength.length; i++) {
                    $('#u-m-access-levels-option-'+res.oplength[i].oplecode+'-'+res.oplength[i].length).prop('selected', true);
                }
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

window.deleteUser = function deleteUser() {
    var userId = $('#u-m-selected-user').val();
    if (userId != window.authUser) {
        confirmBox('Delete Permanently !', 'Do you really want to delete this user account ?');
        $('#u-m-user-avatar').fadeTo(750, 0.4);
        $('#u-m-user-list-item-' + userId).fadeTo(500, 0.4);
        window.cBoxConfirmed = function yourFunctionName() {
            jQuery.ajax({
                type: 'POST',
                url: window.url + '/user-manage-delete-user',
                data: {userid: userId},
                success: function (data) {
                    $('#master-no-connection').fadeOut(200);
                    if (data != 0) {
                        notification('success', 'Deleted!', 'User account has deleted successfully.')
                        $('#u-m-user-list-item-' + userId).slideUp(250, function () {
                            $('#u-m-user-list-item-' + userId).remove();
                        });
                        $('#u-m-user-list-item-' + window.authUser).trigger('click');
                    }
                    else {
                        dbError();
                        $('#u-m-user-avatar').fadeTo(250, 1);
                        $('#u-m-user-list-item-' + userId).fadeTo(250, 1);
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    internetError();
                }
            });
        }
        window.cBoxNotConfirmed = function yourFunctionName() {
            $('#u-m-user-avatar').fadeTo(250, 1);
            $('#u-m-user-list-item-' + userId).fadeTo(250, 1);
        }
    }
}


function iSwitchOnChange(id, status) {
    var userId = $('#u-m-selected-user').val();
    if (id == 'user-enable-disable') {
        if (status == 0) {
            $('#u-m-user-avatar').fadeTo(750, 0.4);
            $('#u-m-user-list-item-' + userId).fadeTo(500, 0.4);
            $('#u-m-account-active-status-icon').addClass('text-red-1').removeClass('text-green-2');
            $('#u-m-account-active-status-text').text('Account is Deactive').addClass('text-red-1');
        }
        else {
            $('#u-m-user-avatar').fadeTo(250, 1);
            $('#u-m-user-list-item-' + userId).fadeTo(250, 1);
            $('#u-m-account-active-status-icon').removeClass('text-red-1').addClass('text-green-2');
            $('#u-m-account-active-status-text').text('Account is Active').removeClass('text-red-1');
        }
        //update the database
        jQuery.ajax({
            type: 'POST',
            url: window.url + '/user-manage-active-deactive-user',
            data: {userid: userId, status: status},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data != 0) {
                    if (status == 0) {
                        notification('success', 'Deactivated', 'User account has successfully deactivated!', 3000);
                    }
                    else {
                        notification('success', 'Activated', 'User account has successfully Activated!', 3000);
                    }
                }
                else {
                    if (status == 0) {
                        $('#u-m-user-avatar').fadeTo(250, 1);
                        $('#u-m-user-list-item-' + userId).fadeTo(250, 1);
                        notification('error', 'Unable to Deactivate', 'Some error occurred. Account is not Deactivated. Try again later...', 7000);
                        $('#u-m-account-active-status-icon').removeClass('text-red-1').addClass('text-green-2');
                        $('#u-m-account-active-status-text').text('Account is Active').removeClass('text-red-1');
                    }
                    else {
                        $('#u-m-user-avatar').fadeTo(750, 0.4);
                        $('#u-m-user-list-item-' + userId).fadeTo(500, 0.4);
                        notification('error', 'Unable to Activate', 'Some error occurred. Account is not Activated. Try again later...', 7000);
                        $('#u-m-account-active-status-icon').addClass('text-red-1').removeClass('text-green-2');
                        $('#u-m-account-active-status-text').text('Account is Deactive').addClass('text-red-1');
                    }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
    else if (id.substring(0, 5) == 'pages') {
        var pageId = id.slice(6);
        if(status == 0) {
            $('#u-m-tab-container-'+pageId).removeClass('panel-blue-2').addClass('panel-lightblue-2');
        }
        else {
            $('#u-m-tab-container-'+pageId).addClass('panel-blue-2').removeClass('panel-lightblue-2');
        }
        jQuery.ajax({
            type: 'POST',
            url: window.url + '/user-manage-allow-deny-system-pages',
            data: {userid: userId, status: status, pageid:pageId},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data != 0) {

                }
                else {
                    if(status == 0) {
                        notification('error', 'Unable Deny Access', 'Some error occurred. Page access cannot deny at this movement. Try again later...');
                    }
                    else {
                        notification('error', 'Unable Allow Access', 'Some error occurred. Page access cannot allow at this movement. Try again later...');
                    }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
    else if(id.substring(0, 2) == 'op') {
        var opId = id.slice(3);
        jQuery.ajax({
            type: 'POST',
            url: window.url + '/user/manage/operation-access',
            data: {userid: userId, status: status, opid:opId},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data != 0) {

                }
                else {
                    if(status == 0) {
                        notification('error', 'Unable Deny Access', 'Some error occurred. Page access cannot deny at this movement. Try again later...', 4000);
                    }
                    else {
                        notification('error', 'Unable Allow Access', 'Some error occurred. Page access cannot allow at this movement. Try again later...', 4000);
                    }
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
}

function getActivityLog(paginate, userId) {
    if (window.aLPaginate != -1) {
        var temPaginate = window.aLPaginate;
        window.aLPaginate = 0;
        var loading = (
            '               <li id="u-m-activity-list-loading" class="media">' +
            '                   <p style="text-align: center;">' +
            '                       <img src="' + url + '/internal/assets/img/loading/loading.gif" /> ' +
            '                   </p>' +
            '               </li>'
        );
        $('#u-m-activity-list').append(loading);
        jQuery.ajax({
            type: 'GET',
            url: window.url + '/internal-user-get-activity-list',
            data: {paginate: paginate, userid: userId},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data == -1) {
                    temPaginate = -1;
                    $('#u-m-activity-list-loading').fadeOut(250, function () {
                        $('#u-m-activity-list-loading').remove();
                    });
                }
                else if (data != 0) {
                    var ret = JSON.parse(data);
                    var activityList = "";
                    for (var i = 0; i < ret.length; i++) {
                        activityList += (
                            '               <li class="media">' +
                            '                   <p>' + ret[i].activity + '<br />' +
                            '                       <i class="pull-right" style="color: rgba(0, 0, 0, 0.25); font-style: italic;" data-toggle="tooltip" title="' + ret[i].createdat + '">' + ret[i].timeago + '</i>' +
                            '                   </p>' +
                            '               </li>'
                        );
                    }
                    $('#u-m-activity-list-loading').fadeOut(250, function () {
                        $('#u-m-activity-list-loading').remove();
                        $('#u-m-activity-list').append(activityList);
                        window.aLPaginate = temPaginate;
                        window.aLPaginate++;
                    });
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
}


function resendActivationEmail() {
    var userId = $('#u-m-selected-user').val();

    jQuery.ajax({
        type: 'POST',
        url: window.url + '/user-manage-resend-activation-email',
        data: {userid: userId},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data != 0) {
                notification('success', 'Resent', 'Activation email has resent to the user', 3000);
                window.emailResend = 0;
                $('#u-m-resend-activation-email').fadeTo(250, 1);
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            // internetError();
        }
    });
}


function addUser() {
    var email = $.trim($('#mm-u-m-new-user-email').val());
    var fname = $.trim($('#mm-u-m-new-user-fname').val());
    var lname = $.trim($('#mm-u-m-new-user-lname').val());

    if(email == "" || fname == "" || lname == ""){
        $('#mm-u-m-new-user-email').trigger('blur');
        $('#mm-u-m-new-user-fname').trigger('blur');
        $('#mm-u-m-new-user-lname').trigger('blur');
    }
    else {
        $('#mm-u-m-new-user-add').removeAttr('onclick').fadeTo(250, 0.4);
        $('#mm-u-m-add-new-user-loading').fadeIn(250);
        jQuery.ajax({
            type: 'POST',
            url: window.url + '/user-manage-add-new-user',
            data: {email: email, fname:fname, lname:lname},
            success: function (data) {
                $('#mm-u-m-add-new-user-loading').fadeOut(250);
                $('#master-no-connection').fadeOut(200);
                if (data == 1) {
                    notification('success', 'Added', 'A new user has successfully added to the system!', 3000);
                    getUserList();
                    $('#mm-u-m-new-user-cancel').trigger('click');
                    $('#mm-u-m-new-user-add').attr('onclick', 'addUser();').fadeTo(250, 1);
                    $('#mm-u-m-new-user-email').val("");
                    $('#mm-u-m-new-user-name').val("");
                }
                else if(data == -1) {
                    $('#mm-u-m-new-user-email').parent().append('<span id="mm-u-m-add-user-email-validation" class="text-red-1">Sorry, This email is already used in the system</span>').addClass('has-error');
                    $('#mm-u-m-new-user-add').attr('onclick', 'addUser();').fadeTo(250, 1);
                }
                else {
                    dbError();
                    $('#mm-u-m-new-user-add').attr('onclick', 'addUser();').fadeTo(250, 1);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
                $('#mm-u-m-new-user-add').attr('onclick', 'addUser();').fadeTo(250, 1);
            }
        });
    }
}
//endregion