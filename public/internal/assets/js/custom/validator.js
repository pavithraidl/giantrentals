/**
 * Created by Isuru on 6/09/16.
 */

function emailValidator(object) {
    var email = $('#' + object.id).val();
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $('.email').remove();

    if (re.test(email)) {
        $('#' + object.id).parent().removeClass('has-error');
        $('.email').remove();
    }
    else {
        $('#' + object.id).parent().addClass('has-error');
        $('#' + object.id).after("<span class='email fa fa-remove text-red-1' style='margin-top: 5px;display: block;'> Invalid Email</span>");

    }
    if (email.trim() == '') {
        $('#' + object.id).parent().removeClass('has-error');
        $('.email').remove();
    }
}

function requiredValidator(object) {
    var field = $('#' + object.id).val();

    $('.required').remove();

    if (field.trim() == '') {
        $('#' + object.id).parent().addClass('has-error');
        $('#' + object.id).after("<span class='required fa fa-remove text-red-1' style='margin-top: 5px; display: block;'> This field is required</span>");
    }
    else {
        $('#' + object.id).parent().removeClass('has-error');
        $('.required').remove();
    }
}

function minValueValidator(object, min) {
    var value = $('#' + object.id).val();

    $('.minvalue').remove();

    if (value.trim() < min) {
        $('#' + object.id).parent().addClass('has-error');
        $('#' + object.id).after("<span class='minvalue fa fa-remove text-red-1' style='margin-top: 5px; display: block;'> Invalid! Minimum value is " + min + "</span>");
    }
    else {
        $('#' + object.id).parent().removeClass('has-error');
        $('.minvalue').remove();
    }
    if (value.trim() == '') {
        $('#' + object.id).parent().removeClass('has-error');
        $('.minvalue').remove();
    }
}

function maxValueValidator(object, max) {
    var value = $('#' + object.id).val();

    $('.maxvalue').remove();

    if (value.trim() > max) {
        $('#' + object.id).parent().addClass('has-error');
        $('#' + object.id).after("<span class='maxvalue fa fa-remove text-red-1' style='margin-top: 5px; display: block;'> Invalid! Max value is " + max + "</span>");
    }
    else {
        $('#' + object.id).parent().removeClass('has-error');
        $('.maxvalue').remove();
    }
    if (value.trim() == '') {
        $('#' + object.id).parent().removeClass('has-error');
        $('.maxvalue').remove();
    }
}

function maxLengthValidator(object, max) {
    var value = $('#' + object.id).val();

    $('.maxlength').remove();

    if (value.length > max) {
        $('#' + object.id).parent().addClass('has-error');
        $('#' + object.id).after("<span class='maxlength fa fa-remove text-red-1' style='margin-top: 5px; display: block;'> Invalid! You can't enter more than " + max + " characters</span>");
    }
    else {
        $('#' + object.id).parent().removeClass('has-error');
        $('.maxlength').remove();
    }
    if (value.trim() == '') {
        $('#' + object.id).parent().removeClass('has-error');
        $('.maxlength').remove();
    }
}

function toFixValidator(object, floatingPoints) {
    $('#' + object.id).val((parseFloat($('#' + object.id).val())).toFixed(floatingPoints));
}


$('.only-digits').bind('keypress', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        if(e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 9 || e.keyCode == 37 || e.keyCode == 38 || e.keyCode == 46 || e.keyCode == 40 || e.keyCode == 39) {

        }
        else {
            return false;
        }
    }
});

$('.only-numbers').bind('keypress', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        if (e.keyCode == 46) {

        }
        else {
            return false;
        }
    }
});

function phoneNumberValidator(object) {
    //develop the function
}