/**
 * Created by Isuru on 4/10/2016 AD.
 */
"use strict";

//Init variable
window.paginate = 1;


//Default setting



//Events
getScaffoldingTypes();
$('#vehicle-booking-type').on('change', function() {
    getScaffoldingTypes();
});



//Functions
function getScaffoldingTypes() {
    var typeId = $('#vehicle-booking-type').val();
    var el = $('.main-widget-header').parents(".widget:first");
    blockUI(el);

    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/scaffolding/get-scaffolding-part-list',
        data:{type_id:typeId},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data != 0) {
                var res = JSON.parse(data);
                var tableRows = '';

                for(var i = 0; i < res.length; i++) {
                    tableRows+= '   <tr class="item-table">' +
                        '               <td>'+res[i].part_id+'</td>' +
                        '               <td>'+(res[i].status == 1 ? '<label class="label label-success">In Stock</label>' : res[i].status == 2 ? '<label class="label label-info">Rented</label>' : res[i].status == -1 ? '<label class="label label-default">Disabled</label>' : '<label class="label label-danger">Missing</label>')+'</td>' +
                        '               <td>'+res[i].created_at+'</td>' +
                        '           </tr>'
                }
                $('#scaffolding-table-data').html(tableRows);
                setTimeout(function() {
                    unblockUI(el);
                }, 200);
            }
            else {
                notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...', 5000);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}