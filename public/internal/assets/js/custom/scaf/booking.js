/**
 * Created by Isuru on 22/09/2016 AD.
 */
"user strict";

//init variables
window.bookingOpen = 0;
window.opening = 0;
window.invoice = 0;
window.paginate = 1;
window.seletedReceiptItems = [];
window.lastRowId = 0;
window.bookingLoading = 1;
window.status = 3;
window.selectedCartId = null;

//default settings
// $('.o-r-container').slideUp(100);

//events
var el = $('.widget-header').parents(".widget:first");
blockUI(el);
getReceiptList(3, 1);

$('#vehicle-booking-type').on('change', function() {
    window.paginate = 1;
    window.bookingLoading = 0;
    var el = $('.main-widget-header').parents(".widget:first");
    blockUI(el);
    var status = $('#vehicle-booking-type').val();
    window.status = status;
    setTimeout(function() {
        $('#all-booking-container').html('');
        getReceiptList(status, 1);
    }, 200);
});

$(window).scroll(function () {
    var elementHeight = $('#s-i-all-booking-containers').height();
    var scrollPosition = $('body').scrollTop()+416;
    if (elementHeight < scrollPosition+200) {
        if(window.bookingLoading != 1) {
            window.bookingLoading = 1;
            $('#s-i-invoice-list-loading-icon').fadeIn(200);
            getReceiptList(window.status, 2, window.paginate);
        }
    }
});

$('#s-i-btn-add-new-invoice').on('mouseenter', function() {
    //rotate plus sign
});

$('#s-i-btn-add-new-invoice').on('click', function() {
    addNewInvoice();
});


//functions
function getReceiptList(status, from) {
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/scaffolding/manage-booking/get-booking-list',
        data: {paginate: paginate, status:status},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data != 0) {
                var res = JSON.parse(data);
                var list = "";
                var dateDivs = [];
                var containers = "";
                if(data != 'null') {
                    for(var i = 0; i < res.length; i++) {
                        var resId = res[i].id;
                        list += ('<span><li id="s-i-sales-list-'+resId+'" class="select-list-2">' +
                            '               <div class="row">' +
                            '                   <div class="col-md-1 col-sm-1 col-xs-3 '+(res[i].deleteaccess == '1' ? 'flip3D' : '')+'">' +
                            '                       <div id="s-i-circle-tag-'+resId+'" class="circle-sm front hidden-xs" style="background-color: '+(status == 1 ? '#2DB42D': status == 2 ? '#0052CC' : status == 3 ? '#990099' : '#ffae4b')+'"><p style="text-align: center;">'+(status == 4 ? '<i class="fa fa-edit"></i>' : res[i].tag)+'</p></div>' +
                            '                       '+(res[i].deleteaccess == '1' ? '<div class = "circle-sm back hidden-xs" onclick="deleteInvoice();" data-toggle="tooltip" title="Delete Invoice" style = "background-color: #ff3d35"><p style="text-align: center;"><i class="fa fa-trash"></i></div></p>' : '') +
                            '                       <div id="s-i-circle-tag-'+resId+'" class="circle-sm front hidden-sm hidden-md hidden-lg" style="margin-left:-25px; background-color: '+(status == 1 ? '#2DB42D': status == 2 ? '#0052CC' : status == 3 ? '#990099' : '#ffae4b')+'"><p style="text-align: center;">'+(status == 4 ? '<i class="fa fa-edit"></i>' : res[i].tag)+'</p></div>' +
                            '                       '+(res[i].deleteaccess == '1' ? '<div class = "circle-sm back hidden-md hidden-lg" onclick="deleteInvoice();" data-toggle="tooltip" title="Delete Invoice" style = " background-color: #ff3d35"><i class="fa fa-trash"></i></div>' : '') +
                            '                   </div>' +
                            '                   <div class="col-md-3 col-sm-3 col-xs-9"  onclick="openSalesReport('+resId+');">' +
                            '                       <div class="hidden-xs" style="margin-top: 6px; font-size: 18px;">'+res[i].inv_id+'</div>' +
                            '                       <div class="hidden-md hidden-sm hidden-lg" style="margin-top: 6px; font-size: 12px;">'+res[i].inv_id+'</div>' +
                            '                   </div>' +
                            '                   <div class="col-md-4 col-sm-4 col-sm-offset-0 col-xs-6 col-xs-offset-3 col-lg-3"  onclick="openSalesReport('+resId+');">' +
                            '                       <div id="s-i-list-header-name-'+resId+'" class="hidden-xs" style="margin-top: -3px; font-size: 16px;"><h4 class="text-blue-3">'+res[i].name+'</h4></div>' +
                            '                       <div id="s-i-list-header-name-'+resId+'" class="hidden-sm hidden-md hidden-lg" style="margin-top: -3px; font-size: 12px;"><h4 class="text-blue-3">test'+res[i].name+'</h4></div>' +
                            '                   </div>' +
                            '                   <div class="hidden-sm hidden-xs col-md-2 col-lg-2" style="margin-top: 12px; font-size: 14px;">' +
                            '                   </div> ' +
                            '                   <div class="col-md-3 col-sm-3  hidden-xs"  onclick="openSalesReport('+resId+');">' +
                            '                       <div id="s-i-list-header-amount-'+resId+'" style="margin-top: -3px;padding-right: 15px;"><h4  id="s-i-list-header-amount-value-'+resId+'" style="text-align: right;">$'+formatCurrency((parseFloat(res[i].price)).toFixed(2))+'</h4></div>' +
                            '                   </div>' +
                            '               </div>' +
                            '               <div id="s-v-open-report-container-'+resId+'" class="row o-r-container"></div>' +
                            '           </li>' +
                            '           <hr id="s-i-bottom-line-'+resId+'" class="style-one"></span>'
                        );
                        $('#s-i-delete-access').val(res[i].deleteaccess);
                    }

                    containers += ('<div class="col-md-10 col-md-offset-1 col-xs-10 col-xs-offset-1 all-booking-container">' +
                    '                   <h5 style="font-weight: 600; color: #86868a">Today</h5>' +
                    '                   <div class="row widget widget-panel-1">' +
                    '                       <ul  class="booking-container-'+resId+' search-filter" style="list-style-type: none; padding-top:20px;">'+list +
                    '                       </ul>' +
                    '                   </div>' +
                    '               </div>');
                    $('#all-booking-container').append(containers);
                    $('#s-i-invoice-list-loading-icon').fadeOut(250);
                    window.paginate++;
                    window.bookingLoading = 0;
                }
                else {
                    if(from == 1) {
                        $('#all-booking-container').html('<h4 style="text-align: center; margin-top: 50px;">No Records!</h4>');
                    }
                    else {
                        //nothing
                    }
                }
                setTimeout(function() {
                    var el = $('.main-widget-header').parents(".widget:first");
                    unblockUI(el);
                }, 200);

            }
            else {
                notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

//open sales report
function openSalesReport(id) {
    if(window.opening == 0) {
        $('#s-i-loading-line').remove();
        $('#s-i-open-invoice-id').val(id);
        var paginate = 1;
        if (window.bookingOpen == id) {
            $('#s-v-open-report-container-' + id).slideUp(500);
            window.bookingOpen = 0;
        }
        else {
            window.opening = 1;
            window.seletedReceiptItems = [];
            $('#s-i-sales-list-' + id).append('<div id="s-i-loading-line" class="glow-line-blue-1" style="position: absolute; top: 45px;"></div>');
            $('.glow-line-blue-1').animate({width: (20) + "%"}, 400);
            if ($('.o-r-container').is(':visible')) {
                $('.o-r-container').slideUp();
            }
            $('.invoice').remove();
            jQuery.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.addEventListener("progress", function (evt) {
                        $('.glow-line-blue-1').animate({width: 85 + "%"}, 500);
                    }, false);
                    return xhr;
                },
                type: 'GET',
                url: window.url + '/internal/scaffolding/get-single-booking-details',
                data: {booking_id: id},
                success: function (data) {
                    $('.glow-line-blue-1').animate({width: 100 + "%"}, 200);
                    setTimeout(function() {
                        $('.glow-line-blue-1').fadeOut(200, function () {
                            $('#s-i-loading-line').remove();
                        });
                    }, 200);
                    $('#master-no-connection').fadeOut(200);
                    if (data != 0) {
                        var res = JSON.parse(data);
                        var itemRow = "";
                        var subtotal = 0;
                        var i = 0;
                        for(i = 0; i < res[0].length; i++) {
                            subtotal += res[0][i].price;

                            itemRow += ('<tr>' +
                                '               <td><span id="cart-status-'+res[0][i].id+'" onclick="changeItemStatus('+res[0][i].id+', '+res[0][i].status+');" class="label '+(res[0][i].status == 3 ? 'label-info" style="cursor:pointer;">Pending': res[0][i].status == 2 ? 'label-success" style="cursor:pointer;">Released' : 'label-default">Done')+'</span></td>' +
                                '               <td>'+res[0][i].type_name+'</td>' +
                                '               <td style="text-align: center;">'+res[0][i].qty+'</td>' +
                                '               <td style="text-align: center;">'+res[0][i].from+'</td>' +
                                '               <td style="text-align: center;">'+res[0][i].to+'</td>' +
                                '               <td style="text-align: right;">$'+formatCurrency(res[0][i].day_rate)+'</td>' +
                                '               <td style="text-align: right;">$'+formatCurrency(res[0][i].price)+'</td>' +
                                '           </tr>'
                            );
                        }

                        var newRow = "";
                        var warehouseList = "";
                        if(res.status == 4) {
                            newRow = ('<tr id="s-i-invoice-row-'+i+'" data-id="null" onmouseenter="fadeInRemove('+i+');" onmouseleave="fadeOutRemove('+i+');">' +
                                '               <td id="s-i-first-column-'+i+'"><span><input id="s-i-new-invoice-item-name-'+i+'" data-itemid="" data-id="'+i+'" data-salesid="0" type="text" class="input-frame-trans-1 popup-input" onfocus="showItemDropDownList(this);" onkeyup="searchFilter(this);" style="width: 100%;" placeholder="Item name"></span></td>' +
                                '               <td><input type="number" id="s-i-new-invoice-item-qty-'+i+'" onfocus="qtyValidator('+i+');" onchange="updateItemQty('+i+'); setTotal('+i+');" class="only-numbers input-frame-trans-1" style="width: 70px; text-align: right;"><span id="s-i-new-invoice-unit-symbol-'+i+'"></span></td>' +
                                '               <td><input type="number" id="s-i-new-invoice-unit-price-'+i+'" onchange="setTotal('+i+'); updateItemUnitPrice('+i+');" onfocus="unitPriceValidator('+i+');" class="input-frame-trans-1 only-numbers"style="width: 100%; text-align: right;"> </td>' +
                                '               <td id="s-i-new-invoice-item-total-'+i+'" style="text-align: right;"></td>' +
                                '           </tr>'
                            );
                        }
                        window.lastRowId = i;

                        var booking = (
                            '<div id="s-m-report-content" data-id="' + id + '" class="widget invoice" style=" cursor: auto !important;" >' +
                            '    <div class="widget-content padding">' +
                            '   <div id="s-i-final-invoice-container">' +
                            '       <div class="row" style="margin-bottom: -30px;">' +
                            '               <h1 style="text-align:center;">JOB SHEET</h1>' +
                            '           <div class="col-xs-8 text-right col-xs-push-4">' +
                            '               <h4>#'+res.booking_id+'</h4>' +
                            '               '+(res.warehousemood != '1' ? '' : res.status == 4 ? 'Warehouse : <select id="s-i-warehouse" onchange="changeWarehouse();" style="height: 37px;background-color: rgba(212, 214, 217, 0.31);border-color: white;border-radius: 7px;padding-left: 5px;padding-right: 5px;">'+warehouseList+'</select>' : '')+
                            '                       <p><strong>ORDER STATUS : </strong> <span class="label label-'+(res.status == 1 ? 'default">Done' : res.status == 2 ? 'info">Return' : res.status == 3 ? 'success">Pending' : 'warning">Editing')+'</span></p>' +
                            '               '+(res.status != 4 ? '<a href="#" class="btn btn-primary btn-sm invoice-print" onclick="printInvoice();" style="margin-bottom: 10px;"><i class="icon-print-2"></i> Print</a><br/>' : '')+
                            '           </div>' +
                            '           <div class="col-xs-4 col-xs-pull-8">' +
                            '               <div class="company-column">' +
                            '                   <h4><img src="'+url+'images/org/logo/'+res.orgid+'/logo.png"></h4>' +
                            '                   <address style="margin-top: -10px;">' +
                            '                       <br>' +
                            '                       111, Newton Road<br>' +
                            '                       Eden Terrace <br>' +
                            '                       Auckland - 1041 <br>' +
                            '                       New Zealand<br>' +
                            '                   </address>' +
                            '               </div>' +
                            '           </div>' +
                            '       </div>' +
                            '       <div class="bill-to">' +
                            '           <div class="row">' +
                            '               <div class="col-sm-6">' +
                            '                   <h4>Customer: <strong>'+res.name+'</strong></h4>' +
                            '                   '+(res.street == null || res.subrb == null || res.city == null ? '' : '<address id="s-i-customer-address">'+(res.street  == null ? '': res.street)+'<br>' +
                            '                       '+(res.subrb  == null ? '': res.subrb)+
                            '                       '+(res.city  == null ? '': res.city))+
                            '                   </address>' +
                            '                   <telephone><strong>Tel: </strong>'+res.telephone+'</telephone><br/>' +
                            '                   <email><strong>Email: </strong>'+res.email+'</email>' +
                            '               </div>' +
                            '               <div class="col-sm-6"><br>' +
                            '                   <small class="text-right">' +
                            '                       <p><strong>ORDER DATE : </strong> '+res.created_at+'</p>' +
                            '                       <p><strong>INVOICE ID : </strong> #SEL'+res.booking_id+'</p>' +
                            '                   </small>' +
                            '               </div>' +
                            '           </div>' +
                            '       </div>' +
                            '       <br><br>' +
                            '       <div class="table-responsive">' +
                            '           <table class="table table-condensed table-striped">' +
                            '               <thead>' +
                            '                   <tr>' +
                            '                       <th>Status</th>' +
                            '                       <th>ITEMS</th>' +
                            '                       <th style="text-align: center;" width="140">QTY</th>' +
                            '                       <th style="text-align: center;">From</th>' +
                            '                       <th style="text-align: center;">To</th>' +
                            '                       <th style="text-align: right;">Per Day</th>' +
                            '                       <th width="200px" style="text-align: right;">TOTAL</th>' +
                            '                   </tr>' +
                            '               </thead>' +
                            '               <tbody>' + itemRow + newRow +
                            '                   <tr>' +
                            '                       <td>&nbsp;</td>' +
                            '                       <td></td>' +
                            '                       <td></td>' +
                            '                       <td style="text-align: right;"></td>' +
                            '                    </tr>'+
                            '                   <tr>' +
                            '                   <td colspan="6" class="text-right"><strong>Subtotal</strong></td>' +
                            '                       <td id="s-i-subtotal" style="text-align: right;">$<span id="invoice-full-total-int">'+formatCurrency(subtotal)+'</span></td>' +
                            '                   </tr>' +
                            '                   <tr>' +
                            '                   <tr>' +
                            '                       <td colspan="6" class="text-right"><strong>Discount</strong></td>' +
                            '                       <td style="text-align: right;"><strong class="text-red-1">'+(res.status == 1 ? '<input id="s-i-invoice-discount" onchange="addDiscount('+parseInt(res.booking_id)+');" type="number" class="input-frame-trans-1" value="'+res.discount+'" min="0" max="100" style="text-align:right; margin-right:-15px;"> %' : res.discount == 0 ? '-': res.discount+' %')+'</strong></td>' +
                            '                   </tr>' +
                            '                   <tr>' +
                            '                       <td colspan="6" class="text-right"><strong>TOTAL</strong></td>' +
                            '                       <td style="text-align: right; font-size: 15px;"><strong id="s-i-invoice-full-total" class="text-primary">$'+formatCurrency(res.full_total)+'</strong></td>' +
                            '                   </tr>' +
                            '               </tbody>' +
                            '           </table>' +
                            '           <br><br>' +
                            '       </div>' +
                            '       </div>' +
                            '       <div id="s-i-invoice-pavements-header" onclick="slidePavements();" class="row" style="cursor: pointer; position: relative;">' +
                            '           <h4 style="text-align: center; margin-bottom: -10px;">' +
                            '               <strong>Options</strong>' +
                            '           </h4><i id="s-i-invoice-pavement-container-icon" class="icon-down-open-mini" style="font-size: 26px; position: absolute; top: 8px; left: 95%;"></i> ' +
                            '       </div>  ' +
                            '       <hr/> ' +
                            '       <div id="s-i-invoice-pavements-container" class="row">' +
                            '           <div class="col-md-10 col-md-offset-1">' +
                            '               <div class="col-md-6">' +
                            '                   <div class="row" style="margin-top: 10px;">' +
                            '                       <div class="col-xs-4">' +
                            '                           <label>Amount</label>' +
                            '                       </div>' +
                            '                       <div class="col-xs-1"> ' +
                            '                           : ' +
                            '                       </div>' +
                            '                       <div id="s-i-invoice-pavements-amount" class="col-xs-6" style="text-align: right;"> ' +
                            '                           <p style=" text-align: left;"><strong>'+(res.pavement == null ? '<font style="color:red;">Not yet</font>' : '<font style="color: green;">'+res.pavement+'</font>')+ '</strong></p>'+
                            '                       </div> ' +
                            '                   </div> ' +
                            '               </div>' +
                            '           </div> ' +
                            '       </div></div></div>' +
                            '       </div> ' +
                            '   </div>'+(res.status == 4 && res.submitaccess == 1 ? '<p id="s-i-submit-button-container" style="text-align: center;"><button class="btn btn-sm btn-success" onclick="submitInvoice('+res.booking_id+');">Submit</button> </p>' : '')+
                            '</div>'
                        );
                        $('#s-v-open-report-container-' + id).append(booking).slideDown(550, function() {
                            if(res.length < 1) {
                                $('#s-i-submit-button-container').hide();
                            }
                        });
                        $('#s-i-client-name-loading-image').hide();
                        $('#s-i-invoice-pavement-method-'+res.method).attr('selected', 'selected');
                        $('#s-i-invoice-pavement-cheque-bank option[value="'+res.currentbankid+'"]').attr('selected', 'selected');

                        if(res.method == 'ch') {
                            $('#cheque-info-container').fadeIn(200);
                        }
                        else {
                            $('#cheque-info-container').fadeOut(200);
                        }

                        $('.remove-row').fadeTo(10, 0);
                    }
                    else {
                        notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
                    }
                    window.opening = 0;
                },
                error: function (xhr, textStatus, errorThrown) {
                    window.opening = 0;
                    internetError();
                }
            });
            window.bookingOpen = id;
        }
    }
}

function changeItemStatus(cartId, status) {
    if(status == 2 || status == 3) {
        window.selectedCartId = cartId;
        if(status == 3)
            var title = 'Release';
        else if(status == 2)
            var title = 'Return';

        $('#mm-s-m-release-return-title').html(title);
        $('#mm-s-m-btn-return-release-item').text(title).attr('disabled', 'disabled');

        $('#btn-show-cart-item-list').trigger('click');

        jQuery.ajax({
            type: 'GET',
            url: window.url + '/scaffolding/get-cart-item-list',
            data:{status:status, cart_id:cartId},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data != 0) {
                    var res = JSON.parse(data);
                    var tableRows = '';

                    for(var i = 0; i < res.length; i++) {
                        tableRows+= '   <tr class="item-table">' +
                            '               <td>'+res[i].item_id+'</td>' +
                            '               <td>'+res[i].name+'</td>' +
                            '               <td>'+res[i].status+'</td><td><input onclick="checkAllChecked();" type="checkbox" class="rows-check"> </td>' +
                            '           </tr>'
                    }
                    $('#mm-s-m-center-loading-img').fadeOut(200);
                    $('#booking-table-cart-item-list').html(tableRows);
                    setTimeout(function() {
                        var el = $('.widget-header').parents(".widget:first");
                        unblockUI(el);
                    }, 200);
                }
                else {
                    notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
}

function checkAllChecked() {
    var checker = 1;

    $(".item-table input").each(function(){
        if(!$(this).is(':checked'))
            checker = 0;
    });

    if(checker == 1)
        $('#mm-s-m-btn-return-release-item').removeAttr('disabled');
    else
        $('#mm-s-m-btn-return-release-item').attr('disabled', 'disabled');
}

function updateItemCartStatus() {
    var checker = 1;

    $(".item-table input").each(function(){
        if(!$(this).is(':checked'))
            checker = 0;
    });

    if(checker == 1) {
        var el = $('.widget-header').parents(".widget:first");
        blockUI(el);

        jQuery.ajax({
            type: 'POST',
            url: window.url + '/scaffolding/update-cart-status',
            data:{cart_id:window.selectedCartId},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data == 1) {
                    notification('success', 'Done!', 'Item Status has successfully updated.', 3000);
                    $('#mm-s-m-btn-return-release-item-cancel').trigger('click');

                    if($('#cart-status-'+window.selectedCartId).hasClass('label-info')) {
                        $('#cart-status-'+window.selectedCartId).addClass('label-success').removeClass('label-info').text('Released');
                    }
                    else if($('#cart-status-'+window.selectedCartId).hasClass('label-success')) {
                        $('#cart-status-'+window.selectedCartId).addClass('label-default').removeClass('label-success').text('Done');
                    }
                }
                else {
                    notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...', 7000);
                }
                unblockUI(el);
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
}

function printInvoice() {
    //$('#s-i-final-invoice-container').hide();
    var restorePage = document.body.innerHTML;
    $('.invoice-print').hide();
    var printContent = $('#s-i-final-invoice-container').html();
    var letterheadMargin = ('<div style="min-height: 3px">&nbsp;</div>');
    document.body.innerHTML = letterheadMargin+printContent;
    window.print();
    document.body.innerHTML = restorePage;
    $('.invoice-print').show();
}

//new invoice
function addNewInvoice() {
    if(invoice == 0) {
        invoice = 1;
        jQuery.ajax({
            type: 'GET',
            url: window.url + 'sales/invoice/new-invoice-id',
            data: {clientid:window.clientId},
            success: function (data) {
                invoice = 0;
                $('#master-no-connection').fadeOut(200);
                if (data != 0) {
                    var res = JSON.parse(data);
                    var deleteAccess = $('#s-i-delete-access').val();
                    var $list = $('<li id="s-i-sales-list-'+res.id+'" class="select-list-2" style="padding-bottom:10px;">' +
                        '               <div class="row">' +
                        '                   <div class="col-md-1 '+(deleteAccess == 1 ? 'flip3D' : '')+'">' +
                        '                       <div id="s-i-circle-tag-'+res.id+'" class="circle-sm front" style="background-color: #ffae4b"><p style="text-align: center;"><i class="fa fa-edit"></i></p></div>' +
                        '                       '+(deleteAccess == 1 ? '<div class = "circle-sm back" onclick="deleteInvoice('+res.id+', \'Today\');" data-toggle="tooltip" title="Delete Invoice" style = "background-color: #ff3d35"><p style="text-align: center;"><i class="fa fa-trash"></i></p></div>' : '')+
                        '                   </div>' +
                        '                   <div class="col-md-3" onclick="openSalesReport('+res.id+');">' +
                        '                       <div style="margin-top: 6px; font-size: 18px;">SEL'+res.invid+'</div>' +
                        '                   </div>' +
                        '                   <div class="col-md-5" onclick="openSalesReport('+res.id+');">' +
                        '                       <div id="s-i-list-header-name-'+res.id+'" style="margin-top: -3px; font-size: 16px;"><h4 id="s-i-client-name-'+res.id+'" class="text-blue-3"></h4></div>' +
                        '                   </div>' +
                        '                   <div class="col-md-3 col-sm-3  hidden-xs"  onclick="openSalesReport('+res.id+');">' +
                        '                       <div id="s-i-list-header-amount-'+res.id+'" style="margin-top: -3px;padding-right: 15px;"><h4  id="s-i-list-header-amount-value-'+res.id+'" style="text-align: right;">0.00</h4></div>' +
                        '                   </div>' +
                        '               </div>' +
                        '               <div id="s-v-open-report-container-'+res.id+'" class="row o-r-container" style="margin-top:10px;"></div>' +
                        '           </li>' +
                        '           <hr id="s-i-bottom-line-'+res.id+'" class="style-one">'
                    ).slideUp();
                    if($('#s-i-today-container').length == 0) {
                        var container = ('<div class="col-md-10 col-md-offset-1 all-booking-container-Today">' +
                        '                   <h5 style="font-weight: 600; color: #86868a">Today</h5>' +
                        '                   <div class="row widget widget-panel-1">' +
                        '                       <ul id="s-i-today-container" class="booking-container-Today" style="list-style-type: none;padding-top: 20px;">' +
                        '                       </ul>' +
                        '                   </div>' +
                        '               </div>');
                        $('#all-booking-containers').prepend(container);
                    }
                    setTimeout(function() {
                        openSalesReport(res.id);
                    }, 200);
                    $('#s-i-today-container').prepend($list);
                    $('#s-i-sales-list-'+data).slideDown(350);
                }
                else {
                    notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
                }

            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
                invoice = 0;
            }
        });
    }
}

$(document).mouseup(function (e)
{
    var container = $(".popup-closer-2");
    var focus = $('.popup-input');

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0 || !focus.is(e.target) && focus.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.slideUp(200, function() {
            $(".popup-closer-2").remove();
        });
    }
});

function deleteInvoice(resId, date) {
    $('#s-i-sales-list-'+resId).fadeTo(700, 0.3);
    nconfirm('Delete Invoice SEL'+pad(resId, 8)+' !', 'Do you really want to delete Invoice SEL'+pad(resId, 8)+' ?');

    window.confirmfunction = function() {
        var amount = $('#s-i-list-header-amount-value-'+resId).html().replace(/\&nbsp;/g, '');
        jQuery.ajax({
            type: 'POST',
            url: window.url + 'sales/invoice/delete',
            data:{resid:resId, amount:amount},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data == 1) {
                    $('#s-i-sales-list-'+resId).slideUp(200, function() {
                        $('#s-i-sales-list-'+resId).remove();
                        $('#s-i-bottom-line-'+resId).remove();
                        var listRemain = $.trim($('.booking-container-'+date).html());
                        if(listRemain == "") {
                            $('.all-booking-container-'+date).fadeOut(250, function() {
                                $('.all-booking-container-'+date).remove();
                            });
                        }
                    });
                }
                else if(data == -1) {
                    notification('warning', 'Unauthorized!', 'Sorry! You don\'t have access to delete invoice.');
                }
                else {
                    notification('error', 'Error Occurred', 'Sorry! Cannot Delete the invoice at this movement. try again later...');
                    $('#s-i-sales-list-'+resId).fadeTo(200, 1);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
    window.notConfirmed = function() {
        $('#s-i-sales-list-'+resId).fadeTo(200, 1);
    }
}

function changeInvoiceClient(clientId, dataId, clientName) {
    $('#s-i-new-invoice-company-name').val(clientName);
    $('#s-i-new-invoice-company-name').attr('data-id', clientId);
    var bookingId = $('#s-i-open-invoice-id').val();
    var whId = $('#s-i-warehouse').val();
    if(whId == undefined) {
        whId = null;
    }

    $('.popup-closer-2').slideUp(200, function() {
        $('.popup-closer-2').remove();
    });

    jQuery.ajax({
        type: 'GET',
        url: window.url + 'sales/invoice/get-client-address',
        data:{clientid:clientId, bookingid:bookingId, whid:whId},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data != 0) {
                var res = JSON.parse(data);
                var address = ((res.street  == null ? '': res.street)+'<br>' +
                    '                       '+(res.addline1  == null ? '': res.addline1)+(res.addline2 == null ? '': res.addline2)+'<br>' +
                    '                       '+(res.city  == null ? '': res.city)+(res.postalcode == null ? '' : res.postalcode)+'<br>' +
                    '                       '+(res.street == null ? '' : 'Sri Lanka')+' <br>'
                );
                $('#s-i-customer-address').html(address);
                $('#s-i-client-name-'+bookingId).text(clientName);

            }
            else {
                notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function selectInvoiceItem(itemId, inputFieldId, itemName, qty, unitSymbol, rowId) {
    var whId = $('#s-i-warehouse').val();
    var bookingId = $('#s-i-open-invoice-id').val();
    var clientId = $('#s-i-new-invoice-company-name').attr('data-id');
    var salesId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-salesid');
    var oldItemId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-itemid');
    $('#s-i-new-invoice-item-name-'+rowId).attr('data-itemid', itemId);
    window.seletedReceiptItems[rowId] = itemId;
    $('.popup-closer-2').slideUp(200, function() {
        $('.popup-closer-2').remove();
    });

    $('#'+inputFieldId).val(itemName);
    $('#s-i-new-invoice-unit-symbol-'+rowId).text(unitSymbol);
    jQuery.ajax({
        type: 'POST',
        url: window.url + 'sales/invoice/update-invoice',
        data:{bookingid:bookingId, salesid: salesId, column: 'itemid', value:itemId, whid:whId, customerid:clientId},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data != 0) {
                $('#s-i-new-invoice-item-name-'+rowId).attr('data-salesid', data);
            }
            else {
                notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}


function qtyValidator(rowId) {
    var salesId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-salesid');
    if(salesId == '0') {
        $('#s-i-new-invoice-item-name-'+rowId).trigger('focus');
    }
}

function unitPriceValidator(rowId) {
    var salesId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-salesid');
    var qty = $('#s-i-new-invoice-item-qty-'+rowId).val();
    if(salesId == '0') {
        $('#s-i-new-invoice-item-name-'+rowId).trigger('focus');
    }
    if(qty < 1) {
        $('#s-i-new-invoice-item-qty-'+rowId).trigger('focus');
    }
}


$('.only-numbers').bind('keypress', function (e) {
    if (e.keyCode < 48 || e.keyCode > 57) {
        if (e.keyCode == 46) {

        }
        else {
            return false;
        }
    }
});

function updateItemQty(rowId) {
    var bookingId = $('#s-i-open-invoice-id').val();
    var salesId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-salesid');
    var itemId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-itemid');
    if(salesId == '0') {
        $('#s-i-new-invoice-item-name-'+rowId).trigger('focus');
    }
    else {
        var qty = $('#s-i-new-invoice-item-qty-'+rowId).val();
        var whId = $('#s-i-warehouse').val();
        if(whId == undefined) {
            whId = 0;
        }
        jQuery.ajax({
            type: 'POST',
            url: window.url + 'sales/invoice/update-quntity',
            data:{salesid: salesId, value: qty, itemid:itemId, whid:whId, bookingid:bookingId},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data == -2) {

                }
                else if(data != -1) {
                    $('#s-i-new-invoice-item-qty-'+rowId).val(data);
                    if(data == 0) {
                        notification('error', 'No Stock!', 'Sorry! This item\'s stock is empty.');
                    }
                    else {
                        notification('error', 'Stock Level Exceed', 'Sorry! Stock only has '+data+' qty.');
                    }
                }
                else {
                    notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
}

function updateItemUnitPrice(rowId) {
    var salesId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-salesid');
    var itemId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-itemid');
    if(salesId == '0') {
        $('#s-i-new-invoice-item-name-'+rowId).trigger('focus');
    }
    else {
        var unitPrice = $('#s-i-new-invoice-unit-price-'+rowId).val();
        unitPrice = (parseFloat(unitPrice)).toFixed(2);
        $('#s-i-new-invoice-unit-price-'+rowId).val(unitPrice);
        jQuery.ajax({
            type: 'POST',
            url: window.url + 'sales/invoice/update-unit-price',
            data:{salesid: salesId, value: unitPrice, itemid:itemId},
            success: function (data) {
                $('#master-no-connection').fadeOut(200);
                if (data != 0) {

                }
                else {
                    notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                noConnection();
            }
        });
    }
}

function setTotal(rowId) {
    var bookingId = $('#s-i-open-invoice-id').val();
    var qty = $('#s-i-new-invoice-item-qty-'+rowId).val();
    var unitPrice = $('#s-i-new-invoice-unit-price-'+rowId).val();
    if(unitPrice != 0 || unitPrice != "" || qty != 0 || qty != "") {
        var total = (parseFloat(qty)*parseFloat(unitPrice)).toFixed(2);
        if(isNaN(total)) {
            total = 0;
        }
        $('#s-i-new-invoice-item-total-'+rowId).html(formatCurrency(total));
        $('#s-i-new-invoice-item-total-'+rowId).attr('data-value', total);

        var salesId = $('#s-i-new-invoice-item-name-'+window.lastRowId).attr('data-salesid');
        if(salesId != '0') {
            addNewInvoiceRow();
        }
        setFullTotal();
        addTaxValue(bookingId);
    }
}

function setFullTotal() {
    //full total
    //set subtotal
    var subTotal = 0;
    for(var i = 0; i < window.lastRowId; i++) {
        var value = parseFloat($('#s-i-new-invoice-item-total-'+i).attr('data-value'));
        if(isNaN(value)) {
            value = 0;
        }
        subTotal += value;
    }
    if(isNaN(subTotal)) {
        subTotal = 0;
    }
    subTotal = formatCurrency(subTotal.toFixed(2));
    var bookingId = $('#s-i-open-invoice-id').val();
    $('#s-i-subtotal').html(subTotal);
    var subtotal = parseFloat(($('#s-i-subtotal').html()).replace(/\&nbsp;/g, ''));
    var discount = parseFloat($('#s-i-invoice-discount').val());
    var taxValue = parseFloat(($('#s-i-invoice-tax-value').html()).replace(/\&nbsp;/g, ''));
    var transport = parseFloat($('#s-i-invoice-transport').val());
    var fullTotal = (subtotal-discount)+taxValue+transport;
    fullTotal = formatCurrency(fullTotal.toFixed(2));
    $('#s-i-invoice-full-total').html(fullTotal);
    $('#s-i-invoice-pavements-amount').html(fullTotal);
    $('#s-i-list-header-amount-value-'+bookingId).html(fullTotal);
}


function addNewInvoiceRow() {
    var i = window.lastRowId+1;
    var newRow = ('<tr id="s-i-invoice-row-'+i+'" data-id="null" onmouseenter="fadeInRemove('+i+');" onmouseleave="fadeOutRemove('+i+');">' +
        '               <td id="s-i-first-column-'+i+'"><span><input id="s-i-new-invoice-item-name-'+i+'" data-itemid="" data-id="'+i+'" data-salesid="0" type="text" class="input-frame-trans-1 popup-input" onfocus="showItemDropDownList(this);" onblur="hideClientDropdown();" onkeyup="searchFilter(this);" style="width: 100%;" placeholder="Item name"></span></td>' +
        '               <td><input type="number" id="s-i-new-invoice-item-qty-'+i+'" onfocus="qtyValidator('+i+');" onchange="updateItemQty('+i+'); setTotal('+i+');" class="only-numbers input-frame-trans-1" style="width: 70px; text-align: right;"><span id="s-i-new-invoice-unit-symbol-'+i+'"></span></td>' +
        '               <td><input type="number" id="s-i-new-invoice-unit-price-'+i+'" onfocus="unitPriceValidator('+i+');" onchange="setTotal('+i+'); updateItemUnitPrice('+i+');" class="input-frame-trans-1 only-numbers"style="width: 100%; text-align: right;"> </td>' +
        '               <td id="s-i-new-invoice-item-total-'+i+'" style="text-align: right;"></td>' +
        '           </tr>'
    );
    $('#s-i-invoice-row-'+window.lastRowId).last().after(newRow);
    window.lastRowId++;
    var iconId = i-1;
    $('#s-i-first-column-'+iconId).prepend('<i id="s-i-remove-row-'+iconId+'" onclick="removeInvoiceRow('+iconId+');" data-toggle="tooltip" title="Remove" class="fa fa-times-circle text-red-1 remove-row" style="position: absolute; font-size: 18px; left: 5px;"></i>');
    if(lastRowId == 1) {
        $('#s-i-submit-button-container').fadeIn(200);
    }
}

function fadeInRemove(rowId) {
    $('#s-i-remove-row-'+rowId).fadeTo(100, 1);
}

function fadeOutRemove(rowId) {
    $('#s-i-remove-row-'+rowId).fadeTo(100, 0);
}

function removeInvoiceRow(rowId) {
    var salesId = $('#s-i-new-invoice-item-name-'+rowId).attr('data-salesid');
    $('#s-i-invoice-row-'+rowId).fadeOut(200);
    window.seletedReceiptItems[rowId] = null;
    $('#s-i-new-invoice-item-total-'+rowId).remove();

    jQuery.ajax({
        type: 'POST',
        url: window.url + 'sales/invoice/remove-row',
        data:{salesid: salesId},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data == 1) {

            }
            else {
                notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
                $('#s-i-invoice-row-'+rowId).fadeIn(200);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
    setTotal(rowId);
}

function addDiscount(bookingId) {
    var discount = $('#s-i-invoice-discount').val();
    var fullTotal = $('#invoice-full-total-int').html();
    var fullTotal = fullTotal - (discount*fullTotal)/100;
    $('#s-i-invoice-full-total').html('$'+fullTotal.toFixed(2));
    jQuery.ajax({
        type: 'POST',
        url: window.url + '/scaffold/invoice/add-discount',
        data:{bookingid: bookingId, value:discount},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data == 1) {

            }
            else {
                notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            notification('warning', 'No Connection', 'Please check your internet connection!');
        }
    });
}

function addTaxValue(bookingId) {
    var value = $('#s-i-invoice-tax-rate').val();
    var setValue = (parseFloat(value)).toFixed(2);
    var subTotal = ($('#s-i-subtotal').html()).replace(/\&nbsp;/g, '');
    subTotal = parseFloat(subTotal);
    var taxValue = parseFloat(subTotal/100)*value;
    $('#s-i-invoice-tax-value').html(taxValue.toFixed(2));
    jQuery.ajax({
        type: 'POST',
        url: window.url + 'sales/invoice/add-tax-value',
        data:{bookingid: bookingId, value:value},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data == 1) {
                setFullTotal();
            }
            else {
                notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function addTransportValue(bookingId) {
    var value = $('#s-i-invoice-transport').val();
    value = (parseFloat(value)).toFixed(2);
    $('#s-i-invoice-transport').val(value);
    jQuery.ajax({
        type: 'POST',
        url: window.url + 'sales/invoice/add-transport',
        data:{bookingid: bookingId, value:value},
        success: function (data) {
            $('#master-no-connection').fadeOut(200);
            if (data == 1) {
                setFullTotal();
            }
            else {
                notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            noConnection();
        }
    });
}

function submitInvoice(bookingId) {
    var chequeNumber = $.trim($('#s-i-invoice-pavement-cheque-number').val());
    var chequeDate = $.trim($('#s-i-invoice-pavement-cheque-date').val());
    var method = $('#s-i-invoice-pavement-method').val();
    var proceed = 0;

    if(method == 'ch') {
        if(chequeNumber == "" || chequeNumber == null) {
            proceed = 0;
            $('#s-i-invoice-pavement-cheque-number').trigger('blur');
        }
        else if(chequeDate == "" || chequeDate == null) {
            proceed = 0;
            $('#s-i-invoice-pavement-cheque-date').trigger('blur');
        }
        else {
            proceed = 1;
        }
    }
    else  {
        proceed = 1;
    }

    if(proceed == 1) {
        nconfirm('Submit Invoice !', 'Do you really want to submit this invoice ?');

        window.confirmfunction = function() {
            var amount = $('#s-i-list-header-amount-value-'+bookingId).html().replace(/\&nbsp;/g, '');
            var letter = $('#s-i-list-header-name-'+bookingId).text();
            letter = letter.slice(0,1);
            $('#s-i-circle-tag-'+bookingId).css('backgroundColor', '#9300ff').html('<p style="text-align: center;">'+letter+'</p>');
            jQuery.ajax({
                type: 'POST',
                url: window.url + 'sales/invoice/submit-invoice',
                data:{bookingid: bookingId, amount:amount, method:method},
                success: function (data) {
                    $('#master-no-connection').fadeOut(200);
                    if (data == 1) {
                        openSalesReport(bookingId);
                        notification('success', 'Invoice Submitted!', 'Your invoice is successfully submitted.');
                        //$(window).scrollTop($('#s-i-list-header-name-'+bookingId).offset().top);

                        $("html, body").delay(200).animate({
                            scrollTop: $('#s-i-list-header-name-'+bookingId).scrollTop() - 100
                        }, 1000);
                    }
                    else if(data == -1) {
                        notification('error', 'Unauthorized!', 'Sorry! You don\'t have access to submit invoices.');
                    }
                    else {
                        notification('error', 'Error Occurred', 'Sorry! Cannot connect with the database at this movement. try again later...');
                    }
                },
                error: function (xhr, textStatus, errorThrown) {
                    noConnection();
                }
            });
        }
    }

}

function slidePavements() {
    if(!$('#s-i-invoice-pavements-container').is(':visible')){
        $('#s-i-invoice-pavement-container-icon').removeClass('icon-down-open-mini').addClass('icon-up-open-mini');
    }
    else {
        $('#s-i-invoice-pavement-container-icon').removeClass('icon-up-open-mini').addClass('icon-down-open-mini');
    }

    $('#s-i-invoice-pavements-container').slideToggle(300);
}