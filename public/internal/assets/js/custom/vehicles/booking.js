/**
 * Created by Isuru on 7/09/16.
 */
"use strict";

//Init global variables
window.moreBookings = true;
window.paginate = 1;
window.bookingType = 3;
window.processing = false;
window.calenderInit = false;
window.initiating = true;
window.sliderUpDay = null;
window.sliderUpMonth = null;
window.scrollOldPosition = 1016;
window.scrollRequired = true;

//Default Settings


//Events
sliderCellsVariables();
initiatingSliderCells();
getBookingList(window.bookingType, 0);
runCalendar();

$('#tab-booking-slider-container').on('click', function() {
    window.scrollRequired = false;
    window.initiating = true;
    initiatingSliderCells();
});

$('#tab-booking-table-container').on('click', function() {
    getBookingList(window.bookingType, 0);
});

$('#tab-booking-calender-container').on('click', function () {
    if(window.calenderInit == false) {
        setTimeout(function() {
            $('.fc-button-today').trigger('click');
        }, 400);
        window.calenderInit = true;
    }

});

$('#slider-cell-container').scroll(function () {
    var elementWidth = $('#slider-table-tbody-cells').width();

    var scrollPosition = $('#slider-cell-container').scrollLeft() + 1800;

    if (elementWidth < scrollPosition) {
        if (window.processing == false) {
            window.initiating = false;
            window.iStartMonth = window.iStartMonth+3;
            if(window.iStartMonth == 13) {
                window.iStartMonth = 1;
            }
            else if(window.iStartMonth == 14) {
                window.iStartMonth = 2;
            }
            else if(window.iStartMonth == 15) {
                window.iStartMonth = 3;
            }
            window.scrollRequired = false;
            initiatingSliderCells();
        }
    }
});

$('#vehicle-booking-type').on('change', function() {
    var type = $('#vehicle-booking-type').val();
    window.bookingType = type;
    getBookingList(window.bookingType, 0);
});

//Functions
//region Table Tab Functions
function getBookingList(type, paginate) {
    window.processing = true;
    $('#exception-list-loading').fadeIn(200);

    if(type == 3) {
        var typeTitle = 'PENDING BOOKING LIST';
    }
    else if(type == 2) {
        var typeTitle = 'RENTED VEHICLE LIST';
    }
    else {
        var typeTitle = 'BOOKING HISTORY';
    }

    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/get-vehicle-booking-list',
        data:{type:type},
        success: function (data) {
            if (data != 0) {
                var res = JSON.parse(data);
                var tableData = "";
                var rows = 0;

                if(res != null) {
                    rows = res.length;
                }

                for(var i = 0; i < rows; i++) {
                    tableData += ('  <tr id="table-row-'+parseInt(res[i].booking_id)+'" '+(res[i].day_status == 1 ? 'style="background-color: rgba(0, 210, 157, 0.15);"' : res[i].day_status == 2 ? 'style="background-color: rgba(210, 30, 0, 0.15);"' : res[i].day_status == 3 ? 'style="background-color: rgba(0, 31, 210, 0.15);"' : '')+'>' +
                    '           <td>VEH'+res[i].booking_id+'</td>' +
                    '           <td><strong>'+res[i].plate+'</strong></td>' +
                    '           <td '+(type == 2 ? 'data-toggle="tooltip" title="'+res[i].release_at+'"' : type == 1 ? 'data-toggle="tooltip" title="'+res[i].release_at+'"' : '')+'>'+res[i].from+'</td>' +
                    '           <td '+(type == 1 ? 'data-toggle="tooltip" title="'+res[i].return_at+'"': '')+'>'+res[i].to+'</td>' +
                    '           <td>'+res[i].fname+' ' + res[i].lname +'</td>' +
                    '           <td>'+res[i].email+'</td>' +
                    '           <td>'+res[i].telephone+'</td>' +
                    '           <td>'+(type == 3 ? '<span id="booking-status-'+parseInt(res[i].booking_id)+'" onclick="vehicleStatusUpdate('+parseInt(res[i].booking_id)+', 2);" style="cursor: pointer;" class="label label-info">Release Vehicle</span>': type == 2 ? '<span id="booking-status-'+parseInt(res[i].booking_id)+'" onclick="vehicleStatusUpdate('+parseInt(res[i].booking_id)+', 1);" style="cursor: pointer;" class="label label-success">Return Vehicle</span>' : '<span id="booking-status-'+parseInt(res[i].booking_id)+'" style="cursor: pointer;" class="label label-default">Done</span>')+'</td>' +
                    '           <td>'+res[i].created_at+'</td> ' +
                    '       </tr>');
                }
                $('#booking-table-data').html(tableData);
                $('#booking-type-title').text(typeTitle);
                setTimeout(function() {
                    $('#exception-list-loading').fadeOut(200);
                    window.processing = false;
                }, 600);
            }
            else {
                dbError();
                setTimeout(function() {
                    $('#exception-list-loading').fadeOut(200);
                    window.processing = false;
                }, 600);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            setTimeout(function() {
                $('#exception-list-loading').fadeOut(200);
                window.processing = false;
            }, 600);
            internetError();
        }
    });
}

function vehicleStatusUpdate(id, status) {

    if(status == 2) {
        var confirmBoxTitle = 'Release Vehicle';
        var confirmBoxMsg = 'Do you really want to <strong>Release</strong> this Vehicle ?'
    }
    else if(status == 1) {
        var confirmBoxTitle = 'Return Vehicle';
        var confirmBoxMsg = 'Did you actually <strong>Receive</strong> this vehicle?'
    }
    confirmBox(confirmBoxTitle, confirmBoxMsg);
    $('#table-row-'+id).fadeTo(800, 0.4);
    window.cBoxConfirmed = function yourFunctionName() {
        if(status == 2) {
            $('#booking-status-'+id).removeClass('label-info').addClass('label-success').text('Returned Vehicle');
        }
        else if(status == 1) {
            $('#booking-status-'+id).removeClass('label-success').addClass('label-default').text('Done');
        }
        jQuery.ajax({
            type: 'GET',
            url: window.url + '/internal/vehicle/change-booking-status',
            data: {id: id, status:status},
            success: function (data) {
                if (data != 0) {
                    setTimeout(function () {
                        $('#table-row-'+id).fadeTo(300, 0).remove();
                    }, 900);

                }
                else {
                    dbError();
                    if(status == 2) {
                        $('#booking-status-'+id).removeClass('label-success').addClass('label-info');
                    }
                    else if(status == 1) {
                        $('#booking-status-'+id).removeClass('label-default').addClass('label-success');
                    }
                    $('#table-row-'+id).fadeTo(300, 1);
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
                if(status == 2) {
                    $('#booking-status-'+id).removeClass('label-success').addClass('label-info');
                }
                else if(status == 1) {
                    $('#booking-status-'+id).removeClass('label-default').addClass('label-success');
                }
                $('#table-row-'+id).fadeTo(300, 1);
            }
        });
    }
    window.cBoxNotConfirmed = function yourFunctionName() {
        $('#table-row-'+id).fadeTo(500, 1);
    }



}
//endregion

//region Calender Tab Functions
function runCalendar() {
    var $modal = $('#event-modal');
    $('#draggable-events div.draggable-event').each(function () {
        var eventObject = {
            title: $.trim($(this).text())
        };

        $(this).data('eventObject', eventObject);

        $(this).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 20
        });
    });
    /*  Initialize the calendar  */
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var form = '';

    //get booking data
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/all-booking-list',
        success: function (data) {
            if (data != 0) {
                var bookings = JSON.parse(data);

                var calendar = $('#calendar').fullCalendar({
                    slotDuration: '01:00:00', /* If we want to split day time each 1 hour */
                    minTime: '08:00:00',
                    maxTime: '19:00:00',
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    events: bookings,
                    editable: false,
                    droppable: false,
                    drop: function (date, allDay) {

                        var originalEventObject = $(this).data('eventObject');
                        var $categoryClass = $(this).attr('data-class');

                        var copiedEventObject = $.extend({}, originalEventObject);

                        copiedEventObject.start = date;
                        copiedEventObject.allDay = allDay;
                        if ($categoryClass)
                            copiedEventObject['className'] = [$categoryClass];

                        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                        if ($('#drop-remove').is(':checked')) {

                            $(this).remove();
                        }
                    },

                    selectable: true,
                    eventClick: function (calEvent, jsEvent, view) {
                        //Event click function codes here
                    },
                    select: function (start, end, allDay) {
                        $modal.modal({
                            backdrop: 'static'
                        });
                        selectDate(start, end);
                    }
                });
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function selectDate(start, end) {
    //Init the datetime picker
    $('#booking-to').bootstrapMaterialDatePicker
    ({
        weekStart: 0, format: 'DD/MM/YYYY HH:mm'
    });
    $('#booking-from').bootstrapMaterialDatePicker
    ({
        weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : true
    }).on('change', function(e, date)
    {
        $('#booking-to').bootstrapMaterialDatePicker('setMinDate', date);
    });
    $('#min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', minDate : new Date() });
    // $.material.init();
    //end init time picker

    var $modal = $('#event-modal');

    $modal.find('.delete-booking').hide().end()
        .find('.save-booking').hide().end()
        .find('.go-back').hide().end()
        .find('.close-booking').hide().end()
        .find('.lbl-wait').hide().end()
        .find('.show-available-vehicles').show().end()
        .find('.loading-img').hide().end()
        .find('.not-available-msg').hide().end()
        .find('.price-label').hide().end()
        .find('.plate-list').hide().end()
        .find('.customer-booking-details').hide().end()
        .find('.b-from').val((moment(end).format('DD/MM/YYYY')+ " 08:00")).end()
        .find('.b-to').val((moment(start).format('DD/MM/YYYY')+ " 08:00")).end();
}
//endregion

//region Booking Modal Functions
function checkAvailability () {
    var $modal = $('#event-modal');
    var from = $.trim($('#booking-from').val());
    var to = $.trim($('#booking-to').val());

    if(from == '' || to == '') {
        $('#booking-from').trigger('blur');
        $('#booking-to').trigger('blur');
    }
    else {
        $modal.find('.loading-img').fadeIn(200);
        $modal.find('.show-available-vehicles').hide();
        $modal.find('.lbl-wait').fadeIn(200);
        $modal.find('.go-back').fadeIn(200);

        //get available vehicle list
        jQuery.ajax({
            type: 'GET',
            url: window.url + '/internal/vehicle/get-available-vehicles',
            data: {from: from, to:to},
            success: function (data) {
                if (data != 0) {
                    var res = JSON.parse(data);
                    var selectOptions = "<option selected>Select a vehicle</option>";

                    if(res == "" || res == null) {
                        $modal.find('.img-loading').fadeOut(500);
                        $modal.find('.lbl-wait').fadeOut(500, function () {
                            $modal.find('.not-available-msg').fadeIn(200);
                            setTimeout(function () {
                                $modal.find('.show-available-vehicles').show();
                                $modal.find('.not-available-msg').fadeOut(200);
                            }, 2000);
                        });
                    }
                    else {
                        for(var i = 0; i < res.length; i++) {
                            selectOptions += "<option value='"+res[i].id+"'>"+res[i].plate+"</option>";
                        }
                        $('#mm-v-b-plate').html(selectOptions);
                        $modal.find('.loading-img').fadeOut(200, function() {
                            $modal.find('.lbl-wait').fadeOut(200);
                            $modal.find('.plate-list').fadeIn(300);
                            $modal.find('.close-booking').fadeIn(200);
                        });

                    }


                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
}

function getBookingPrice() {
    var $modal = $('#event-modal');
    var from = $.trim($modal.find('.b-from').val());
    var to = $.trim($modal.find('.b-to').val());
    var vehicleId = $.trim($modal.find('.plate').val());

    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/get-booking-price',
        data: {from: from, to:to, vehicle_id:vehicleId},
        success: function (data) {
            if (data != 0) {
                var res = JSON.parse(data);
                var priceDisplay = '$'+res.rate.toFixed(2)+' X '+res.days+' days = $'+res.price.toFixed(2);
                $modal.find('.booking-price').html(priceDisplay).end()
                    .find('.price-label').fadeIn(200).end()
                    .find('.save-booking').fadeIn(200).end()
                    .find('.customer-booking-details').fadeIn(200);

            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function addBooking() {
    var $modal = $('#event-modal');

    $modal.find('.loading-img').fadeIn(200);

    var from = $.trim($modal.find('.b-from').val());
    var to = $.trim($modal.find('.b-to').val());
    var vehicleId = $.trim($modal.find('.plate').val());
    var fname = $.trim($modal.find('.fname').val());
    var lname = $.trim($modal.find('.lname').val());
    var email = $.trim($modal.find('.email').val());
    var telephone = $.trim($modal.find('.telephone').val());

    if(fname == "" || lname == "" || email == "" || telephone == "") {
        $modal.find('.fname').trigger('blur');
        $modal.find('.lname').trigger('blur');
        $modal.find('.email').trigger('blur');
        $modal.find('.telephone').trigger('blur');
    }
    else {
        jQuery.ajax({
            type: 'GET',
            url: window.url + '/internal/vehicle/save-vehicle-booking',
            data: {from: from, to:to, vehicle_id:vehicleId, fname:fname, lname:lname, email:email, telephone:telephone},
            success: function (data) {
                if (data != 0) {
                    $('#calendar').fullCalendar( 'destroy');
                    runCalendar();
                    setTimeout(function () {
                        $modal.find('.loading-img').fadeIn(300, function () {
                            $modal.find('.close-booking').trigger('click');
                        });
                        notification('success', 'Success', 'Booking was added to the system', 2000);
                    }, 200);
                    getBookingList(window.bookingType, 0);
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
}

function showBookingModal() {
    var $modal = $('#event-modal');

    $modal.modal({
        backdrop: 'static'
    });

    var start = (moment(new Date()).format('MM/DD/YYYY')+ " 08:00");
    var end = (moment(new Date()).format('MM/DD/YYYY')+ " 08:00");

    selectDate(start, end);

}

function goBackBooking() {
    var $modal = $('#event-modal');

    $modal.modal({
        backdrop: 'static'
    });

    var start = $modal.find('.b-from').val();
    var end = $modal.find('.b-to').val();

    selectDate(start, end);
}
//endregion

//Slider Cell Initiating
//----------------------------------------------------------------------------------------------------------------------
function sliderCellsVariables() {
    var d = new Date();
    window.sliderUpDay = parseInt((moment(d).format("DD")));
    window.sliderUpMonth = parseInt((moment(d).format("MM")));
    var startDate = moment(d.setDate(d.getDate()-20)).format("YYYY/MM/DD");
    window.iStartMonth = parseInt(moment(startDate).format("MM"));
    window.startDay = parseInt((moment(startDate).format("DD")));
}

function initiatingSliderCells() {
    window.processing = true;

    //Initiate public variables
    window.month_short = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    window.month_days = ['', 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    //end initiate public variables

    //Start the loading animation
    var el = $('#manage-booking-widget-reload').parents(".widget:first"); //setup widget loading icon until initiating
    blockUI(el);
    //Loading animation started

    //Request required data from the backend
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/get-booking-slider-initiating-data',
        data: {from_day:window.startDay, from_month:window.iStartMonth},
        success: function (data) {
            if (data != 0) {
                var res = JSON.parse(data); //convert response data set

                //generate the most left column
                var plates = "";
                for(var i = 0; i < res[0].plate.length; i++) {
                    plates += '<tr><td style="border-right: 5px solid rgba(0, 0, 0, 0.26);">'+
                        res[0].plate[i]+'</td></tr>';
                }
                $('#slider-table-freeze-column-plate-list').html(plates);
                //end most left column

                //generates the internal cells
                var thead = $('#slider-table-thead-list').html(); // thead contains the table header data (dates and months)
                if(thead.length > 0) {
                    thead.substring(0, thead.length - 5)
                }
                else {
                    thead = '<th>';
                }
                var tbody = "";
                var loopMonth = window.iStartMonth;


                //loop for generate number of rows for number of vehicles
                for(var k = 0; k < res[0].plate.length; k++) { //k=0
                    if(window.initiating == true) {
                        tbody += '<tr id="cell-row-'+k+'">';
                    }
                    else {
                        tbody = "";
                    }

                    var loopStartDay = window.startDay;

                    //loop for generate number of months in forward
                    for(var i = 1; i < 5; i++) { //i=0
                        var monthDays = window.month_days[loopMonth]; //monthDays =
                        //loop for generate number of days in each month
                        for(var j = loopStartDay; j <= monthDays; j++) {
                            //generate top date cells in table header
                            if(k == 0) {
                                //create the table header with dates
                                thead += '<th class="right-border-1" style="width: 200px !important; '+
                                    (loopMonth == window.sliderUpMonth && j == window.sliderUpDay ?
                                        'border-right: 1px solid #a0351f !important;' : '')+
                                    (loopMonth == window.sliderUpMonth && j == window.sliderUpDay-1 ?
                                        'border-right: 1px solid #a0351f !important;' : '')+'">'+j+
                                    '&nbsp;'+month_short[loopMonth]+'</th>';
                            }
                            var day = j;
                            //creates the number of slider cells
                            tbody += '<td id="'+res[0].plate[k]+'-'+day+'-'+loopMonth+
                                '" class="right-border-1" data-date="" style=" '+
                                (loopMonth == window.sliderUpMonth && j == window.sliderUpDay ?
                                    'border-right: 1px solid #a0351f !important;' : '')+
                                (loopMonth == window.sliderUpMonth && j == window.sliderUpDay-1 ?
                                    'border-right: 1px solid #a0351f !important;' : '')+'"></td>';
                        }
                        loopStartDay = 1;
                        loopMonth ++;
                        if(loopMonth > 12) {
                            loopMonth = 1;
                        }
                    }
                    if(window.initiating == false) {
                        $('#cell-row-'+k).append(tbody);
                    }
                    loopMonth = window.iStartMonth;
                }

                thead = thead+'</tr>';
                $('#slider-table-thead-list').html(thead);
                if(window.initiating == true) {
                    $('#slider-table-tbody-cells').html(tbody);
                }

                if(window.scrollRequired == true)
                    $("#slider-cell-container").animate( { scrollLeft: '+=1210' }, 500);
                //end top date columns and data cells

                //adding booking to the slider
                for(var i = 1; i < res.length; i++) {
                    var d = new Date(res[i].from);
                    var bookingDay = parseInt((moment(d).format("DD")));
                    var bookingMonth = parseInt((moment(d).format("MM")));
                    var bookingLabel = '<p class="slider-label" style="text-align: center; margin-bottom: -5px; ' +
                        'margin-top: -10px; overflow: hidden; height: 33px !important; width: 100%;" ' +
                        'data-toggle="tooltip" title="'+res[i].time+'"><strong ' +
                        'onmouseenter="zoomInSliderBlock('+res[i].days+', this);" ' +
                        'onmouseleave="zoomOutSliderBlock(2, this);">VEH'+res[i].booking_id+'</strong> - '+
                        res[i].name+'<br/>Tel: '+res[i].telephone+'</p><i onclick="bookingStatusChange('+res[i].id+', '+res[i].status+', this);" class="label '+
                        (res[i].status == 3 ? 'slider-cell-info-label label-info">Release' : res[i].status == 2 ?
                            'slider-cell-success-label label-success">Return' : res[i].status == 1 ?
                            'slider-cell-default-label label-default" style="cursor: default !important;" >Done' :
                            '')+'</i> ';
                    $('#'+res[i].plate+'-'+bookingDay+'-'+bookingMonth).attr('style', 'background-color: '+
                        res[i].color+'; border: 2px solid '+(res[i].status == 3 ? '#65BBD6' : res[i].status == 2 ?
                            '#68C39F' : '#777')+' !important; cursor: pointer; position: relative;').attr('colspan',
                        res[i].days).append(bookingLabel);
                    //remove the extra table cells after column spans generated for bookings
                    for(var j = 0; j < res[i].days-1; j++) {
                        var removeDay = bookingDay+(j+1);
                        $('#'+res[i].plate+'-'+removeDay+'-'+bookingMonth).remove();
                    }
                }
                unblockUI(el);//end loading animation
                window.processing = false;
            }
            else {
                dbError();
                window.processing = false;
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
            window.processing = false;
        }
    });
}
//End slider cell initiation
//----------------------------------------------------------------------------------------------------------------------

//Zoom out selected cell
function zoomInSliderBlock(spans, object) {
    var id = $(object).closest("td").attr("id");
    if(spans < 4) {
        $('#'+id).animate({
            height: "100px"
        }, {
            queue: false,
            duration: 300
        });
        $('#'+id).find('.slider-label').animate({
            height: "100px"
        }, {
            queue: false,
            duration: 300
        });
    }
}

function zoomOutSliderBlock(spans, object) {
    var id = $(object).closest("td").attr("id");

    if(spans < 4) {
        var height = 37;
        $('#'+id).animate({
            height: height+"px"
        }, {
            queue: false,
            duration: 300
        });
        $('#'+id).find('.slider-label').animate({
            height: (height-4)+"px"
        }, {
            queue: false,
            duration: 300
        });

    }
}

//slider cell booking status change function
function bookingStatusChange(id, status, object) {
    status = status - 1;

    var elementId = $(object).closest("td").attr("id");

    if(status == 2) {
        var confirmBoxTitle = 'Release Vehicle';
        var confirmBoxMsg = 'Do you really want to <strong>Release</strong> this Vehicle ?'
    }
    else if(status == 1) {
        var confirmBoxTitle = 'Return Vehicle';
        var confirmBoxMsg = 'Did you actually <strong>Receive</strong> this vehicle?'
    }
    confirmBox(confirmBoxTitle, confirmBoxMsg);
    $('#'+elementId).attr('class', 'right-border-1 border-blink-info');
    window.cBoxConfirmed = function yourFunctionName() {
        jQuery.ajax({
            type: 'GET',
            url: window.url + '/internal/vehicle/change-booking-status',
            data: {id: id, status:status},
            success: function (data) {
                if (data != 0) {
                    $("#slider-cell-container").animate( { scrollLeft: '-=1210' }, 5);
                    window.initiating = true;
                    window.scrollRequired = false;
                    initiatingSliderCells();
                }
                else {
                    dbError();
                    $('#'+elementId).attr('class', 'right-border-1');
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
                $('#'+elementId).attr('class', 'right-border-1');
            }
        });
    }
    window.cBoxNotConfirmed = function yourFunctionName() {
        $('#'+elementId).attr('class', 'right-border-1');
    }
}