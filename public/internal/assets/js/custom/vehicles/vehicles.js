/**
 * Created by Isuru on 7/09/16.
 */
"use strict";

//Init global variables
window.selectedVehicleId = $('#selected-vehicle-id').val();

//Default Settings
$('#mm-v-m-loading-img').hide();



//Events
getVehicleDetails(window.selectedVehicleId);
runCalendar(window.selectedVehicleId);

//Functions
function selectVehicle(vehicleId) {
    window.selectedVehicleId = vehicleId;
    $('#calendar').fullCalendar( 'destroy');

    $(".search-filter tr").each(function () {
        $(this).removeClass('row-highlight');
    });
    $('#vehicle-' + vehicleId).addClass('row-highlight');

    runCalendar(vehicleId);
    getVehicleDetails(vehicleId);
}


function runCalendar(vehicleId) {
    var $modal = $('#event-modal');
    $('#draggable-events div.draggable-event').each(function () {
        var eventObject = {
            title: $.trim($(this).text())
        };

        $(this).data('eventObject', eventObject);

        $(this).draggable({
            zIndex: 999,
            revert: true,
            revertDuration: 20
        });
    });
    /*  Initialize the calendar  */
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var form = '';

    //get booking data
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/single-vehicle-booking-list',
        data: {vehicle_id: vehicleId},
        success: function (data) {
            if (data != 0) {
                var bookings = JSON.parse(data);

                var calendar = $('#calendar').fullCalendar({
                    slotDuration: '01:00:00', /* If we want to split day time each 1 hour */
                    minTime: '08:00:00',
                    maxTime: '19:00:00',
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    events: bookings,
                    editable: false,
                    droppable: true,
                    drop: function (date, allDay) {

                        var originalEventObject = $(this).data('eventObject');
                        var $categoryClass = $(this).attr('data-class');

                        var copiedEventObject = $.extend({}, originalEventObject);

                        copiedEventObject.start = date;
                        copiedEventObject.allDay = allDay;
                        if ($categoryClass)
                            copiedEventObject['className'] = [$categoryClass];

                        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                        if ($('#drop-remove').is(':checked')) {

                            $(this).remove();
                        }
                    },

                    selectable: true,
                    eventClick: function (calEvent, jsEvent, view) {
                        var form = $("<form></form>");
                        form.append("<label>Change event name</label>");
                        form.append("<div class='input-group'><input class='form-control' type=text value='" + calEvent.title + "' /><span class='input-group-btn'><button type='submit' class='btn btn-success'><i class='fa fa-check'></i> Save Changes</button></span></div>");
                        $modal.modal({
                            backdrop: 'static'
                        });
                        $modal.find('.delete-event').show().end().find('.save-event').hide().end().find('.modal-body').empty().prepend(form).end().find('.delete-event').unbind('click').click(function () {
                            calendar.fullCalendar('removeEvents', function (ev) {
                                return (ev._id == calEvent._id);
                            });
                            $modal.modal('hide');
                        });
                        $modal.find('form').on('submit', function () {
                            calEvent.title = form.find("input[type=text]").val();
                            calendar.fullCalendar('updateEvent', calEvent);
                            $modal.modal('hide');
                            return false;
                        });
                    },
                    select: function (start, end, allDay) {
                        $modal.modal({
                            backdrop: 'static'
                        });
                        form = $("<form></form>");
                        form.append("<div class='row'></div>");
                        form.find(".row").append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Event Name</label><input class='form-control' placeholder='Insert Event Name' type='text' name='title'/></div></div>").append("<div class='col-md-6'><div class='form-group'><label class='control-label'>Category</label><select class='form-control' name='category'></select></div></div>").find("select[name='category']").append("<option value='bg-red'>Work</option>")
                            .append("<option value='bg-green-1'>Entertainment</option>").append("<option value='bg-pink-1'>Meeting</option>").append("<option value='bg-lightblue-1'>Lunch</option>").append("<option value='bg-red-1'>Work</option>").append("<option value='bg-blue-1'>Sport</option>");
                        $modal.find('.delete-event').hide().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.save-event').unbind('click').click(function () {
                            form.submit();
                        });
                        $modal.find('form').on('submit', function () {
                            //title = form.find("input[name='title']").val();
                            //$categoryClass = form.find("select[name='category'] option:checked").val();
                            if (title !== null && title.length != 0) {
                                calendar.fullCalendar('renderEvent', {
                                    title: title,
                                    start: start,
                                    end: end,
                                    allDay: false,
                                    className: $categoryClass
                                }, true);
                            }
                            else {
                                alert('You need a title for the event!');
                            }
                            $modal.modal('hide');
                            return false;
                        });
                    }
                });
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function getVehicleDetails(vehicleId) {
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/single-vehicle-details',
        data: {vehicle_id: vehicleId},
        success: function (data) {
            if (data != 0) {
                var res = JSON.parse(data);
                $('#vehicle-description-header').html(res.plate);
                $('#vehicle-plate').html(res.plate);
                $('#vehicle-modal').html(res.modal);
                $('#vehicle-year').html(res.year);
                $('#vehicle-rate').html(res.rate);
                $('#vehicle-description').html(res.description);

                if(res.status == 1) {
                    $('#is-disable-enable-vehicle').addClass('on');
                }
                else {
                    $('#is-disable-enable-vehicle').removeClass('on');
                }
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function addVehicle() {
    var plate = $.trim($('#mm-v-m-plate').val());
    var modal = $.trim($('#mm-v-m-modal').val());
    var year = $.trim($('#mm-v-m-year').val());
    var rate = $.trim($('#mm-v-m-rate').val());
    var color = $.trim($('#mm-v-m-color').val());
    var description = $.trim($('#mm-v-m-description').val());

    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/add-new-vehicle',
        data:{plate:plate, modal:modal, year:year, rate:rate, color:color, description:description},
        success: function (data) {
            if (data == 1) {
                notification('success', 'Success!', plate+' was successfully added.', 3000);
                $('#mm-v-m-new-vehicle-cancel').trigger('click');
                $('#mm-v-m-plate').val('');
                $('#mm-v-m-modal').val('');
                $('#mm-v-m-year').val('');
                $('#mm-v-m-rate').val('');
                $('#mm-v-m-description').val('');
                location.reload();
            }
            else if(data == -1) {
                notification('warning', 'Deny Operation', 'Sorry... You are not allowed to preform this operation')
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function addVehicleConfig() {
    $('#mm-v-m-plate').val("").removeAttr("disabled");;
    $('#mm-v-m-modal').val("");
    $('#mm-v-m-year').val("");
    $('#mm-v-m-rate').val("");
    $('#mm-v-m-description').val("");
    $('#mm-v-m-new-vehicle-add').show();
    $('#mm-v-m-update-vehicle').hide();
}

function updateVehicleInfo() {
    $('#mm-v-m-plate').val($('#vehicle-plate').html()).attr("disabled", "disabled");
    $('#mm-v-m-modal').val($('#vehicle-modal').html());
    $('#mm-v-m-year').val($('#vehicle-year').html());
    $('#mm-v-m-rate').val($('#vehicle-rate').html());
    $('#mm-v-m-description').val($('#vehicle-description').html());
    $('#mm-v-m-new-vehicle-add').hide();
    $('#mm-v-m-update-vehicle').show();
}

function updateVehicle() {
    var plate = $.trim($('#mm-v-m-plate').val());
    var modal = $.trim($('#mm-v-m-modal').val());
    var year = $.trim($('#mm-v-m-year').val());
    var rate = $.trim($('#mm-v-m-rate').val());
    var color = $.trim($('#mm-v-m-color').val());
    var description = $.trim($('#mm-v-m-description').val());

    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/edit-vehicle-info',
        data:{plate:plate, modal:modal, year:year, rate:rate, color:color, description:description},
        success: function (data) {
            if (data == 1) {
                notification('success', 'Success!', plate+' was successfully added.', 3000);
                $('#mm-v-m-new-vehicle-cancel').trigger('click');
                $('#mm-v-m-plate').val('');
                $('#mm-v-m-modal').val('');
                $('#mm-v-m-year').val('');
                $('#mm-v-m-rate').val('');
                $('#mm-v-m-description').val('');
                location.reload();
            }
            else if(data == -1) {
                notification('warning', 'Deny Operation', 'Sorry... You are not allowed to preform this operation')
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function iSwitchOnChange(id, status) {
    if(status == 1) {
        var status = 1;
        $('#vehicle-'+window.selectedVehicleId).fadeTo(200, 1);
    }
    else {
        var status = 2;
        $('#vehicle-'+window.selectedVehicleId).fadeTo(200, 0.3);
    }
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/internal/vehicle/enable-disable-vehicle',
        data:{vehicle_id: window.selectedVehicleId,status:status},
        success: function (data) {
            if (data == 1) {

            }
            else if(data == -1) {
                notification('warning', 'Deny Operation', 'Sorry... You are not allowed to preform this operation')
            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}