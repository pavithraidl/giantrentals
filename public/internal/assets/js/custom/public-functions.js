/**
 * Created by Isuru on 5/09/16.
 */
"use strict";

// Search Functions
//----------------------------------------------------------------------------------------------------------------------
function searchFilter(object){
    var filter = $("#"+object.id).val(), count = 0;
    var elements = 0;
    $(".search-filter li").each(function(){
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut(200);
        } else {
            $(this).show();
            count++;
        }
        if($(this).is(':visible')) {
            elements++;
        }
    });

    if(elements == 0) {
        window.searchListEmpty();
    }
    var numberItems = count;
    $("#filter-count").text("Number of Comments = "+count);
}

function searchAll(object, element) {
    var filter = $("#"+object.id).val(), count = 0;
    var elements = 0;
    $(".search-filter "+element).each(function(){
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut(200);
        } else {
            $(this).show();
            count++;
        }
        if($(this).is(':visible')) {
            elements++;
        }
    });

    if(elements == 0) {
        window.searchListEmpty();
    }
    var numberItems = count;
    $("#filter-count").text("Number of Comments = "+count);
}

function tableSearch(object){
    var filter = $("#"+object.id).val(), count = 0;
    $(".search-filter tr").each(function(){
        if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).fadeOut(200);
        } else {
            $(this).show();
            count++;
        }
    });
    var numberItems = count;
    $("#filter-count").text("Number of Comments = "+count);
}

function formatCurrency(x) {
    var x = parseFloat(x).toFixed(2);
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&nbsp;");
}

// Internal Notifications
//---------------------------------------------------------------------------------------------------------------------
// Notification functions
function notification(type, header, msg, time) {
    var icon = '';
    if(type == 'success') {
        icon = '<i class="fa fa-check" style="font-size: 30px; margin-top: 20px;"></i>';
    }
    else if(type == 'error') {
        icon = '<i class="fa fa-times" style="font-size: 30px; margin-top: 20px;"></i>';
    }
    else if(type == 'warning') {
        icon = '<i class="fa fa-bolt" style="font-size: 30px; margin-top: 20px;"></i>';
    }
    else {
        icon = '<i class="fa fa-exclamation" style="font-size: 30px; margin-top: 20px;"></i>';
    }

    var htmlContainer = $('   <div id="master-notification" class="notify notify-'+type+'">'+(time > 0 ? '':
                                    '<i class="fa fa-times" onclick="hideNotification();" ' +
                                    'style="position: absolute; top: 5px; right: 5px; cursor: pointer;"></i>')+
        '                       <div class="notify-icon">' + icon +
        '                       </div>' +
        '                       <div class="notify-body">' +
        '                           <div id="notify-header" style="font-weight: 700; font-size: 17px;">' +
        '                               '+header+' ! ' +
        '                           </div> ' +
        '                           <div id="notify-msg">' +
        '                               '+msg+'! ' +
        '                           </div> ' +
        '                       </div> ' +
        '                   </div>').hide();

    $("#notification-container").html(htmlContainer);
    htmlContainer.show("slide", { direction: "right" }, 300);

    if(time > 0) {
        setTimeout(function() {
            htmlContainer.hide("slide", { direction: "right" }, 500);
        }, time);
    }
    else {

    }
}

function hideNotification() {
    $('#master-notification').hide("slide", { direction: "right" }, 500);
}

function dbError() {
    notification('error', 'Sorry!', 'Error, Cannot connect with the database. Try again later', 8000);
}

function internetError() {
    notification('warning', 'No Connection', 'You are not connected. Please check your internet connection', 0);
}


// Confirmation Function
//----------------------------------------------------------------------------------------------------------------------
function confirmBox(header, msg) {

    var htmlContainer = $('   <div id="master-confirmation" class="confirmation">'+
        '                       <div class="confirmation-body">' +
        '                           <div id="notify-header" style="font-weight: 700; font-size: 17px;">' +
        '                               '+header+
        '                           </div> ' +
        '                           <div id="notify-msg">' +
        '                               '+msg+
        '                           </div> ' +
        '                       </div> ' +
        '                       <p style="text-align: center;">' +
        '                           <span class="btn btn-info btn-sm" onclick="confirmed();" style="position: absolute; bottom: 5px; left: 130px;">Yes</span> ' +
        '                           <span class="btn btn-danger btn-sm" onclick="hideConfirmation();" style="position: absolute; bottom: 5px;">No</span> ' +
        '                       </p>' +
        '                   </div>').hide();

    $("#notification-container").html(htmlContainer);
    htmlContainer.show("slide", { direction: "right" }, 300);
}

function hideConfirmation() {
    window.cBoxNotConfirmed();
    $('#master-confirmation').hide("slide", { direction: "right" }, 500);
}

function confirmed() {
    window.cBoxConfirmed();
    $('#master-confirmation').fadeOut(200);
}