/**
 * Created by Isuru on 16/09/2016 AD.
 */
"use strict";

//Init global variables
window.moreIntrusions = true;
window.paginate = 1;
window.processing = false;

//Default settings



//Init functions
getIntrusionList(window.paginate);


//Events
$(window).scroll(function () {
    var elementHeight = $('#intrusion-table-data').height();
    var scrollPosition = $('body').scrollTop() + 416;
    if (elementHeight < scrollPosition + 200) {
        if (window.moreIntrusions == true && window.processing == false) {
            getIntrusionList(window.paginate);
        }
    }
});


//Functions
function getIntrusionList(paginate) {
    window.processing = true;
    $('#intrusion-list-loading').fadeIn(200);

    jQuery.ajax({
        type: 'POST',
        url: window.url + '/internal-get-system-intrusion-list',
        data:{paginate:paginate},
        success: function (data) {
            if (data != 0) {
                var res = JSON.parse(data);
                var tableData = "";
                var rows = 0;

                if(res != null) {
                    rows = res.length;
                }
                else {
                    rows = 0;
                }

                if(rows > 9) {
                    window.paginate++;
                }
                else if(rows > 0){
                    window.moreIntrusions == false;
                }
                else {
                    window.moreIntrusions == false;
                    setTimeout(function() {
                        $('#intrusion-list-loading').fadeOut(200);
                        window.processing = false;
                    }, 600);
                }

                for(var i = 0; i < rows; i++) {
                    tableData += ('  <tr>' +
                    '           <td>'+res[i].id+'</td>' +
                    '           <td><strong>'+res[i].controller+'</strong></td>' +
                    '           <td>'+res[i].func+'</td>' +
                    '           <td>'+res[i].user+'</td>' +
                    '           <td>'+res[i].exe_msg+'</td>' +
                    '           <td>'+(res[i].status == 2 ? '<span id="intrusion-status-'+res[i].id+'" class="label label-danger" style="cursor: pointer;" onclick="changeStatus('+res[i].id+');">Active</span>' : '<span id="intrusion-status-'+res[i].id+'" class="label label-success">Ignored</span>')+'</td> ' +
                    '           <td>'+res[i].created_at+'</td>' +
                    '       </tr>');
                }
                $('#intrusion-table-data').append(tableData);

                setTimeout(function() {
                    $('#intrusion-list-loading').fadeOut(200);
                    window.processing = false;
                }, 600);
            }
            else {
                dbError();
                setTimeout(function() {
                    $('#intrusion-list-loading').fadeOut(200);
                    window.processing = false;
                }, 600);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            setTimeout(function() {
                $('#intrusion-list-loading').fadeOut(200);
                window.processing = false;
            }, 600);
            internetError();
        }
    });
}

function changeStatus(id) {
    $('#intrusion-status-'+id).text('Ignored');
    $('#intrusion-status-'+id).removeClass('label-danger').addClass('label-success');
    jQuery.ajax({
        type: 'POST',
        url: window.url + '/internal-update-system-intrusion-status',
        data: {intrusion_id: id},
        success: function (data) {
            if (data != 0) {

            }
            else {
                $('#intrusion-status-'+id).removeClass('label-success').addClass('label-danger');
                $('#intrusion-status-'+id).text('Active');
                dbError();
            }

        },
        error: function (xhr, textStatus, errorThrown) {
            $('#intrusion-status-'+id).removeClass('label-success').addClass('label-danger');
            $('#intrusion-status-'+id).text('Active');
            internetError();
        }
    });
}