/**
 * Created by Isuru on 2/09/16.
 */
"use strict";

//Init global variables
window.moreExceptions = true;
window.paginate = 1;
window.processing = false;


//Default settings



//Init functions
getExceptionList(window.paginate);


//Events
$(window).scroll(function () {
    var elementHeight = $('#exception-table-data').height();
    var scrollPosition = $('body').scrollTop() + 616;
    if (elementHeight < scrollPosition + 200) {
        if (window.moreExceptions == true && window.processing == false) {
            getExceptionList(window.paginate);
        }
    }
});


//Functions
function getExceptionList(paginate) {
    window.processing = true;
    $('#exception-list-loading').fadeIn(200);

    jQuery.ajax({
        type: 'POST',
        url: window.url + '/internal-get-system-exception-list',
        data:{paginate:paginate},
        success: function (data) {
            if (data != 0) {
                var res = JSON.parse(data);
                var tableData = "";
                var rows = 0;

                if(res != null) {
                    rows = res.length;
                }
                else {
                    rows = 0;
                }

                if(rows > 9) {
                    window.paginate++;
                }
                else if(rows > 0){
                    window.moreExceptions == false;
                }
                else {
                    window.moreExceptions == false;
                    setTimeout(function() {
                        $('#exception-list-loading').fadeOut(200);
                        window.processing = false;
                    }, 600);
                }

                for(var i = 0; i < rows; i++) {
                    tableData += ('  <tr>' +
                            '           <td>'+res[i].id+'</td>' +
                            '           <td><strong>'+res[i].controller+'</strong></td>' +
                            '           <td>'+res[i].func+'</td>' +
                            '           <td>'+res[i].exe_msg+'</td>' +
                            '           <td>'+(res[i].status == 2 ? '<span id="exception-status-'+res[i].id+'" class="label label-danger" style="cursor: pointer;" onclick="changeStatus('+res[i].id+');">Active</span>' : '<span id="exception-status-'+res[i].id+'" class="label label-success">Solved</span>')+'</td> ' +
                            '           <td>'+res[i].created_at+'</td>' +
                            '       </tr>');
                }
                $('#exception-table-data').append(tableData);

                setTimeout(function() {
                    $('#exception-list-loading').fadeOut(200);
                    window.processing = false;
                }, 600);
            }
            else {
                dbError();
                setTimeout(function() {
                    $('#exception-list-loading').fadeOut(200);
                    window.processing = false;
                }, 600);
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            setTimeout(function() {
                $('#exception-list-loading').fadeOut(200);
                window.processing = false;
            }, 600);
            internetError();
        }
    });
}

function changeStatus(id) {
    $('#exception-status-'+id).text('Solved');
    $('#exception-status-'+id).removeClass('label-danger').addClass('label-success');
    jQuery.ajax({
        type: 'POST',
        url: window.url + '/internal-update-system-exception-status',
        data: {exception_id: id},
        success: function (data) {
            if (data != 0) {

            }
            else {
                $('#exception-status-'+id).removeClass('label-success').addClass('label-danger');
                $('#exception-status-'+id).text('Active');
                dbError();
            }

        },
        error: function (xhr, textStatus, errorThrown) {
            $('#exception-status-'+id).removeClass('label-success').addClass('label-danger');
            $('#exception-status-'+id).text('Active');
            internetError();
        }
    });
}