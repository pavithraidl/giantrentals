/**
 * Created by Isuru on 3/11/16.
 */
"use strict";

//Init global variables


//Default settings
$('#add-another-vehicle-container').html
    ('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/vehicle">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Vehicle</button>' +
    '</a>');

$('#add-another-scaffold-container').html
('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/scaffolding">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Scaffold</button>' +
    '</a>');



//Functions
function addGear() {
    var qty = $('#qty').val();
    var type = $('#drp-item-type').val();
    var pickupd = $('#pickup-date').val();
    var pickupt = $('#pickup-time').val();
    var returnd = $('#return-date').val();
    var returnt = $('#return-time').val();

    jQuery.ajax({
        type: 'GET',
        url: window.url + '/event/add-event',
        data: {pickupd: pickupd, pickupt:pickupt, returnd: returnd, returnt: returnt, type:type, qty:qty},
        success: function (data) {
            if(data == -1) {
                alert('There\'re no available stock for the given time frame');
            }
            else if (data != 0) {
                var res = JSON.parse(data);

                var searchResult = $('#event-result').html();
                if(searchResult == '') {
                    var innerHtml = ('<tr class="cart_item" id="event-booking-row-'+res.cart_id+'">' +
                    '                       <td class="cart-product-remove"> ' +
                    '                           <a href="javascript:void(0);" class="remove" onclick="removeEventBooking('+res.cart_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                    '                       </td>' +
                    '                       <td class="cart-product-name">' +
                    '                           <a href="#">'+res.name+'</a>' +
                    '                       </td>' +
                    '                       <td>'+res.from+'</td>' +
                    '                       <td>'+res.to+'</td>' +
                    '                       <td class="cart-product-price">' +
                    '                           <span class="amount">$'+res.rate+'</span>' +
                    '                       </td>' +
                    '                       <th style="text-align: center;">'+qty+'</th>' +
                    '                       <td class="cart-product-quantity">'+res.days+'</td>' +
                    '                       <td class="cart-product-subtotal">' +
                    '                           <span id="event-total-amount-'+res.cart_id+'" class="amount">$'+res.total+'</span>' +
                    '                       </td>' +
                    '                   </tr>');
                    window.eventSubTotal += res.total_int;
                    $("#event-result").html(innerHtml);
                    
                    setFinalParameters();
                    updateEventCart();
                }
                else {
                    var htmlAppend = ('<tr class="cart_item" id="event-booking-row-'+res.cart_id+'">' +
                    '                       <td class="cart-product-remove"> ' +
                    '                           <a href="javascript:void(0);" class="remove" onclick="removeEventBooking('+res.cart_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                    '                       </td>' +
                    '                       <td class="cart-product-name">' +
                    '                           <a href="#">'+res.name+'</a>' +
                    '                       </td>' +
                    '                       <td>'+res.from+'</td>' +
                    '                       <td>'+res.to+'</td>' +
                    '                       <td class="cart-product-price">' +
                    '                           <span class="amount">$'+res.rate+'</span>' +
                    '                       </td>' +
                    '                       <th style="text-align: center;">'+qty+'</th>' +
                    '                       <td class="cart-product-quantity">'+res.days+'</td>' +
                    '                       <td class="cart-product-subtotal">' +
                    '                           <span id="event-total-amount-'+res.cart_id+'" class="amount">$'+res.total+'</span>' +
                    '                       </td>' +
                    '                   </tr>');
                    window.eventSubTotal += res.total_int;
                    $("#event-result").append(htmlAppend);

                    setFinalParameters();
                    updateEventCart();
                }

            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function setFinalParameters() {
    window.eventBookingContuer++;
    changeFullTotals();
    $('#event-total-amount').html('$ '+window.eventSubTotal.toFixed(2));
    $('#event-inbox-total').html('$ '+window.eventSubTotal.toFixed(2));

    $("#event-booking-sub-total").fadeIn(200);
    updateCartCount(1);
    $('#event-booking-list-container').fadeIn(200);
    $('#booking-result').slideDown(300);

    $('html,body').animate({
            scrollTop: $("#booking-result").offset().top-100},
        1000);
}