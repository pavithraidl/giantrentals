/**
 * Created by Isuru on 3/11/16.
 */
"use strict";

//Init global variables


//Default settings
$('#add-another-gear-container').html
('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/event-gear">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Gear</button>' +
    '</a>');

$('#add-another-vehicle-container').html
('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/vehicle">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Vehicle</button>' +
    '</a>');


//Functions
function bookScaffold() {
    var qty = $('#scaffold-qty').val();
    var type = $('#scaffold-type-id').val();
    var pickupd = $('#scaffold-pickup-date').val();
    var pickupt = $('#scaffold-pickup-time').val();
    var returnd = $('#scaffold-return-date').val();
    var returnt = $('#scaffold-return-time').val();
    $('#booking-table-loading-img').fadeIn(200);
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/scaffold/add-scaffold',
        data: {pickupd: pickupd, pickupt:pickupt, returnd: returnd, returnt: returnt, type:type, qty:qty},
        success: function (data) {
            if(data == -1) {
                alert('There\'re no available stock for the given time frame');
            }
            else if (data != 0) {
                var res = JSON.parse(data);

                var searchResult = $('#scaffold-result').html();
                if(searchResult == '') {
                    var innerHtml = ('<tr class="cart_item" id="scaffold-booking-row-'+res.cart_id+'">' +
                    '                       <td class="cart-product-remove"> ' +
                    '                           <a href="javascript:void(0);" class="remove" onclick="removeScaffoldBooking('+res.cart_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                    '                       </td>' +
                    '                       <td class="cart-product-name">' +
                    '                           <a href="#">'+res.name+'</a>' +
                    '                       </td>' +
                    '                       <td>'+res.from+'</td>' +
                    '                       <td>'+res.to+'</td>' +
                    '                       <td class="cart-product-price">' +
                    '                           <span class="amount">$'+res.rate+'</span>' +
                    '                       </td>' +
                    '                       <th style="text-align: center;">'+qty+'</th>' +
                    '                       <td class="cart-product-quantity">'+res.days+'</td>' +
                    '                       <td class="cart-product-subtotal">' +
                    '                           <span id="scaffold-total-amount-'+res.cart_id+'" class="amount">$'+res.total+'</span>' +
                    '                       </td>' +
                    '                   </tr>');
                    window.scaffoldSubTotal += res.total_int;
                    $("#scaffold-result").html(innerHtml);

                    setFinalParameters();
                    updateScaffoldCart();
                }
                else {
                    var htmlAppend = ('<tr class="cart_item" id="scaffold-booking-row-'+res.cart_id+'">' +
                    '                       <td class="cart-product-remove"> ' +
                    '                           <a href="javascript:void(0);" class="remove" onclick="removeScaffoldBooking('+res.cart_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                    '                       </td>' +
                    '                       <td class="cart-product-name">' +
                    '                           <a href="#">'+res.name+'</a>' +
                    '                       </td>' +
                    '                       <td>'+res.from+'</td>' +
                    '                       <td>'+res.to+'</td>' +
                    '                       <td class="cart-product-price">' +
                    '                           <span class="amount">$'+res.rate+'</span>' +
                    '                       </td>' +
                    '                       <th style="text-align: center;">'+qty+'</th>' +
                    '                       <td class="cart-product-quantity">'+res.days+'</td>' +
                    '                       <td class="cart-product-subtotal">' +
                    '                           <span id="scaffold-total-amount-'+res.cart_id+'" class="amount">$'+res.total+'</span>' +
                    '                       </td>' +
                    '                   </tr>');
                    window.scaffoldSubTotal += res.total_int;
                    $("#scaffold-result").append(htmlAppend);

                    setFinalParameters();
                    updateScaffoldCart();
                }
            }
            else {
                dbError();
            }
            setTimeout(function () {
                $('#booking-table-loading-img').fadeIn(300);
            }, 500);
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function setFinalParameters() {

    window.scaffoldBookingCounter++;
    changeFullTotals();
    $('#scaffold-total-amount').html('$ '+window.scaffoldSubTotal.toFixed(2));
    $('#scaffold-inbox-total').html('$ '+window.scaffoldSubTotal.toFixed(2));

    $("#scaffold-booking-sub-total").fadeIn(200);
    updateCartCount(1);
    $('#scaffold-booking-list-container').fadeIn(200);
    $('#booking-result').slideDown(300);

    $('html,body').animate({
            scrollTop: $("#scaffold-booking-list-container").offset().top-100},
        1000);
}