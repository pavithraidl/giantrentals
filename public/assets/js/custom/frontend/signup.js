/**
 * Created by Isuru on 25/11/16.
 */
"use strict";

//Init variables


//default settings


//Events



//Functions
function firstNameValidator() {
    var idcheck = $('#lbl-first-name-error');
    var value = $.trim($('#txt-first-name').val());
    if(idcheck.length) {
        if(value == '') {

        }
        else {
            $('#txt-first-name').attr('style', '');
            $('#lbl-first-name-error').fadeOut(300, function () {
                $('#lbl-first-name-error').remove();
            });
        }
    }
    else {
        if(value == '') {
            $('#txt-first-name').attr('style', 'border-color:red;').after("<span id='lbl-first-name-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Please Enter Your First Name!</span>");
            $('#lbl-first-name-error').fadeIn(300);
        }
    }
}

function firstNameMinLengthValidator() {
    var idcheck = $('#lbl-first-name-length-error');
    var value = $.trim($('#txt-first-name').val());
    if(value != '') {
        if(idcheck.length) {
            if(value.length < 3) {

            }
            else {
                $('#txt-first-name').attr('style', '');
                $('#lbl-first-name-length-error').fadeOut(300, function () {
                    $('#lbl-first-name-length-error').remove();
                });
            }
        }
        else {
            if(value.length < 3) {
                $('#txt-first-name').attr('style', 'border-color:red;').after("<span id='lbl-first-name-length-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Minimum 3 characters!</span>");
                $('#lbl-first-name-length-error').fadeIn(300);
            }
        }
    }
    else {
        $('#lbl-first-name-length-error').fadeOut(300, function () {
            $('#lbl-first-name-length-error').remove();
        });
    }
}

function lastNameValidator() {
    var idcheck = $('#lbl-last-name-error');
    var value = $.trim($('#txt-last-name').val());
    if(idcheck.length) {
        if(value == '') {

        }
        else {
            $('#txt-last-name').attr('style', '');
            $('#lbl-last-name-error').fadeOut(300, function () {
                $('#lbl-last-name-error').remove();
            });
        }
    }
    else {
        if(value == '') {
            $('#txt-last-name').attr('style', 'border-color:red;').after("<span id='lbl-last-name-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Please Enter Your Last Name!</span>");
            $('#lbl-last-name-error').fadeIn(300);
        }
    }
}

function lastNameMinLengthValidator() {
    var idcheck = $('#lbl-last-name-length-error');
    var value = $.trim($('#txt-last-name').val());
    if(value != '') {
        if(idcheck.length) {
            if(value.length < 3) {

            }
            else {
                $('#txt-last-name').attr('style', '');
                $('#lbl-last-name-length-error').fadeOut(300, function () {
                    $('#lbl-last-name-length-error').remove();
                });
            }
        }
        else {
            if(value.length < 3) {
                $('#txt-last-name').attr('style', 'border-color:red;').after("<span id='lbl-last-name-length-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Minimum 3 characters!</span>");
                $('#lbl-last-name-length-error').fadeIn(300);
            }
        }
    }
    else {
        $('#lbl-last-name-length-error').fadeOut(300, function () {
            $('#lbl-last-name-length-error').remove();
        });
    }
}

function phoneValidator() {
    var idcheck = $('#lbl-phone-number-error');
    var value = $.trim($('#txt-phone-number').val());
    if(idcheck.length) {
        if(value == '') {

        }
        else {
            $('#txt-phone-number').attr('style', '');
            $('#lbl-phone-number-error').fadeOut(300, function () {
                $('#lbl-phone-number-error').remove();
            });
        }
    }
    else {
        if(value == '') {
            $('#txt-phone-number').attr('style', 'border-color:red;').after("<span id='lbl-phone-number-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Please Enter Your Phone Number!</span>");
            $('#lbl-phone-number-error').fadeIn(300);
        }
    }
}

function licenceValidator() {
    var idcheck = $('#lbl-licence-error');
    var value = $.trim($('#txt-licence').val());
    if(idcheck.length) {
        if(value == '') {

        }
        else {
            $('#txt-licence').attr('style', '');
            $('#lbl-licence-error').fadeOut(300, function () {
                $('#lbl-licence-error').remove();
            });
        }
    }
    else {
        if(value == '') {
            $('#txt-licence').attr('style', 'border-color:red;').after("<span id='lbl-licence-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Please Enter Licence Number!</span>");
            $('#lbl-licence-error').fadeIn(300);
        }
    }
}

function emailAddressValidator() {
    var idcheck = $('#lbl-email-error');
    var value = $.trim($('#txt-email').val());
    if(idcheck.length) {
        if(value == '') {

        }
        else {
            $('#txt-email').attr('style', '');
            $('#lbl-email-error').fadeOut(300, function () {
                $('#lbl-email-error').remove();
            });
        }
    }
    else {
        if(value == '') {
            $('#txt-email').attr('style', 'border-color:red;').after("<span id='lbl-email-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Please Enter Your Email!</span>");
            $('#lbl-email-error').fadeIn(300);
        }
    }
}

function passwordValidator() {
    var idcheck = $('#lbl-password-error');
    var value = $.trim($('#txt-password').val());
    if(idcheck.length) {
        if(value == '') {

        }
        else {
            $('#txt-password').attr('style', '');
            $('#lbl-password-error').fadeOut(300, function () {
                $('#lbl-password-error').remove();
            });
        }
    }
    else {
        if(value == '') {
            $('#txt-password').attr('style', 'border-color:red;').after("<span id='lbl-password-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Please Enter a Password!</span>");
            $('#lbl-password-error').fadeIn(300);
        }
    }
}

function confirmPasswordValidator() {
    $('.match-password').remove();
    var idcheck = $('#lbl-confirm-password-error');
    var value = $.trim($('#txt-confirm-password').val());
    if(idcheck.length) {
        if(value == '') {

        }
        else {
            $('#txt-confirm-password').attr('style', '');
            $('#lbl-confirm-password-error').fadeOut(300, function () {
                $('#lbl-confirm-password-error').remove();
            });
        }
    }
    else {
        if(value == '') {
            $('#txt-confirm-password').attr('style', 'border-color:red;').after("<span id='lbl-confirm-password-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Please Enter a Password Again for Confirm!</span>");
            $('#lbl-confirm-password-error').fadeIn(300);
        }
    }
}

function minLengthValidator() {
    var idcheck = $('#lbl-password-length-error');
    var value = $.trim($('#txt-password').val());
    if(value != '') {
        if(idcheck.length) {
            if(value.length < 3) {

            }
            else {
                $('#txt-password').attr('style', '');
                $('#lbl-password-length-error').fadeOut(300, function () {
                    $('#lbl-password-length-error').remove();
                });
            }
        }
        else {
            if(value.length < 3) {
                $('#txt-password').attr('style', 'border-color:red;').after("<span id='lbl-password-length-error' class='fa fa-remove' style='margin-top: 5px; display: block; color: red; display: none;'> Minimum 3 characters!</span>");
                $('#lbl-password-length-error').fadeIn(300);
            }
        }
    }
    else {
        $('#lbl-password-length-error').fadeOut(300, function () {
            $('#lbl-password-length-error').remove();
        });
    }
}

function matchPassword(object, topId) {
    $('#lbl-confirm-password-error').fadeOut(300, function () {
        $('#lbl-confirm-password-error').remove();
    });
    var value = $('#'+object.id).val();
    var matchValue = $('#'+topId).val();
    $('.match-password').remove();
    $('#' + object.id).attr('style', '');
    if(value != matchValue) {
        $('#' + object.id).attr('style', 'border-color:red;').after("<span class='match-password fa fa-remove' style='margin-top: 5px; display: block; color: red;'> Passwords are not matched!</span>");
    }
}

function emailValidator(object) {
    var email = $('#' + object.id).val();
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $('.email').remove();

    if (re.test(email)) {
        $('#' + object.id).attr('style', '');
        $('.email').remove();
    }
    else {
        $('#' + object.id).parent().addClass('has-error');
        $('#' + object.id).attr('style', 'border-color:red;').after("<span class='email fa fa-remove' style='margin-top: 5px;display: block; color: red;'> Invalid Email</span>");

    }
    if (email.trim() == '') {
        $('#' + object.id).parent().removeClass('has-error');
        $('.email').remove();
    }
}

function createAccount() {
    var fName = $.trim($('#txt-first-name').val());
    var lName = $.trim($('#txt-last-name').val());
    var phone = $.trim($('#txt-phone-number').val());
    var licence = $.trim($('#txt-licence').val());
    var email = $.trim($('#txt-email').val());
    var password = $.trim($('#txt-password').val());
    var confirmPass = $.trim($('#txt-confirm-password').val());
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(fName == '' || lName == '' || phone == '' || licence == '' || email == '' || password == '' || confirmPass == '' || !re.test(email)) {
        $('#txt-first-name').trigger('blur');
        $('#txt-last-name').trigger('blur');
        $('#txt-phone-number').trigger('blur');
        $('#txt-licence').trigger('blur');
        $('#txt-email').trigger('blur');
        $('#txt-password').trigger('blur');
        $('#txt-confirm-password').trigger('blur');
    }
    else if(password != confirmPass) {
        $('#txt-confirm-password').trigger('keyup');
    }
    else {
        jQuery.ajax({
            type: 'POST',
            url: window.url + '/post/sign-in',
            data: {fname: fName, lname:lName, phone: phone, licence: licence, email:email, password:password, roll: 1045},
            success: function (data) {
                if(data == 1) {
                    alert('wow.... you are done!')
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
}