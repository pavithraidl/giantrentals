/**
 * Created by Isuru on 16/10/16.
 */
"use strict";

//Init global variables

//Default settings
$('#add-another-gear-container').html
('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/event-gear">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Gear</button>' +
    '</a>');

$('#add-another-scaffold-container').html
('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/scaffolding">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Scaffold</button>' +
    '</a>');


//Functions
function findMyVehicle() {
    var pickupd = $('#pickup-date').val();
    var pickupt = $('#pickup-time').val();
    var returnd = $('#return-date').val();
    var returnt = $('#return-time').val();

    if(!$('#licence').is(':checked')) {
        $('#licence-number').shake(50, 5, 5);
    }
    else {
        $('#booking-table-loading-img').fadeIn(200);
        jQuery.ajax({
            type: 'GET',
            url: window.url + '/vehicle/find-my-vehicle',
            data: {pickupd: pickupd, pickupt:pickupt, returnd: returnd, returnt: returnt},
            success: function (data) {
                if(data == -1) {
                    alert('no available vehicles for the given time frame');
                }
                else if (data != 0) {
                    var res = JSON.parse(data);

                    var searchResult = $('#vehicle-result').html();
                    if(searchResult == '') {
                        var innerHtml = ('<tr class="cart_item" id="vehicle-booking-row-'+res.booking_id+'">' +
                        '                       <td class="cart-product-remove"> ' +
                        '                           <a href="javascript:void(0);" class="remove" onclick="removeVehicleBooking('+res.booking_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                        '                       </td>' +
                        '                       <td class="cart-product-name">' +
                        '                           <a href="#">'+res.plate+'</a>' +
                        '                       </td>' +
                        '                       <td>'+res.from+'</td>' +
                        '                       <td>'+res.to+'</td>' +
                        '                       <td class="cart-product-price">' +
                        '                           <span class="amount">$'+res.rate+'</span>' +
                        '                       </td>' +
                        '                       <td class="cart-product-quantity">'+res.days+'</td>' +
                        '                       <td class="cart-product-subtotal">' +
                        '                           <span id="vehicle-total-amount-'+res.booking_id+'" class="amount">$'+res.total+'</span>' +
                        '                       </td>' +
                        '                   </tr>');
                        $("#vehicle-result").html(innerHtml);
                        window.vehicleSubTotal += res.total_int;

                        setFinalParameters();
                        updateVehicleCart();
                    }
                    else {

                        var htmlAppend = ('<tr class="cart_item" id="vehicle-booking-row-'+res.booking_id+'">' +
                        '                       <td class="cart-product-remove"> ' +
                        '                           <a href="javascript:void(0);" class="remove" onclick="removeVehicleBooking('+res.booking_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                        '                       </td>' +
                        '                       <td class="cart-product-name">' +
                        '                           <a href="#">'+res.plate+'</a>' +
                        '                       </td>' +
                        '                       <td>'+res.from+'</td>' +
                        '                       <td>'+res.to+'</td>' +
                        '                       <td class="cart-product-price">' +
                        '                           <span class="amount">$'+res.rate+'</span>' +
                        '                       </td>' +
                        '                       <td class="cart-product-quantity">'+res.days+'</td>' +
                        '                       <td class="cart-product-subtotal">' +
                        '                           <span id="vehicle-total-amount-'+res.booking_id+'" class="amount">$'+res.total+'</span>' +
                        '                       </td>' +
                        '                   </tr>');
                        window.vehicleSubTotal += res.total_int;
                        $("#vehicle-result").append(htmlAppend);

                        setFinalParameters();
                        updateVehicleCart();
                    }
                }
                else {
                    dbError();
                }

                setTimeout(function () {
                    $('#booking-table-loading-img').fadeOut(200);
                }, 600);
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
}

function setFinalParameters() {
    window.vehicleBookingCounter++;
    changeFullTotals();
    $('#vehicle-total-amount').html('$ '+window.vehicleSubTotal);
    $('#vehicle-inbox-total').html('$ '+window.vehicleSubTotal);
    $("#vehicle-booking-sub-total").fadeIn(200);

    $('#vehicle-booking-list-container').fadeIn(200);
    $('#booking-result').slideDown(300);
    updateCartCount(1);
    $('html,body').animate({
            scrollTop: $("#booking-result").offset().top-100},
        1000);
}