/**
 * Created by Isuru on 8/11/16.
 */

//Init global variable
window.eventBookingContuer = 0;
window.scaffoldBookingCounter = 0;
window.vehicleBookingCounter = 0;
window.eventSubTotal = 0;
window.scaffoldSubTotal = 0;
window.vehicleSubTotal = 0;



//Default Settings






//region Events
$(document).ready(function() {
    getPreviousBookingList();
});

$("#pickup-date").on('change', function () {
    var pickupd = $(this).val();
    var pickupt = $('#pickup-time').val();

    $('#return-date').attr('min', pickupd);
    $('#return-date').val(pickupd);
    updateReturnDateTime();
});

$('#return-date').on('change', function() {
    updateReturnDateTime();
});

$('#return-time').on('change', function() {
    updateReturnDateTime();
});

$('#pickup-time').on('change', function() {
    updateReturnDateTime();
});

$('#return-date').on('click', function () {
    $('#pickup-date').focus();
});

$('#top-cart-trigger').on('click', function() {
    updateCartList();
});
//endregion


//Functions
function addMore() {
    $('html,body').animate({
            scrollTop: $(".rent-form-container").offset().top-200},
        300);
    $('.rent-form-container').shake(30, 15, 20);
}

function updateReturnDateTime() {
    var pickupd = $('#pickup-date').val();
    var pickupt = $('#vehicle-pickup-time').val();
    var returnd = $('#vehicle-return-date').val();

    if(pickupd == returnd) {
        for(var i = 8; i < pickupt; i++) {
            $('#vehicle-return-time-'+i).attr('disabled', 'disabled');
        }
        $('#vehicle-return-time-'+ i).attr('disabled', 'disabled');
        i++;
        $('#vehicle-return-time-'+ i).attr('selected', 'true');
    }
    else {
        $('.booking-time-option').removeAttr('disabled');
        $('#vehicle-return-time-8').attr('selected', 'true');
    }
}

function getPreviousBookingList() {
    $('#booking-table-loading-img').fadeIn(200);
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/vehicle/get-session-vehicle-list',
        success: function (data) {
            if(data == null) {

            }
            else if (data != 0) {
                var res = JSON.parse(data);
                if(res != null) {
                    var tableRows = '';
                    for(var i = 0; i < res.length; i++) {
                        tableRows += '<tr class="cart_item" id="vehicle-booking-row-'+res[i].booking_id+'">' +
                            '                       <td class="cart-product-remove"> ' +
                            '                           <a href="javascript:void(0);" class="remove" onclick="removeVehicleBooking('+res[i].booking_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                            '                       </td>' +
                            '                       <td class="cart-product-name">' +
                            '                           <a href="#">'+res[i].plate+'</a>' +
                            '                       </td>' +
                            '                       <td>'+res[i].from+'</td>' +
                            '                       <td>'+res[i].to+'</td>' +
                            '                       <td class="cart-product-price">' +
                            '                           <span class="amount">$'+res[i].rate+'</span>' +
                            '                       </td>' +
                            '                       <td class="cart-product-quantity">'+res[i].days+'</td>' +
                            '                       <td class="cart-product-subtotal">' +
                            '                           <span id="vehicle-total-amount-'+res[i].booking_id+'" class="amount">$'+res[i].total+'</span>' +
                            '                       </td>' +
                            '                   </tr>';

                        window.vehicleBookingCounter++;
                        window.vehicleSubTotal += res[i].total_int;

                        updateCartCount(1);
                        changeFullTotals();

                        $("#vehicle-booking-sub-total").fadeIn(200);
                    }

                    updateVehicleCart();

                    $("#vehicle-result").html(tableRows);
                    $('#booking-result').slideDown(300);
                    $('#vehicle-booking-list-container').fadeIn(200);

                }
            }
            else {
                window.console.log('No connection!');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });

    jQuery.ajax({
        type: 'GET',
        url: window.url + '/scaffold/get-session-scaffold-list',
        success: function (data) {
            if(data == null) {

            }
            else if (data != 0) {
                var res = JSON.parse(data);

                if(res != null) {
                    var tableRows = '';

                    for(var i = 0; i < res.length; i++) {
                        tableRows += '<tr class="cart_item" id="scaffold-booking-row-'+res[i].cart_id+'">' +
                            '                       <td class="cart-product-remove"> ' +
                            '                           <a href="javascript:void(0);" class="remove" onclick="removeScaffoldBooking('+res[i].cart_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                            '                       </td>' +
                            '                       <td class="cart-product-name">' +
                            '                           <a href="#">'+res[i].name+'</a>' +
                            '                       </td>' +
                            '                       <td>'+res[i].from+'</td>' +
                            '                       <td>'+res[i].to+'</td>' +
                            '                       <td class="cart-product-price">' +
                            '                           <span class="amount">$'+res[i].rate+'</span>' +
                            '                       </td>' +
                            '                       <td style="text-align: center;">'+res[i].qty+'</td>' +
                            '                       <td class="cart-product-quantity">'+res[i].days+'</td>' +
                            '                       <td class="cart-product-subtotal">' +
                            '                           <span id="scaffold-total-amount-'+res[i].cart_id+'" class="amount">$'+res[i].total+'</span>' +
                            '                       </td>' +
                            '                   </tr>';
                        window.scaffoldBookingCounter++;
                        window.scaffoldSubTotal += res[i].total_int;

                        updateCartCount(1);
                        changeFullTotals();

                        $("#scaffold-booking-sub-total").fadeIn(200);
                    }

                    updateScaffoldCart();

                    $("#scaffold-result").html(tableRows);
                    $('#booking-result').slideDown(300);
                    $('#scaffold-booking-list-container').fadeIn(200);
                }
            }
            else {
                window.console.log('No connection!');
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
    $('#booking-table-loading-img').fadeIn(300);
    jQuery.ajax({
        type: 'GET',
        url: window.url + '/events/get-session-gear-list',
        success: function (data) {
            if(data == null) {

            }
            else if (data != 0) {
                var res = JSON.parse(data);

                if(res != null) {
                    var tableRows = '';

                    for(var i = 0; i < res.length; i++) {
                        tableRows += '<tr class="cart_item" id="event-booking-row-'+res[i].cart_id+'">' +
                            '                       <td class="cart-product-remove"> ' +
                            '                           <a href="javascript:void(0);" class="remove" onclick="removeEventBooking('+res[i].cart_id+');" title="Remove this item"><i class="icon-trash2"></i></a> ' +
                            '                       </td>' +
                            '                       <td class="cart-product-name">' +
                            '                           <a href="#">'+res[i].name+'</a>' +
                            '                       </td>' +
                            '                       <td>'+res[i].from+'</td>' +
                            '                       <td>'+res[i].to+'</td>' +
                            '                       <td class="cart-product-price">' +
                            '                           <span class="amount">$'+res[i].rate+'</span>' +
                            '                       </td>' +
                            '                       <td style="text-align: center;">'+res[i].qty+'</td>' +
                            '                       <td class="cart-product-quantity">'+res[i].days+'</td>' +
                            '                       <td class="cart-product-subtotal">' +
                            '                           <span id="event-total-amount-'+res[i].cart_id+'" class="amount">$'+res[i].total+'</span>' +
                            '                       </td>' +
                            '                   </tr>';
                        window.eventBookingContuer++;
                        window.eventSubTotal += res[i].total_int;

                        updateCartCount(1);
                        changeFullTotals();

                        $("#event-booking-sub-total").fadeIn(200);
                    }

                    updateEventCart();

                    $("#event-result").html(tableRows);
                    $('#booking-result').slideDown(300);
                    $('#event-booking-list-container').fadeIn(200);
                }
            }
            else {
                dbError();
            }
            setTimeout(function () {
                $('#booking-table-loading-img').fadeOut(300);
            }, 500);
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });

    setTimeout(function () {
        $('#booking-table-loading-img').fadeOut(300);
    }, 500);
}

function updateVehicleCart() {
    if(window.vehicleBookingCounter == 0 && window.scaffoldBookingCounter == 0 && window.eventBookingContuer == 0) {
        $('#cart-count').html(0);
        $('#shopping-cart').fadeOut(500, function() {
            $('#top-cart-trigger').html('');
        });
    }
    else if(window.vehicleBookingCounter > 0) {
        $('#vehicle-cart-container').remove();
        var vehicleCartHtml = ' <div id="vehicle-cart-container" class="top-cart-item clearfix"> ' +
            '                       <div class="top-cart-item-image"> ' +
            '                           <a href="#"><img src="'+window.url+'/assets/images/icons/vehicle-icon.png"/></a> ' +
            '                       </div> ' +
            '                       <div class="top-cart-item-desc" id="vehicle-cart-statistics"> ' +
            '                           <a href="'+window.url+'/book/vehicle">Vehicle Booking</a> ' +
            '                           <span class="top-cart-item-price">$ '+vehicleSubTotal+'</span> ' +
            '                           <span class="top-cart-item-quantity">x '+vehicleBookingCounter+'</span> ' +
            '                       </div> ' +
            '                   </div>';

        updateCartList(vehicleCartHtml);
    }
    else {
        $('#vehicle-cart-container').remove();
    }

}

function updateScaffoldCart() {
    if(window.vehicleBookingCounter == 0 && window.scaffoldBookingCounter == 0 && window.eventBookingContuer == 0) {
        $('#cart-count').html(0);
        $('#shopping-cart').fadeOut(500, function() {
            $('#top-cart-trigger').html('');
        });
    }
    else if(window.scaffoldBookingCounter > 0) {
        $('#scaffold-cart-container').remove();
        var scaffoldCartHtml = ' <div id="scaffold-cart-container" class="top-cart-item clearfix"> ' +
            '                       <div class="top-cart-item-image"> ' +
            '                           <a href="#"><img src="'+window.url+'/assets/images/icons/scaffold-icon.png"/></a> ' +
            '                       </div> ' +
            '                       <div class="top-cart-item-desc" id="scaffold-cart-statistic"> ' +
            '                           <a href="'+window.url+'/book/scaffold">Scaffold Booking</a> ' +
            '                           <span class="top-cart-item-price">$ '+scaffoldSubTotal+'</span> ' +
            '                           <span class="top-cart-item-quantity">x '+scaffoldBookingCounter+'</span> ' +
            '                       </div> ' +
            '                   </div>';

        updateCartList(scaffoldCartHtml);
    }
    else {
        $('#scaffold-cart-container').remove();
    }
}

function updateEventCart() {
    if(window.vehicleBookingCounter == 0 && window.scaffoldBookingCounter == 0 && window.eventBookingContuer == 0) {
        $('#cart-count').html(0);
        $('#shopping-cart').fadeOut(500, function() {
            $('#top-cart-trigger').html('');
        });
    }
    else if(window.eventBookingContuer > 0) {
        $('#event-cart-container').remove();
        var eventCartHtml = ' <div id="event-cart-container" class="top-cart-item clearfix"> ' +
            '                       <div class="top-cart-item-image"> ' +
            '                           <a href="'+window.url+'/book/event-gear"><img src="'+window.url+'/assets/images/icons/event-icon.png"/></a> ' +
            '                       </div> ' +
            '                       <div class="top-cart-item-desc" id="event-cart-statistics"> ' +
            '                           <a href="#">Event Booking</a> ' +
            '                           <span class="top-cart-item-price">$ '+eventSubTotal+'</span> ' +
            '                           <span class="top-cart-item-quantity">x '+eventBookingContuer+'</span> ' +
            '                       </div> ' +
            '                   </div>';
        updateCartList(eventCartHtml);
    }
    else {
        $('#event-cart-container').remove();
    }
}

function updateCartCount(type) {
    if(type == 1) {
        if(window.vehicleBookingCounter > 0 || window.scaffoldBookingCounter > 0 || window.eventBookingContuer > 0) {
            var totalCartCount = window.vehicleBookingCounter+window.scaffoldBookingCounter+window.eventBookingContuer;
            var oldHtml = $('#top-cart-trigger').html();
            if(oldHtml == '') {
                $('#top-cart-trigger').html('<i id="shopping-cart" class="icon-shopping-cart"></i><span id="cart-count">'+totalCartCount+'</span>');
            }
            else {
                $('#cart-count').html(totalCartCount);
            }
        }
    }
    else {
        if(window.vehicleBookingCounter == 0 && window.scaffoldBookingCounter == 0 && window.eventBookingContuer == 0) {
            $('#cart-count').html(0);
            $('#shopping-cart').fadeOut(500, function() {
                $('#top-cart-trigger').html('');
            });
        }
        else {
            var currentValue = parseInt($('#cart-count').html());
            $('#cart-count').html(--currentValue);
        }
    }
}

function confirmBooking() {
    location.replace(window.url+'/book/confirm');
}

function updateCartList(dataList) {
    var fullTotal = window.vehicleSubTotal+window.scaffoldSubTotal+window.eventSubTotal;
    $('#total-price').html('$ '+fullTotal);
    $('#cart-items').append(dataList);
    $('#cart-loading-image').fadeOut(300);
}

function changeFullTotals() {
    //sub totals
    $('#vehicle-total-amount').html('$ '+window.vehicleSubTotal.toFixed(2));
    $('#scaffold-total-amount').html('$ '+window.scaffoldSubTotal.toFixed(2));
    $('#event-total-amount').html('$ '+window.eventSubTotal.toFixed(2));

    $('#vehicle-inbox-total').html('$ '+window.vehicleSubTotal.toFixed(2));
    $('#scaffold-inbox-total').html('$ '+window.scaffoldSubTotal.toFixed(2));
    $('#event-inbox-total').html('$ '+window.eventSubTotal.toFixed(2));


    //full total
    var fullTotal = window.vehicleSubTotal+window.scaffoldSubTotal+window.eventSubTotal;
    fullTotal = '$'+fullTotal.toFixed(2);
    $('#full-total').html(fullTotal);
}

function removeVehicleBooking(bookingId) {
    $('#vehicle-booking-row-'+bookingId).fadeTo(800, 0.3);
    jQuery.ajax({
        type: 'POST',
        url: window.url + '/vehicle/remove-booking-item',
        data: {bookingid: bookingId},
        success: function (data) {
            if(data == 1) {
                window.vehicleBookingCounter--;
                var bookingAmount = $('#vehicle-total-amount-'+bookingId).html();
                bookingAmount = parseFloat(bookingAmount.substr(1));
                window.vehicleSubTotal -= bookingAmount;
                $('#vehicle-sub-total').html('$ '+window.vehicleSubTotal);
                var fullTotal = window.vehicleSubTotal+window.scaffoldSubTotal+window.eventSubTotal;
                $("#vehicle-booking-sub-total").fadeIn(200);
                fullTotal = '$'+fullTotal.toFixed(2);
                $('#full-total').html(fullTotal);
                updateVehicleCart();
                changeFullTotals();

                $('#vehicle-booking-row-'+bookingId).fadeTo(200, 0, function () {
                    $("#vehicle-booking-row-"+bookingId).remove(function () {
                        setTimeout(function () {
                            if($('#booking-list').html() == '') {
                                $('#table-cart').fadeOut(300);
                            }
                        }, 50)
                    });
                });
                setTimeout(function() {
                    updateCartCount(2);
                    if(window.vehicleBookingCounter == 0 && window.eventBookingContuer == 0 && window.scaffoldBookingCounter == 0) {
                        $('#booking-result').slideUp(300, function() {
                            $('#vehicle-booking-row-'+bookingId).remove();
                        });
                    }
                    else if(window.vehicleBookingCounter == 0) {
                        $('#vehicle-booking-sub-total').fadeOut(200);
                        $('#vehicle-booking-list-container').fadeOut(200, function () {
                            $('#vehicle-booking-row-'+bookingId).remove();
                        });
                    }
                    else {
                        $('#vehicle-booking-row-'+bookingId).remove();
                    }
                }, 500);


            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function removeScaffoldBooking(cartId) {
    $('#scaffold-booking-row-'+cartId).fadeTo(800, 0.3);
    jQuery.ajax({
        type: 'POST',
        url: window.url + '/scaffold/remove-scaffold',
        data: {cart_id: cartId},
        success: function (data) {
            if(data == 1) {
                window.scaffoldBookingCounter--;
                var bookingAmount = $('#scaffold-total-amount-'+cartId).html();
                bookingAmount = parseFloat(bookingAmount.substr(1));
                window.scaffoldSubTotal -= bookingAmount;
                $('#scaffold-total-amount').html('$ ' + window.scaffoldSubTotal);
                changeFullTotals();
                updateScaffoldCart();
                updateCartCount(2);

                $('#scaffold-booking-row-'+cartId).fadeTo(200, 0, function () {
                    $("#scaffold-booking-row-"+cartId).remove(function () {
                        setTimeout(function () {
                            if($('#booking-list').html() == '') {
                                $('#table-cart').fadeOut(300);
                            }
                        }, 50)
                    });
                });
                setTimeout(function() {
                    if(window.vehicleBookingCounter == 0 && window.eventBookingContuer == 0 && scaffoldBookingCounter == 0) {
                        $('#booking-result').slideUp(300, function() {
                            $('#scaffold-booking-row-'+cartId).remove();
                        });
                    }
                    if(window.scaffoldBookingCounter == 0) {
                        $('#scaffold-booking-sub-total').fadeOut(200);
                        $('#scaffold-booking-list-container').fadeOut(200, function () {
                            $('#scaffold-booking-row-'+cartId).remove();
                        });
                    }
                    else {
                        $('#scaffold-booking-row-'+cartId).remove();
                    }
                }, 500);

            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}

function removeEventBooking(cartId) {
    $('#event-booking-row-'+cartId).fadeTo(800, 0.3);
    jQuery.ajax({
        type: 'POST',
        url: window.url + '/event/remove-gear',
        data: {cart_id: cartId},
        success: function (data) {
            if(data == 1) {
                window.eventBookingContuer--;
                var bookingAmount = $('#event-total-amount-'+cartId).html();
                bookingAmount = parseFloat(bookingAmount.substr(1));
                window.eventSubTotal -= bookingAmount;
                $('#event-total-amount').html('$ ' + window.eventSubTotal);
                changeFullTotals();
                updateEventCart();
                updateCartCount(2);

                $('#event-booking-row-'+cartId).fadeTo(200, 0, function () {
                    $("#event-booking-row-"+cartId).remove(function () {
                        setTimeout(function () {
                            if($('#booking-list').html() == '') {
                                $('#table-cart').fadeOut(300);
                            }
                        }, 50)
                    });
                });
                setTimeout(function() {
                    if(window.vehicleBookingCounter == 0 && window.eventBookingContuer == 0) {
                        $('#booking-result').slideUp(300, function() {
                            $('#event-booking-row-'+cartId).remove();
                        });
                    }
                    else if(window.eventBookingContuer == 0) {
                        $('#vehicle-booking-list-container').fadeOut(200, function() {
                            $('#event-booking-row-'+cartId).remove();
                        });
                    }
                    else {
                        $('#event-booking-row-'+cartId).remove();
                    }
                }, 500);

            }
            else {
                dbError();
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            internetError();
        }
    });
}