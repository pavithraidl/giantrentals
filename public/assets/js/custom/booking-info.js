/**
 * Created by Isuru on 19/10/16.
 */
"use strict";
//Init Variables



//Default Settings
$('#add-another-vehicle-container').html
('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/vehicle">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Vehicle</button>' +
    '</a>');

$('#add-another-scaffold-container').html
('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/scaffolding">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Scaffold</button>' +
    '</a>');

$('#add-another-gear-container').html
('<a style="color: rgb(255, 255, 255);" href="'+window.url+'/book/event-gear">' +
    '   <button class="btn btn-sm" style="background-color: rgba(249, 100, 33, 0.9);">Add Another Gear</button>' +
    '</a>');


//Functions
function submitGuestForm() {
    var fName = $.trim($('#guest-form-fname').val());
    var lName = $.trim($('#guest-form-lname').val());
    var phone = $.trim($('#guest-form-phone').val());
    var licence = $.trim($('#guest-form-licence').val());
    var email = $.trim($('#guest-form-email').val());
    var jobid = $.trim($('#jobid').val());

    if(fName == '' || lName == '' || phone == '' || licence == '' || email == '') {
        $('#guest-form-fname').trigger('blur');
        $('#guest-form-lname').trigger('blur');
        $('#guest-form-phone').trigger('blur');
        $('#guest-form-licence').trigger('blur');
        $('#guest-form-email').trigger('blur');
    }
    else {
        $('#register-form-submit').fadeOut(100, function() {
            $('#confirm-booking-loading-img').fadeIn(200);
        });

        jQuery.ajax({
            type: 'POST',
            url: window.url + '/book/confirm-booking',
            data: {fname: fName, lname:lName, phone: phone, licence: licence, email:email, jobid:jobid},
            success: function (data) {
                if(data == 1) {
                    var newLocation = window.url+'/booking/recorded';
                    window.location = newLocation;
                }
                else {
                    dbError();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                internetError();
            }
        });
    }
}